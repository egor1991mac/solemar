<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("T_PAGE_BOOKING_DESC_LIST"),
	"DESCRIPTION" => GetMessage("T_PAGE_BOOKING_DESC_LIST"),
    "ICON" => "",
    "SORT" => 30,
    "COMPLEX" => "N",
    "PATH" => array(
        "ID" => "travelsoft",
        "CHILD" => array(
            "ID" => "mastertour",
            "NAME" => "Мастер-Тур",
        ),
    ),
);
?>
