<?php

namespace travelsoft\sts\stores;

use travelsoft\sts\adapters\Iblock;
use travelsoft\sts\stores\RelationDirectionsResorts;

/**
 * Класс для работы с инфоблоком городов
 *
 * @author juliya.sharlova
 * @copyright (c) 2017, travelsoft
 */
class Regions extends Iblock
{
    protected static $storeName = 'regions';

    public function getSubordinateCities(int $cityFrom, int $country, array $id): array {

        $arData_ = array();
        $arData = self::get(array("filter" => array("ID" => $id, "ACTIVE" => "Y")));

        if(!empty($arData)){

            $value = array();
            $arCities = \travelsoft\sts\stores\RelationDirectionsResorts::getResortsToDirect($cityFrom,$country);

            foreach ($arData as $region) {
                if(!empty($region["PROPERTIES"]["TOWN"]["VALUE"])) {
                    $value = array_merge($value,$region["PROPERTIES"]["TOWN"]["VALUE"]);
                }
            }

            if(!empty($value) && !empty($arCities)) {

                foreach ($value as $city) {
                    if(in_array($city,$arCities)){
                        $arData_[] = $city;
                    }
                }

            }

        }

        return (array)$arData_;

    }

}