<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => GetMessage("T_PAGE_SEARCH_RESULT_DESC_LIST"),
    "DESCRIPTION" => GetMessage("T_PAGE_SEARCH_RESULT_LIST_DESC"),
    "ICON" => "/images/form.gif",
    "SORT" => 10,
    "COMPLEX" => "N",
    "CACHE_PATH" => "Y",
    "PATH" => array(
        "ID" => "travelsoft",
        "CHILD" => array(
            "ID" => "mastertour",
            "NAME" => "Мастер-Тур",
        ),
    ),
);
?>