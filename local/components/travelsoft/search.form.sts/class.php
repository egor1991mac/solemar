<?php

class  TravelsoftSearchFormComponent extends CBitrixComponent {

    public $dr;

    // include component_prolog.php
    public function includeComponentProlog()
    {
        $file = "component_prolog.php";

        $template_name = $this->GetTemplateName();

        if ($template_name == "")
            $template_name = ".default";

        $relative_path = $this->GetRelativePath();

        $this->dr = Bitrix\Main\Application::getDocumentRoot();

        $file_path = $this->dr . SITE_TEMPLATE_PATH . "/components" . $relative_path . "/" . $template_name . "/" . $file;

        $arParams = &$this->arParams;

        if(file_exists($file_path))
            require $file_path;
        else {

            $file_path = $this->dr . "/bitrix/templates/.default/components" . $relative_path . "/" . $template_name . "/" . $file;

            if(file_exists($file_path))
                require $file_path;
            else {
                $file_path = $this->dr . $this->__path . "/templates/" . $template_name . "/" . $file;
                if(file_exists($file_path))
                    require $file_path;
                else {

                    $file_path = $this->dr . "/local/components" . $relative_path . "/templates/" . $template_name . "/" . $file;

                    if(file_exists($file_path))
                        require $file_path;
                }

            }
        }
    }

    /**
     * Проверка параметров компонента
     * @throws Exception
     */
    protected function checkInputParameters () {



        if(!empty($this->arParams['GET_PARAMS_STS']))
            $this->arParams['GET_PARAMS_STS'] = (array)$this->arParams['GET_PARAMS_STS'];
        else
            $this->arParams['GET_PARAMS_STS'] = array();

        if ($this->arParams['ACTION_URL'] == "") {
            throw new Exception("Укажите путь к странице поиска");
        }

        if ($this->arParams['COUNTRY_ID'] != "") {
            $this->arParams['COUNTRY_ID'] = (int)$this->arParams['COUNTRY_ID'];
        }

        if ($this->arParams['CITY_ID'] != "") {
            $this->arParams['CITY_ID'] = (int)$this->arParams['CITY_ID'];

            if ($this->arParams['CITY_NAME'] != "") {
                $this->arParams['CITY_NAME'] = strip_tags($this->arParams['CITY_NAME']);
            } else {
                throw new Exception("Отсутствует название города");
            }

        }

        if ($this->arParams['HOTEL_ID'] != "") {
            $this->arParams['HOTEL_ID'] = (int)$this->arParams['HOTEL_ID'];
        }

    }

    /**
     * Определение массива городов вылета
     */
    protected function getCitiesSelect () {

        $this->arResult["CITIES"] = array();

        $cache = \Bitrix\Main\Data\Cache::createInstance();
        $cache_id = "main_search_form_sts_items_iblock_cities_" . md5(serialize($this->arResult["REQUEST"]));
        $cache_dir = "/travelsoft/main_search_form_sts_items_iblock_cities_" . \travelsoft\sts\Settings::relationcitiesStoreId();

        if ($cache->initCache(360000000, $cache_id, $cache_dir)) {
            $this->arResult["CITIES"] = $cache->getVars();
        } elseif ($cache->startDataCache()) {

            $cities = \travelsoft\sts\stores\RelationCities::get(array("filter" => array("UF_CITYFROM" => 1)));
            foreach ($cities as $city) {
                $this->arResult["CITIES"][] = array(
                    "id" => $city["UF_BX_ID"],
                    "name" => \travelsoft\sts\stores\Cities::getElementName(\travelsoft\sts\stores\RelationCities::getBxId(array("UF_BX_ID" => $city["UF_BX_ID"])))
                );
            }
            $this->arResult["CITIES"] = \travelsoft\sts\Utils::customMultiSort($this->arResult["CITIES"], "name");

            if (!empty($this->arResult["CITIES"])) {
                $cache->endDataCache($this->arResult["CITIES"]);
            } else {
                $cache->abortDataCache();
            }

            global $CACHE_MANAGER;
            $CACHE_MANAGER->StartTagCache($cache_dir);
            $CACHE_MANAGER->RegisterTag("iblock_id_" . \travelsoft\sts\Settings::citiesStoreId());
            $CACHE_MANAGER->EndTagCache();

        }
    }

    /**
     * Определение массива стран прилета
     */
    protected function getCountriesSelect () {

        $this->arResult["COUNTRIES"] = array();

        $cache = \Bitrix\Main\Data\Cache::createInstance();
        $cache_id = "main_search_form_sts_items_iblock_countries_" . md5(serialize($this->arResult["REQUEST"]));
        $cache_dir = "/travelsoft/main_search_form_sts_items_iblock_countries_" . \travelsoft\sts\Settings::relationcountriesStoreId();

        if ($cache->initCache(360000000, $cache_id, $cache_dir)) {
            $this->arResult["COUNTRIES"] = $cache->getVars();
        } elseif ($cache->startDataCache()) {

            $countries = \travelsoft\sts\stores\RelationCountries::get();
            foreach ($countries as $country) {
                $this->arResult["COUNTRIES"][] = array(
                    "id" => $country["UF_BX_ID"],
                    "name" => \travelsoft\sts\stores\Countries::getElementName(\travelsoft\sts\stores\RelationCountries::getBxId(array("UF_BX_ID" => $country["UF_BX_ID"])))
                );
            }
            $this->arResult["COUNTRIES"] = \travelsoft\sts\Utils::customMultiSort($this->arResult["COUNTRIES"], "name");

            if (!empty($this->arResult["COUNTRIES"])) {
                $cache->endDataCache($this->arResult["COUNTRIES"]);
            } else {
                $cache->abortDataCache();
            }

            global $CACHE_MANAGER;
            $CACHE_MANAGER->StartTagCache($cache_dir);
            $CACHE_MANAGER->RegisterTag("iblock_id_" . \travelsoft\sts\Settings::countriesStoreId());
            $CACHE_MANAGER->EndTagCache();

        }

    }

    /**
     * Определение массива типов тура
     */
    protected function getTypeToursSelect(){

        $this->arResult['TOUR_TYPES'] = array();

        $cache = \Bitrix\Main\Data\Cache::createInstance();
        $cache_id = "main_search_form_sts_items_iblock_tourtypes_" . md5(serialize($this->arResult["REQUEST"]));
        $cache_dir = "/travelsoft/main_search_form_sts_items_iblock_tourtypes_" . \travelsoft\sts\Settings::relationtourtypesStoreId();

        if ($cache->initCache(360000000, $cache_id, $cache_dir)) {
            $this->arResult["TOUR_TYPES"] = $cache->getVars();
        } elseif ($cache->startDataCache()) {

            $IBtourTypes = \travelsoft\sts\stores\TourTypes::get(array('filter' => array('PROPERTY_SHOW_FILTER_ONLINE_SEARCH_VALUE' => 'Y'), 'select' => array("ID", "NAME")));

            $IBtourTypesId = array_column($IBtourTypes, "ID");

            $tourTypes = \travelsoft\sts\stores\RelationTourTypes::get(array('order' => array('UF_SORT'), 'filter' => array('UF_BX_ID' => $IBtourTypesId)));

            foreach ($tourTypes as $tourType) {
                $this->arResult["TOUR_TYPES"][$tourType["UF_BX_ID"]] = array(
                    "id" => $tourType["UF_BX_ID"],
                    "name" => $IBtourTypes[$tourType["UF_BX_ID"]]["NAME"],
                    "type_search" => $tourType["UF_TYPE_SEARCH"]
                );
            }

            if (!empty($this->arResult["TOUR_TYPES"])) {
                $cache->endDataCache($this->arResult["TOUR_TYPES"]);
            } else {
                $cache->abortDataCache();
            }

            global $CACHE_MANAGER;
            $CACHE_MANAGER->StartTagCache($cache_dir);
            $CACHE_MANAGER->RegisterTag("iblock_id_" . \travelsoft\sts\Settings::tourtypesStoreId());
            $CACHE_MANAGER->EndTagCache();

        }
    }

    /**
     * Определение массива город отправления - страна прибытия
     */
    protected function getCitiesAndCountriesSelect () {

        $this->arResult["DIRECTIONS"] = array();

        $cache = \Bitrix\Main\Data\Cache::createInstance();
        $cache_id = "main_search_form_sts_items_directions_countries_for_cityfrom_" . md5(serialize($this->arResult["REQUEST"]));
        $cache_dir = "/travelsoft/main_search_form_sts_items_directions_countries_for_cityfrom_" . \travelsoft\sts\Settings::relationdirectionsStoreId();

        if ($cache->initCache(360000000, $cache_id, $cache_dir)) {
            $this->arResult["DIRECTIONS"] = $cache->getVars();
        } elseif ($cache->startDataCache()) {

            $directions = \travelsoft\sts\stores\RelationDirections::get();

            foreach ($directions as $direction) {

                if (!isset($this->arResult["DIRECTIONS"][$direction["UF_CITYFROM_BX_ID"]])) {

                    if (isset($this->arParams["PREDLOG"]) && $this->arParams["PREDLOG"] == "Y") {
                        $this->arResult["DIRECTIONS"][$direction["UF_CITYFROM_BX_ID"]] = array(
                            "id" => $direction["UF_CITYFROM_BX_ID"],
                            "name" => \travelsoft\sts\stores\Cities::getElementNamePredlog($direction["UF_CITYFROM_BX_ID"]),
                            "items" => array()
                        );
                    } else {
                        $this->arResult["DIRECTIONS"][$direction["UF_CITYFROM_BX_ID"]] = array(
                            "id" => $direction["UF_CITYFROM_BX_ID"],
                            "name" => \travelsoft\sts\stores\Cities::getElementName(\travelsoft\sts\stores\RelationCities::getBxId(array("UF_BX_ID" => $direction["UF_CITYFROM_BX_ID"]))),
                            "items" => array()
                        );
                    }
                }

                if (!empty($direction["UF_COUNTRY_BX_ID"])) {

                    foreach ($direction["UF_COUNTRY_BX_ID"] as $k => $countryD) {

                        $this->arResult["DIRECTIONS"][$direction["UF_CITYFROM_BX_ID"]]["items"][$direction["UF_COUNTRY_BX_ID"][$k]] = array(
                            "id" => $direction["UF_COUNTRY_BX_ID"][$k],
                            "name" => \travelsoft\sts\stores\Countries::getElementName(\travelsoft\sts\stores\RelationCountries::getBxId(array("UF_BX_ID" => $direction["UF_COUNTRY_BX_ID"][$k])))
                        );

                    }

                    $this->arResult["DIRECTIONS"][$direction["UF_CITYFROM_BX_ID"]]["items"] = \travelsoft\sts\Utils::customMultiSort($this->arResult["DIRECTIONS"][$direction["UF_CITYFROM_BX_ID"]]["items"], "name", true);

                }

            }

            $this->arResult["DIRECTIONS"] = \travelsoft\sts\Utils::customMultiSort($this->arResult["DIRECTIONS"], "name", true);

            if (!empty($this->arResult["DIRECTIONS"])) {
                $cache->endDataCache($this->arResult["DIRECTIONS"]);
            } else {
                $cache->abortDataCache();
            }

            global $CACHE_MANAGER;
            $CACHE_MANAGER->StartTagCache($cache_dir);
            $CACHE_MANAGER->RegisterTag("iblock_id_" . \travelsoft\sts\Settings::citiesStoreId());
            $CACHE_MANAGER->RegisterTag("iblock_id_" . \travelsoft\sts\Settings::countriesStoreId());
            $CACHE_MANAGER->EndTagCache();

        }

    }


    public function executeComponent () {

        \Bitrix\Main\Loader::includeModule("travelsoft.sts");

        $this->includeComponentProlog();

        $this->checkInputParameters();

        $this->arResult["REQUEST"] = array();

        $period = unserialize(\travelsoft\sts\Settings::get("DATE_RANGE"));
        $this->arResult["PERIOD_DATE"] = $period[1];

        $night = \travelsoft\sts\Settings::getDefaultSearchNight();

        if(!empty($this->arParams['GET_PARAMS_STS'])){

            $this->arResult["REQUEST"] = array(

                "cityFrom" => (int)$this->arParams['GET_PARAMS_STS']['cityFrom'],
                "country" => (int)$this->arParams['GET_PARAMS_STS']['country'],
                "cities" => isset($this->arParams['GET_PARAMS_STS']['cities']) && !empty($this->arParams['GET_PARAMS_STS']['cities']) ? (array)$this->arParams['GET_PARAMS_STS']['cities'] : array(),
                "dateFrom" => $this->arParams['GET_PARAMS_STS']['dateFrom'],
                "dateTo" => ($this->arParams['GET_PARAMS_STS']['dateTo'] ?? $this->arParams['GET_PARAMS_STS']['dateFrom']),
                "nightFrom" => (int)$this->arParams['GET_PARAMS_STS']['nightFrom'],
                "nightTo" => (int)($this->arParams['GET_PARAMS_STS']['nightTo'] ?? ($this->arParams['GET_PARAMS_STS']['nightFrom'] + $night['nightTo'])),
                "adults" => (int)$this->arParams['GET_PARAMS_STS']['adults'],
                "children" => $this->arParams['GET_PARAMS_STS']['children'] && !empty($this->arParams['GET_PARAMS_STS']['children']) ? (int)$this->arParams['GET_PARAMS_STS']['children'] : 0,
                "tourTypes" => isset($this->arParams['GET_PARAMS_STS']['tourTypes']) && !empty($this->arParams['GET_PARAMS_STS']['tourTypes']) ? (array)$this->arParams['GET_PARAMS_STS']['tourTypes'] : array(),
                "stars" => isset($this->arParams['GET_PARAMS_STS']['stars']) && !empty($this->arParams['GET_PARAMS_STS']['stars']) ? (array)$this->arParams['GET_PARAMS_STS']['stars'] : array(),
                "meals" => isset($this->arParams['GET_PARAMS_STS']['meals']) && !empty($this->arParams['GET_PARAMS_STS']['meals']) ? (array)$this->arParams['GET_PARAMS_STS']['meals'] : array(),

            );

        }
        else {

            $date = \travelsoft\sts\Settings::getDefaultSearchDate();

            $this->arResult["REQUEST"] = array(

                "cityFrom" => \travelsoft\sts\Settings::getDefaultCityFrom(),
                "country" => \travelsoft\sts\Settings::getDefaultCountry(),
                "cities" => array(),
                "dateFrom" => $date['dateFrom'],
                "dateTo" => $date['dateTo'],
                "nightFrom" => $night['nightFrom'],
                "nightTo" => $night['nightTo'],
                "adults" => \travelsoft\sts\Settings::getDefaultAdults(),
                "children" => \travelsoft\sts\Settings::getDefaultChildren(),
                "tourTypes" => array(),
                "stars" => array(),
                "meals" => array(),

            );

        }

        if($this->arResult["REQUEST"]["children"] > 0){

            if($this->arResult["REQUEST"]["children"] == 1){
                $this->arResult["REQUEST"]["age1"] = $this->arParams['GET_PARAMS_STS']['age1'] && !empty($this->arParams['GET_PARAMS_STS']['age1']) ? (int)$this->arParams['GET_PARAMS_STS']['age1'] : 6;
            } elseif ($this->arResult["REQUEST"]["children"] >= 1) {
                $this->arResult["REQUEST"]["age1"] = $this->arParams['GET_PARAMS_STS']['age1'] && !empty($this->arParams['GET_PARAMS_STS']['age1']) ? (int)$this->arParams['GET_PARAMS_STS']['age1'] : 6;
                $this->arResult["REQUEST"]["age2"] = $this->arParams['GET_PARAMS_STS']['age2'] && !empty($this->arParams['GET_PARAMS_STS']['age2']) ? (int)$this->arParams['GET_PARAMS_STS']['age2'] : 6;
            }

        }

        if(!empty($this->arParams['COUNTRY_ID'])) {

            $this->arResult["REQUEST"]["country"] = $this->arParams['COUNTRY_ID'];

            if(!empty($this->arResult["DIRECTIONS"])) {

                foreach ($this->arResult["DIRECTIONS"] as $key=>$cityfrom_directions) {

                    if(!isset($cityfrom_directions["items"][$this->arResult["REQUEST"]["country"]])){
                        unset($this->arResult["DIRECTIONS"][$key]);
                    }
                }

            }

        }

        if(!empty($this->arParams['CITY_ID'])) {

            $this->arResult["REQUEST"]["cities"] = (array)$this->arParams['CITY_ID'];

        }

        if(!empty($this->arParams['TOUR_TYPE_ID'])) {

            $this->arResult["REQUEST"]["tourTypes"] = (array)$this->arParams['TOUR_TYPE_ID'];

        }

        if(!empty($this->arParams['HOTEL_ID'])) {

            $this->arResult["REQUEST"]["hotels"] = (array)$this->arParams['HOTEL_ID'];

        }

        $this->getCitiesAndCountriesSelect();
        $this->getTypeToursSelect();

        if(empty($this->arResult["DIRECTIONS"])) {
            $this->getCitiesSelect();
            $this->getCountriesSelect();
            $this->arResult["RESORT"] = \travelsoft\sts\Utils::getRegions();
        } elseif(isset($this->arResult["DIRECTIONS"][$this->arResult["REQUEST"]["cityFrom"]]["items"][$this->arResult["REQUEST"]["country"]])) {
            $this->arResult["RESORT"][$this->arResult["REQUEST"]["country"]] =\travelsoft\sts\Utils::getCitiesResortSelect($this->arResult["REQUEST"]["country"],$this->arResult["REQUEST"]["cityFrom"]);
            $this->arResult["TOURTYPES"] = \travelsoft\sts\Utils::getTourTypesDirections($this->arResult["REQUEST"]["country"],$this->arResult["REQUEST"]["cityFrom"],$this->arResult["TOUR_TYPES"]);
            foreach ($this->arResult["DIRECTIONS"] as $DIRECTION){
                foreach ($DIRECTION["items"] as $item){
                    $this->arResult["RESORT_"][$DIRECTION["id"]][$item["id"]] = \travelsoft\sts\Utils::getCitiesResortSelect($item["id"],$DIRECTION["id"]);
                    $this->arResult["TOUR_TYPES_"][$DIRECTION["id"]][$item["id"]] = \travelsoft\sts\Utils::getTourTypesDirections($item["id"],$DIRECTION["id"],$this->arResult["TOUR_TYPES"]);
                }
            }
        }

        $this->IncludeComponentTemplate();

    }

}