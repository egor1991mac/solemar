<?php

return array(
    array(
        "ENTITY_ID" => 'USER',
        "FIELD_NAME" => "UF_USER_ID",
        "USER_TYPE_ID" => 'string',
        "XML_ID" => "",
        "SORT" => 100,
        "MULTIPLE" => 'N',
        'MANDATORY' => 'N',
        'SHOW_FILTER' => 'N',
        'SHOW_IN_LIST' => 'Y',
        'IS_SEARCHABLE' => 'N',
        'SETTINGS' => array(
            'DEFAULT_VALUE' => "",
            'SIZE' => '20',
            'ROWS' => 1,
            'MIN_LENGTH' => 0,
            'MAX_LENGTH' => 0,
            'REGEXP' => ''
        ),
        'EDIT_FORM_LABEL' => array(
            'ru' => 'ID пользователя',
            'en' => 'User ID',
        ),
        'LIST_COLUMN_LABEL' => array(
            'ru' => 'ID пользователя',
            'en' => 'User ID',
        ),
        'LIST_FILTER_LABEL' => array(
            'ru' => 'ID пользователя',
            'en' => 'User ID',
        ),
        'ERROR_MESSAGE' => array(
            'ru' => 'Ошибка при заполнении поля "ID пользователя" ',
            'en' => 'An error in completing the field "User ID"',
        ),
        'HELP_MESSAGE' => array(
            'ru' => '',
            'en' => '',
        ),
    ),
    array(
        "ENTITY_ID" => 'USER',
        "FIELD_NAME" => "UF_AGENT_NOT_ACTIVE",
        "USER_TYPE_ID" => 'boolean',
        "XML_ID" => "",
        "SORT" => 100,
        "MULTIPLE" => 'N',
        'MANDATORY' => 'N',
        'SHOW_FILTER' => 'N',
        'SHOW_IN_LIST' => 'Y',
        'IS_SEARCHABLE' => 'N',
        'SETTINGS' => array(
            'DEFAULT_VALUE' => "0",
            'LABEL' => array(0 => "нет", 1 => "да"),
            "DISPLAY" => "CHECKBOX",
            'LABEL_CHECKBOX' => "да"
        ),
        'EDIT_FORM_LABEL' => array(
            'ru' => 'Агент в стадии активации',
            'en' => 'Agent not active',
        ),
        'LIST_COLUMN_LABEL' => array(
            'ru' => 'Агент в стадии активации',
            'en' => 'Agent not active',
        ),
        'LIST_FILTER_LABEL' => array(
            'ru' => 'Агент в стадии активации',
            'en' => 'Agent not active',
        ),
        'ERROR_MESSAGE' => array(
            'ru' => 'Ошибка при заполнении поля "Агент в стадии активации" ',
            'en' => 'An error in completing the field "Agent not active"',
        ),
        'HELP_MESSAGE' => array(
            'ru' => '',
            'en' => '',
        ),
    ),
    array(
        "ENTITY_ID" => 'USER',
        "FIELD_NAME" => "UF_LEGAL_NAME",
        "USER_TYPE_ID" => 'string',
        "XML_ID" => "",
        "SORT" => 100,
        "MULTIPLE" => 'N',
        'MANDATORY' => 'N',
        'SHOW_FILTER' => 'N',
        'SHOW_IN_LIST' => 'Y',
        'IS_SEARCHABLE' => 'N',
        'SETTINGS' => array(
            'DEFAULT_VALUE' => "",
            'SIZE' => '20',
            'ROWS' => 1,
            'MIN_LENGTH' => 0,
            'MAX_LENGTH' => 0,
            'REGEXP' => ''
        ),
        'EDIT_FORM_LABEL' => array(
            'ru' => 'Юр. название',
            'en' => 'Legal name',
        ),
        'LIST_COLUMN_LABEL' => array(
            'ru' => 'Юр. название',
            'en' => 'Legal name',
        ),
        'LIST_FILTER_LABEL' => array(
            'ru' => 'Юр. название',
            'en' => 'Legal name',
        ),
        'ERROR_MESSAGE' => array(
            'ru' => 'Ошибка при заполнении поля "Юр. название" ',
            'en' => 'An error in completing the field "Legal name"',
        ),
        'HELP_MESSAGE' => array(
            'ru' => '',
            'en' => '',
        ),
    ),
    array(
        "ENTITY_ID" => 'USER',
        "FIELD_NAME" => "UF_LEGAL_ADDRESS",
        "USER_TYPE_ID" => 'string',
        "XML_ID" => "",
        "SORT" => 100,
        "MULTIPLE" => 'N',
        'MANDATORY' => 'N',
        'SHOW_FILTER' => 'N',
        'SHOW_IN_LIST' => 'Y',
        'IS_SEARCHABLE' => 'N',
        'SETTINGS' => array(
            'DEFAULT_VALUE' => "",
            'SIZE' => '20',
            'ROWS' => 1,
            'MIN_LENGTH' => 0,
            'MAX_LENGTH' => 0,
            'REGEXP' => ''
        ),
        'EDIT_FORM_LABEL' => array(
            'ru' => 'Юр. адрес',
            'en' => 'Legal address',
        ),
        'LIST_COLUMN_LABEL' => array(
            'ru' => 'Юр. адрес',
            'en' => 'Legal address',
        ),
        'LIST_FILTER_LABEL' => array(
            'ru' => 'Юр. адрес',
            'en' => 'Legal address',
        ),
        'ERROR_MESSAGE' => array(
            'ru' => 'Ошибка при заполнении поля "Юр. адрес" ',
            'en' => 'An error in completing the field "Legal address"',
        ),
        'HELP_MESSAGE' => array(
            'ru' => '',
            'en' => '',
        ),
    ),
    array(
        "ENTITY_ID" => 'USER',
        "FIELD_NAME" => "UF_BANK_NAME",
        "USER_TYPE_ID" => 'string',
        "XML_ID" => "",
        "SORT" => 100,
        "MULTIPLE" => 'N',
        'MANDATORY' => 'N',
        'SHOW_FILTER' => 'N',
        'SHOW_IN_LIST' => 'Y',
        'IS_SEARCHABLE' => 'N',
        'SETTINGS' => array(
            'DEFAULT_VALUE' => "",
            'SIZE' => '20',
            'ROWS' => 1,
            'MIN_LENGTH' => 0,
            'MAX_LENGTH' => 0,
            'REGEXP' => ''
        ),
        'EDIT_FORM_LABEL' => array(
            'ru' => 'Наименование банка',
            'en' => 'Name of the bank',
        ),
        'LIST_COLUMN_LABEL' => array(
            'ru' => 'Наименование банка',
            'en' => 'Name of the bank',
        ),
        'LIST_FILTER_LABEL' => array(
            'ru' => 'Наименование банка',
            'en' => 'Name of the bank',
        ),
        'ERROR_MESSAGE' => array(
            'ru' => 'Ошибка при заполнении поля "Наименование банка" ',
            'en' => 'An error in completing the field "Name of the bank"',
        ),
        'HELP_MESSAGE' => array(
            'ru' => '',
            'en' => '',
        ),
    ),
    array(
        "ENTITY_ID" => 'USER',
        "FIELD_NAME" => "UF_BANK_ADDRESS",
        "USER_TYPE_ID" => 'string',
        "XML_ID" => "",
        "SORT" => 100,
        "MULTIPLE" => 'N',
        'MANDATORY' => 'N',
        'SHOW_FILTER' => 'N',
        'SHOW_IN_LIST' => 'Y',
        'IS_SEARCHABLE' => 'N',
        'SETTINGS' => array(
            'DEFAULT_VALUE' => "",
            'SIZE' => '20',
            'ROWS' => 1,
            'MIN_LENGTH' => 0,
            'MAX_LENGTH' => 0,
            'REGEXP' => ''
        ),
        'EDIT_FORM_LABEL' => array(
            'ru' => 'Адрес банка',
            'en' => 'Address of the bank',
        ),
        'LIST_COLUMN_LABEL' => array(
            'ru' => 'Адрес банка',
            'en' => 'Address of the bank',
        ),
        'LIST_FILTER_LABEL' => array(
            'ru' => 'Адрес банка',
            'en' => 'Address of the bank',
        ),
        'ERROR_MESSAGE' => array(
            'ru' => 'Ошибка при заполнении поля "Адрес банка" ',
            'en' => 'An error in completing the field "Address of the bank"',
        ),
        'HELP_MESSAGE' => array(
            'ru' => '',
            'en' => '',
        ),
    ),
    array(
        "ENTITY_ID" => 'USER',
        "FIELD_NAME" => "UF_BANK_CODE",
        "USER_TYPE_ID" => 'string',
        "XML_ID" => "",
        "SORT" => 100,
        "MULTIPLE" => 'N',
        'MANDATORY' => 'N',
        'SHOW_FILTER' => 'N',
        'SHOW_IN_LIST' => 'Y',
        'IS_SEARCHABLE' => 'N',
        'SETTINGS' => array(
            'DEFAULT_VALUE' => "",
            'SIZE' => '20',
            'ROWS' => 1,
            'MIN_LENGTH' => 0,
            'MAX_LENGTH' => 0,
            'REGEXP' => ''
        ),
        'EDIT_FORM_LABEL' => array(
            'ru' => 'Код банка',
            'en' => 'Bank code',
        ),
        'LIST_COLUMN_LABEL' => array(
            'ru' => 'Код банка',
            'en' => 'Bank code',
        ),
        'LIST_FILTER_LABEL' => array(
            'ru' => 'Код банка',
            'en' => 'Bank code',
        ),
        'ERROR_MESSAGE' => array(
            'ru' => 'Ошибка при заполнении поля "Код банка" ',
            'en' => 'An error in completing the field "Bank code"',
        ),
        'HELP_MESSAGE' => array(
            'ru' => '',
            'en' => '',
        ),
    ),
    array(
        "ENTITY_ID" => 'USER',
        "FIELD_NAME" => "UF_CHECKING_ACCOUNT",
        "USER_TYPE_ID" => 'string',
        "XML_ID" => "",
        "SORT" => 100,
        "MULTIPLE" => 'N',
        'MANDATORY' => 'N',
        'SHOW_FILTER' => 'N',
        'SHOW_IN_LIST' => 'Y',
        'IS_SEARCHABLE' => 'N',
        'SETTINGS' => array(
            'DEFAULT_VALUE' => "",
            'SIZE' => '20',
            'ROWS' => 1,
            'MIN_LENGTH' => 0,
            'MAX_LENGTH' => 0,
            'REGEXP' => ''
        ),
        'EDIT_FORM_LABEL' => array(
            'ru' => 'Расчётный счёт',
            'en' => 'Checking account',
        ),
        'LIST_COLUMN_LABEL' => array(
            'ru' => 'Расчётный счёт',
            'en' => 'Checking account',
        ),
        'LIST_FILTER_LABEL' => array(
            'ru' => 'Расчётный счёт',
            'en' => 'Checking account',
        ),
        'ERROR_MESSAGE' => array(
            'ru' => 'Ошибка при заполнении поля "Расчётный счёт" ',
            'en' => 'An error in completing the field "Checking account"',
        ),
        'HELP_MESSAGE' => array(
            'ru' => '',
            'en' => '',
        ),
    ),
    array(
        "ENTITY_ID" => 'USER',
        "FIELD_NAME" => "UF_UNP",
        "USER_TYPE_ID" => 'string',
        "XML_ID" => "",
        "SORT" => 100,
        "MULTIPLE" => 'N',
        'MANDATORY' => 'N',
        'SHOW_FILTER' => 'N',
        'SHOW_IN_LIST' => 'Y',
        'IS_SEARCHABLE' => 'N',
        'SETTINGS' => array(
            'DEFAULT_VALUE' => "",
            'SIZE' => '20',
            'ROWS' => 1,
            'MIN_LENGTH' => 0,
            'MAX_LENGTH' => 0,
            'REGEXP' => ''
        ),
        'EDIT_FORM_LABEL' => array(
            'ru' => 'УНП',
            'en' => 'UNP',
        ),
        'LIST_COLUMN_LABEL' => array(
            'ru' => 'УНП',
            'en' => 'UNP',
        ),
        'LIST_FILTER_LABEL' => array(
            'ru' => 'УНП',
            'en' => 'UNP',
        ),
        'ERROR_MESSAGE' => array(
            'ru' => 'Ошибка при заполнении поля "УНП" ',
            'en' => 'An error in completing the field "UNP"',
        ),
        'HELP_MESSAGE' => array(
            'ru' => '',
            'en' => '',
        ),
    ),
    array(
        "ENTITY_ID" => 'USER',
        "FIELD_NAME" => "UF_OKPO",
        "USER_TYPE_ID" => 'string',
        "XML_ID" => "",
        "SORT" => 100,
        "MULTIPLE" => 'N',
        'MANDATORY' => 'N',
        'SHOW_FILTER' => 'N',
        'SHOW_IN_LIST' => 'Y',
        'IS_SEARCHABLE' => 'N',
        'SETTINGS' => array(
            'DEFAULT_VALUE' => "",
            'SIZE' => '20',
            'ROWS' => 1,
            'MIN_LENGTH' => 0,
            'MAX_LENGTH' => 0,
            'REGEXP' => ''
        ),
        'EDIT_FORM_LABEL' => array(
            'ru' => 'ОКПО',
            'en' => 'OKPO',
        ),
        'LIST_COLUMN_LABEL' => array(
            'ru' => 'ОКПО',
            'en' => 'OKPO',
        ),
        'LIST_FILTER_LABEL' => array(
            'ru' => 'ОКПО',
            'en' => 'OKPO',
        ),
        'ERROR_MESSAGE' => array(
            'ru' => 'Ошибка при заполнении поля "ОКПО" ',
            'en' => 'An error in completing the field "OKPO"',
        ),
        'HELP_MESSAGE' => array(
            'ru' => '',
            'en' => '',
        ),
    ),
    array(
        "ENTITY_ID" => 'USER',
        "FIELD_NAME" => "UF_ACTUAL_ADDRESS",
        "USER_TYPE_ID" => 'string',
        "XML_ID" => "",
        "SORT" => 100,
        "MULTIPLE" => 'N',
        'MANDATORY' => 'N',
        'SHOW_FILTER' => 'N',
        'SHOW_IN_LIST' => 'Y',
        'IS_SEARCHABLE' => 'N',
        'SETTINGS' => array(
            'DEFAULT_VALUE' => "",
            'SIZE' => '20',
            'ROWS' => 1,
            'MIN_LENGTH' => 0,
            'MAX_LENGTH' => 0,
            'REGEXP' => ''
        ),
        'EDIT_FORM_LABEL' => array(
            'ru' => 'Фактический адрес',
            'en' => 'Actual address',
        ),
        'LIST_COLUMN_LABEL' => array(
            'ru' => 'Фактический адрес',
            'en' => 'Actual address',
        ),
        'LIST_FILTER_LABEL' => array(
            'ru' => 'Фактический адрес',
            'en' => 'Actual address',
        ),
        'ERROR_MESSAGE' => array(
            'ru' => 'Ошибка при заполнении поля "Фактический адрес" ',
            'en' => 'An error in completing the field "Actual address"',
        ),
        'HELP_MESSAGE' => array(
            'ru' => '',
            'en' => '',
        ),
    ),
    array(
        "ENTITY_ID" => 'USER',
        "FIELD_NAME" => "UF_ACOUNT_CURRENCY",
        "USER_TYPE_ID" => 'string',
        "XML_ID" => "",
        "SORT" => 100,
        "MULTIPLE" => 'N',
        'MANDATORY' => 'N',
        'SHOW_FILTER' => 'N',
        'SHOW_IN_LIST' => 'Y',
        'IS_SEARCHABLE' => 'N',
        'SETTINGS' => array(
            'DEFAULT_VALUE' => "",
            'SIZE' => '20',
            'ROWS' => 1,
            'MIN_LENGTH' => 0,
            'MAX_LENGTH' => 0,
            'REGEXP' => ''
        ),
        'EDIT_FORM_LABEL' => array(
            'ru' => 'Валюта счёта',
            'en' => 'Account currency',
        ),
        'LIST_COLUMN_LABEL' => array(
            'ru' => 'Валюта счёта',
            'en' => 'Account currency',
        ),
        'LIST_FILTER_LABEL' => array(
            'ru' => 'Валюта счёта',
            'en' => 'Account currency',
        ),
        'ERROR_MESSAGE' => array(
            'ru' => 'Ошибка при заполнении поля "Валюта счёта" ',
            'en' => 'An error in completing the field "Account currency"',
        ),
        'HELP_MESSAGE' => array(
            'ru' => '',
            'en' => '',
        ),
    ),
);
