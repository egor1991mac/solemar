<?

use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;
Bitrix\Main\Loader::includeModule('highloadblock');

/**
 * "Принт" переменной
 * 
 * @global CUser $USER
 * 
 * @param mixed $var переменная для debug
 * 
 * @param boolean $vd использовать var_dump для debug
 * 
 * @param boolean $tofile вывод в файл $_SERVER['DOCUMENT_ROOT'] . "/debug.txt"
 * 
 * @param boolean $onlyAdmin доступна ли функция только для админа
 */
function dm($var, $vd = false, $tofile = false, $onlyAdmin = true) {

    global $USER;
	
    if ($onlyAdmin && !$USER->IsAdmin()) {
            return;
    }

    if ($tofile) {
        ob_start();
    }
    
    echo "<pre>";

    if ($vd) {
        var_dump($var);		
    } else {
        print_r($var);
    }
	
    echo "</pre>";
    
    if ($tofile) {
        file_put_contents(Bitrix\Main\Application::getDocumentRoot() . "/debug.txt", ob_get_clean());
    }
 	
}

function dmtf($var) {

    ob_start();
    echo "<pre>";
    print_r($var);
    echo "</pre>";
    file_put_contents($_SERVER['DOCUMENT_ROOT'] . "/dm.txt", ob_get_clean());

}

/**
 * Обёртка для implode
 * @var array $arr
 * @return string
 */
function implode2 ($arr, $delimiter = ', ', $no_strip = false) {
 
    return   implode(
                $delimiter,
                array_map(
                        function ($it) {

                            return $no_strip ? $it : strip_tags($it);

                        }, array_filter(
                                $arr,
                                function ($el) { return ($el && !empty($el)); }
                         )
                )
             );
}


/**
 * Путь к изрбражению по его id
 * @var int $img - id изображения
 * @var array $resize 
 * @return string
 */
function getSrcImage($img, $resize = array(), $noPhoto = "", $resizeType = null) {

    $img = (int)$img; 
    
    if ($noPhoto == "")
        $src = "";
    else
        $src = $noPhoto;

    if (is_null($resizeType))
        $resizeType_ = "BX_RESIZE_IMAGE_EXACT";
    else
        $resizeType_ = $resizeType;
 
    if ($img > 0) {
        
        if ($resize['width'] > 0 && $resize['height'] > 0) {
            $file = CFile::ResizeImageGet($img, $resize, $resizeType_, true);
            $src = $file['src'];
        } else {
            $file = CFile::GetFileArray($img);
            $src = $file['SRC'];
        }
    }
    
    return $src;
    
}

function getSrc($imgArrayID, $resize = array(), $noPhoto = "", $count = -1, $watermark = true)
{
    $imgArrayID = (array)$imgArrayID;

    $arWaterMark = Array(
        array(
            "name" => "watermark",
            "position" => "topright", // Положение
            "type" => "image",
            "size" => "real",
            "file" => NO_PHOTO_PATH_WATERMARK, // Путь к картинке
            "fill" => "exact",
        )
    );

    $src = [];
    if (isset($imgArrayID) && !empty($imgArrayID)) {
        foreach ($imgArrayID as $k => $img) {
            if($count == $k){
                break;
            }
            $img = (int)$img;
            if ($img > 0) {

                if ($resize['width'] > 0 && $resize['height'] > 0) {
                    if($watermark) {
                        $file = CFile::ResizeImageGet($img, $resize, BX_RESIZE_IMAGE_EXACT, true, $arWaterMark);
                    } else {
                        $file = CFile::ResizeImageGet($img, $resize, BX_RESIZE_IMAGE_EXACT, true);
                    }
                    $src[] = $file['src'];
                } else {
                    $file = CFile::GetFileArray($img);
                    $src[] = $file['SRC'];
                }
            }
        }
    }
    if(empty($src)){
        $src[] = $noPhoto;
    }
    return $src;
}

/*
 * Дынные вводить в формате 3.5
 */
function star_rating($rating, $str_repeat = '<i class="stars"></i>') {
    $full_stars = floor( $rating );
    $half_stars = ceil( $rating - $full_stars );
    $empty_stars = 5 - $full_stars - $half_stars;

    $output = '';
    $output .= str_repeat($str_repeat, $full_stars );
//    $output .= str_repeat( '<i class="stars"></i>', $half_stars );
//    $output .= str_repeat( '<i class="stars"></i>', $empty_stars );

    return $output;
}


/**
 * Обёртка для substr
 * @var string $str
 * @var int $nos
 * @return string
 */
function substr2($str, $nos = null) {
    
    $str = strip_tags($str);
    
    if ($nos === null || strlen($str) <= $nos) return $str;
    
    return substr($str, 0, $nos) . "...";
}


/**
 * Получаем поля элемента по его ID
 * @var integer $id
 * @return mixed
 */
function getIBElement ($id) {
    
    if (! ($res = getIBElementFields($id)) )
        return false;
   
    $arr = $res;
    
    if (! ($res = getIBElementProperties($id)) )
        return false;
    
    $arr['PROPERTIES'] = $res;
    
    return $arr;
    
}

// только поля
function getIBElementFields ($id) {
    
    if (! ($res = CIBlockElement::GetByID($id)->GetNextElement()) )
        return false;
    
    return $res->GetFields();
    
}

// только свойства
function getIBElementProperties ($id) {
    
    if (! ($res = CIBlockElement::GetByID($id)->GetNextElement()) )
        return false;
    
    return $res->GetProperties();
    
}

function num2word($num, $words, $show_num = true)
{
    $num_text = '';
    $num = $num % 100;
    if ($num > 19) {
        $num = $num % 10;
    }

    if($show_num){
        $num_text = $num." ";
    }
    else{
        $num_text = "";
    }
    switch ($num) {
        case 1: {
            return $num_text.$words[0];
        }
        case 2: case 3: case 4: {
        return $num_text.$words[1];
    }
        default: {
            return $num_text.$words[2];
        }
    }
}

function textLength($str, $num) {

    $str = rtrim(substr(strip_tags($str),0,$num),"!,.-");
    $str = substr($str, 0, strrpos($str, ' '));
    return $str."… ";

}

function imp ($del, $arr, $str = '', $required = null) {

    $result = '';

    if(is_array($required)) {
        foreach ($required as $k=>$value){
            if(!empty($value))
                $result = $str.': ';
        }
    }
    else {
        $result = $str.': ';
    }

    foreach ($arr as $k=>$value) {
        if(empty($value))
            unset($arr[$k]);
    }

    return $result.implode($del, $arr);

}

function vowelLetter($str, $tolowerRegister = true) {

    $first_char = strtolower(mb_substr($str,0,1,'UTF-8'));

    $p = array('а','е','ё','и','о','у','ы','э','ю','я');

    if(in_array($first_char,$p)){
        return $tolowerRegister ? 'об '.$str : 'Об '.$str;
    } else {
        return $tolowerRegister ? 'о '.$str : 'О '.$str;
    }
}