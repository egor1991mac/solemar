<?php

namespace travelsoft\booking\sts\_interfaces;

/**
 * Абстрактный класс для операторов
 *
 * @author juliya.sharlova
 * @copyright (c) 2017, travelsoft
 */
abstract class Operator
{

    abstract public function __construct();
    abstract public function createUser(array $parameters);
    abstract public function updateUser(array $parameters);
    abstract public function authorizeUser(array $parameters);
    abstract public function makeBooking(array $parameters);
    abstract public function getOrderList(array $parameters);
    abstract public function getOrderDetail(array $parameters);
    abstract public function getServiceDetail(array $parameters);
    abstract public function orderToCancel(array $parameters);

}