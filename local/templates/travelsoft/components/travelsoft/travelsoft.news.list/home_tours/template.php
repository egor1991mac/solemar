<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if(!empty($arResult["ITEMS"])):?>
    <div class="tours-simple-list promo-tours ts-my-4 ts-py-0 ts-px-xl-0">
    <div class="container">
        <div class="ts-wrap ts-px-md-0">
            <div class="ts-row">
            <div class="tours box-1 ts-col-24  ts-col-lg-12 ts-px-0 ts-flex-direction__row">

        <?foreach($arResult["ITEMS"] as $key=>$arItem):?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <?$arImg = getSrc($arItem["DISPLAY_PROPERTIES"]["PICTURES"]["VALUE"], array("width"=>258,"height"=>170), NO_PHOTO_258_170, 1);?>
            <div class="ts-col-24 ts-col-sm-12 ts-pb-4">
                <div id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="tour">
                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                    <?if($arItem["DISPLAY_PROPERTIES"]["DATE"]["DATES_FORMAT"]):?>
                        <span class="dates">
                            <p><b><?=GetMessage('NEAREST_DEPARTURES')?></b><br />
                                <?foreach ($arItem["DISPLAY_PROPERTIES"]["DATE"]["DATES_FORMAT"] as $date): ?>
                                    <?= $date ?><br />
                                <?endforeach ?>
                            </p>
                        </span>
                    <?endif?>
                    <div class="img" style="background-image:url('<?= $arImg[0] ?>')"></div>

                    <b class="title ts-d-block ts-mx-2 ts-py-1"><span><?=$arItem["NAME"]?></span></b>
                </a>
                <div class="text-info ts-px-2 ts-pb-2 ts-d-flex ts-flex-direction__column">
                    <?if(!empty($arItem["DISPLAY_PROPERTIES"]["PREVIEW_TEXT"]["VALUE"]["TEXT"])):?>
                        <p class="desc ts-mx-0 ts-my-0 ts-pb-1"><?= substr2($arItem["DISPLAY_PROPERTIES"]["PREVIEW_TEXT"]["DISPLAY_VALUE"],200) ?></p>
                    <?endif?>

                    <?if(!empty($arItem["DISPLAY_PROPERTIES"]["PRICE"]["VALUE"]) && !empty($arItem["DISPLAY_PROPERTIES"]["CURRENCY"]["VALUE"])):?>
                        <?=\travelsoft\currency\factory\Converter::getInstance()->convert($arItem["DISPLAY_PROPERTIES"]["PRICE"]["VALUE"], $arItem["DISPLAY_PROPERTIES"]["CURRENCY"]["VALUE"])->getResult();?>
                    <?else:?>
                        <div class="no-price ts-mx-0 ts-my-0"><?=GetMessage('ON_REQUEST')?></div>
                    <?endif?>

                    <?if (!empty($arItem["DISPLAY_PROPERTIES"]["DAYS"]["VALUE"])): ?>
                        <span class="per"><?=GetMessage('ON')?> <?= num2word($arItem["DISPLAY_PROPERTIES"]["DAYS"]["VALUE"],array(GetMessage('NIGHT_1'),GetMessage('NIGHT_2'),GetMessage('NIGHT_ALL')))?></span>
                    <?php endif ?>
                </div>
            </div>
            </div>
            <?if($key == 3):?><?break;?><?endif?>
        <?endforeach;?>
            </div>


            <div class="big tours box-2 ts-col-24 ts-col-lg-12 ts-pb-4">

        <?if(!empty($arResult["BIG_TOUR"])):?>

            <?
            $this->AddEditAction($arResult["BIG_TOUR"]['ID'], $arResult["BIG_TOUR"]['EDIT_LINK'], CIBlock::GetArrayByID($arResult["BIG_TOUR"]["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arResult["BIG_TOUR"]['ID'], $arResult["BIG_TOUR"]['DELETE_LINK'], CIBlock::GetArrayByID($arResult["BIG_TOUR"]["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <?$arImg = getSrc($arResult["BIG_TOUR"]["PROPERTIES"]["PICTURES"]["VALUE"], array("width"=>685,"height"=>582), NO_PHOTO_685_582, 1);?>

            <div class="tour big" id="<?=$this->GetEditAreaId($arResult["BIG_TOUR"]['ID']);?>">
                <a href="<?=$arResult["BIG_TOUR"]["DETAIL_PAGE_URL"]?>">
                    <?/*if($arResult["BIG_TOUR"]["DISPLAY_PROPERTIES"]["DATE"]["DATES_FORMAT"]):*/?><!--
                        <span class="dates">
                            <p><b><?/*=GetMessage('NEAREST_DEPARTURES')*/?></b><br />
                                <?/*foreach ($arItem["DISPLAY_PROPERTIES"]["DATE"]["DATES_FORMAT"] as $date): */?>
                                    <?/*= $date */?><br />
                                <?/*endforeach */?>
                            </p>
                        </span>
                    --><?/*endif*/?>
                    <div class="img" style="background-image:url('<?= $arImg[0] ?>')"></div>
                    <b class="title ts-d-block ts-mx-2 ts-py-1"><span><?=$arResult["BIG_TOUR"]["NAME"]?></span></b>
                </a>
                <div class="text-info ts-px-2 ts-pb-2 ts-d-flex ts-flex-direction__column">
                    <?if(!empty($arResult["BIG_TOUR"]["PROPERTIES"]["PREVIEW_TEXT"]["VALUE"]["TEXT"])):?>
                        <p class="desc ts-mx-0 ts-my-0 ts-pb-1"><?= substr2($arResult["BIG_TOUR"]["PROPERTIES"]["PREVIEW_TEXT"]["~VALUE"],200) ?></p>
                    <?endif?>

                    <?if(!empty($arResult["BIG_TOUR"]["PROPERTIES"]["PRICE"]["VALUE"]) && !empty($arResult["BIG_TOUR"]["PROPERTIES"]["CURRENCY"]["VALUE"])):?>
                        <?=\travelsoft\currency\factory\Converter::getInstance()->convert($arResult["BIG_TOUR"]["PROPERTIES"]["PRICE"]["VALUE"], $arResult["BIG_TOUR"]["PROPERTIES"]["CURRENCY"]["VALUE"])->getResult();?>
                    <?else:?>
                        <div class="no-price ts-mx-0 ts-my-0"><?=GetMessage('ON_REQUEST')?></div>
                    <?endif?>

                    <?if (!empty($arResult["BIG_TOUR"]["PROPERTIES"]["DAYS"]["VALUE"])): ?>
                        <span class="per"><?=GetMessage('ON')?> <?= num2word($arResult["BIG_TOUR"]["PROPERTIES"]["DAYS"]["VALUE"],array(GetMessage('NIGHT_1'),GetMessage('NIGHT_2'),GetMessage('NIGHT_ALL')))?></span>
                    <?php endif ?>
                </div>
            </div>

        <?endif?>

    </div>
        </div>

            <div class="tours box-3 ts-row">
                <?foreach($arResult["ITEMS"] as $key=>$arItem):?>
                    <?
                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                    ?>
                    <?if($key >= 4):?>
                    <?$arImg = getSrc($arItem["DISPLAY_PROPERTIES"]["PICTURES"]["VALUE"], array("width"=>258,"height"=>170), NO_PHOTO_258_170, 1);?>

                        <div class="ts-col-24 ts-col-sm-12 ts-col-lg-6 ts-pb-4 ts-pb-lg-0">
                            <div class="tour " id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                                <?if($arItem["DISPLAY_PROPERTIES"]["DATE"]["DATES_FORMAT"]):?>
                                    <span class="dates">
                                        <p><b><?=GetMessage('NEAREST_DEPARTURES')?></b><br />
                                            <?foreach ($arItem["DISPLAY_PROPERTIES"]["DATE"]["DATES_FORMAT"] as $date): ?>
                                                <?= $date ?><br />
                                            <?endforeach ?>
                                        </p>
                                    </span>
                                <?endif?>
                                <div class="img" style="background-image:url('<?= $arImg[0] ?>')"></div>
                                <b class="title ts-d-block ts-mx-2 ts-py-1"><span><?=$arItem["NAME"]?></span></b>
                            </a>
                                <div class="text-info ts-px-2 ts-pb-2 ts-d-flex ts-flex-direction__column">
                                    <?if(!empty($arItem["DISPLAY_PROPERTIES"]["PREVIEW_TEXT"]["VALUE"]["TEXT"])):?>
                                        <p class="desc ts-mx-0 ts-my-0 ts-pb-1"><?= substr2($arItem["DISPLAY_PROPERTIES"]["PREVIEW_TEXT"]["DISPLAY_VALUE"],200) ?></p>
                                    <?endif?>

                                    <?if(!empty($arItem["DISPLAY_PROPERTIES"]["PRICE"]["VALUE"]) && !empty($arItem["DISPLAY_PROPERTIES"]["CURRENCY"]["VALUE"])):?>
                                        <?=\travelsoft\currency\factory\Converter::getInstance()->convert($arItem["DISPLAY_PROPERTIES"]["PRICE"]["VALUE"], $arItem["DISPLAY_PROPERTIES"]["CURRENCY"]["VALUE"])->getResult();?>
                                    <?else:?>
                                        <div class="no-price ts-mx-0 ts-my-0"><?=GetMessage('ON_REQUEST')?></div>
                                    <?endif?>

                                    <?if (!empty($arItem["DISPLAY_PROPERTIES"]["DAYS"]["VALUE"])): ?>
                                        <span class="per"><?=GetMessage('ON')?> <?= num2word($arItem["DISPLAY_PROPERTIES"]["DAYS"]["VALUE"],array(GetMessage('NIGHT_1'),GetMessage('NIGHT_2'),GetMessage('NIGHT_ALL')))?></span>
                                    <?php endif ?>
                                </div>
                        </div>
                        </div>

                    <?endif?>
                <?endforeach;?>
            </div>

    </div>
    </div>
    </div>
<?endif?>