<?php


return array(
    "table" => "tsdates",
    "table_data" => array(
        "NAME" => "TSDATES",
        "ERR" => "Ошибка при создании highloadblock'a дат",
        "LANGS" => array(
            "ru" => 'Даты',
            "en" => "Dates"
        ),
        "OPTION_PARAMETER" => "DATES_HL"
    ),
    "fields" => array(
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_CITYFROM_ID",
            "USER_TYPE_ID" => 'integer',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'ID города отправления',
                'en' => 'Departure City ID',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'ID города отправления',
                'en' => 'Departure City ID',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'ID города отправления',
                'en' => 'Departure City ID',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "ID города отправления" ',
                'en' => 'An error in completing the field "Departure City ID"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_COUNTRY_ID",
            "USER_TYPE_ID" => 'integer',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'ID страны прибытия',
                'en' => 'ID of arrival country',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'ID страны прибытия',
                'en' => 'ID of arrival country',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'ID страны прибытия',
                'en' => 'ID of arrival country',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "ID страны прибытия" ',
                'en' => 'An error in completing the field "ID of arrival country"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_TOURTYPE_ID",
            "USER_TYPE_ID" => 'integer',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'ID типа тура',
                'en' => 'Tour type ID',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'ID типа тура',
                'en' => 'Tour type ID',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'ID типа тура',
                'en' => 'Tour type ID',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "ID типа тура" ',
                'en' => 'An error in completing the field "Tour type ID"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_BX_TOURTYPE_ID",
            "USER_TYPE_ID" => 'integer',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'ID типа тура (Битрикс)',
                'en' => 'Tour type ID (Bitrix)',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'ID типа тура (Битрикс)',
                'en' => 'Tour type ID (Bitrix)',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'ID типа тура (Битрикс)',
                'en' => 'Tour type ID (Bitrix)',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "ID типа тура (Битрикс)" ',
                'en' => 'An error in completing the field "Tour type ID (Bitrix)"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_DATES",
            "USER_TYPE_ID" => 'string',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Даты вылетов',
                'en' => 'Departure dates',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Даты вылетов',
                'en' => 'Departure dates',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Даты вылетов',
                'en' => 'Departure dates',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Даты вылетов" ',
                'en' => 'An error in completing the field "Departure dates"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_NIGHTS",
            "USER_TYPE_ID" => 'string',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Количество ночей',
                'en' => 'Number of nights',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Количество ночей',
                'en' => 'Number of nights',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Количество ночей',
                'en' => 'Number of nights',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Количество ночей" ',
                'en' => 'An error in completing the field "Number of nights"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_OPERATOR_ID",
            "USER_TYPE_ID" => 'integer',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'ID Оператора',
                'en' => 'Operator ID',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'ID Оператора',
                'en' => 'Operator ID',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'ID Оператора',
                'en' => 'Operator ID',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "ID Оператора" ',
                'en' => 'An error in completing the field "Operator ID"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
    )
);
