<?php

use Bitrix\Main\Localization\Loc,
    Bitrix\Main\ModuleManager,
    Bitrix\Main\Loader,
    Bitrix\Main\Config\Option;

Loc::loadMessages(__FILE__);

class travelsoft_sts extends CModule {

    public $MODULE_ID = "travelsoft.sts";
    public $MODULE_VERSION;
    public $MODULE_VERSION_DATE;
    public $MODULE_NAME;
    public $MODULE_DESCRIPTION;
    public $MODULE_GROUP_RIGHTS = "N";
    protected $namespaceFolder = "travelsoft";
    public $componentsList = array(
        "search.form.sts",
        "page.search.result.sts",
        "filter.sts",
        "html.ajax.render.sts",
        "html.list.render.sts",
    );
    public $adminFilesList = array(
        "travelsoft_sts_directions_edit.php",
        "travelsoft_sts_directions_list.php",
        "travelsoft_sts_directions_resorts_edit.php",
        "travelsoft_sts_directions_resorts_list.php",
        "travelsoft_sts_tables_list.php",
        "travelsoft_sts_tables_edit.php",
    );
    public $highloadblocksFiles = array();

    function __construct() {
        $arModuleVersion = array();
        $path = str_replace("\\", "/", __FILE__);
        $path = substr($path, 0, strlen($path) - strlen("/index.php"));
        include($path . "/version.php");
        if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        }
        $this->MODULE_NAME = "Модуль поиска туров";
        $this->MODULE_DESCRIPTION = "Модуль поиска туров представленных операторов";
        $this->PARTNER_NAME = "juliya-alexandrovna";
        $this->PARTNER_URI = "";

        Loader::includeModule('iblock');
        Loader::includeModule("highloadblock");
        $this->__loadHigloadblockFiles();
        set_time_limit(0);

    }

    public function copyFiles() {

        foreach ($this->adminFilesList as $file) {
            CopyDirFiles(
                $_SERVER["DOCUMENT_ROOT"] . "/local/modules/" . $this->MODULE_ID . "/install/admin/" . $file, $_SERVER["DOCUMENT_ROOT"] . "/bitrix/admin/" . $file, true, true
            );
        }

        foreach ($this->componentsList as $componentName) {
            CopyDirFiles(
                $_SERVER["DOCUMENT_ROOT"] . "/local/modules/" . $this->MODULE_ID . "/install/components/" . $componentName, $_SERVER["DOCUMENT_ROOT"] . "/local/components/" . $this->namespaceFolder . "/" . $componentName, true, true
            );
        }
    }

    public function deleteFiles() {
        foreach ($this->adminFilesList as $file) {
            DeleteDirFilesEx("/bitrix/admin/" . $file);
        }
        foreach ($this->componentsList as $componentName) {
            DeleteDirFilesEx("/local/components/" . $this->namespaceFolder . "/" . $componentName);
        }
        if (!glob($_SERVER["DOCUMENT_ROOT"] . "/local/components/" . $this->namespaceFolder . "/*")) {
            DeleteDirFilesEx("/local/components/" . $this->namespaceFolder);
        }
        return true;
    }

    public function DoInstall() {
        try {

            # проверка зависимостей модуля
            if (!ModuleManager::isModuleInstalled("travelsoft.currency")) {
                $errors[] = "Для работы модуля необходимо наличие установленного модуля <a target='_blank' href='https://github.com/dimabresky/travelsoft.currency'>travelsoft.currency</a>";
            }

            if (isset($errors) && !empty($errors)) {
                throw new Exception(implode("<br>", $errors));
            }

            # регистрируем модуль
            ModuleManager::registerModule($this->MODULE_ID);

            # создание higloadblock модуля
            $this->createHighloadblockTables();

            # копирование файлов
            $this->copyFiles();

            # добавление зависимостей модуля
            $this->addModuleDependencies();

            # добавление параметров выбора для модуля
            $this->addOptions();

        } catch (Exception $ex) {

            $GLOBALS["APPLICATION"]->ThrowException($ex->getMessage());

            $this->DoUninstall();

            return false;

        }

        return true;
    }

    public function DoUninstall() {

        # удаляем зависимости модуля
        $this->deleteModuleDependencies();

        # удаление таблиц higloadblock
        $this->deleteHighloadblockTables();

        # удаление файлов
        $this->deleteFiles();

        # удаление параметров модуля
        $this->deleteOptions();

        ModuleManager::unRegisterModule($this->MODULE_ID);

        return true;
    }

    public function addModuleDependencies () {

        RegisterModuleDependences("main", "OnBuildGlobalMenu", $this->MODULE_ID, "\\travelsoft\\sts\\EventsHandlers", "addGlobalAdminMenuItem");
        RegisterModuleDependences("iblock", "OnAfterIBlockElementAdd", $this->MODULE_ID, "\\travelsoft\\sts\\EventsHandlers", "bxOnAfterIBlockElementAdd");
        RegisterModuleDependences("iblock", "OnAfterIBlockElementUpdate", $this->MODULE_ID, "\\travelsoft\\sts\\EventsHandlers", "bxOnAfterIBlockElementUpdate");
        RegisterModuleDependences("", "TSOPERATORSOnAfterAdd", $this->MODULE_ID, "\\travelsoft\\sts\\EventsHandlers", "onAfterOperatorAdd");
        RegisterModuleDependences("", "TSOPERATORSOnAfterDelete", $this->MODULE_ID, "\\travelsoft\\sts\\EventsHandlers", "onAfterOperatorDelete");

    }

    public function deleteModuleDependencies () {

        UnRegisterModuleDependences("main", "OnBuildGlobalMenu", $this->MODULE_ID, "\\travelsoft\\sts\\EventsHandlers", "addGlobalAdminMenuItem");
        UnRegisterModuleDependences("iblock", "OnAfterIBlockElementAdd", $this->MODULE_ID, "\\travelsoft\\sts\\EventsHandlers", "bxOnAfterIBlockElementAdd");
        UnRegisterModuleDependences("iblock", "OnAfterIBlockElementUpdate", $this->MODULE_ID, "\\travelsoft\\sts\\EventsHandlers", "bxOnAfterIBlockElementUpdate");
        UnRegisterModuleDependences("", "TSOPERATORSOnAfterAdd", $this->MODULE_ID, "\\travelsoft\\sts\\EventsHandlers", "onAfterOperatorAdd");
        UnRegisterModuleDependences("", "TSOPERATORSOnAfterDelete", $this->MODULE_ID, "\\travelsoft\\sts\\EventsHandlers", "onAfterOperatorDelete");

    }

    public function addOptions() {
        Option::set($this->MODULE_ID, "CITIES_IB");
        Option::set($this->MODULE_ID, "COUNTRIES_IB");
        Option::set($this->MODULE_ID, "REGIONS_IB");
        Option::set($this->MODULE_ID, "HOTELS_IB");
        Option::set($this->MODULE_ID, "STARS_IB");
        Option::set($this->MODULE_ID, "TOURS_IB");
        Option::set($this->MODULE_ID, "TOURTYPES_IB");
        Option::set($this->MODULE_ID, "STARS_IB");
        Option::set($this->MODULE_ID, "PLACEMENTS_IB");
        Option::set($this->MODULE_ID, "CITYFROM_ID");
        Option::set($this->MODULE_ID, "COUNTRY_ID");
        Option::set($this->MODULE_ID, "DATE_RANGE", array(0, 7));
        Option::set($this->MODULE_ID, "NIGHT_RANGE", array(7, 14));
        Option::set($this->MODULE_ID, "ADULT", 2);
        Option::set($this->MODULE_ID, "CHILD", 0);
        Option::set($this->MODULE_ID, "PATH_NOPHOTO");
    }

    public function deleteOptions() {
        Option::delete($this->MODULE_ID, array("name" => "CITIES_IB"));
        Option::delete($this->MODULE_ID, array("name" => "COUNTRIES_IB"));
        Option::delete($this->MODULE_ID, array("name" => "REGIONS_IB"));
        Option::delete($this->MODULE_ID, array("name" => "HOTELS_IB"));
        Option::delete($this->MODULE_ID, array("name" => "STARS_IB"));
        Option::delete($this->MODULE_ID, array("name" => "TOURS_IB"));
        Option::delete($this->MODULE_ID, array("name" => "TOURTYPES_IB"));
        Option::delete($this->MODULE_ID, array("name" => "STARS_IB"));
        Option::delete($this->MODULE_ID, array("name" => "PLACEMENTS_IB"));
        Option::delete($this->MODULE_ID, array("name" => "CITYFROM_ID"));
        Option::delete($this->MODULE_ID, array("name" => "COUNTRY_ID"));
        Option::delete($this->MODULE_ID, array("name" => "DATE_RANGE"));
        Option::delete($this->MODULE_ID, array("name" => "NIGHT_RANGE"));
        Option::delete($this->MODULE_ID, array("name" => "ADULT"));
        Option::delete($this->MODULE_ID, array("name" => "CHILD"));
        Option::delete($this->MODULE_ID, array("name" => "PATH_NOPHOTO"));
    }

    public function createHighloadblockTables() {

        foreach ($this->highloadblocksFiles as $file) {

            $arr = include "highloadblocks/" . $file;

            $result = Bitrix\Highloadblock\HighloadBlockTable::add(array(
                'NAME' => $arr["table_data"]["NAME"],
                'TABLE_NAME' => $arr["table"],
//                        'LANGS' => $arr["table_data"]["LANGS"]
            ));

            if (!$result->isSuccess()) {
                throw new Exception($arr["table_data"]['ERR'] . "<br>" . implode("<br>", (array) $result->getErrorMessages()));
            }

            $table_id = $result->getId();

            Option::set($this->MODULE_ID, $arr["table_data"]["OPTION_PARAMETER"], $table_id);

            $arr_fields = $arr["fields"];

            $oUserTypeEntity = new CUserTypeEntity();

            foreach ($arr_fields as $arr_field) {

                $arr_field["ENTITY_ID"] = str_replace("{{table_id}}", $table_id, $arr_field["ENTITY_ID"]);

                if (!$oUserTypeEntity->Add($arr_field)) {
                    throw new Exception("Возникла ошибка при добавлении свойства " . $arr_field["ENTITY_ID"] . "[" . $arr_field["FIELD_NAME"] . "]" . $oUserTypeEntity->LAST_ERROR);
                }
            }

            if (isset($arr["items"]) && !empty($arr["items"])) {

                $entity   = Bitrix\Highloadblock\HighloadBlockTable::compileEntity(
                    Bitrix\Highloadblock\HighloadBlockTable::getById($table_id)->fetch());
                $class = $entity->getDataClass();
                foreach ($arr["items"] as $item) {
                    $class::add($item);
                }
            }
        }
    }

    public function deleteHighloadblockTables() {

        foreach ($this->highloadblocksFiles as $file) {

            $arr = include "highloadblocks/" . $file;

            $table_id = Option::get($this->MODULE_ID, $arr["table_data"]["OPTION_PARAMETER"]);
            if ($table_id > 0) {
                Bitrix\Highloadblock\HighloadBlockTable::delete($table_id);
            }
            Option::delete($this->MODULE_ID, array("name" => $arr["table_data"]["OPTION_PARAMETER"]));
        }
    }

    private function __loadHigloadblockFiles() {

        $this->highloadblocksFiles = $this->__loadFiles("highloadblocks");
    }

    private function __loadFiles($dirName) {

        $directory = __DIR__ . "/" . $dirName;
        return array_diff(scandir($directory), array('..', '.'));
    }

}
