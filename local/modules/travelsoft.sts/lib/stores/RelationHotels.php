<?php

namespace travelsoft\sts\stores;

use travelsoft\sts\adapters\MiddlewareHighloadblock;

/**
 * Класс для работы с таблицей отелей
 *
 * @author juliya.sharlova
 * @copyright (c) 2017, travelsoft
 */
class RelationHotels extends MiddlewareHighloadblock
{
    protected static $storeName = 'relationhotels';
}