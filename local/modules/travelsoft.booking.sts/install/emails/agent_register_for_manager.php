<?php

return array(
    "NAME" => "Регистрация агента на сайте #SITE_NAME#",
    "SUBJECT" => "Регистрация агента на сайте #SITE_NAME#",
    "MESSAGE" => "На сайте зарегистрирован новый <a href=\"#https://#SERVER_NAME#/bitrix/admin/user_edit.php?lang=ru&ID=#USER_ID#\">агент</a> и находится в режиме ожидания активации."
    . "<br>---------------------------------------------------------------------<br>"
    . "Сообщение сгенерировано автоматически.",
    "OPTION_PARAMETER" => "AGENT_REGISTER_MAIL_TEMPLATE_FOR_MANAGER",
    "OPTION_NAME" => "Письмо о регистрации нового агента менеджеру"
);