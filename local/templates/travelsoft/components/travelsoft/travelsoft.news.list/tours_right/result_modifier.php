<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
foreach ($arResult["ITEMS"] as $k=>$arItem){

    if(!empty($arItem["DISPLAY_PROPERTIES"]["DATE"]["VALUE"])){

        $date = array();

        if(is_array($arItem["DISPLAY_PROPERTIES"]["DATE"]["VALUE"])){
            $date_ = date("d.m.Y");
            foreach($arItem["DISPLAY_PROPERTIES"]["DATE"]["VALUE"] as $t=>$item){

                if(strtotime($date_) < strtotime($item)){
                    $date[] = CIBlockFormatProperties::DateFormat("d.m.Y", MakeTimeStamp($item, CSite::GetDateFormat()));
                }

            }
        }

        $arResult["ITEMS"][$k]["DISPLAY_PROPERTIES"]["DATE"]["DATES_FORMAT"] = $date;

    }

}