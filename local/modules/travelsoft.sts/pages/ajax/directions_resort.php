<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
try {

    $request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();

    if(!$request->isPost())
        throw new Exception();

    Bitrix\Main\Loader::includeModule('iblock');
    Bitrix\Main\Loader::includeModule('highloadblock');
    Bitrix\Main\Loader::includeModule("travelsoft.sts");

    $data = Bitrix\Main\Web\Json::decode($request->getPost('query_data'), true);

    if ($data == null) {
        $response = array('error' => true, 'result' => array());
        throw new \Exception();
    } else {
        $response['result'] = array();
    }

    if(!empty($data)){


        $country_bx = \travelsoft\sts\stores\RelationCountries::getBxId(array("ID"=>(int)$data));
        $cityforcountry = \travelsoft\sts\stores\Cities::getBxIdAr(array("PROPERTY_COUNTRY"=>(int)$country_bx));

        if($cityforcountry) {

            $arResortsHL = \travelsoft\sts\stores\RelationCities::get(array('filter' => array("UF_BX_ID" => $cityforcountry), 'select' => array('ID', 'UF_BX_ID')));

            $response["result"] = $arResortsHL;

        }

    }

    if (!empty($response)) {
        $response['error'] = false;
    }

    throw new \Exception();

} catch(\Exception $e) {
    header('Content-Type: application/json');
    echo json_encode($response);
}