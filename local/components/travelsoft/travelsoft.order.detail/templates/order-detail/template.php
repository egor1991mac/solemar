<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if(!$GLOBALS["USER"]->IsAuthorized()){
    $APPLICATION->AuthForm();
} else {

if ($arResult["ERRORS"]) {
    ?>

    <div class="alert alert-danger mt-20" role="alert">

        <? for ($i = 0, $cnt = count($arResult["ERRORS"]); $i < $cnt; $i++) { ?>
            <span><?= GetMessage($arResult["ERRORS"][$i]) ?></span>
        <? } ?>

    </div>

    <?
    return;
}

$currency = htmlspecialchars($arResult["ORDER"]["currencyTour"]);

$ORDER_ID = htmlspecialchars($arResult["ORDER"]["dogovor"]["name"]);
?>

<div class="my-order tariff 2">
    <div id="total__info">
        <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-9">
                <div class="sidebar-title color-dark-2 tariff-price color-dark-2"><?= GetMessage("ORDER"); ?> <span><?= $ORDER_ID ?></span></div>
                <?
                if ($arResult["ORDER"]["dogovor_status"]["key"] == 19 && $_SESSION['__TRAVELSOFT']['JUST_NOW_CANCELLATION']):
                    unset($_SESSION['__TRAVELSOFT']['JUST_NOW_CANCELLATION'])
                    ?>
                    <div class='mt-20 alert-box alert-attention'><h6><?= GetMessage("IN_CANCELLATION_PROCESS") ?></h6></div>
                <? endif ?>
                <ul>
                    <li class="tariff-line color-grey status"><span><?= GetMessage("STATUS"); ?></span> <?= htmlspecialchars($arResult["ORDER"]["dogovor_status"]["name"]) ?></li>
                    <li class="tariff-line color-grey date__create"><span><?= GetMessage("CREATION"); ?></span> <?= htmlspecialchars($arResult["ORDER"]["create_date"]) ?></li>
                    <li class="tariff-line color-grey date__from"><span><?= GetMessage("DATE_BEGIN"); ?></span> <?= htmlspecialchars($arResult["ORDER"]["tour_date"]) ?></li>
                    <li class="tariff-line color-grey duration"><span><?= GetMessage("DURATIONS"); ?></span> <?= num2word(htmlspecialchars($arResult["ORDER"]["duration"]),array(GetMessage('NIGHT_1'),GetMessage('NIGHT_2'),GetMessage('NIGHT_ALL'))) ?></li>
                    <li class="tariff-line color-grey count__people"><span><?= GetMessage("PERSONS"); ?></span> <?= htmlspecialchars($arResult["ORDER"]["count_men"]) ?></li>
                    <li class="tariff-line color-grey price"><span><?= GetMessage("PRICE"); ?></span> <?= htmlspecialchars($arResult["ORDER"]["price"][$currency]) . " " . $currency ?><?if($currency != "BYN"):?><?if(!empty($arResult["ORDER"]["price"]["BYN"]) && $arResult["ORDER"]["price"]["BYN"] != 0):?> / <?= htmlspecialchars($arResult["ORDER"]["price"]["BYN"]) . " BYN"?><?elseif($arResult["ORDER"]["price"][$currency] > 0):?><?=\travelsoft\currency\factory\Converter::getInstance()->convert($arResult["ORDER"]["price"][$currency], $currency)->getResult()?><?endif?><?endif;?></li>
                    <li class="tariff-line color-grey discount"><span><?= GetMessage("DISCOUNT"); ?></span> <?= htmlspecialchars($arResult["ORDER"]["discount"][$currency]) . " " . $currency ?></li>
                    <li class="tariff-line color-grey paid"><span><?= GetMessage("PAID"); ?></span> <?= $arResult["ORDER"]["paid"][$currency] . " " . $currency ?></li>
                    <li class="tariff-line color-grey to__pay"><span><?= GetMessage("TOPAY"); ?></span> <?= $arResult["ORDER"]["toPay"][$currency] . " " . $currency ?><?if($currency != "BYN" && $arResult["ORDER"]["toPay"][$currency] > 0):?><?if(!empty($arResult["ORDER"]["price"]["BYN"]) && $arResult["ORDER"]["price"]["BYN"] > 0):?> / <?= htmlspecialchars($arResult["ORDER"]["price"]["BYN"]) . " BYN"?><?elseif($arResult["ORDER"]["toPay"][$currency] > 0):?> / <?=\travelsoft\currency\factory\Converter::getInstance()->convert($arResult["ORDER"]["toPay"][$currency], $currency)->getResult()?><?endif;?><?endif;?></li>
                </ul>

                <? if (!empty($arResult["ORDER"]["documents"])): ?>
                    <?
                    for ($i = 0, $cnt = count($arResult["ORDER"]["documents"]); $i < $cnt; $i++) {

                        $item = $arResult["ORDER"]["documents"][$i];
                        if ($item['title'] == 'Счет'  || $item["title"] == 'Счет и Акт') {
                            continue;
                        }
                        $arDocs[] = '<div class="doc_block"><img src="'.SITE_TEMPLATE_PATH.'/img/clip_icon.png"> <a target="_blank" class="color-dr-blue-2 link-dark-2 document__" href="' . htmlspecialchars($item["url"]) . '">' . htmlspecialchars($item["title"]) . '</a></div>';
                    }


                    if (!empty($arDocs)):
                        ?>
                        <div class="simple-text doc"><h4><?= GetMessage("DOCUMENTATION"); ?></h4>
                            <div class="custom-panel bg-grey-2 radius-4">
                                <? echo implode("<br>", $arDocs); ?>
                            </div>
                        </div>
                    <? endif;
                endif;
                ?>

            </div>
<? if (!in_array($arResult["ORDER"]["dogovor_status"]["key"], array(2, 3, 19, 21))): ?>
                <div class="col-lg-3 col-md-3 col-sm-3"><a href="<?= $APPLICATION->GetCurPageParam("order_id=" . $ORDER_ID . "&action=cancel&" . bitrix_sessid_get(), array("order_id", "action", "sessid")) ?>" class="btn-primary thm-btn btn_m c-button bg-dr-blue-2 hv-dr-blue-2-o"><?= GetMessage("CANCEL"); ?></a></div>
<? endif ?>
        </div>
    </div>
    <div id="services__container clearfix">
        <?$services_icon = array(
            1 => 'fly_icon.png',
            2 => 'car_icon.png',
            3 => 'home_icon.png',
            5 => 'visa_icon.png',
            6 => 'med_icon.png',
        );?>
        <div class="simple-text"><h4><?= GetMessage("SERVICES"); ?></h4></div>
        <div class="table-responsive">
            <table class="table-striped table-condensed offers table style-1 striped">
                <thead>
                    <tr class="table-label color-grey">
                        <th class="dow"></th>
                        <th class="dow"><?= GetMessage("DATE")?></th>
                        <th class="dow"><?= GetMessage("SERVICE_DESC")?></th>
                    </tr>
                </thead>
                <tbody>
                    <?
                    $forspotpayment = true;
                    for ($i = 0, $cnt = count($arResult["ORDER"]["services"]); $i < $cnt; $i++):
                        $item = $arResult["ORDER"]["services"][$i];
                        if (!$item['parts']['for_spot_payment']) {
                            $forspotpayment = false;
                        }
                        ?>
                        <tr class="table-label color-dark-2">
                            <td valign="top" data-label="" class="day"><?= $i+1?></td>
                            <td valign="top" data-label="<?= GetMessage("DATE")?>" class="day"><?= $item["dateBegin"]?></td>
                            <td valign="top" data-label="<?= GetMessage("SERVICE_DESC")?>" class="day"><?if(!empty($item['service_class']['key']) && isset($services_icon[$item['service_class']['key']])):?><img src="<?=SITE_TEMPLATE_PATH?>/img/<?=$services_icon[$item['service_class']['key']]?>"><?endif?><?= $item["title"]?></td>
                        </tr>
                    <? endfor ?>
                </tbody>
            </table>
        </div>
    </div>
    <div id="turists__container clearfix">
        <div class="simple-text"><h4><?= GetMessage("TURISTS"); ?></h4></div>
        <div class="row block-touists">
            <?$i = 1;?>

            <?foreach ($arResult["ORDER"]["turists"] as $turist):?>

                <div class="col-sm-6 col-sm-offser-3 col-md-6 tourist-block">
                    <div class="no-border payment-traveller">

                        <div class="gap-0">

                            <h4><b><?= GetMessage('TURIST')?><?=$i?></b></h4>

                        </div>


                        <div class="form-horizontal tariff-line">
                            <div class="gap-20">
                                <span><?= GetMessage("FIO"); ?>:</span> <?= htmlspecialchars($turist["last_name"]) ?> <?= htmlspecialchars($turist["first_name"]) ?>
                            </div>
                        </div>

                        <div class="form-horizontal tariff-line">
                            <div class="gap-20">
                                <span><?= GetMessage("SEX"); ?>:</span> <?= htmlspecialchars(GetMessage("SEX_".$turist["gender"])) ?>
                            </div>
                        </div>

                        <div class="form-horizontal tariff-line">
                            <div class="gap-20">
                                <span><?= GetMessage("BRTDAY"); ?>:</span> <?= htmlspecialchars($turist["birth_date"]) ?>
                            </div>
                        </div>

                        <div class="form-horizontal tariff-line">
                            <div class="gap-20">
                                <span><?= GetMessage("PASSPORT_NUM"); ?>:</span> <?= htmlspecialchars($turist["passport_num"]) ?>
                            </div>
                        </div>
                        <div class="form-horizontal tariff-line">
                            <div class="gap-20">
                                <span><?= GetMessage("PASSPORT_DATE"); ?>:</span> <?= htmlspecialchars($turist["passport_date"]) ?>
                            </div>
                        </div>
                        <div class="form-horizontal tariff-line">
                            <div class="gap-20">
                                <span><?= GetMessage("CITIZENSHIP"); ?>:</span> <?= htmlspecialchars($turist["citizenship"]) ?>
                            </div>
                        </div>
                        <?if(!empty($turist["passport_id"])):?>
                            <div class="form-horizontal tariff-line">
                                <div class="gap-20">
                                    <span><?= GetMessage("PASSPORT_ID"); ?>:</span> <?= htmlspecialchars($turist["passport_id"]) ?>
                                </div>
                            </div>
                        <?endif?>

                    </div>
                </div>

                <?$i++?>
            <?endforeach;?>
        </div>
    </div>
    <br>
    <?
    if ($arResult["ORDER"]["dogovor_status"]["key"] == 7 && !$arResult["ORDER"]["dogovor_status"]["isPaid"]) :
        $formTpl = "<div class='mt-20 payment__form'>";
        $formTpl .= "<form target='_blank' method='POST' action='" . $arParams["URL_TO_PAY"] . "'>";
        $formTpl .= "<input type='hidden' name='token' value='" . $_SESSION["__TRAVELSOFT"]["TOKEN"] . "'>";
        $formTpl .= "<input type='hidden' name='orderCode' value='" . $ORDER_ID . "'>";
        $formTpl .= "<input type='hidden' name='method' value='login'>";
        $formTpl .= "<input type='hidden' name='typePayment' value='#TYPE_PAYMENT#'>";
        $formTpl .= "<button class='btn-primary c-button bg-dr-blue-2 hv-dr-blue-2-o' name='submit' type='submit'>" . GetMessage("PAY_BTN_TITLE") . "</button>";
        $formTpl .= "</form>";
        $formTpl .= "</div>";
        ?>
        <?
        $arGroups = $USER->GetUserGroupArray();
        $expendedFirst = !$forspotpayment && !$arResult["ORDER"]["dogovor_status"]["key"] != 22;
        $expendedForSpotPayment = $forspotpayment || $arResult["ORDER"]["dogovor_status"]["key"] == 22;
        ?>
        <div id="pay__container">
            <div class="simple-text"><h4><?= GetMessage("PAYMENTOPTIONS"); ?></h4></div>
            <ul class="tabs-head nav-tabs-one color-grey dot-blue-2">
                <li <? if ($expendedFirst): ?>class="active"<? endif ?>><a data-toggle="tab" href="#section1"><?= GetMessage("BANKCARD"); ?></a></li>
                <? if (!in_array("9", $arGroups)): ?>
                    <li><a data-toggle="tab" href="#section2"><?= GetMessage("ERIP"); ?></a></li>
                <? endif ?>
    <? if ($expendedForSpotPayment): ?>
                    <li class="active"><a data-toggle="tab" aria-expanded="true" href="#section5"><?= GetMessage("FORSPOTPAYMENT"); ?></a></li>
    <? endif ?>
                <li><a data-toggle="tab" href="#section6"><?= GetMessage("PAY_BY_INVOICE"); ?></a></li>
            </ul>
            <div class="tab-content">
                <div id="section1" class="tab-pane <? if ($expendedFirst): ?>active in<? else: ?>fade<? endif ?>">
                <?= GetMessage("PAYMENTOPTIONSTEXT", array("#PAY_FORM#" => str_replace("#TYPE_PAYMENT#", "card", $formTpl))); ?>
                </div>
                    <? $arGroups = $USER->GetUserGroupArray(); ?>
                <? if (!in_array("9", $arGroups)): ?>
                    <div id="section2" class="tab-pane fade">
                    <?= GetMessage("ERIPTEXT", array("#PAY_FORM#" => str_replace("#TYPE_PAYMENT#", "erip", $formTpl))); ?>
                    </div>
                <? endif ?>
                <? /* <div id="section3" class="tab-pane fade">
                  <?= GetMessage("CASHTEXT"); ?>
                  </div>
                  <div id="section4" class="tab-pane fade">
                  <?= GetMessage("BANKPAYMENTTEXT"); ?>
                  </div> */ ?>
                    <? if ($expendedForSpotPayment): ?>
                    <div id="section5" class="tab-pane active in">
                        <? if ($arResult["ORDER"]["dogovor_status"]["key"] != 22): ?>
                            <a class="btn-primary c-button bg-dr-blue-2 hv-dr-blue-2-o" href="<?= $APPLICATION->GetCurPageParam("order_id=" . $ORDER_ID . "&action=fsp&sessid=" . bitrix_sessid(), array("order_id", "action", "sessid"), false) ?>"><?= GetMessage('PAY_BTN_TITLE') ?></a>
                            <?= GetMessage("FORSPOTPAYMENTTEXT"); ?>
                    <? else: ?>
                            <p style="color: green"><b><?= GetMessage('FORSPOTPAYMENTCHOOSE') ?></b></p>
                        <? endif ?>
                    </div>
                    <? endif ?>
                <div id="section6" class="tab-pane fade">
                    <?
                    for ($i = 0, $cnt = count($arResult["ORDER"]["documents"]); $i < $cnt; $i++) {
                        $item = $arResult["ORDER"]["documents"][$i];
                        if ($item["title"] == 'Счет' || $item["title"] == 'Счет и Акт') {
                            ?>
                            <a target="__blank" class="btn-primary c-button bg-dr-blue-2 hv-dr-blue-2-o" href="<?= htmlspecialchars($item['url']) ?>"><?= GetMessage("PAY_BY_INVOICE_BUTTON") ?></a>
                            <p><?= GetMessage("PAY_BY_INVOICE_TEXT"); ?></p>
        <? }
    }
    ?>
                </div>
            </div>
        </div>
<? elseif (!$arResult["ORDER"]["dogovor_status"]["isPaid"] && !in_array($arResult["ORDER"]["dogovor_status"]["key"], array(21, 2))): ?>
        <div class='mt-20 alert-box alert-attention'><h6><?= GetMessage("NOT_ALLOW_TO_PAY") ?></h6></div>
<? endif ?>
</div>

<script>
    /**
     * @param {jQuery} $
     * @returns {undefined}
     */
    (function ($) {

        $($("input[name='pay_variant']:checked").data("href")).show();

        $("input[name='pay_variant']").on("click", function () {

            $("#pay__variants__container div[id$='_description']").hide();

            $($("input[name='pay_variant']:checked").data("href")).show();

        });

    })(jQuery);
</script>
<!--<div class="booking-messanger mt-20">
    <?/*
    $APPLICATION->IncludeComponent(
            "travelsoft:travelsoft.booking.messanger", ".default", array(
        "FREQREQUEST" => "20",
        "ORDER_ID" => htmlspecialchars($ORDER_ID),
        "TOKEN" => $_SESSION["__TRAVELSOFT"]["TOKEN"],
        "COMPONENT_TEMPLATE" => ".default",
        "USE_AJAX" => "Y",
        "DATE_FROM" => htmlspecialchars($arResult["ORDER"]["create_date"]) . " 00:00:00"
            ), false
    );
    */?>
</div>-->
<?}?>