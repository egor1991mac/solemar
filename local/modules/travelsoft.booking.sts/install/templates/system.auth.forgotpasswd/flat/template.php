<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 */

//one css for all system.auth.* forms
$APPLICATION->SetAdditionalCSS("/bitrix/css/main/system.auth/flat/reg.css");
?>

<div class="bx-authform">

<?
if(!empty($arParams["~AUTH_RESULT"])):
	$text = str_replace(array("<br>", "<br />"), "\n", $arParams["~AUTH_RESULT"]["MESSAGE"]);
?>
	<div class="alert <?=($arParams["~AUTH_RESULT"]["TYPE"] == "OK"? "alert-success":"alert-danger")?>"><?=nl2br(htmlspecialcharsbx($text))?></div>
<?endif?>

<!-- Контейнер -->
<div class="ts-theme ts-border-radius ts-box-shadow ts-max-width-768 ts-mx-auto ts-mb-2">
    <input type="hidden" name="AUTH_FORM" value="Y" />
    <input type="hidden" name="TYPE" value="REGISTRATION" />
    <div class="ts-title ts-px-1 ts-py-1">
        <h2 class="ts-h2 ts-reset ts-color-white">Запрос пароля пользовотеля</h2>
        <button class="ts-d-none">x</button>
    </div>


	<form name="bform" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>" class="">
        <?if($arResult["BACKURL"] <> ''):?>
                <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
        <?endif?>
		<input type="hidden" name="AUTH_FORM" value="Y">
		<input type="hidden" name="TYPE" value="SEND_PWD">

        <!-- Контент -->
        <div class="ts-content ts-w-100 ts-d-flex ts-justify-center ts-flex-wrap min-height ts-align-content-around ts-pt-3 ts-pb-1">
            <div class="ts-col-8 ts-mx-auto ts-mb-2">
                <div class="ts-link ts-d-flex ts-flex-wrap ts-justify-center">
                    <p class="ts-reset"><?=GetMessage("AUTH_FORGOT_PASSWORD_1")?></p>
                </div>
            </div>
            <!-- Логин -->
            <div class="ts-col-8 ts-mb-2">
                <div class="ts-input">
                    <label for="" class="ts-d-none"></label>
                    <div class="placeholder"><?=GetMessage("AUTH_LOGIN_EMAIL")?></div>
                    <div class="validation"></div>
                    <div class="input">
                        <input type="text" name="USER_LOGIN" maxlength="255" value="<?=$arResult["LAST_LOGIN"]?>">
                        <input type="hidden" name="USER_EMAIL" />
                    </div>
                </div>
            </div>
            <?if ($arResult["USE_CAPTCHA"]):?>
                <input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />

                <div class="ts-col-8">
                    <div class="ts-input">
                        <label for="" class="ts-d-none"></label>
                        <div class="placeholder"><?=GetMessage("system_auth_captcha")?></div>
                        <div class="validation"></div>
                        <div class="bx-captcha"><img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" /></div>
                        <div class="input">
                            <input type="text" name="captcha_word" maxlength="50" value="" autocomplete="off">
                        </div>
                    </div>
                </div>

            <?endif?>

            <!-- Submit -->
            <div class="ts-col-8">
                <div class="ts-submit ts-max-width-370">
                    <input type="submit" name="send_account_info" class="c-button w-100" value="<?=GetMessage("AUTH_SEND")?>">
                </div>
            </div>

        </div>

	</form>
    <noindex>
        <div class="ts-col-5 ts-mx-auto ts-mb-3">
            <div class="ts-link ts-d-flex ts-flex-wrap ts-justify-center">
                <a href="<?=$arResult["AUTH_AUTH_URL"]?>"><?=GetMessage("AUTH_AUTH")?></a>
                <a href="/personal/?register=yes" rel="nofollow"><?=GetMessage("AUTH_REGISTER")?>
                <a href="/agencies/registration/" rel="nofollow"><?=GetMessage("AUTH_REGISTER_AGENT")?></a>


            </div>
    </noindex>
</div>
</div>

<script type="text/javascript">

    let input = document.querySelectorAll('.ts-input input');
    let checkboxSaveMe = $('.ts-checkbox i');

    if($(input).val().length != 0){
        $(input).closest('.ts-input').addClass('active');
    }

    checkboxSaveMe.on('click',function(e){
        $(this).toggleClass('active');
    });
    $(input).on('focus',function(e){
        $(this).closest('.ts-input').addClass('active focus');

    });
    $(input).on('focusout',function(e){
        if($(this).val().length == 0){
            $(this).closest('.ts-input').removeClass('active focus');
        }
        else $(this).closest('.ts-input').removeClass('focus');

    });
    $(input).on('keyup',function(e){
        if($(this).val().length != 0){
            $(this).closest('.ts-input').addClass('active');
        }
        else{
            $(this).closest('.ts-input').removeClass('active');
        }
    });

document.bform.onsubmit = function(){document.bform.USER_EMAIL.value = document.bform.USER_LOGIN.value;};
document.bform.USER_LOGIN.focus();
</script>
