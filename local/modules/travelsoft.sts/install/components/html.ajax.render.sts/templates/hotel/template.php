<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if(isset($arResult["ITEMS"]) && !empty($arResult["ITEMS"])):?>

    <? foreach ($arResult["ITEMS"] as $arItem): ?>
        <?$name = !empty($arItem["hotel"]["name"]) ? $arItem["hotel"]["name"] : $arItem["hotel"]["originalName"];
        $food = !empty($arItem["food"]["name"]) ? $arItem["food"]["name"] : $arItem["food"]["originalName"];
        $placement = !empty($arItem["placement"]["name"]) ? $arItem["placement"]["name"] : $arItem["placement"]["originalName"];
        $roomType = !empty($arItem["roomType"]["name"]) ? $arItem["roomType"]["name"] : $arItem["roomType"]["originalName"];
        $roomCatType = !empty($arItem["roomCatType"]["name"]) ? $arItem["roomCatType"]["name"] : $arItem["roomCatType"]["originalName"];
        ?>
        <tr>
            <td valign="top" data-label="Дата вылета" class="day font-bold-13"><?=$arItem["tourDate"]["dateFrom"]?></td>
            <td valign="top" data-label="Кол-во ночей" class="day font-bold-13"><span><?=num2word($arItem["night"],array("ночь","ночи","ночей"))?></span></td>
            <td valign="top" data-label="Размещение" class="day"><?=$placement?></td>
            <td valign="top" data-label="Тип номера" class="day"><?=$roomType?></td>
            <td valign="top" data-label="Категория номера" class="day"><?=$roomCatType?></td>
            <td valign="top" data-label="Питание" class="day"><?=$food?>
                <?if(!empty($arItem["food"]["fullName"])):?>
                    <span class="hotel-offer-text-food-desc text-grey" title="<?=$arItem["food"]["fullName"]?>"> — <?=$arItem["food"]["fullName"]?></span>
                <?endif?>
                <?if(!empty($arItem["food"]["desc"])):?>
                    <i class="fa fa-info hotel-offer-text-food_info" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?=$arItem["food"]["desc"]?>"></i>
                <?endif?>
            </td>
            <td valign="top" data-label="Стоимость" class="day price font-bold-16"><span class="booking-item-price number"><?= \travelsoft\currency\Converter::getInstance()->convert($arItem["prices"], $arItem["defCurrency"])->getResult() ?></span></td>
            <td valign="top" class="day">
                <form method="post">
                    <button type="submit" name="baction" value="add2cart" class="thm-btn detail">Заказать</button>
                </form>
            </td>
        </tr>
    <?endforeach;?>

<?else:?>
    <p class="hotel-offer">Нет других вариантов размещения.</p>
<?endif?>
<script>
    // tooltip
    $('[data-toggle="tooltip"]').tooltip();
</script>
