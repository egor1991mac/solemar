
<?/* if ($APPLICATION->GetDirProperty("MAIN_PAGE") != "Y"): */?><!--
            </div>
		</div>
	</div>
--><?/*endif;*/?>
<?/* $APPLICATION->IncludeComponent(
    "bitrix:main.include",
    "",
    Array(
        "AREA_FILE_SHOW" => "file",
        "AREA_FILE_SUFFIX" => "inc",
        "EDIT_TEMPLATE" => "",
        "PATH" => SITE_DIR . "include/footer.php"
    )
); */?>

<?if($APPLICATION->GetDirProperty("SHOW_TOP_PANEL") != "N" && HOME_PAGE !== true):?>
        </div>
    </div>
<?endif?>

<div id="footer">
    <div class="container">
        <div class="wrap">
            <div class="col1-1">
                <h2>Наши контакты</h2>
                <p class="phones">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "AREA_FILE_SHOW" => "file",
                            "AREA_FILE_SUFFIX" => "inc",
                            "EDIT_TEMPLATE" => "",
                            "PATH" => SITE_DIR . "include/footerPhonesList.php"
                        )
                    ); ?>
                </p>
                <p>
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "AREA_FILE_SHOW" => "file",
                            "AREA_FILE_SUFFIX" => "inc",
                            "EDIT_TEMPLATE" => "",
                            "PATH" => SITE_DIR . "include/footerEmail.php"
                        )
                    ); ?>
                </p>
            </div>
            <div class="col1-2">
                <h2>Адрес</h2>
                <p><? $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "",
                        "PATH" => SITE_DIR . "include/footerAddress.php"
                    )
                ); ?>
                </p>
                <h2>Режим работы</h2>
                <p>
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "AREA_FILE_SHOW" => "file",
                            "AREA_FILE_SUFFIX" => "inc",
                            "EDIT_TEMPLATE" => "",
                            "PATH" => SITE_DIR . "include/headerMode.php"
                        )
                    ); ?>
                </p>
            </div>
            <? $APPLICATION->IncludeComponent(
                "bitrix:menu",
                "bottom.menu",
                Array(
                    "ALLOW_MULTI_SELECT" => "N",
                    "CHILD_MENU_TYPE" => "bottom",
                    "DELAY" => "N",
                    "MAX_LEVEL" => "1",
                    "MENU_CACHE_GET_VARS" => array(""),
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_TYPE" => "N",
                    "MENU_CACHE_USE_GROUPS" => "N",
                    "ROOT_MENU_TYPE" => "bottom",
                    "USE_EXT" => "N"
                )
            ); ?>
            <? $APPLICATION->IncludeComponent(
                "bitrix:menu",
                "bottom2.menu",
                Array(
                    "ALLOW_MULTI_SELECT" => "N",
                    "CHILD_MENU_TYPE" => "bottom2",
                    "DELAY" => "N",
                    "MAX_LEVEL" => "1",
                    "MENU_CACHE_GET_VARS" => array(""),
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_TYPE" => "N",
                    "MENU_CACHE_USE_GROUPS" => "N",
                    "ROOT_MENU_TYPE" => "bottom2",
                    "USE_EXT" => "N"
                )
            ); ?>
            <div class="clear"></div>
        </div>
    </div>
</div>
<div id="footer-dark">
    <div class="container">
        <div class="wrap">
            <div class="copy">
                <? $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "",
                        "PATH" => SITE_DIR . "include/copyright.php"
                    )
                ); ?>
            </div>
            <div class="social">
                <p>Оставайтесь с нами:</p>
                <? $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "",
                        "PATH" => SITE_DIR . "include/socServices.php"
                    )
                ); ?>
            </div>
            <div class="payments"></div>
            <div style="margin-top: 10px;">
                <a href="/travelsoft/">
                    <img src="/local/templates/travelsoft/img/logo_travelsoft.jpg" title="Разработчик - ТрэвелСофт Консалтинг">
                </a>
            </div>
        </div>
    </div>
</div>

<!--

<div id="scroll-top"><i class="fa fa-angle-up"></i></div>-->

<? $APPLICATION->ShowPanel() ?>
</body>
</html>