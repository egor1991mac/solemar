/* 
 * Jquery script ajax
 * 
 *  use bootstrap css classes 
 * 
 * travelsoft.booking.sts component
 * 
 * регистрация
 * авторизация 
 *
 * Доступные action запроса:
 *  
 *    check_auth - проверка существования пользователя по email
 *    do_registration - попытка регистрации пользоватя
 *    do_authorize - попытка авторизации пользователя
 *    delete - удаление позиции из корзины
 * 
 * Статусы ответов от ajax обработчика:
 * 
 *    0 - не удалось удалить позицию в корзине
 *    1 - удаление позиции прошло удачно
 *    2 - введён некорректный email
 *    3 - удачная авторизация
 *    4 - ошибка авторизации (неверный email или пароль)
 *    5 - email и подтверждение email не совпадают
 *    6 - успешная регистрация
 *    7 - ошибка регистрации
 *    8 - пользователя с введенным $email не существует
 *    9 - пользователь с введенным $email существует
 * 
 */

/**
 * @param {jQuery} $
 * @param {Object} JSON
 * @returns {undefined}
 */

"use strict";

(function ($, JSON) {

    var __ajax_url = "/local/components/travelsoft/travelsoft.page.booking/ajax.php",
            __global_data_scope = {

                sessid: null,

                cart_item_class_container: "item-cart",

                booking_form_id: "booking",

                preloader: null,

                active_service_count: 0,

                cart_items_count: 0,

                empty_cart_message: "your cart is empty",

                del_pos_array: null,

                messages: {
                    booking_btn: "BOOKING NOW",
                    confirm_email_error: "error",
                    confirm_email_not_equal_email: "error",
                    placeholder_confirm_email: "Confirm email",
                    placeholder_password: "Password",
                    do_registration_button: "Register",
                    do_authorize_button: "Authorize",
                    forgot_password: "forgot password",
                    empty_email: "empty email",
                    enter_the_password: "Enter the password",
                    _404: "System error. Please contact the site administrator (_404)",
                    _500: "System error. please contact the site administrator (_500)",
                    default_error_text: "Error. Please contact the site administrator (_default)",
                    status_0: "error",
                    status_1: "error",
                    status_2: "error",
                    status_3: "error",
                    status_4: "error",
                    status_5: "error",
                    status_6: "error",
                    status_7: "error",
                    status_8: "error",
                    status_9: "error"
                },

                email_input_id: null,

                forgot_password_link: null,

                message_box_id: "__message-box",

                is_authorized: false,

                error_box_html_id: "__error-box",

                registration_box_id: "__registration_box",

                authorize_box_id: "__authorize_box",

                ajax_flag: false,

                $email_input: null,

                $mount_point: null,

                $booking_btn: null

            };

    /**
     * проверка email на корректность 
     * @param {String} email
     * @returns {Boolean}
     */
    function __email_validation(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }

    /**
     * отрисовка области текста ошибки
     * @param {String} message
     * @returns {undefined}
     */
    function __render_error_box(message) {

        var error_box = __global_data_scope.$email_input.siblings("#" + __global_data_scope.error_box_html_id), next = null;

        if (!error_box.length) {
            //next = __global_data_scope.$email_input.next();
            next = __global_data_scope.$email_input;
            if (next.length) {
                next.closest('.input').before("<div id='" + __global_data_scope.error_box_html_id + "' class='validation' style='color: red; display:none'></div>");
            } else {
                __global_data_scope.$email_input.closest('.input').before("<div id='" + __global_data_scope.error_box_html_id + "' class='validation' style='color: red; display:none'></div>");
            }
            error_box = $("#" + __global_data_scope.error_box_html_id);
        }

        error_box.text(message);
        error_box.slideDown();

    }

    /**
     * удаление области текста ошибки
     * @returns {undefined}
     */
    function __delete_error_box() {

        $("#" + __global_data_scope.error_box_html_id).remove();

    }

    /**
     * отрисовка области текста сообщения
     * @param {String} message
     * @returns {undefined}
     */
    function __render_message_box(message) {

        var message_box = __global_data_scope.$email_input.next("#" + __global_data_scope.message_box_id);

        if (!message_box.length) {
            __global_data_scope.$email_input.closest('.input').before("<div id='" + __global_data_scope.message_box_id + "' class='validation' style='color: green; display:none'></div>");
            message_box = $("#" + __global_data_scope.message_box_id);
        }

        message_box.text(message);
        message_box.slideDown();

    }

    /**
     * удаление области текста сообщения
     * @returns {undefined}
     */
    function __delete_message_box() {
        $("#" + __global_data_scope.message_box_id).remove();
    }

    /**
     * инициализация области для регистрации пользователя
     * отправка запроса
     * @returns {undefined}
     */
    function __init_registration_box() {

        var reg_box = __global_data_scope.$email_input.next("#" + __global_data_scope.registration_box_id);
        //var reg_box = $("#" + __global_data_scope.registration_box_id);

        if (!reg_box.length) {

            // отрисовка
            __global_data_scope.$email_input.closest('.row.form').append(
                    "<div class='ts-col-24 ts-col-lg-12 col-xs-12 col-sm-6 ts-mb-2 ts-order__0 ts-order-lg__10' id='" + __global_data_scope.registration_box_id + "'><div class='ts-input form-block active'><label for='' class='ts-d-none'></label>" +
                    "<div class='placeholder'>" + __global_data_scope.messages.placeholder_confirm_email + "</div>" +
                    "<div class='input'><input onpaste='return false;' name='confirm_email' required='' type='email' placeholder='" + __global_data_scope.messages.placeholder_confirm_email + "'></div>" +
//                        "<div style='text-align: right; margin-top: 5px;'>" + 
//                            "<button id='__reg_button'>"+__global_data_scope.messages.do_registration_button+"</button>" +
//                        "</div>" + 
                    "</div></div>"
                    );

            var reg_box_input = $("#" + __global_data_scope.registration_box_id + " input[name='confirm_email']");

            if(reg_box_input.val().length != 0){
                reg_box_input.closest('.ts-input').addClass('active');
            }
            $(reg_box_input).on('focus',function(e){
                reg_box_input.closest('.ts-input').addClass('active focus');

            });
            reg_box_input.on('focusout',function(e){
                if(reg_box_input.val().length == 0){
                    reg_box_input.closest('.ts-input').removeClass('active focus');
                }
                else reg_box_input.closest('.ts-input').removeClass('focus');

            });
            reg_box_input.on('keyup',function(e){
                if(reg_box_input.val().length != 0){
                    reg_box_input.closest('.ts-input').addClass('active');
                }
                else{
                    reg_box_input.closest('.ts-input').removeClass('active');
                }
            });

            //$("#__reg_button").on("click", function (e) {
            $("#" + __global_data_scope.booking_btn_id).on("click", function (e) {

                var email = __global_data_scope.$email_input.val(),
                        confirm_email = $("#" + __global_data_scope.registration_box_id).find("input[name='confirm_email']").val(), $this = $(this);
                console.log(confirm_email);
                e.preventDefault();

                if (!__email_validation(confirm_email)) {
                    __delete_message_box();
                    __render_error_box(__global_data_scope.messages.confirm_email_error);
                    return;
                }

                if (confirm_email.toString() !== email.toString()) {
                    __delete_message_box();
                    __render_error_box(__global_data_scope.messages.confirm_email_not_equal_email);
                    return;
                }

                // запрос на регистрацию
                __send_request({
                    beforeSend: function () {
                        $('#form-shading').show();
                        //$this.html("<img src='" + __global_data_scope.preloader + "' alt='gif-preloader'>");
                    },
                    complete: function () {
                        $('#form-shading').hide();
                        $this.html(__global_data_scope.messages.booking_btn);
                    },
                    action: "do_registration",
                    email: email,
                    confirm_email: confirm_email
                });

            });

        }

    }

    /** удаление области для регистрации **/
    function __delete_registration_box() {

        // $("#__reg_button").off("click");
        $("#" + __global_data_scope.booking_btn_id).off("click");
        $("#" + __global_data_scope.registration_box_id).remove();

    }

    /** инициализация блока авторизации **/
    function __init_authorize_box() {

        var auth_box = $("#" + __global_data_scope.authorize_box_id);

        if (!auth_box.length) {

            // отрисовка
            __global_data_scope.$email_input.closest('.row.form').append(
                "<div class='ts-col-24 ts-col-lg-12 col-xs-12 col-sm-6 ts-mb-2 ts-order__0 ts-order-lg__10' id='" + __global_data_scope.authorize_box_id + "'><div class='ts-input form-block active'><label for='' class='ts-d-none'></label>" +
                "<div class='placeholder'>" + __global_data_scope.messages.placeholder_password + "</div>" +
                "<div class='input'><input name='password' required='' type='password' placeholder='" + __global_data_scope.messages.placeholder_password + "'></div>" +
                "<div style='text-align: right'>" +
                "<div style='padding: 5px 0'><a style='text-decoration: underline;font-size: 14px;' href='" + __global_data_scope.forgot_password_link + "'>" + __global_data_scope.messages.forgot_password + "</a></div>" +
                "</div>" +
                "</div></div>"
            );

            var auth_box_input = $("#" + __global_data_scope.authorize_box_id + " input[name='password']");

            if(auth_box_input.val().length != 0){
                auth_box_input.closest('.ts-input').addClass('active');
            }
            $(auth_box_input).on('focus',function(e){
                auth_box_input.closest('.ts-input').addClass('active focus');

            });
            auth_box_input.on('focusout',function(e){
                if(auth_box_input.val().length == 0){
                    auth_box_input.closest('.ts-input').removeClass('active focus');
                }
                else auth_box_input.closest('.ts-input').removeClass('focus');

            });
            auth_box_input.on('keyup',function(e){
                if(auth_box_input.val().length != 0){
                    auth_box_input.closest('.ts-input').addClass('active');
                }
                else{
                    auth_box_input.closest('.ts-input').removeClass('active');
                }
            });

//             $("#__auth_button").on("click", function (e) {
            $("#" + __global_data_scope.booking_btn_id).on("click", function (e) {

                var password = $("#" + __global_data_scope.authorize_box_id + " input[name='password']").val(), $this = $(this);

                if (password.toString() === "") {
                    __render_error_box(__global_data_scope.messages.enter_the_password);
                    return;
                }

                __send_request({
                    beforeSend: function () {
                        $('#form-shading').show();
//                        $this.html("<img src='" + __global_data_scope.preloader + "' alt='gif-preloader'>");
                    },
                    complete: function () {
                        $('#form-shading').hide();
//                        $this.html(__global_data_scope.messages.booking_btn);
                    },
                    email: __global_data_scope.$email_input.val(),
                    password: password,
                    action: "do_authorize"
                });

                e.preventDefault();
            });

        }

    }

    /** удаление блока атовризации **/
    function __delete_authorize_box() {

//        $("#__auth_button").off("click");
        $("#" + __global_data_scope.booking_btn_id).off("click");
        $("#" + __global_data_scope.authorize_box_id).remove();

    }

    /**
     * @param {Boolean} full
     * @returns {undefined}
     */
    function __destroy(full) {

        if (full) {

            __global_data_scope.$email_input.off("focusout");
            __delete_error_box();
            __delete_registration_box();
            __delete_authorize_box();

        } else {

            __delete_error_box();
            __delete_message_box();
            __delete_registration_box();
            __delete_authorize_box();

        }

    }

    /**
     * отправка ajax запроса
     * @param {Object} scope
     * @returns {undefined}
     */
    function __send_request(scope) {

        var request_data = {
            sessid: __global_data_scope.sessid,
            action: scope.action
        };

        if (scope.email) {
            request_data.email = scope.email;
        }

        if (scope.confirm_email) {
            request_data.confirm_email = scope.confirm_email;
        }

        if (scope.password) {
            request_data.password = scope.password;
        }

        if (typeof scope.position === "number") {
            request_data.position = scope.position;
        }
        
        if (typeof scope.promo === "string") {
            request_data.promo = scope.promo;
        }

        $.ajax({
            url: __ajax_url,
            data: request_data,
            method: "POST",
            dataType: "Json",
            complete: function () {
                __global_data_scope.ajax_flag = false;
                if (typeof scope.complete === "function") {
                    scope.complete();
                }
            },
            beforeSend: function (xhr) {

                if (__global_data_scope.ajax_flag) {
                    xhr.abort();
                    return false;
                }

                __global_data_scope.ajax_flag = false;

                if (typeof scope.beforeSend === "function") {
                    scope.beforeSend();
                }

            },
            statusCode: {
                404: function () {
                    alert(__global_data_scope.messages._404);
                }
            },
            success: function (data) {

                data = JSON.parse(data);
                console.log(data);

                if (typeof data.status !== "undefined") {

                    if (data.status === 0) {

                        // крестик вместо preloader
                        scope.that.html("&times");

                        // вывод ошибки
                        alert(__global_data_scope.messages.status_0);
                        return true;

                    }

                    if (data.status === 1) {

                        // удаление обработчика удаленеия элемента корзины
                        scope.that.off("click");

                        // уменьшаем количество активных услуг на 1
                        __global_data_scope.active_service_count--;

                        // уменьшаем количество всех услуг на 1
                        __global_data_scope.cart_items_count--;

                        if (__global_data_scope.active_service_count <= 0) {
                            // удаляем форму для бронироваия, так как нет активных услуг
                            $("#" + __global_data_scope.booking_form_id).remove();
                        }

                        if (__global_data_scope.cart_items_count <= 0) {
                            // показываем сообщение о том, что карзина пуста
                            scope.that.closest("." + __global_data_scope.cart_item_class_container).replaceWith(__global_data_scope.empty_cart_message);
                        } else {
                            // удаляем позицию
                            scope.that.closest("." + __global_data_scope.cart_item_class_container).remove();
                            // отображаем текущее состояние цен
                            $(__global_data_scope.discountAreaSelector).text(data.discount);
                            $(__global_data_scope.costAreaSelector).text(data.cost);
                            $(__global_data_scope.totalAreaSelector).text(data.total);
                            
                            if (data.count_of_people <= __global_data_scope.totalCountOfPeople) {
                                $('#add-tourist-btn').remove();
                            }
                        }

                        return true;

                    }

                    if (data.status === 4 || data.status === 5 || data.status === 7) {

                        // вывод ошибки
                        __render_error_box(__global_data_scope.messages["status_" + data.status] || data.message);
                        return true;

                    }

                    if (data.status === 6 || data.status === 3) {

                        // вывод сообщения
                        __render_message_box(__global_data_scope.messages["status_" + data.status] || data.message);

                        __global_data_scope.is_authorized = true;

                        __destroy(true);

                        $("#" + __global_data_scope.booking_btn_id).trigger("click");

                        if (data.status === 3 && data.is_agent) {

                            $("#accept").parent().remove();

                        }

                        return true;

                    }

                    if (data.status === 8) {

                        // инициализируем блок регистрации
                        __init_registration_box();

                        // вывод сообщения
                        __render_message_box(__global_data_scope.messages.status_8);

                        return true;

                    }

                    if (data.status === 9) {

                        // инициализируем блок с авторизацией
                        __init_authorize_box();

                        // вывод сообщения
                        __render_message_box(__global_data_scope.messages.status_9);

                        return true;
                    }
                    
                    // обработка результата ввода промокода
                    if ([10, 11, 12, 13, 14, 15, 16, 17, 18].indexOf(data.status) !== -1) {
                        if (data.status === 17) {
                            // отображаем текущее состояние цен
                            $(__global_data_scope.discountAreaSelector).text(data.discount);
                            $(__global_data_scope.costAreaSelector).text(data.cost);
                            $(__global_data_scope.totalAreaSelector).text(data.total);
                        }
                        alert(__global_data_scope.messages["status_" + data.status]);
                        
                        return true;
                    }

                }

                alert(__global_data_scope.messages.default_error_text);

            },
            error: function () {
                alert(__global_data_scope.messages._500);
            }
        });

    }

    /**
     * @returns {undefined}
     */
    function __init_ajax_user_dialog() {

        __global_data_scope.$email_input.on("focusout", function () {

            var email = __global_data_scope.$email_input.val();

            __destroy();

            if (email === "") {

                __render_error_box(__global_data_scope.messages.empty_email);
                return false;

            }

            if (!__email_validation(email)) {
                // отрисовываем блок с ошибками
                __render_error_box(__global_data_scope.messages.status_2);

                return false;

            }

            // запрос на проверку пользователя
            __send_request({
                email: email,
                action: "check_auth"
            });

        });

    }

    /**
     * 
     * @param {String} html_id
     * @param {Number} position
     * @returns {undefined}
     */
    function __set_del_position(html_id, position) {

        $("#" + html_id).on("click", function () {

            var $this = $(this);

            __send_request({
                action: "delete_pos",
                position: position,
                that: $this,
                beforeSend: function () {
                    $this.html("<img src='" + __global_data_scope.preloader + "' alt='gif-preloader'>");
                }

            });

        });

    }

    function  __init_ajax_delete_cart_position() {

        var i, cnt = __global_data_scope.del_pos;

        for (i in __global_data_scope.del_pos) {
            __set_del_position(__global_data_scope.del_pos[i].html_id, __global_data_scope.del_pos[i].position);
        }

    }

    $.make_booking_component_dialog = function (options) {

        if (typeof options.sessid === "string" && options.sessid !== "") {
            __global_data_scope.sessid = options.sessid;
        }

        if (typeof options.forgot_password_link === "string" || options.forgot_password_link !== "") {
            __global_data_scope.forgot_password_link = options.forgot_password_link;
        }

        if (typeof options.is_authorized === "boolean") {
            __global_data_scope.is_authorized = options.is_authorized;
        }

        if (typeof options.messages === "object") {

            if (typeof options.messages.booking_btn === "string") {
                __global_data_scope.messages.booking_btn = options.messages.booking_btn;
            }

            if (typeof options.messages.confirm_email_error === "string") {
                __global_data_scope.messages.confirm_email_error = options.messages.confirm_email_error;
            }

            if (typeof options.messages.confirm_email_not_equal_email === "string") {
                __global_data_scope.messages.confirm_email_not_equal_email = options.messages.confirm_email_not_equal_email;
            }

            if (typeof options.messages.placeholder_confirm_email === "string") {
                __global_data_scope.messages.placeholder_confirm_email = options.messages.placeholder_confirm_email;
            }

            if (typeof options.messages.placeholder_password === "string") {
                __global_data_scope.messages.placeholder_password = options.messages.placeholder_password;
            }

            if (typeof options.messages.do_registration_button === "string") {
                __global_data_scope.messages.do_registration_button = options.messages.do_registration_button;
            }

            if (typeof options.messages.do_authorize_button === "string") {
                __global_data_scope.messages.do_authorize_button = options.messages.do_authorize_button;
            }

            if (typeof options.messages.enter_the_password === "string") {
                __global_data_scope.messages.enter_the_password = options.messages.enter_the_password;
            }

            if (typeof options.messages._404 === "string") {
                __global_data_scope.messages._404 = options.messages._404;
            }

            if (typeof options.messages._500 === "string") {
                __global_data_scope.messages._500 = options.messages._500;
            }

            if (typeof options.messages.default_error_text === "string") {
                __global_data_scope.messages.default_error_text = options.messages.default_error_text;
            }

            if (typeof options.messages.status_0 === "string") {
                __global_data_scope.messages.status_0 = options.messages.status_0;
            }

            if (typeof options.messages.status_1 === "string") {
                __global_data_scope.messages.status_1 = options.messages.status_1;
            }

            if (typeof options.messages.status_2 === "string") {
                __global_data_scope.messages.status_2 = options.messages.status_2;
            }

            if (typeof options.messages.status_3 === "string") {
                __global_data_scope.messages.status_3 = options.messages.status_3;
            }

            if (typeof options.messages.status_4 === "string") {
                __global_data_scope.messages.status_4 = options.messages.status_4;
            }

            if (typeof options.messages.status_5 === "string") {
                __global_data_scope.messages.status_5 = options.messages.status_5;
            }

            if (typeof options.messages.status_6 === "string") {
                __global_data_scope.messages.status_6 = options.messages.status_6;
            }

            if (typeof options.messages.status_7 === "string") {
                __global_data_scope.messages.status_7 = options.messages.status_7;
            }

            if (typeof options.messages.status_8 === "string") {
                __global_data_scope.messages.status_8 = options.messages.status_8;
            }

            if (typeof options.messages.status_9 === "string") {
                __global_data_scope.messages.status_9 = options.messages.status_9;
            }

            if (typeof options.messages.empty_email === "string") {
                __global_data_scope.messages.empty_email = options.messages.empty_email;
            }

            if (typeof options.preloader === "string" && options.preloader !== "") {
                __global_data_scope.preloader = options.preloader;
            }

        }

        if (typeof options.booking_btn_id === "string") {
            __global_data_scope.booking_btn_id = options.booking_btn_id;
        }

        if (typeof options.email_input_id === "string") {
            __global_data_scope.$email_input = $("#" + options.email_input_id);
        }

        if (typeof options.mount_point_id === "string") {
            __global_data_scope.$mount_point = $("#" + options.mount_point_id);
        }

        if (!__global_data_scope.is_authorized && typeof options.email_input_id === "string") {
            __global_data_scope.$email_input = $("#" + options.email_input_id);
            if (__global_data_scope.$email_input.length) {
                __init_ajax_user_dialog();
            }
        }

        if (typeof options.empty_cart_message === "string") {
            __global_data_scope.empty_cart_message = options.empty_cart_message;
        }

        if (typeof options.cart_items_count === "number") {
            __global_data_scope.cart_items_count = options.cart_items_count;
        }

        if (typeof options.active_service_count === "number") {
            __global_data_scope.active_service_count = options.active_service_count;
        }

        if (typeof options.cart_item_class_container === "string") {
            __global_data_scope.cart_item_class_container = options.cart_item_class_container;
        }

        if (typeof options.booking_form_id === "string") {

            __global_data_scope.booking_form_id = options.booking_form_id;
            $("#" + __global_data_scope.booking_form_id).on("submit", function () {
                if (typeof options.sendBeforeForm === "function") {
                    return options.sendBeforeForm(__global_data_scope);
                }
            });
        }

        if (typeof options.del_pos === "object") {

            __global_data_scope.del_pos = options.del_pos;

            __init_ajax_delete_cart_position();

        }
        
        if (typeof options.additional_tourists === 'object') {
            
            var opt = options.additional_tourists,
                    count = opt.count,
                    index = opt.index;
            
            __global_data_scope.totalCountOfPeople = opt.count;
            
            $(document).on('click', '#add-tourist-btn', function (e) {
                
                var area = null, lastRow = null, template = opt.template;
                
                if (count > 0) {
                    
                    lastRow = $('.tourist-row:last');
                    
                    area = lastRow.find('.additional-tourist-area');
                    
                    template = template.replace('#number#', index + 1);
                    
                    while (template.indexOf('#index#') !== -1) {
                        
                        template = template.replace('#index#', index);
                    }
                    
                    
                    if (area.length) {
                        
                        area.replaceWith(template);
                    } else if (lastRow.length) {
                        
                        lastRow.after('<div class="row form bg-none tourist-row">' + template + '<span class="additional-tourist-area"></span></div>');
                    }
                    
                    index++;
                    count--;
                    
                    if (count === 0) {
                        
                        $('#add-tourist-btn').remove();
                    }
                    
                    $(".birthdate-field").mask("99.99.9999");
                }
                
                e.preventDefault();
            });
        }
        
        if (typeof options.promoSelectorBtn === 'string') {
            
            if (typeof options.messages.status_10 === "string") {
                __global_data_scope.messages.status_10 = options.messages.status_10;
            }
            
            if (typeof options.messages.status_11 === "string") {
                __global_data_scope.messages.status_11 = options.messages.status_11;
            }
            
            if (typeof options.messages.status_12 === "string") {
                __global_data_scope.messages.status_12 = options.messages.status_12;
            }
            
            if (typeof options.messages.status_13 === "string") {
                __global_data_scope.messages.status_13 = options.messages.status_13;
            }
            
            if (typeof options.messages.status_14 === "string") {
                __global_data_scope.messages.status_14 = options.messages.status_14;
            }
            
            if (typeof options.messages.status_15 === "string") {
                __global_data_scope.messages.status_15 = options.messages.status_15;
            }
            
            if (typeof options.messages.status_16 === "string") {
                __global_data_scope.messages.status_16 = options.messages.status_16;
            }
            
            if (typeof options.messages.status_17 === "string") {
                __global_data_scope.messages.status_17 = options.messages.status_17;
            }
            
            if (typeof options.messages.status_18 === "string") {
                __global_data_scope.messages.status_18 = options.messages.status_18;
            }
            
            if (typeof options.totalAreaSelector === 'string') {
                 __global_data_scope.totalAreaSelector = options.totalAreaSelector;
            }
            
            if (typeof options.costAreaSelector === 'string') {
                 __global_data_scope.costAreaSelector = options.costAreaSelector;
            }
            
            if (typeof options.discountAreaSelector === 'string') {
                 __global_data_scope.discountAreaSelector = options.discountAreaSelector;
            }
            
            $(document).on('click', options.promoSelectorBtn,  function (e) {
                
                var $input = $($(this).data('input-link'));
                var promo = $input.val().toString();
                
                if (promo !== '') {
                    __send_request({
                        action: 'apply_promo',
                        promo: promo,
                        complete: function () {
                            $input.val('');
                        }
                    });
                } else {
                    alert(__global_data_scope.messages.status_10);
                }
                
                e.preventDefault();
            });
        }
    };

})(jQuery, JSON);