<?php

namespace travelsoft\sts\_interfaces;

/**
 * Абстрактный класс для операторов
 *
 * @author juliya.sharlova
 * @copyright (c) 2017, travelsoft
 */
abstract class Operator
{
    /**
     * Переменная, которая содержит префикс кода поля связи с таблицей оператора
     * @var string
     */
    public static $fieldCodeName = 'UF_OP_';

    abstract public function __construct(array $request);

    abstract public function sendRequest();

    abstract public function getResult(): array;

    /*protected function _getBxId(array $data): int {

        return (int)$data["UF_BX_ID"];

    }*/

}