<?php

namespace travelsoft\sts\pages;

/*use travelsoft\sts\Settings;
use travelsoft\sts\pages\Settings;
use travelsoft\sts\stores\RelationDirections;*/

/*
 * Функционал для страниц crm
 */

class Utils {

    /**
     * Определяет права доступа к странице настроек поиска
     * @global type $USER
     * @return bool
     */
    /*public static function access(): bool {

        global $USER;

        $access = false;
        if ($USER->IsAdmin()) {

            $access = true;
        } else {

            $allowGroups = array(
                Settings::managersUGroup(),
                Settings::cashersUGroup()
            );
            $arUserGroups = $USER->GetUserGroupArray();

            foreach ($arUserGroups as $groupId) {

                if (in_array($groupId, $allowGroups)) {
                    $access = true;
                    break;
                }
            }
        }

        return $access;
    }*/

    /**
     * HTML поля редактирования
     * @param string $label
     * @param string $field
     * @param bool $required
     * @param bool $hide
     * @return string
     */
    public static function getEditFieldHtml(string $label, string $field, bool $required = false, bool $hide = false): string {

        if ($required) {
            $label .= '<span class="required">*</span>';
        }

        $content = '<tr ' . ($hide ? 'style="display:none"' : "") . '>';
        $content .= '<td width="40%">' . $label . '</td>';
        $content .= '<td width="60%">' . $field . '</td>';
        $content .= '</tr>';

        return $content;
    }

    /**
     * Является ли запрос запросом от формы редактирования
     * @return bool
     */
    public static function isEditFormRequest(): bool {

        return $_SERVER['REQUEST_METHOD'] === 'POST' && check_bitrix_sessid() && ($_POST['SAVE'] || $_POST['APPLY']);
    }

}
