<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="hotel-list-content">
    <?if(isset($arResult["ITEMS"]) && !empty($arResult["ITEMS"])):?>
        <? foreach ($arResult["ITEMS"] as $arItem): ?>
            <?$name = !empty($arItem["hotel"]["name"]) ? $arItem["hotel"]["name"] : $arItem["hotel"]["originalName"];
            $detail_page_url = !empty($arItem["hotel"]["link"]) ? 'href="'.$arItem["hotel"]["link"].'"' : '';
            $hotel_id = md5($arItem["operator_id"]."_".$arItem["hotel"]["id"]);
            //$hotel_id = md5($arItem["hotel"]["id_bx"]);?>

            <div class="hotel-item news-item">
                <div class="hotel-info-big">
                    <div class="hotel-image">
                        <a <?=$detail_page_url?>>
                            <div class="img">
                                <img class="img-responsive" src="<?= $arItem["hotel"]["image"] ?>"
                                     alt="<?= $name ?>"/>
                            </div>
                        </a>
                    </div>
                    <div class="hotel-body">
                        <?if(!empty($arItem["hotel"]["star"])):?>
                            <div class="ratting">
                                <?$star = (int)$arItem["hotel"]["star"]?>
                                <?if(!empty($star) && !is_null($star)):?>
                                    <?for($i = 1; $i <= $star; $i++):?>
                                        <i class="fa fa-star"></i>
                                    <? endfor; ?>
                                    <?if($star < 5):?>
                                        <? for ($i = $star+1; $i <= 5; $i++): ?>
                                            <i class="fa fa-star-o"></i>
                                        <? endfor; ?>
                                    <?endif?>
                                <?else:?>
                                    <?=$arItem["hotel"]["star"]?>
                                <?endif?>
                            </div>
                        <?endif?>
                        <a <?=$detail_page_url?>><b><?= $name ?></b></a><br/>
                        <? if (!empty($arItem["country"]["name"])): ?>
                            <?$country_link = !empty($arItem["country"]["link"]) ? 'href="'.$arItem["country"]["link"].'"' : '';?>
                            <a <?=$country_link?>><?= $arItem["country"]["name"] ?></a>
                            <? if (!empty($arItem["city"]["name"]) || !empty($arItem["city"]["originalName"])): ?>
                                <?$city_link = !empty($arItem["city"]["link"]) ? 'href="'.$arItem["city"]["link"].'"' : '';?>
                                <?$city_name = !empty($arItem["city"]["name"]) ? !empty($arItem["city"]["name"]) : $arItem["city"]["originalName"];?>
                                /
                                <a <?= $city_link?>><?= $city_name ?></a>
                            <? endif ?>
                            <br>
                        <? endif; ?>
                        <ul class="list-info-travel">

                            <!--отправление-->
                            <?$departure = array($arItem["cityFrom"]["name"],$arItem["tourDate"]["dateFrom"],num2word($arItem["night"], array("ночь","ночи","ночей")));?>
                            <li><span class="icon"><i class="fa fa-map-signs"></i></span><?=imp(", ",$departure,"Отправление",array($arItem["tourDate"]["dateFrom"],$arItem["cityFrom"]["name"]));?></li>

                            <!--размещение-->
                            <?$placement = !empty($arItem["placement"]["name"]) ? $arItem["placement"]["name"] : $arItem["placement"]["originalName"];?>
                            <?$roomType = !empty($arItem["roomType"]["name"]) ? $arItem["roomType"]["name"] : $arItem["roomType"]["originalName"];?>
                            <?$roomCatType = !empty($arItem["roomCatType"]["name"]) ? $arItem["roomCatType"]["name"] : $arItem["roomCatType"]["originalName"];?>
                            <li><span class="icon"><i class="fa fa-home"></i></span><?=imp(", ",array($placement,$roomType,$roomCatType),"Размещение");?></li>

                            <!--питание-->
                            <?$food = !empty($arItem["food"]["name"]) ? $arItem["food"]["name"] : $arItem["food"]["originalName"];?>
                            <li><span class="icon"><i class="fa fa-cutlery"></i></span><?=imp(", ",array($food),"Питание");?></li>

                        </ul>
                        <? if (!empty($arItem["hotel"]["services"])): ?>
                            <div class="free-service">

                                <? foreach ($arItem["hotel"]["services"] as $k => $item): ?>
                                    <? if (!empty($arResult["SERVICES"][$item]["PICTURES"])): ?>
                                        <div class="img-or-flaticon">
                                            <img src="<?= $arResult["SERVICES"][$item]["PICTURES"] ?>" data-toggle="tooltip"
                                                 data-placement="top" title="<?= $arResult["SERVICES"][$item]["DESC"] ?>"
                                                 data-original-title="<?= $arResult["SERVICES"][$item]["NAME"] ?>">
                                        </div>
                                    <? elseif (!empty($arResult["SERVICES"][$item]["FLATICON"])): ?>
                                        <i class="flaticon-<?= $arResult["SERVICES"][$item]["FLATICON"] ?>"
                                           data-toggle="tooltip" data-placement="top"
                                           title="<?= $arResult["SERVICES"][$item]["DESC"] ?>"
                                           data-original-title="<?= $arResult["SERVICES"][$item]["NAME"] ?>"></i>
                                    <? endif; ?>
                                <? endforeach; ?>

                            </div>
                        <? endif; ?>

                    </div>
                    <div class="hotel-right">
                        <?if(!empty($arItem["prices"]) && !empty($arItem["defCurrency"])):?>
                            <div class="hotel-person">
                                от
                                <span class="color-blue">
                                    <?= travelsoft\currency\Converter::getInstance()->convert($arItem["prices"], $arItem["defCurrency"])->getResult() ?>
                                </span>
                                <? if (!empty($arItem["price_for"])): ?>
                                    <?= $arItem["price_for"] ?>
                                <? endif; ?>
                            </div>
                        <?endif?>
                        <?
                        $requestHotel = !empty($arItem["hotel"]["id_bx"]) ? $arItem["hotel"]["id_bx"] : $arItem["hotel"]["id"];?>
                        <a class="thm-btn detail" onclick="app.getItemsDetail({hotels: [<?=(int)$requestHotel?>]<?if(empty($arItem["hotel"]["id_bx"])):?>, operators: [<?=(int)$arItem["operator_id"]?>]<?endif?>},'hotel_<?=$hotel_id?>')">Показать варианты</a>
                        <a class="thm-btn dis" onclick="app.displayBtn('hotel_<?=$hotel_id?>')">Скрыть варианты</a>
                    </div>
                </div>
                <div id="hotel_<?=$hotel_id?>"></div>
                <div class="more-btn"></div>
            </div>

        <?endforeach;?>
    <?else:?>
        <p>Поиск не дал результатов. Попробуйте изменить параметры запроса.</p>
    <?endif;?>
</div>