
    <div class="hotel-item ts-wrap ts-mb-2">

        <div class="ts-row tour" >
            <div class="ts-col-24 ts-col-md-7 ts-py-2 ts-position__relative">
                <div class="stars">#stars#</div>
                <a href="#detail_page_url#"  target="_blank" class="img_link">
                   <img src="#image#" alt="#name#" class="">
                </a>


            </div>
            <div class="ts-col-24 ts-col-md-17 ts-col-xl-13 ts-py-2 ts-justify-content-lg__space-between title hotel-middle cell-view ">

                <!-- Заголовок -->
                <div class="tour__header">
                    <h2 class="ts-my-0 ts-py-0"><a href="#detail_page_url#" target="_blank" ><b>#name#</b></a></h2>
                    <span class="f-14 color-dark-2 grid-hidden">#country# #city#</span>
                </div>
                <!-- Текстовая часть -->

                <p class="f-14 color-grey-3 ts-py-1 ts-my-0">#previewText#</p>





                <!-- Информационная часть -->
                <div class="tour_info">
                    <!--<img src="<?=SITE_TEMPLATE_PATH?>/img/calendar_icon_grey.png" alt="date"> -->
                    <ul class="ts-d-lg-flex ts-px-0 ts-mx-0 ts-py-0 ts-my-0">
                        <li>Выезд: <span class="font-style-2 color-grey-3"><strong>#dateFrom#</strong></span>  </li>
                        <li class="ts-px-xl-1"> <span class="font-style-2 color-grey-3"><?=GetMessage('DEPARTURE_OF')?> #departure#</span></li>
                        <li> <?=GetMessage('FROM')?> #night#</li>
                    </ul>

                </div>




                <!-- <div class="tour-info fl">
                    <img src="<?=SITE_TEMPLATE_PATH?>/img/loc_icon_small_grey.png" alt="">
                    <span class="font-style-2 color-grey-3">
                        <?=GetMessage('DEPARTURE_OF')?> #departure#
                    </span>
                </div> -->
            </div>
            <!-- Прайс -->

            <div class="ts-col-24 ts-col-xl-4 ts-align-items-xl__flex-start ts-justify-content__flex-end ts-py-2 ">
                <ul class="ts-px-0 ts-mx-0 ts-py-0 ts-my-0 ts-d-flex ts-d-xl-block ts-justify-content__center">
                    <li><?=GetMessage('FROM')?>: </li>
                    <li class="ts-px-1 ts-px-xl-0"><span class="tour-price__main">#prices# #defCurrency# </span></li>
                    <li><span class="tour-price__second">#price#</span></li>
                    <li class="ts-px-1 ts-px-xl-0">#price_for# </li>
                </ul>
                <div class="buttons-block ts-pt-2 ts-width-100">
                    #about_hotel#
                    <a class=" more_button c-button b-40 bg-white hv-transparent fr js-hotel-options-link thm-btn detail" onclick="app.getItemsDetail(#requestHotel#,'hotel_#md5HotelId#')"><?=GetMessage('SHOW_OFFERS')?></a>
                    <a class=" more_button c-button b-40 bg-grey-3-t hv-grey-3-t b-1 js-hotel-options-link thm-btn dis" onclick="app.displayBtn('hotel_#md5HotelId#')"><?=GetMessage('HIDE_OFFERS')?></a>
                </div>
            </div>
        </div>
        <div class="catalog-tour__list " id="hotel_#md5HotelId#"></div>
        <div class="more-btn">еще варианты</div>
    </div>