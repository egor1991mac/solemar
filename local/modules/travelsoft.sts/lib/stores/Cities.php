<?php

namespace travelsoft\sts\stores;

use travelsoft\sts\adapters\Iblock;

/**
 * Класс для работы с инфоблоком городов
 *
 * @author juliya.sharlova
 * @copyright (c) 2017, travelsoft
 */
class Cities extends Iblock
{
    protected static $storeName = 'cities';

    public function getElementCode(int $id): string {

        $arData = self::get(array("filter" => array("ID" => $id, "ACTIVE" => ""),"select" => array("ID", "CODE")));
        return (string) $arData[$id]["CODE"];

    }

}