<?
$MESS["TSCOASTLINE_TITLE"] = "Пляжная линия";
$MESS["TSSTARS_TITLE"] = "Категория отеля";
$MESS["TSFOOD_TITLE"] = "Питание";
$MESS["TSTOURTYPES_TITLE"] = "Тип тура";
$MESS["TSCOASTLINE"] = "hotels";
$MESS["TSSTARS"] = "stars";
$MESS["TSFOOD"] = "meals";
$MESS["TSTOURTYPES"] = "type_tours";

$MESS["coastline"] = "coastline";
$MESS["stars"] = "stars";
$MESS["food"] = "meals";
$MESS["type_tours"] = "tourTypes";
$MESS["tourTypes"] = "type_tours";

$MESS["coastline_TITLE"] = "Пляжная линия";
$MESS["stars_TITLE"] = "Категория отеля";
$MESS["food_TITLE"] = "Питание";
$MESS["type_tours_TITLE"] = "Тип Тура";
$MESS["services_TITLE"] = "Услуги";

$MESS["FILTER_TITLE"] = "Фильтр";
$MESS["is_selected"] = "выбрано";
$MESS["CT_BCSF_SET_FILTER"] = "Применить";
$MESS["CT_BCSF_DEL_FILTER"] = "Сбросить";

$MESS["PRICE_TITLE"] = "Цена";
$MESS["PRICE_FROM"] = "от";
$MESS["PRICE_TO"] = "до";
?>