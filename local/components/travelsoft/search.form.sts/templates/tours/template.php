<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$this->addExternalCss("/local/modules/travelsoft.sts/plugins/css/icomoon_style.min.css");
$this->addExternalJs("/local/modules/travelsoft.sts/plugins/js/moment.min.js");
$this->addExternalJs("/local/modules/travelsoft.sts/plugins/js/moment_locales.min.js");
$this->addExternalJs("/local/modules/travelsoft.sts/plugins/js/daterangepicker.js");
$this->addExternalCss("/local/modules/travelsoft.sts/plugins/css/DateTimePicker.min.css");
$this->addExternalCss("/local/modules/travelsoft.sts/plugins/css/select2.min.css");
$this->addExternalJs("/local/modules/travelsoft.sts/plugins/js/select2.min.js", true);
?>


<div class="sidebar-block">
    <h4 class="sidebar-title color-dark-2 form-title"><?=GetMessage('TITLE_FORM')?></h4>

    <form id="stsSearchForm" method='GET' action="<?= $arParams['ACTION_URL']?>" autocomplete="off" class="form">

        <div class="search-inputs">

            <?if(!empty($arResult["REQUEST"]["stars"])):?>
                <?foreach ($arResult["REQUEST"]["stars"] as $star):?>
                    <input type="hidden" name="stsSearch[stars][]" value="<?=$star?>">
                <?endforeach;?>
            <?endif?>
            <?if(!empty($arResult["REQUEST"]["meals"])):?>
                <?foreach ($arResult["REQUEST"]["meals"] as $meal):?>
                    <input type="hidden" name="stsSearch[meals][]" value="<?=$meal?>">
                <?endforeach;?>
            <?endif?>

            <div class="form-block clearfix">
                <h4 class="sidebar-title color-dark-2"><?=GetMessage('CITYFROM')?></h4>
                <div class="input-style-1 b-50 color-3">
                    <img src="<?=SITE_TEMPLATE_PATH?>/img/loc_icon_small_grey.png" alt="">
                    <select class="form__select js-select2" id="cityFrom" name="stsSearch[cityFrom]" placeholder="<?=GetMessage('CITYFROM')?>">
                        <?if(!empty($arResult["DIRECTIONS"])):?>

                            <?foreach ($arResult["DIRECTIONS"] as $city):?>
                                <option value="<?=$city["id"]?>" <?if($arResult["REQUEST"]["cityFrom"] == $city["id"]):?>selected<?endif?>><?=$city["name"]?></option>
                            <?endforeach;?>

                        <?else:?>

                            <?foreach ($arResult["CITIES"] as $city):?>
                                <option value="<?=$city["id"]?>" <?if($arResult["REQUEST"]["cityFrom"] == $city["id"]):?>selected<?endif?>><?=$city["name"]?></option>
                            <?endforeach;?>

                        <?endif?>
                    </select>
                </div>
            </div>

            <div class="form-block clearfix">
                <h4 class="sidebar-title color-dark-2"><?=GetMessage('COUNTRYTO')?></h4>
                <div class="input-style-1 b-50 color-3">
                    <img src="<?=SITE_TEMPLATE_PATH?>/img/loc_icon_small_grey.png" alt="">
                    <select class="form__select js-select2" name="stsSearch[country]" id="country" placeholder="<?=GetMessage('COUNTRYTO')?>">
                        <?if(!empty($arResult["DIRECTIONS"])):?>
                            <?foreach ($arResult["DIRECTIONS"][$arResult["REQUEST"]["cityFrom"]]["items"] as $country):?>
                                <option value="<?=$country["id"]?>" <?if($arResult["REQUEST"]["country"] == $country["id"]):?>selected<?endif?>><?=$country["name"]?></option>
                            <?endforeach;?>
                        <?else:?>
                            <?foreach ($arResult["COUNTRIES"] as $country):?>
                                <option value="<?=$country["id"]?>" <?if($arResult["REQUEST"]["country"] == $country["id"]):?>selected<?endif?>><?=$country["name"]?></option>
                            <?endforeach;?>
                        <?endif?>
                    </select>
                </div>
            </div>

            <div class="form-block clearfix">
                <h4 class="sidebar-title color-dark-2"><?=GetMessage('RESORT')?></h4>
                <div class="input-style-1 b-50 color-3">
                    <img src="<?=SITE_TEMPLATE_PATH?>/img/loc_icon_small_grey.png" alt="">
                    <select class="form__select js-select2" id="cities" name="stsSearch[cities]" placeholder="<?=GetMessage('RESORT')?>">
                        <option value="0" <? if ($arResult["REQUEST"]["cities"] == 0): ?>selected<? endif ?>>Все
                        </option>
                        <?foreach ($arResult["RESORT"][$arResult["REQUEST"]["country"]] as $resort):?>
                            <option value="<?=$resort["id"]?>" <?if(in_array($resort["id"],$arResult["REQUEST"]["cities"])):?>selected<?endif?>><?=$resort["name"]?></option>
                        <?endforeach;?>
                    </select>
                </div>
            </div>
            <div class="form-block clearfix">
                <h4 class="sidebar-title color-dark-2"><?=GetMessage('TYPETOUR')?></h4>
                <div class="input-style-1 b-50 color-3">
                    <img src="<?=SITE_TEMPLATE_PATH?>/img/loc_icon_small_grey.png" alt="">
                    <select class="form__select js-select2" name="stsSearch[tourTypes]" id="tourTypes" placeholder="<?=GetMessage('TYPETOUR')?>">
                        <option value="0" <? if ($arResult["REQUEST"]["tourTypes"] == 0): ?>selected<? endif ?>>Все
                        </option>
                        <? foreach ($arResult['TOURTYPES'] as $tourType): ?>
                            <option value="<?= $tourType['id'] ?>"
                                    <? if (in_array($tourType['id'], $arResult['REQUEST']['tourTypes'])): ?>selected<? endif; ?>>
                                <?= $tourType['name'] ?>
                            </option>
                        <? endforeach; ?>
                    </select>
                </div>
            </div>

            <div class="form-block clearfix">
                <h4 class="sidebar-title color-dark-2"><?= GetMessage('DATE_FROM')?></h4>
                <div class="input-style-1 b-50 color-3">
                    <img class="date_pic" src="<?=SITE_TEMPLATE_PATH?>/img/calendar_icon_grey.png">
                    <input autocomplete="off" type="text" data-date-format="dd.mm.yyyy" name="stsSearch[dateFrom]" value="<?= $arResult["REQUEST"]["dateFrom"]?>" placeholder="<?= GetMessage('DATE_FROM')?>" class="datepicker" id="dateFrom" required>
                </div>
            </div>

            <div class="form-block clearfix">
                <h4 class="sidebar-title color-dark-2"><?= GetMessage('DATE_TO')?></h4>
                <div class="input-style-1 b-50 color-3">
                    <img class="date_pic" src="<?=SITE_TEMPLATE_PATH?>/img/calendar_icon_grey.png">
                    <input autocomplete="off" readonly type="text" data-date-format="dd.mm.yyyy" value="<?= $arResult["REQUEST"]["dateTo"]?>" name="stsSearch[dateTo]" placeholder="<?= GetMessage('DATE_TO')?>" class="datepicker" id="dateTo" required>
                </div>
            </div>

            <div class="form-block clearfix">
                <h4 class="sidebar-title color-dark-2"><?= GetMessage('NIGHTS')?> <?=GetMessage('NIGHTSFROM')?> / <?=GetMessage('NIGHTSTO')?></h4>
                <div class="input-style-1 b-50 color-3">
                    <div class="form__col reset">
                        <select name="stsSearch[nightFrom]" class="form__select js-select2 select-night" id="start-night" required>
                            <?for($i = 1; $i < 29; $i++):?>
                                <option value="<?=$i?>" <?if($i == $arResult["REQUEST"]["nightFrom"]):?>selected<?endif?>><?=$i?></option>
                            <?endfor;?>
                        </select>
                    </div>
                    <div class="form__col reset">
                        <select name="stsSearch[nightTo]" class="form__select js-select2 select-night" id="end-night" required>
                            <?for($i = 1; $i < 29; $i++):?>
                                <option value="<?=$i?>" <?if($i == $arResult["REQUEST"]["nightTo"]):?>selected<?endif?>><?=$i?></option>
                            <?endfor;?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-block clearfix">
                <div class="input-style-1 b-50 color-3">
                    <div class="form__col reset">
                        <h4 class="sidebar-title color-dark-2"><?= GetMessage('ADULTS')?></h4>
                        <select name="stsSearch[adults]" class="form__select js-select2" id="adults" required>
                            <?for($i = 1; $i < 7; $i++):?>
                                <option value="<?=$i?>" <?if($i == $arResult["REQUEST"]["adults"]):?>selected<?endif?>><?=$i?></option>
                            <?endfor;?>
                        </select>
                    </div>
                    <div class="form__col reset">
                        <h4 class="sidebar-title color-dark-2"><?= GetMessage('CHILDRENS')?></h4>
                        <select name="stsSearch[children]" class="form__select js-select2" id="children" required>
                            <?for($i = 0; $i < 6; $i++):?>
                                <option value="<?=$i?>" <?if($i == $arResult["REQUEST"]["children"]):?>selected<?endif?>><?=$i?></option>
                            <?endfor;?>
                        </select>
                    </div>
                </div>
            </div>


        </div>

        <input type="submit" class="form__send thm-btn c-button bg-dr-blue-2 hv-dr-blue-2-o" value="<?=GetMessage('TEXT_SUBMIT')?>">

    </form>

</div>
<script>

    /**
     * @param {jQuery} $
     * @param {Window} window
     */
    (function ($, moment, window) {

        var
            /**
             * инициализируем select2
             * @param {object} options
             */
            __initSelect2 = function (options) {

                // инициализируем select
                function select2 (obj) {
                    obj.select2({
                        allowClear: true,
                        formatNoMatches: function () {
                            return "<?= GetMessage("NOMATCHES")?>";
                        },
                        minimumResultsForSearch: -1
                    });
                }

                select2(options);
                options.select2("val", options.val());

            },

            dateFormat = "DD.MM.YYYY",
            form = $("#stsSearchForm"),
            dates = {},
            nights = {},
            html = '',
            directions = <?=\travelsoft\sts\Utils::jsonEncode($arResult["DIRECTIONS"])?>,
            get_params =  <?=\travelsoft\sts\Utils::jsonEncode($arParams['GET_PARAMS_STS'])?>;

        $('#stsSearchForm select.js-select2').each(function() {
            __initSelect2($(this));
        });

        function setDates (cityFrom, country, tourTypes) {

            if (typeof dates[cityFrom] === "object" && $.isArray(dates[cityFrom][country])){
                return;
            }

            $.ajax({
                method: "post",
                url: '<?=$componentPath?>/ajax.php',
                dataType: 'json',
                data: {cityFrom: cityFrom, country: country, tourTypes: tourTypes},
                success: function (data) {

                    dates[cityFrom] = {};
                    dates[cityFrom][country] = {};
                    nights[cityFrom] = {};
                    nights[cityFrom][country] = {};

                    if (data.error !== true && typeof data.result !== "undefined" && data.result !== null && typeof data.result.dates !== "undefined" && data.result.dates !== null && data.result.dates.length > 0) {
                        dates[cityFrom][country][tourTypes] = data.result.dates;
                        minDateTo = dates[cityFrom][country][tourTypes][0];
                        initDate();


                    } else {
                        minDateTo = form.find("input[name='stsSearch[dateFrom]']").val();
                        initDate();

                    }


                    if (data.error !== true && typeof data.result !== "undefined" && data.result !== null && typeof data.result.nights !== "undefined" && data.result.nights !== null && data.result.nights.length > 0) {
                        nights[cityFrom][country][tourTypes] = data.result.nights;
                        selectNights('nightFrom', def_night_from, 'first');
                        selectNights('nightTo', def_night_to, 'last');
                    }

                }
            });

        }

        var period_date = <?=$arResult["PERIOD_DATE"]?>;

        var region = JSON.parse('<?=\Bitrix\Main\Web\Json::encode($arResult["RESORT_"])?>');
        var type = JSON.parse('<?=\Bitrix\Main\Web\Json::encode($arResult["TOUR_TYPES_"])?>');

        var cityFrom = form.find("select[name='stsSearch[cityFrom]']").val();
        var country = form.find("select[name='stsSearch[country]']").val();
        var tourTypes = form.find("select[name='stsSearch[tourTypes]']").val();
        var dateFrom_ = form.find("input[name='stsSearch[dateFrom]']").val();
        var date_from = form.find("input[name='stsSearch[dateFrom]']");
        var dateTo_ = form.find("input[name='stsSearch[dateTo]']").val();
        var date_to = form.find("input[name='stsSearch[dateTo]']");
        var minDateTo = dateFrom_;
        var def_night_from = '<?=$arResult["REQUEST"]["nightFrom"]?>';
        var def_night_to = '<?=$arResult["REQUEST"]["nightTo"]?>';
        var startDateTour = new Date(),
            today_date = startDateTour.toLocaleDateString('ru-RU',{day:'2-digit',year:'numeric',month:'2-digit',timeZone:'Africa/Asmera'});
        var dateF = form.find("input[name='stsSearch[dateFrom]']").val().split(".");
        dateF = new Date(dateF[2],dateF[1]-1,dateF[0]);
        var maxDate = dateF;
        maxDate.setDate(maxDate.getDate() + 20);
        var dateToStart = dateF;
        dateToStart.setDate(dateToStart.getDate() + period_date);
        dateToStart = moment(dateToStart).format(dateFormat);

        setDates(cityFrom,country,tourTypes);

        function setMaxDate() {

            dateF = form.find("input[name='stsSearch[dateFrom]']").val().split(".");

            dateToStart = new Date(dateF[2],dateF[1]-1,dateF[0]);
            dateToStart.setDate(dateToStart.getDate() + period_date);
            dateToStart = moment(dateToStart).format(dateFormat);

            maxDate = new Date(dateF[2],dateF[1]-1,dateF[0]);
            maxDate.setDate(maxDate.getDate() + 20);

        }

        function initDate () {

            initDateFrom();
            setMaxDate();
            initDateTo();

            if(typeof get_params == "undefined" || get_params.length <= 0) {

                setTimeout(function () {
                    checkDate();
                }, 100);


            }
        }

        function initDateFrom () {

            $("#dateFrom").datepicker({
                showOtherMonths: !0,
                selectOtherMonths: !0,
                minDate: today_date,
                defaultDate: dateFrom_,
                firstDay: 1,
                dateFormat: "dd.mm.yy",
                dayNamesMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
                monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
                    'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'
                ],
                beforeShowDay: function (date) {

                    var date_ = date.toLocaleDateString('ru-RU',{day:'2-digit',year:'numeric',month:'2-digit',timeZone:'Africa/Asmera'});

                    if(typeof dates[cityFrom] !== "undefined" && typeof dates[cityFrom][country] !== "undefined" && typeof dates[cityFrom][country][tourTypes] !== "undefined") {

                        if(dateFrom_ >= today_date && dateFrom_ == date_) {
                            return [true, "today_active",""];
                        } else if ($.inArray(date_, dates[cityFrom][country][tourTypes]) >= 0) {
                            return [true, "active",""];
                        } else {
                            return [true, "",""];
                        }

                    } else {

                        if(dateFrom_ >= today_date && dateFrom_ == date_) {
                            return [true, "today_active",""];
                        } else {
                            return [true, "",""];
                        }

                    }

                },
                onSelect: function( date ) {

                    dateF = $("#dateFrom").val().split(".");
                    //dateF = new Date(dateF[2],dateF[1]-1,dateF[0]);

                    dateToStart = new Date(dateF[2],dateF[1]-1,dateF[0]);
                    dateToStart = dateToStart.setDate(dateToStart.getDate() + period_date);
                    minDateTo = moment(dateToStart).format(dateFormat);

                    setMaxDate();

                    $('#dateTo').datepicker('destroy');
                    initDateTo();
                    $("#dateTo").datepicker("setDate", minDateTo);

                    $("#dateFrom").datepicker("hide");
                }
            });

            // хак для нормальной работы в safari
            $('#dateFrom').on('focus', function(ev) {
                $(this).trigger('blur');
            });

        }

        function initDateTo() {

            $("#dateTo").datepicker({
                showOtherMonths: !0,
                selectOtherMonths: !0,
                minDate: $('#dateFrom').val(),
                defaultDate: dateToStart,
                firstDay: 1,
                dateFormat: "dd.mm.yy",
                dayNamesMin: ["Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс"],
                monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
                    'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'
                ],
                beforeShowDay: function (date) {

                    var date_ = date.toLocaleDateString('ru-RU',{day:'2-digit',year:'numeric',month:'2-digit',timeZone:'Africa/Asmera'});

                    if(typeof dates[cityFrom] !== "undefined" && typeof dates[cityFrom][country] !== "undefined" && typeof dates[cityFrom][country][tourTypes] !== "undefined") {

                        if($('#dateTo').val() >= today_date && $('#dateTo').val() == date_) {
                            if(date > maxDate){
                                return [false, "ui-state-disabled today_active",""];
                            } else {
                                return [true, "today_active",""];
                            }
                        } else if ($.inArray(date_, dates[cityFrom][country][tourTypes]) >= 0) {
                            if(date > maxDate){
                                return [false, "ui-state-disabled active",""];
                            } else {
                                return [true, "active",""];
                            }
                        } else if(date > maxDate){
                            return [false, "ui-state-disabled",""];
                        } else {
                            return [true, "",""];
                        }

                    } else {

                        if($('#dateTo').val() >= today_date && $('#dateTo').val() == date_) {
                            if(date > maxDate){
                                return [false, "ui-state-disabled today_active",""];
                            } else {
                                return [true, "today_active", ""];
                            }
                        } else if(date > maxDate){
                            return [false, "ui-state-disabled",""];
                        } else {
                            return [true, "",""];
                        }

                    }

                },
            });

            // хак для нормальной работы в safari
            $('#dateTo').on('focus', function(ev) {
                $(this).trigger('blur');
            });

        }

        function checkDate() {

            if (typeof dates[cityFrom] !== "undefined" && typeof dates[cityFrom][country] !== "undefined" && typeof dates[cityFrom][country][tourTypes] !== "undefined") {

                for (var j = 0; j < dates[cityFrom][country][tourTypes].length; j++) {
                    arDateF = dates[cityFrom][country][tourTypes][j];

                    arDateF = dates[cityFrom][country][tourTypes][j].split(".");
                    arDateF = new Date(arDateF[2], arDateF[1] - 1, arDateF[0]);
                    today_date_ = today_date.split(".");
                    today_date_ = new Date(today_date_[2], today_date_[1] - 1, today_date_[0]);

                    if (arDateF >= today_date_) {

                        $('#dateFrom').val(dates[cityFrom][country][tourTypes][j]);
                        $('#dateFrom').datepicker('setDate', dates[cityFrom][country][tourTypes][j]);

                        arDateT = arDateF;
                        arDateT.setDate(arDateT.getDate() + period_date);

                        setMaxDate();

                        day = arDateT.getDate() < 10 ? '0'+arDateT.getDate() : arDateT.getDate();
                        month = (arDateT.getMonth()+1) < 10 ? '0'+(arDateT.getMonth()+1) : (arDateT.getMonth()+1);

                        dateToStart = day+'.'+month+'.'+arDateT.getFullYear();
                        $('#dateTo').val(dateToStart);

                        $('#dateTo').datepicker('destroy');

                        initDateTo();

                        break;

                    }

                }

            }
        }

        function selectResort(city) {

            var resorts_for_country = region[city][country];

            var option_resorts = '<option #selected# value="#resort_id#">#resort_name#</option>';

            var htmlResorts = '<option value="0" selected>Все</option>';

            if (typeof resorts_for_country !== "undefined" && resorts_for_country != '') {

                var k = 0;
                var resort_selected = null;

                for (resort_ in resorts_for_country) {

                    option_resorts_ = option_resorts;
                    option_resorts_ = option_resorts_.replace("#resort_id#", resorts_for_country[resort_]["id"]);
                    option_resorts_ = option_resorts_.replace("#resort_name#", resorts_for_country[resort_]["name"]);
                    //option_regions_ = option_regions_.replace("#selected#", k == 0 ? 'selected' : '');
                    option_resorts_ = option_resorts_.replace("#selected#", '');
                    htmlResorts += option_resorts_;

                    /*if(k == 0) {
                     region_selected = regions_for_city[region_]["id"];
                     }

                     k++;*/

                }

            }


            form.find("select[name='stsSearch[cities]']").select2('destroy');
            form.find("select[name='stsSearch[cities]']").html(htmlResorts);
            __initSelect2(form.find("select[name='stsSearch[cities]']"));

            /*if(typeof region_selected !== "null"){
             region = region_selected;
             form.find("select[name='stsSearch[regions]']").trigger('change');
             }*/

        }

        function selectCountries(city) {

            var countries_for_city = directions[city];
            var option_countries = '<option #selected# value="#country_id#">#country_name#</option>';

            var htmlCountries = '';

            if (typeof countries_for_city["items"] !== "undefined" && countries_for_city["items"] != '') {

                var k = 0;
                var country_selected = null;

                for (country_ in countries_for_city["items"]) {

                    option_countries_ = option_countries;
                    option_countries_ = option_countries_.replace("#country_id#", countries_for_city["items"][country_]["id"]);
                    option_countries_ = option_countries_.replace("#country_name#", countries_for_city["items"][country_]["name"]);
                    option_countries_ = option_countries_.replace("#selected#", k == 0 ? 'selected' : '');
                    htmlCountries += option_countries_;

                    if(k == 0) {
                        country_selected = countries_for_city["items"][country_]["id"];
                    }

                    k++;

                }

            }

            form.find("select[name='stsSearch[country]']").select2('destroy');
            form.find("select[name='stsSearch[country]']").html(htmlCountries);
            __initSelect2(form.find("select[name='stsSearch[country]']"));

            if(typeof country_selected !== "null"){
                country = country_selected;
                form.find("select[name='stsSearch[country]']").trigger('change');
            }

        }

        function selectTourType(city, country) {

            var tourtype_for_country = type[city][country];

            var option_tourtypes = '<option #selected# value="#type_id#">#type_name#</option>';

            //var htmlTourTypes = '<option value="0" selected>Все</option>';
            var htmlTourTypes = '';

            if (typeof tourtype_for_country !== "undefined" && tourtype_for_country != '') {

                var k = 0;
                var type_selected = null;

                for (type_ in tourtype_for_country) {

                    option_tourtypes_ = option_tourtypes;
                    option_tourtypes_ = option_tourtypes_.replace("#type_id#", tourtype_for_country[type_]["id"]);
                    option_tourtypes_ = option_tourtypes_.replace("#type_name#", tourtype_for_country[type_]["name"]);
                    //option_regions_ = option_regions_.replace("#selected#", k == 0 ? 'selected' : '');
                    option_resorts_ = option_resorts_.replace("#selected#", '');
                    htmlTourTypes += option_tourtypes_;

                    /*if(k == 0) {
                     type_selected = tourtype_for_country[type_]["id"];
                     }

                     k++;*/

                }

            }


            form.find("select[name='stsSearch[tourTypes]']").select2('destroy');
            form.find("select[name='stsSearch[tourTypes]']").html(htmlTourTypes);
            __initSelect2(form.find("select[name='stsSearch[tourTypes]']"));

        }

        function selectNights(name_prop, def_param, position) {

            var select_nights = nights[cityFrom][country][tourTypes];

            var option_nights = '<option #selected# value="#night_val#">#night_name#</option>';

            var htmlNights = '';

            if (typeof select_nights !== "undefined" && select_nights != '') {

                var p = 0, k = false;
                position = position || 'first';

                if(position == 'first')
                    p = 0;
                else if(position == 'last') {

                    var valNightFrom = form.find("select[name='stsSearch[nightFrom]']").val() + 8;

                    if(valNightFrom <= (Number(select_nights.length) - 1)){
                        p = Number(select_nights.length) - 1;
                    } else if($.inArray(valNightFrom,select_nights) > -1) {
                        p = valNightFrom;
                    } else {
                        p = form.find("select[name='stsSearch[nightFrom]']").val();
                    }


                }

                for (night__ in select_nights) {

                    if(select_nights[night__] == Number(def_param))
                        k = true;

                    option_nights_ = option_nights;
                    option_nights_ = option_nights_.replace("#night_val#", select_nights[night__]);
                    option_nights_ = option_nights_.replace("#night_name#", select_nights[night__]);
                    option_nights_ = option_nights_.replace("#selected#", (select_nights[night__] == Number(def_param)) ? 'selected' : (night__ == p) && !k ? 'selected' : '');
                    htmlNights += option_nights_;

                }

            }

            form.find("select[name='stsSearch[" + name_prop + "]']").select2('destroy');
            form.find("select[name='stsSearch[" + name_prop + "]']").html(htmlNights);
            __initSelect2(form.find("select[name='stsSearch[" + name_prop + "]']"));

        }

        form.find("select[name='stsSearch[nightTo]']").on("change", function(){
            var valNightFrom = Number(form.find("select[name='stsSearch[nightFrom]']").val()) + 8;
            if(Number($(this).val()) > valNightFrom) {
                if($.inArray(valNightFrom,nights[cityFrom][country][tourTypes]) > -1){
                    form.find("select[name='stsSearch[nightTo]']").select2("val", valNightFrom);
                } else {
                    alert("Ошибка\n" +
                        "[Интервал количества ночей не может быть больше 8 ночей]");
                    form.find("select[name='stsSearch[nightTo]']").select2("val", form.find("select[name='stsSearch[nightFrom]']").val());
                    //form.find("select[name='stsSearch[nightTo]']").val(form.find("select[name='stsSearch[nightFrom]']").val()).trigger('change');
                }
            } else if(Number($(this).val()) < Number(form.find("select[name='stsSearch[nightFrom]']").val())) {
                form.find("select[name='stsSearch[nightTo]']").select2("val", form.find("select[name='stsSearch[nightFrom]']").val());
            }
        });
        form.find("select[name='stsSearch[nightFrom]']").on("change", function(){
            var valNightFrom = Number($(this).val()) + 8;
            if(Number(form.find("select[name='stsSearch[nightTo]']").val()) > valNightFrom) {
                if($.inArray(valNightFrom,nights[cityFrom][country][tourTypes]) > -1){
                    form.find("select[name='stsSearch[nightTo]']").select2("val", valNightFrom);
                } else {
                    alert("Ошибка\n" +
                        "[Интервал количества ночей не может быть больше 8 ночей]");
                    form.find("select[name='stsSearch[nightTo]']").select2("val", $(this).val());
                }
            } else if(Number(form.find("select[name='stsSearch[nightTo]']").val()) < Number($(this).val())) {
                form.find("select[name='stsSearch[nightTo]']").select2("val", $(this).val());
            }
        });

        form.find("select[name='stsSearch[cityFrom]']").on("change", function(){
            cityFrom = $(this).val();

            if(typeof directions === "object" && directions[cityFrom] !== "undefined"){

                selectCountries(cityFrom);
            }

            setDates(cityFrom,country,tourTypes);
            if(typeof get_params !== "undefined" || get_params.length > 0){
                setTimeout(function () {
                    checkDate();
                    setMaxDate();
                },200);
                form.find("#dateFrom").datepicker("refresh");
                form.find("#dateTo").datepicker("refresh");
            } else {
                form.find("#dateFrom").datepicker("refresh");
                form.find("#dateTo").datepicker("refresh");
            }
        });

        form.find("select[name='stsSearch[country]']").on("change", function(){
            country = $(this).val();

            if (region !== "undefined" && region != '') {
                selectResort(cityFrom);
            }
            if (type !== "undefined" && type != '') {
                selectTourType(cityFrom,country);
            }

            setDates(cityFrom,country,tourTypes);

            if(typeof get_params !== "undefined" || get_params.length > 0){
                form.find("#dateFrom").datepicker("refresh");
                form.find("#dateTo").datepicker("refresh");
                setTimeout(function () {
                    checkDate();
                    setMaxDate();
                },200);
            } else {
                form.find("#dateFrom").datepicker("refresh");
                form.find("#dateTo").datepicker("refresh");
            }


        });

        form.find("select[name='stsSearch[tourTypes]']").on("change", function () {
            tourTypes = $(this).val();

            setDates(cityFrom, country, tourTypes);

        });


        if($("select[name='stsSearch[children]']").val() > 0){
            //$('#stsSearchForm .age-container').show();
            for (var i = 1; i <= $("select[name='stsSearch[children]']").val(); i++) {
                $('select[name="stsSearch[age' + i + ']"]').prop('disabled', false);
            }
        }

        $("select[name='stsSearch[children]']").on("change", function () {
            //$(document).on("change", "input[name='stsSearch[children]']", function () {
            var ch = $("select[name='stsSearch[children]']").val();

            if(ch > 0) {

                $('#stsSearchForm .age-container').show();
                $('select[name="stsSearch[age1]"]').prop('disabled', true);
                $('select[name="stsSearch[age2]"]').prop('disabled', true);
                for (var i = 1; i <= ch; i++) {
                    $('select[name="stsSearch[age' + i + ']"]').prop('disabled', false);
                }

            } else if(ch == 0) {

                $('select[name="stsSearch[age1]"]').prop('disabled', true);
                $('select[name="stsSearch[age2]"]').prop('disabled', true);
                /*$('select[name^="stsSearch[age]"]').prop('disabled', true);*/
                $('#stsSearchForm .age-container').hide();
                $('#stsSearchForm .age-container').css("display","none");

            }
        });

        $('select[name="stsSearch[children]"]').on("click", function () {

            $('#stsSearchForm .age-container').show();

        });

        $("#stsSearchForm").on("click", ".age-closer", function (e) {
            $(this).closest(".age-container").hide();
        });

        moment.locale("<?= LANGUAGE_ID?>");

        $('.date_pic').on('click', function () {
            $(this).siblings('input').datepicker("show");
        });

    })(jQuery, moment, window);

</script>