<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arResult['ROUTE_INFO'] = array();

$towns = array();
$arTowns = array();
if(!empty($arResult["DISPLAY_PROPERTIES"]["DEPARTURE"]["VALUE"])){
    $towns[] = $arResult["DISPLAY_PROPERTIES"]["DEPARTURE"]["VALUE"];
}
if(!empty($arResult["DISPLAY_PROPERTIES"]["TOWN"]["VALUE"])){
    $towns = array_merge($towns,$arResult["DISPLAY_PROPERTIES"]["TOWN"]["VALUE"]);
}
if(!empty($towns)){
    $towns = array_unique($towns);
    $db_towns = CIBlockElement::GetList(false, Array("IBLOCK_ID"=>CITIES_IBLOCK_ID, "ACTIVE"=>"Y", "ID"=>$towns, "!PROPERTY_MAP_VALUE"=>false), false, false, Array("IBLOCK_ID", "ID", "NAME", "PROPERTY_MAP"));
    while($ar_towns = $db_towns->GetNext())
    {
        $arTowns[$ar_towns["ID"]] = $ar_towns;
    }
}
if(!empty($arResult["DISPLAY_PROPERTIES"]["DEPARTURE"]["VALUE"]) && isset($arTowns[$arResult["DISPLAY_PROPERTIES"]["DEPARTURE"]["VALUE"]])){
    $LATLNG = explode(",", $arTowns[$arResult["DISPLAY_PROPERTIES"]["DEPARTURE"]["VALUE"]]["PROPERTY_MAP_VALUE"]);
    $arResult['ROUTE_INFO'][] = array(
        "lat" => $LATLNG[0],
        "lng" => $LATLNG[1],
        "title" => $arTowns[$arResult["DISPLAY_PROPERTIES"]["DEPARTURE"]["VALUE"]]['NAME']
    );
}
if(!empty($arResult["DISPLAY_PROPERTIES"]["TOWN"]["VALUE"])){
    foreach ($arResult["DISPLAY_PROPERTIES"]["TOWN"]["VALUE"] as $town){
        if(isset($arTowns[$town])) {
            $LATLNG = explode(",", $arTowns[$town]["PROPERTY_MAP_VALUE"]);
            $arResult['ROUTE_INFO'][] = array(
                "lat" => $LATLNG[0],
                "lng" => $LATLNG[1],
                "title" => $arTowns[$town]['NAME']
            );
        }
    }
}

$arResult["DIRECTIONS"] = false;
$directions = \travelsoft\sts\stores\RelationDirections::get();
foreach ($directions as $direction) {

    foreach ($arResult["DISPLAY_PROPERTIES"]["COUNTRY"]["VALUE"] as $country) {
        if (in_array($country, $direction["UF_COUNTRY_BX_ID"])) {
            $arResult["DIRECTIONS"] = true;
            break;
        }
    }

}

$cp = $this->__component; // объект компонента

if (is_object($cp))
{
    $cp->arResult['DIRECTIONS'] = $arResult["DIRECTIONS"];
    $cp->arResult['DETAIL_PAGE_URL'] = $arResult["DETAIL_PAGE_URL"];
    $cp->arResult['DISPLAY_PROPERTIES'] = $arResult["DISPLAY_PROPERTIES"];
    $cp->SetResultCacheKeys(array('DISPLAY_PROPERTIES','DETAIL_PAGE_URL','DIRECTIONS'));
}