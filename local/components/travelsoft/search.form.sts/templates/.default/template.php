<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$this->addExternalCss(SITE_TEMPLATE_PATH . "/assets/css/icomoon/auth.min.css");
$this->addExternalCss(SITE_TEMPLATE_PATH . "/assets/css/date/daterangepicker.min.css");
$this->addExternalJs(SITE_TEMPLATE_PATH . "/assets/js/date/moment.min.js");
$this->addExternalJs(SITE_TEMPLATE_PATH . "/assets/js/date/moment_locales.min.js");
$this->addExternalJs(SITE_TEMPLATE_PATH . "/assets/js/date/daterangepicker.js");
$this->addExternalCss(SITE_TEMPLATE_PATH . "/assets/css/select2/select2.min.css");
$this->addExternalJs(SITE_TEMPLATE_PATH . "/assets/js/select2/select2.min.js");
?>

<!-- booking -->

            <div class="panel">
                <?if($arParams["SHOW_PANEL_HEADING"] == "Y"):?>

                    <div class="panel-heading">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab1default" data-toggle="tab"><i
                                            class="flaticon-paper-plane"></i><?=GetMessage('TITLE_HEADING')?></a></li>
                            <!--<li><a href="#tab2default" data-toggle="tab"> <i
                                            class="flaticon-airplane-flight-in-circle-around-earth"></i>Авиабилеты</a>
                            </li>-->
                        </ul>
                    </div>

                <?endif?>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="tab1default">
                            <form id="stsSearchForm" method='GET' action="<?= $arParams['ACTION_URL']?>" autocomplete="off">

                                <?if(!empty($arResult["REQUEST"]["stars"])):?>
                                    <?foreach ($arResult["REQUEST"]["stars"] as $star):?>
                                        <input type="hidden" name="stsSearch[stars][]" value="<?=$star?>">
                                    <?endforeach;?>
                                <?endif?>
                                <?if(!empty($arResult["REQUEST"]["tourTypes"])):?>
                                    <?foreach ($arResult["REQUEST"]["tourTypes"] as $tour):?>
                                        <input type="hidden" name="stsSearch[tourTypes][]" value="<?=$tour?>">
                                    <?endforeach;?>
                                <?endif?>
                                <?if(!empty($arResult["REQUEST"]["meals"])):?>
                                    <?foreach ($arResult["REQUEST"]["meals"] as $meal):?>
                                        <input type="hidden" name="stsSearch[meals][]" value="<?=$meal?>">
                                    <?endforeach;?>
                                <?endif?>

                                <div class="row">
                                    <div class="col-xs-12 col-sm-9 col-md-10 sts-form">
                                        <div class="row panel-margin">
                                            <div class="col-xs-6 col-sm-6 col-md-2 panel-padding">
                                                <label><?=GetMessage('CITYFROM')?></label><br>
                                                <!-- filters select -->
                                                <div class="select-filters">
                                                    <select name="stsSearch[cityFrom]" id="cityFrom">
                                                        <?foreach ($arResult["CITIES"] as $city):?>
                                                            <option value="<?=$city["id"]?>" <?if($arResult["REQUEST"]["cityFrom"] == $city["id"]):?>selected<?endif?>><?=$city["name"]?></option>
                                                        <?endforeach;?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-md-2 panel-padding">
                                                <label><?=GetMessage('COUNTRYTO')?></label><br>
                                                <!-- filters select -->
                                                <div class="select-filters">
                                                    <select name="stsSearch[country]" id="country">
                                                        <?foreach ($arResult["COUNTRIES"] as $country):?>
                                                            <option value="<?=$country["id"]?>" <?if($arResult["REQUEST"]["country"] == $country["id"]):?>selected<?endif?>><?=$country["name"]?></option>
                                                        <?endforeach;?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-6 col-md-3 panel-padding">
                                                <label><?=GetMessage('DATE')?></label><br>
                                                <div class="icon-addon form-field field-date" id="date-range-container">
                                                    <?
                                                    $date_from_value = $arResult["REQUEST"]["dateFrom"] > 0 ? "value='".$arResult["REQUEST"]["dateFrom"]."'" : "";
                                                    $date_to_value = $arResult["REQUEST"]["dateTo"] > 0 ? "value='".$arResult["REQUEST"]["dateTo"]. "'" : "";
                                                    ?>
                                                    <input <?= $date_from_value?> data-start-date="<?= $arResult["REQUEST"]["dateFrom"]?>" type="hidden" name="stsSearch[dateFrom]" class="form-control minDate">
                                                    <input <?= $date_to_value?> data-end-date="<?= $arResult["REQUEST"]["dateTo"]?>" type="hidden" name="stsSearch[dateTo]" class="form-control maxDate">
                                                    <input readonly required type="text" class="form-control calendar-input" placeholder="<?=GetMessage('DATE')?>">
                                                    <i class="icon-calendar glyphicon fa fa-calendar"></i>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-6 col-md-3 panel-padding">

                                                <!-- filters select -->

                                                    <div class="form-group">
                                                        <label><?=GetMessage('NIGHTS')?></label><br>
                                                        <div class="nights-block">

                                                            <span class="pre-n"><?=GetMessage('NIGHTSFROM')?></span>&nbsp;&nbsp;
                                                            <div class="night-select">
                                                                <select name="stsSearch[nightFrom]" id="min-filter-duration" class="form-control" required>
                                                                    <?for($i = 1; $i < 29; $i++):?>
                                                                        <option value="<?=$i?>" <?if($i == $arResult["REQUEST"]["nightFrom"]):?>selected<?endif?>><?=$i?></option>
                                                                    <?endfor;?>
                                                                </select>
                                                            </div>
                                                            &nbsp;&nbsp;<span class="pre-n"><?=GetMessage('NIGHTSTO')?></span>&nbsp;&nbsp;
                                                            <div class="night-select">
                                                                <select name="stsSearch[nightTo]" id="max-filter-duration" class="form-control" required>
                                                                    <?for($i = 1; $i < 29; $i++):?>
                                                                        <option value="<?=$i?>" <?if($i == $arResult["REQUEST"]["nightTo"]):?>selected<?endif?>><?=$i?></option>
                                                                    <?endfor;?>
                                                                </select>
                                                            </div>

                                                        </div>
                                                    </div>

                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-md-1 panel-padding adults-form">
                                                <label><?=GetMessage('ADULTS')?></label><br>
                                                <!-- filters select -->
                                                <div class="select-filters">
                                                    <input type="number" min="1" max="6" class="form-control" name="stsSearch[adults]" id="adults" placeholder="2" value="<?=!empty($arResult["REQUEST"]['adults']) ? $arResult["REQUEST"]['adults'] : 2?>" required>
                                                </div>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-md-1 panel-padding children-form">
                                                <label><?=GetMessage('CHILDRENS')?></label><br>
                                                <!-- filters select -->
                                                <div class="select-filters">
                                                    <input type="number" min="0" max="3" class="form-control" name="stsSearch[children]" id="children" placeholder="0" value="<?=!empty($arResult["REQUEST"]['children']) ? $arResult["REQUEST"]['children'] : 0?>" >
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-3 col-md-2">
                                        <button type="submit" class="thm-btn">Искать</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
<script>

    /**
     * @param {jQuery} $
     * @param {Window} window
     */
    (function ($, moment, window) {

        var
            /**
             * инициализируем select2
             * @param {object} options
             */
            __initSelect2 = function (options) {

                // инициализируем select
                function select2 (obj) {
                    obj.select2({
                        allowClear: true,
                        formatNoMatches: function () {
                            return "<?= GetMessage("NOMATCHES")?>";
                        },
                        minimumResultsForSearch: -1
                    });
                }

                select2(options);
                options.select2("val", options.val());

            },

            dateFormat = "DD.MM.YYYY",
            form = $("#stsSearchForm"),
            dates = {};

            $('#stsSearchForm select').each(function() {
                __initSelect2($(this));
            });

        function setDates (cityFrom, country) {

            if (typeof dates[cityFrom] === "object" && $.isArray(dates[cityFrom][country])){
                return;
            }

            $.ajax({
                method: "post",
                url: '<?=$componentPath?>/ajax.php',
                dataType: 'json',
                data: {cityFrom: cityFrom, country: country},
                success: function (data) {

                    if (data.error !== true && typeof data.result !== "undefined" && data.result !== null && data.result.length > 0) {
                        dates[cityFrom] = {};
                        dates[cityFrom][country] = data.result;
                    }

                    /*if(datepicker) {
                        datepicker.remove();
                    }
                    datepicker = initDatePicker($('.panel-body .calendar-input'));*/

                }
            });

        }

        var cityFrom = form.find("select[name='stsSearch[cityFrom]']").val();
        var country = form.find("select[name='stsSearch[country]']").val();
        var datepicker;
        var picker;
        var calendars;
        setDates(cityFrom,country);

        function initDatePicker ($this) {
            var parent = $this.parent(".field-date"),
                date_from = parent.find(".minDate"), date_to = parent.find(".maxDate"), options = {};

            /*options.minDate = '27.11.2017';*/

            if (date_from.length) {
                options.startDate = (function (date_from) {

                    var val = date_from.val(), defVal;
                    if (!val) {
                        defVal = date_from.data("start-date");
                        date_from.val(defVal);
                        return defVal;
                    }

                    return val;

                })(date_from);
            }

            if (date_to.length) {
                options.endDate = (function (date_to) {

                    var val = date_to.val(), defVal;
                    if (!val) {
                        defVal = date_to.data("end-date");
                        date_to.val(defVal);
                        return defVal;
                    }

                    return val;

                })(date_to);
            }

            options.singleDatePicker = $this.data("single-date-picker") == "Y";
            options.autoApply = true;
            options.locale = {
                format: dateFormat,
                separator: ' - ',
                daysOfWeek: moment.weekdaysMin(),
                monthNames: moment.monthsShort(),
                firstDay: moment.localeData().firstDayOfWeek(),
            };

            var datepicker = $this.daterangepicker(options);

            datepicker.on("apply.daterangepicker", function (ev, picker) {

                var calendars = picker.container.find('.calendars');

                var arVals = $(this).val().split(" - ") || [], parent;

                if (arVals.length) {

                    date_from.val(arVals[0]);
                    if (date_to.length) {
                        date_to.val(arVals[1]);
                    }

                }

                if(typeof dates[cityFrom][country] !== "undefined") {
                    calendars.find('td.available').on('click', function () {
                        console.log(234);
                        calendars.find('td.available').each(function () {

                            if (!$(this).hasClass('available') || $(this).hasClass('off')) return;

                            var title = $(this).attr('data-title');
                            var row = title.substr(1, 1);
                            var col = title.substr(3, 1);
                            var cal = $(this).parents('.calendar');
                            var date = cal.hasClass('left') ? picker.leftCalendar.calendar[row][col] : picker.rightCalendar.calendar[row][col];
                            if ($.inArray(moment(date._d).format(dateFormat), dates[cityFrom][country]) >= 0) {
                                $(this).addClass('active_');
                            }

                        });
                    });
                }

            }).on("show.daterangepicker", function (ev, picker_) {

                calendars = picker_.container.find('.calendars');
                picker = picker_;

                if(typeof dates[cityFrom][country] !== "undefined") {

                    calendars.find('td.available').each(function () {

                        if (!$(this).hasClass('available') || $(this).hasClass('off')) return;

                        var title = $(this).attr('data-title');
                        var row = title.substr(1, 1);
                        var col = title.substr(3, 1);
                        var cal = $(this).parents('.calendar');
                        var date = cal.hasClass('left') ? picker_.leftCalendar.calendar[row][col] : picker_.rightCalendar.calendar[row][col];
                        if ($.inArray(moment(date._d).format(dateFormat), dates[cityFrom][country]) >= 0) {
                            $(this).addClass('active_');
                        }

                    });
                }

            });

            return datepicker;

        }

        setInterval(function () {

            if (picker && calendars) {
                if(typeof dates[cityFrom][country] !== "undefined") {

                    calendars.find('td.available').each(function () {

                        if (!$(this).hasClass('available') || $(this).hasClass('off')) return;

                        var title = $(this).attr('data-title');
                        var row = title.substr(1, 1);
                        var col = title.substr(3, 1);
                        var cal = $(this).parents('.calendar');
                        var date = cal.hasClass('left') ? picker.leftCalendar.calendar[row][col] : picker.rightCalendar.calendar[row][col];
                        if ($.inArray(moment(date._d).format(dateFormat), dates[cityFrom][country]) >= 0) {
                            $(this).addClass('active_');
                        }

                    });
                }
            }

        }, 300);
        /*$(document).on('click','td.available',function (e) {
            console.log(datepicker);
            var calendars = $(this).closest('.calendars');

            if(typeof dates[cityFrom][country] !== "undefined") {

                calendars.find('td.available').each(function () {

                    if (!$(this).hasClass('available') || $(this).hasClass('off')) return;

                    var title = $(this).attr('data-title');
                    var row = title.substr(1, 1);
                    var col = title.substr(3, 1);
                    var cal = $(this).parents('.calendar');
                    var date = cal.hasClass('left') ? picker.leftCalendar.calendar[row][col] : picker.rightCalendar.calendar[row][col];
                    if ($.inArray(moment(date._d).format(dateFormat), dates[cityFrom][country]) >= 0) {
                        $(this).addClass('active_');
                    }

                });

            }
            console.log(777);
        });*/

        form.find("select[name='stsSearch[cityFrom]']").on("change", function(){
            cityFrom = $(this).val();
            setDates(cityFrom,country);
        });

        form.find("select[name='stsSearch[country]']").on("change", function(){
            country = $(this).val();
            setDates(cityFrom,country);
        });

        moment.locale("<?= LANGUAGE_ID?>");

        datepicker = initDatePicker($('.panel-body .calendar-input'));

        $('.icon-calendar').on('click', function () {
            $(this).parent().find('input[type="text"]').click();
        });

    })(jQuery, moment, window);

</script>