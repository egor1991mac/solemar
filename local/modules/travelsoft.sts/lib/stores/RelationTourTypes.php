<?php

namespace travelsoft\sts\stores;

use travelsoft\sts\adapters\MiddlewareHighloadblock;

/**
 * Класс для работы с таблицей типов туров
 *
 * @author juliya.sharlova
 * @copyright (c) 2017, travelsoft
 */
class RelationTourTypes extends MiddlewareHighloadblock
{
    protected static $storeName = 'relationtourtypes';

}