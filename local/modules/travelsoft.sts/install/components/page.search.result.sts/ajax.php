<?
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define("NO_AGENT_STATISTIC",true);
define('NO_AGENT_CHECK', true);
define("PUBLIC_AJAX_MODE", true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

Bitrix\Main\Loader::includeModule("travelsoft.sts");

$arResponse = array(
    "result" => null,
    "error" => false
);

try {

    $arResponse["result"] = (new \travelsoft\sts\ClientServerProxy($_REQUEST))->run();
    echo \travelsoft\sts\Utils::jsonEncode($arResponse);

} catch(\Exception $e) {

    // in log

    $arResponse["error"] = true;
    $arResponse["error_message"] = $e->getMessage();
    echo \travelsoft\sts\Utils::jsonEncode($arResponse);

}

