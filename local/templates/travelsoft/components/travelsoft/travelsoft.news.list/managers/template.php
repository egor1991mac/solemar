<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if(!empty($arResult["ITEMS"])):?>

    <div class="workers">
        <?if(!empty($arParams["TEXT_TITLE"])):?>
            <h2><?=$arParams["TEXT_TITLE"]?></h2>
        <?endif?>

        <?foreach($arResult["ITEMS"] as $arItem):?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <?$arImg = getSrc($arItem["DISPLAY_PROPERTIES"]["PICTURES"]["VALUE"], array("width"=>120,"height"=>120), NO_PHOTO_AVATAR_120_120, 1, false);?>

            <div id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                <p class="name"><?= $arItem["NAME"] ?></p>
                <img src="<?= $arImg[0] ?>" alt="" />
                <p>
                    <?if(!empty($arItem["DISPLAY_PROPERTIES"]["SKYPE"]["VALUE"])):?>
                        <span class="skype"><?= $arItem["DISPLAY_PROPERTIES"]["SKYPE"]["VALUE"] ?></span>
                    <?endif ?>
                    <?if(!empty($arItem["DISPLAY_PROPERTIES"]["EMAIL"]["VALUE"])):?>
                        <a href="mailto:<?=$arItem["DISPLAY_PROPERTIES"]["EMAIL"]["VALUE"]?>" class="mail"><?= $arItem["DISPLAY_PROPERTIES"]["EMAIL"]["VALUE"] ?></a>
                    <?endif ?>
                </p>
            </div>

        <?endforeach;?>

    </div>
<?endif?>