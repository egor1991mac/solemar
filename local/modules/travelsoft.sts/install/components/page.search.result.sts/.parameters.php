<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if (!CModule::IncludeModule("highloadblock"))
{
    ShowError(GetMessage("Модуль highloadblock не установлен."));
    return;
}

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;
use Bitrix\Main\Loader;

$arHiloadblocks = Bitrix\Highloadblock\HighloadBlockTable::getList(array(
    "order" => array("ID" => "ASC")
))->fetchAll();

foreach ($arHiloadblocks as $arHL) {
    $arHLDef[$arHL['ID']] = $arHL['NAME'];
}

$arComponentParameters['PARAMETERS'] = array(

    "GET_PARAMS_STS" => array(
        "PARENT" => "BASE",
        "NAME" => GetMessage("T_DESC_SEARCH"),
        "TYPE" => "STRING",
        "VALUES" => '={$_REQUEST["stsSearch"]}',
    ),
    "BOOKING_PAGE" => array(
        "PARENT" => "BASE",
        "NAME" => GetMessage("T_BOOKING_PAGE_URL"),
        "TYPE" => "STRING",
        "VALUE" => ''
    )

);