<?
// подключим все необходимые файлы:
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php"); // первый общий пролог

Bitrix\Main\Loader::includeModule("travelsoft.sts");
Bitrix\Main\Loader::includeModule('highloadblock');
Bitrix\Main\Loader::includeModule('iblock');

function setValuesTable($idHotel, $hotelKey, $operatorCode) {

    $relHotel = current(\travelsoft\sts\stores\RelationCities::get(array("filter" => array("UF_BX_ID" => $idHotel), "select" => array("ID", $operatorCode))));

    $data = array(
        "UF_BX_ID" => $idHotel,
        $operatorCode => $hotelKey
    );

    if (isset($relHotel["ID"]) && ($relHotel[$operatorCode] == '' || (int)$relHotel[$operatorCode] != (int)$hotelKey)) {

        \travelsoft\sts\stores\RelationHotels::update($relHotel["ID"], $data);

    } elseif (!isset($relCity["ID"])) {

        \travelsoft\sts\stores\RelationHotels::add($data);

    }

}

?>

<?$APPLICATION->SetTitle("Актуализация таблицы отелей");?>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php"); // второй общий пролог
?>


<?
if (check_bitrix_sessid() && strlen($_REQUEST["update_hotel"]) > 0) {

    $operator = current(\travelsoft\sts\stores\Operators::get(array("filter" => array("UF_MAIN_OPERATOR" => 1))));

    if ($operator) {

        $db_hotels = \travelsoft\sts\stores\Hotels::get(array('filter' => array('ACTIVE' => 'Y', '!HOTELKEY' => false), 'select' => array('ID', 'PROPERTY_HOTELKEY')));
        if (!empty($db_hotels)) {
            $k = 1;$cnt = count($db_hotels);

            foreach ($db_hotels as $hotel) {

                setValuesTable($hotel["ID"],$hotel["PROPERTY_HOTELKEY_VALUE"],$operator["UF_CODE"]);
                $k++;

            }

            if($k == $cnt) {
                echo 'Обновление завершено!';
            }

        }

    }

}

/*if ($optionsFormResponse["errors"]) {
    CAdminMessage::ShowMessage(array(
        "MESSAGE" => implode('<br>', $optionsFormResponse["errors"]),
        "TYPE" => "ERROR",
        "HTML" => true
    ));
}*/


?>

<div class=adm-workarea">

    <p>Запускать скрипт актуализации таблицы отелей необходимо после модульной загрузки отелей в информационный блок.</p>

    <form id="table-update-hotel-form" action="<?= $APPLICATION->GetCurPage("lang=" . LANG, array('lang')) ?>" method="get">
        <input type="hidden" name="lang" value="<?= LANGUAGE_ID ?>">
        <?= bitrix_sessid_post(); ?>
        <button name="update_hotel" value="Обновить" type="submit" class="adm-btn adm-btn-save">Обновить таблицу</button>
    </form>

</div>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");?>
