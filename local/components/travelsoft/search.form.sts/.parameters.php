<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if (!CModule::IncludeModule("highloadblock"))
{
    ShowError(GetMessage("Модуль highloadblock не установлен."));
    return;
}

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;
use Bitrix\Main\Loader;

$arHiloadblocks = Bitrix\Highloadblock\HighloadBlockTable::getList(array(
    "order" => array("ID" => "ASC")
))->fetchAll();

foreach ($arHiloadblocks as $arHL) {
    $arHLDef[$arHL['ID']] = $arHL['NAME'];
}

$arComponentParameters['PARAMETERS'] = array(

    "GET_PARAMS_STS" => array(
        "PARENT" => "BASE",
        "NAME" => GetMessage("T_DESC_SEARCH"),
        "TYPE" => "STRING",
        "VALUES" => '={$_REQUEST["stsSearch"]}',
    ),
    "ACTION_URL" => array(
        "PARENT" => "BASE",
        "NAME" => GetMessage("T_DESC_ACTION_URL"),
        "TYPE" => "STRING",
    ),
    "SHOW_PANEL_HEADING" => array(
        "PARENT" => "BASE",
        "NAME" => GetMessage("T_DESC_SHOW_PANEL_HEADING"),
        "TYPE" => "CHECKBOX",
        "DEFAULT" => "N"
    ),
    "COUNTRY_ID" => array(
        "PARENT" => "BASE",
        "NAME" => GetMessage("T_DESC_COUNTRY_ID"),
        "TYPE" => "STRING",
    ),
    "CITY_ID" => array(
        "PARENT" => "BASE",
        "NAME" => GetMessage("T_DESC_CITY_ID"),
        "TYPE" => "STRING",
    ),
    "TOUR_TYPE_ID" => array(
        "PARENT" => "BASE",
        "NAME" => GetMessage("T_DESC_TOUR_TYPE_ID"),
        "TYPE" => "STRING",
    ),
    "HOTEL_ID" => array(
        "PARENT" => "BASE",
        "NAME" => GetMessage("T_DESC_HOTEL_ID"),
        "TYPE" => "STRING",
    )

);