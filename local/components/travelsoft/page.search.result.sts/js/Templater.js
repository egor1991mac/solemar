/**
 * @version: 1.1
 * @author: julia
 * @copyright: Copyright (c) 2017 julia. Все права защищены.
 */


    /**
     * Конструтор html шаблона
     * @param       {Object} htmlTemplatesList
     * @constructor
     */
    function Templater (htmlTemplatesList) {

        /**
         * Генерирует и возвращает html для шаблона списка
         * @param       {Array} item
         * @return      {String}
         */
        function htmlListItem(item, services) {

            var i,
                html = htmlTemplatesList.list.list,
                htmlStars = htmlTemplatesList.list.stars,
                htmlStar = htmlTemplatesList.list.star,
                htmlStar_ = htmlTemplatesList.list.star_,
                htmlCountry = htmlTemplatesList.list.country,
                htmlCity = htmlTemplatesList.list.city;
            /*htmlServices = htmlTemplatesList.list.services,
             htmlServicePic = htmlTemplatesList.list.service_pic,
             htmlServiceIcon = htmlTemplatesList.list.service_icon;*/

            console.log(item);

            var countryUrl = item.country.link != '' ? item.country.link : 'javascript:void(0)';
            var cityUrl = item.city.link != '' ? item.city.link : 'javascript:void(0)';

            var map_hotel = item.hotel.map != '' ? '<span style="display: inline-block;height: 25px;cursor: pointer" class="hotel-options__link hotel-options__link_on-map dashed-wrapper" data-must-init="true" id="map-container-' + item.hotel.mdId + '" data-route-title="' + item.hotel.viewName + '" data-html-map-id="map_' + item.hotel.mdId + '" data-maps="' + item.hotel.map + '">На карте</span>' : '';

            var coordinates_ = item.hotel.map != '' ? item.hotel.map.split(",") : '';

            htmlCountry = htmlCountry.replace("#country_link#", countryUrl);
            htmlCountry = htmlCountry.replace("#country_name#", item.country.name);
            html = html.replace("#country#", htmlCountry);

            if(typeof item.city.viewName !== "null" && typeof item.city.viewName !== "undefined") {
                htmlCity = htmlCity.replace("#city_link#", cityUrl);
                htmlCity = htmlCity.replace("#city_name#", item.city.viewName);
            } else {
                htmlCity = '';
            }
            html = html.replace("#dateFrom#", item.tourDate.dateFrom);
            html = html.replace("#city#", htmlCity);
            html = html.replace("#departure#", item.cityFrom.from_name);
            html = html.replace("#price#", item.priceCurrency_);
            html = html.replace("#prices#", item.prices);
            html = html.replace("#defCurrency#", item.defCurrency);
            html = html.replace("#price_for#", item.price_for);
            html = html.replace("#map#", map_hotel);
            html = html.replace("#coordinates#", item.hotel.map);
            html = html.replace("#lat#", coordinates_[0]);
            html = html.replace("#lng#", coordinates_[1]);

            switch (item.tourType.typeSearch){
                case "tour":

                    var tourUrl = item.tour.link !== '' ? item.tour.link : 'javascript:void(0)';
                    html = html.replace("#image#", item.tour.image);
                    html = html.replace(new RegExp("#detail_page_url#",'g'), tourUrl);
                    html = html.replace(new RegExp("#name#",'g'), item.tour.viewName);
                    html = html.replace("#about_hotel#", '');
                    html = html.replace("#stars#", '');

                    html = html.replace("#previewText#", item.tour.description);
                    //html = html.replace("#night#", (item.tour.days !== null) ? item.tour.days + ' ночей' : '');
                    html = html.replace("#night#", item.nightFormatted);
                    html = html.replace(new RegExp("#md5HotelId#",'g'), item.tour.mdId);
                    html = html.replace("#requestHotel#", item.tour.requestTour);

                    break;
                default:

                    var stars = parseInt(item.hotel.star), stars_ = '', star = '';
                    if(typeof stars === "number" && stars !== 'NaN') {
                        for (i = 1; i<= stars; i++){
                            stars_ += htmlStar;
                        }
                        star = stars;
                    } else if(stars !== 'NaN') {
                        star = item.hotel.star;
                    }
                    htmlStars = htmlStars.replace("#stars#", stars_);
                    htmlStars = htmlStars.replace("#star#", star);
                    html = html.replace("#stars#", htmlStars);

                    var hotelUrl = item.hotel.link != '' ? item.hotel.link : 'javascript:void(0)';
                    var about_hotel = item.hotel.link != '' ? '<a class="c-button b-40 bg-grey-3-t hv-grey-3-t b-1" target="_blank" href="' + item.hotel.link + '">Об отеле</a>' : '';

                    html = html.replace("#image#", item.hotel.image);
                    html = html.replace(new RegExp("#detail_page_url#",'g'), hotelUrl);
                    html = html.replace(new RegExp("#name#",'g'), item.hotel.viewName);
                    html = html.replace("#about_hotel#", about_hotel);
                    html = html.replace("#previewText#", item.hotel.previewText);
                    html = html.replace("#night#", item.nightFormatted);
                    html = html.replace(new RegExp("#md5HotelId#",'g'), item.hotel.mdId);
                    html = html.replace("#requestHotel#", item.hotel.requestHotel);

                    break;
            }

            return html;
        }

        /**
         * Генерирует и возвращает html для шаблона списка вариантов
         * @param       {Array} item
         * @return      {String}
         */
        function htmlDetailItem (item, services, bookingPage) {

            var html = htmlTemplatesList.detail.list,
                htmlFoodFullName = htmlTemplatesList.detail.food_full_name,
                htmlFoodDesc = htmlTemplatesList.detail.food_desc;

            html = html.replace("#bookingUrl#", bookingPage);
            html = html.replace("#operatorId#", item.operator_id);
            html = html.replace("#priceKey#", item.resultKey);
            html = html.replace("#hotelIdBx#", item.hotel.id_bx);
            html = html.replace("#hotelName#", item.hotel.viewName);
            html = html.replace("#hotelStars#", item.hotel.star);
            html = html.replace("#hotelDetailUrl#", item.hotel.link);
            html = html.replace("#country#", item.country.name);
            html = html.replace("#city#", item.city.viewName);
            html = html.replace(new RegExp("#dateFrom#",'g'), item.tourDate.dateFrom);
            html = html.replace(new RegExp("#dateTo#",'g'), item.tourDate.dateTo);
            html = html.replace(new RegExp("#night#",'g'), item.nightFormatted);
            html = html.replace(new RegExp("#children#",'g'), item.children);
            html = html.replace(new RegExp("#adults#",'g'), item.adults);
            html = html.replace(new RegExp("#food#",'g'), item.food.viewName);

            html = html.replace("#foodFullName#", item.food.fullName);

            if(item.food.desc) {
                htmlFoodDesc = htmlFoodDesc.replace("#foodDesc#", item.food.desc);
            } else {
                htmlFoodDesc = '';
            }
            html = html.replace("#foodDesc#", htmlFoodDesc);

            html = html.replace(new RegExp("#placement#",'g'), item.placement.viewName);
            html = html.replace(new RegExp("#roomType#",'g'), item.roomType.viewName);
            html = html.replace(new RegExp("#roomCatType#",'g'), item.roomCatType.viewName);

            html = html.replace("#tourType#", item.tourType.viewName);

            html = html.replace(new RegExp("#price#",'g'), item.priceCurrency_);
            html = html.replace("#priceNotCurrency#", item.priceCurrency.price.toFixed(2));
            html = html.replace("#currency#", item.priceCurrency.ISO);
            html = html.replace("#prices#", item.prices);
            html = html.replace("#defCurrency#", item.defCurrency);

            return html;

        }

        /**
         * Генерирует и возвращает html для шаблона списка элементов на детальной странице
         * @param       {Array} item
         * @return      {String}
         */
        function htmlHotelItem (item, services, bookingPage) {

            var html = htmlTemplatesList.hotel.list,
                htmlFoodFullName = htmlTemplatesList.hotel.food_full_name,
                htmlFoodDesc = htmlTemplatesList.hotel.food_desc;

            var hotelUrl = item.hotel.link != '' ? item.hotel.link : 'javascript:void(0)';

            html = html.replace("#bookingUrl#", bookingPage);
            html = html.replace("#operatorId#", item.operator_id);
            html = html.replace("#priceKey#", item.resultKey);
            html = html.replace("#hotelIdBx#", item.hotel.id_bx);
            html = html.replace(new RegExp("#hotelName#",'g'), item.hotel.viewName);
            html = html.replace(new RegExp("#hotelStars#",'g'), item.hotel.star);
            html = html.replace(new RegExp("#hotelDetailUrl#",'g'), hotelUrl);
            html = html.replace("#country#", item.country.name);
            html = html.replace("#city#", item.city.viewName);
            html = html.replace(new RegExp("#dateFrom#",'g'), item.tourDate.dateFrom);
            html = html.replace(new RegExp("#dateTo#",'g'), item.tourDate.dateTo);
            html = html.replace(new RegExp("#night#",'g'), item.nightFormatted);
            html = html.replace(new RegExp("#children#",'g'), item.children);
            html = html.replace(new RegExp("#adults#",'g'), item.adults);
            html = html.replace(new RegExp("#food#",'g'), item.food.viewName);

            html = html.replace("#foodFullName#", item.food.fullName);

            if(item.food.desc) {
                htmlFoodDesc = htmlFoodDesc.replace("#foodDesc#", item.food.desc);
            } else {
                htmlFoodDesc = '';
            }
            html = html.replace("#foodDesc#", htmlFoodDesc);

            html = html.replace(new RegExp("#placement#",'g'), item.placement.viewName);
            html = html.replace(new RegExp("#roomType#",'g'), item.roomType.viewName);
            html = html.replace(new RegExp("#roomCatType#",'g'), item.roomCatType.viewName);

            html = html.replace("#tourType#", item.tourType.viewName);

            html = html.replace(new RegExp("#price#",'g'), item.priceCurrency_);
            html = html.replace("#priceNotCurrency#", item.priceCurrency.price.toFixed(2));
            html = html.replace("#currency#", item.priceCurrency.ISO);
            html = html.replace("#prices#", item.prices);
            html = html.replace("#defCurrency#", item.defCurrency);

            switch (item.tourType.typeSearch){
                case "tour":

                    var tourUrl = item.tour.link !== '' ? item.tour.link : 'javascript:void(0)';
                    html = html.replace("#name#", '<a target="_blank" href="' + tourUrl + '">' + item.tour.viewName + '</a>, ');

                    break;
                default:

                    html = html.replace("#name#", '');

                    break;
            }

            return html;

        }

        /**
         * Возвращает html шаблона
         * @param       {String} func
         * @param       {Array} items
         * @return      {String}
         */
        function getHtmlItems (func, items, services, bookingPage) {

            var html = '';

            if(items.length > 0) {

                items.forEach(function (el) {

                    html += func(el, services, bookingPage);

                });

            }

            return html;

        }

        /**
         * Генерация шаблонов
         * @param  {String} templateName
         * @param  Array} items
         * @return {String}
         */
        this.generate = function (templateName, data) {

            var items = data.items,
                services = data.services,
                bookingPage = data.bookingPage;

            switch (templateName) {

                case "list":
                    return htmlTemplatesList.list.container.replace("#list#", getHtmlItems(htmlListItem, items, services));
                    break;
                case "detail":
                    return htmlTemplatesList.detail.container.replace("#list#", getHtmlItems(htmlDetailItem, items, null, bookingPage));
                    break;
                case "hotel":
                    return htmlTemplatesList.hotel.container.replace("#list#", getHtmlItems(htmlHotelItem, items, null, bookingPage));
                    //return getHtmlItems(htmlHotelItem, items, null, bookingPage);
                    break;
                default:
                    throw new Error('Шаблона ' + templateName + ' не существует');

            }

        }

    }
