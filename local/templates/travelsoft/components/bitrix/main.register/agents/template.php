<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2014 Bitrix
 */
/**
 * Bitrix vars
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @param array $arParams
 * @param array $arResult
 * @param CBitrixComponentTemplate $this
 * @var string $templateName
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

CJSCore::Init(array('ajax'));

$arResult['HASH'] = md5(serialize($arParams) . $templateName);


function _er(string $code, bool $div = false, string $message = '') {

    $message = strlen($message) > 0 ? $message : GetMessage($code);

    if ($div) {
        echo '<div class="validation">' . $message . '</div>';
    } else {
        echo '<span class="error">' . $message . '</span>';
    }
}

?>

<div class="bx-auth-reg">

    <?if ($USER->IsAuthorized()): ?>

        <div class="alert alert-success"><? echo GetMessage("MAIN_REGISTER_AUTH") ?></div>

    <?elseif (!empty($arResult["VALUES"]["USER_ID"]) && isset($_POST['IS_AGENT']) && $_POST['IS_AGENT'] == 'Y' && !$USER->IsAuthorized()): ?>

        <div class="alert alert-success"><? echo GetMessage("MAIN_REGISTER_AUTH_AGENT") ?></div>

    <?elseif (empty($arResult["ERRORS"]) && empty($arResult["VALUES"]["USER_ID"]) && isset($_POST['IS_AGENT']) && $_POST['IS_AGENT'] == 'Y' && !$USER->IsAuthorized()): ?>

        <div class="alert alert-warning"><? echo GetMessage("MAIN_NO_REGISTER_AUTH_AGENT") ?></div>

    <? else: ?>
        <?
        if (count($arResult["ERRORS"]) > 0) {

            ?><div class="alert alert-warning"><?
            foreach ($arResult["ERRORS"] as $key => $error) {
                if(strpos("логин",$error) === false) {
                    ShowError("Пользователь с таким e-mail уже существует.");
                }
                else{
                    ShowError($error);
                }
                /**/?><!--<p><?/* echo $error */?></p>--><?
            }
            ?></div><?
        }
        elseif ($arResult["USE_EMAIL_CONFIRMATION"] === "Y"){
            ?>
            <p><? echo GetMessage("REGISTER_EMAIL_WILL_BE_SENT") ?></p>
        <?}?>

    <!-- Контейнер -->
    <div class=" ts-theme">
        <!-- Форма регистрации агента -->

        <form method="post" action="<?= POST_FORM_ACTION_URI ?>" name="regform" id="reg-form-<?=$arResult['HASH']?>" enctype="multipart/form-data" class="ts-wrap ts-px-0 ts-px-md-2">
            <input name="HASH" type="hidden" value="<?= $arResult['HASH'] ?>">
            <input type="hidden" name="REGISTER[LOGIN]" value="temp_login">
            <?
            if ($arResult["BACKURL"] <> ''):
                ?>
                <input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>" />
                <?
            endif;
            ?>

            <!-- Заголовок -->
            <div class="ts-title ts-px-0 ts-py-0 ts-mx-0 ts-md-mx-2">
                <h2><?=GetMessage("TITLE_AUTH_REGISTER")?></h2>
                <button class="ts-d-none">x</button>
            </div>

            <!-- Контент -->
            <div class="ts-content">

                <? foreach ($arResult["SHOW_FIELDS"] as $FIELD): ?>
                    <?
                    if ($FIELD == 'LOGIN') {
                        continue;
                    }
                    ?>

                    <div class="ts-col-24 ts-col-lg-12  ts-mb-2 ts-px-0 ts-md-px-2">
                        <div class="ts-input form-block <? if (in_array('WRONG_'.$FIELD, $arResult['CODE_ERRORS']) || in_array('USER_NOT_FOUND', $arResult['CODE_ERRORS']) || in_array('WRONG_ENTERED_'.$FIELD, $arResult['CODE_ERRORS'])): ?>has-error<? endif ?>" >
                            <label for="REGISTER[<?= $FIELD ?>]" class="ts-d-none form-label" ></label>
                            <div class="placeholder"><?= GetMessage("REGISTER_FIELD_" . $FIELD) ?></div>

                            <div class="input">

                        <?
                        switch ($FIELD) {

                            case "EMAIL":
                                ?>
                                <?
                                if (in_array('WRONG_'.$FIELD, $arResult['CODE_ERRORS'])) {
                                    _er('REG_FORM_WRONG_'.$FIELD);
                                }
                                if (in_array('USER_NOT_FOUND', $arResult['CODE_ERRORS'])) {
                                    _er('REG_FORM_USER_NOT_FOUND');
                                }
                                if (in_array('REGISTER_FAIL', $arResult['CODE_ERRORS'])) {
                                    _er('REG_FORM_REGISTER_FAIL');
                                }
                                ?>
                                <input <?
                                /*if ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y") {
                                    echo 'required=""';
                                }*/
                                ?> type="text" name="REGISTER[<?= $FIELD ?>]" value="<?= $arResult["VALUES"][$FIELD] ?>">
                                    <?
                                    break;

                                case "PERSONAL_PHONE":
                                    ?>
                                    <?
                                    if (in_array('WRONG_'.$FIELD, $arResult['CODE_ERRORS'])) {
                                        _er('REG_FORM_WRONG_'.$FIELD);
                                    }
                                    ?>
                                <input <?
                                /*if ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y") {
                                    echo 'required=""';
                                }*/
                                ?> size="30" type="tel" name="REGISTER[<?= $FIELD ?>]" value="<?= $arResult["VALUES"][$FIELD] ?>" placeholder="+375292221133">
                                    <?
                                    break;

                                case "PASSWORD":
                                    ?>
                                    <?
                                    if (in_array('WORNG_'.$FIELD, $arResult['CODE_ERRORS'])) {
                                        _er('WORNG_'.$FIELD, false, $arResult['SYSTEM_ERRORS_MESSAGES']['WORNG_PASSWORD']);
                                    }
                                    if (in_array('WRONG_ENTERED_'.$FIELD, $arResult['CODE_ERRORS'])) {
                                        _er('REG_FORM_WRONG_ENTERED_'.$FIELD);
                                    }
                                    ?>
                                    <input <?
                                /*if ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y") {
                                    echo 'required=""';
                                }*/
                                ?> size="30" type="password" name="REGISTER[<?= $FIELD ?>]" value="<?= $arResult["VALUES"][$FIELD] ?>" autocomplete="off">
                                    <? /* if ($arResult["SECURE_AUTH"]): ?>
                                      <span class="bx-auth-secure" id="bx_auth_secure" title="<? echo GetMessage("AUTH_SECURE_NOTE") ?>" style="display:none">
                                      <div class="bx-auth-secure-icon"></div>
                                      </span>
                                      <noscript>
                                      <span class="bx-auth-secure" title="<? echo GetMessage("AUTH_NONSECURE_NOTE") ?>">
                                      <div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
                                      </span>
                                      </noscript>
                                      <script type="text/javascript">
                                      document.getElementById('bx_auth_secure').style.display = 'inline-block';
                                      </script>
                                      <? endif */ ?>
                                    <?
                                    break;
                                case "CONFIRM_PASSWORD":
                                    ?>
                                    <?
                                    if (in_array('WRONG_'.$FIELD, $arResult['CODE_ERRORS'])) {
                                        _er('REG_FORM_WRONG_'.$FIELD);
                                    }
                                    ?>
                                    <input <?
                                /*if ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y") {
                                    echo 'required=""';
                                }*/
                                ?> size="30" type="password" name="REGISTER[<?= $FIELD ?>]" value="<?= $arResult["VALUES"][$FIELD] ?>" autocomplete="off" ><?
                                    break;

                                case "NAME":
                                    ?>
                                    <?
                                    if (in_array('WRONG_'.$FIELD, $arResult['CODE_ERRORS'])) {
                                        _er('REG_FORM_WRONG_'.$FIELD);
                                    }
                                    ?>
                                    <input <?
                                    /*if ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y") {
                                        echo 'required=""';
                                    }*/
                                    ?> size="30" type="text" name="REGISTER[<?= $FIELD ?>]" value="<?= $arResult["VALUES"][$FIELD] ?>" autocomplete="off" ><?
                                    break;

                                case "LAST_NAME":
                                    ?>
                                    <?
                                    if (in_array('WRONG_'.$FIELD, $arResult['CODE_ERRORS'])) {
                                        _er('REG_FORM_WRONG_'.$FIELD);
                                    }
                                    ?>
                                    <input <?
                                    /*if ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y") {
                                        echo 'required=""';
                                    }*/
                                    ?> size="30" type="text" name="REGISTER[<?= $FIELD ?>]" value="<?= $arResult["VALUES"][$FIELD] ?>" autocomplete="off" ><?
                                    break;

                                case "PERSONAL_COUNTRY":
                                case "WORK_COUNTRY":
                                    ?><select <?
                                if ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y") {
                                    echo 'required=""';
                                }
                                ?> class="form-control" name="REGISTER[<?= $FIELD ?>]"><?
                                    foreach ($arResult["COUNTRIES"]["reference_id"] as $key => $value) {
                                        ?><option value="<?= $value ?>"<? if ($value == $arResult["VALUES"][$FIELD]): ?> selected="selected"<? endif ?>><?= $arResult["COUNTRIES"]["reference"][$key] ?></option>
                                        <?
                                    }
                                    ?></select><?
                                break;

                            case "PERSONAL_PHOTO":
                            case "WORK_LOGO":
                                ?><input <?
                                    if ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y") {
                                        echo 'required=""';
                                    }
                                    ?> size="30" type="file" name="REGISTER_FILES_<?= $FIELD ?>"><?
                                    break;

                                case "PERSONAL_NOTES":
                                case "WORK_NOTES":
                                    ?><textarea <?
                                if ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y") {
                                    echo 'required=""';
                                }
                                ?> cols="30" rows="5" name="REGISTER[<?= $FIELD ?>]"><?= $arResult["VALUES"][$FIELD] ?></textarea><?
                                    break;
                                default:
                                    if ($FIELD == "PERSONAL_BIRTHDAY"):
                                        ?><small><?= $arResult["DATE_FORMAT"] ?></small><br /><? endif;
                                    ?><input <?
                                if ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y") {
                                    echo 'required=""';
                                }
                                ?> size="30" type="text" name="REGISTER[<?= $FIELD ?>]" value="<?= $arResult["VALUES"][$FIELD] ?>"><?
                                    if ($FIELD == "PERSONAL_BIRTHDAY")
                                        $APPLICATION->IncludeComponent(
                                                'bitrix:main.calendar', '', array(
                                            'SHOW_INPUT' => 'N',
                                            'FORM_NAME' => 'regform',
                                            'INPUT_NAME' => 'REGISTER[PERSONAL_BIRTHDAY]',
                                            'SHOW_TIME' => 'N'
                                                ), null, array("HIDE_ICONS" => "Y")
                                        );
                                    ?><? }
                            ?>
                            <? // endif  ?>

                            </div>
                        </div>
                    </div>
                <? endforeach ?>


                    <? // ********************* User properties ***************************************************?>
                    <?
                    if ($arResult["USER_PROPERTIES"]["SHOW"] == "Y"):
                        $arAgentFields = array(
                            0 => "UF_LEGAL_NAME",
                            1 => "UF_LEGAL_ADDRESS",
                            2 => "UF_BANK_NAME",
                            3 => "UF_BANK_ADDRESS",
                            4 => "UF_BANK_CODE",
                            5 => "UF_CHECKING_ACCOUNT",
                            6 => "UF_OKPO",
                            7 => "UF_UNP",
                        );
                        $agentFieldsHtml = $simpleClientFieldsHtml = $html = '';
                        ?>

                        <?
                        foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField):

                            $html = '<div class="ts-width-100 ts-mb-2"><div class="ts-input form-block">
                                <label class="ts-d-none form-label" for="'.$FIELD_NAME.'"></label>
                                <div class="placeholder">' . $arUserField["EDIT_FORM_LABEL"] . '</div>';

                            $html .= '<div class="validation"></div><div class="input">';

                            ob_start();
                            $APPLICATION->IncludeComponent(
                                    "bitrix:system.field.edit", $arUserField["USER_TYPE"]["USER_TYPE_ID"], array("bVarsFromForm" => $arResult["bVarsFromForm"], "arUserField" => $arUserField, "form_name" => "regform"), null, array("HIDE_ICONS" => "Y"));

                            $html .= ob_get_contents();
                            ob_end_clean();
                            $html .= '</div></div></div>';

                            if (in_array($FIELD_NAME, $arAgentFields)) {

                                $agentFieldsHtml .= $html;
                            } else {

                                $simpleClientFieldsHtml .= $html;
                            }
                        endforeach;
                        ?>

                        <? if (strlen($agentFieldsHtml) > 0): ?>

                            <div class="ts-col-24 ts-mb-2 confirm-terms ts-d-none ts-px-0 ts-md-px-2">
                                <div class="ts-checkbox ts-w-100 save_param"><i class="icon fa" disabled></i>
                                    <label for="IS_AGENT" class="ts-pl-1 ts-py-0 ts-mx-0 ts-my-0"><?=GetMessage("IS_AGENT")?></label>
                                    <input type="checkbox" <?
                                    if ($arParams["USER_TYPE"] == "agent" || isset($_POST['IS_AGENT']) && $_POST['IS_AGENT'] == 'Y') {
                                        echo 'checked=""';
                                    }
                                    ?> id="is-agent-btn" type="radio" name="IS_AGENT" value="Y">
                                </div>
                            </div>
                            <div id="agent-fields-area" class="ts-col-24 ts-col-lg-12  ts-px-0 ts-md-px-2 <? if ($arParams["USER_TYPE"] == "agent" || $_POST['IS_AGENT'] == 'Y'): ?> show<? else: ?> hide<? endif ?>">
                                <?= $agentFieldsHtml; ?>
                            </div>
                            <div id="client-fields-area" <? if ($arParams["USER_TYPE"] == "client" || $_POST['IS_AGENT'] != 'Y'): ?>class="show"<? else: ?>class="hide"<? endif ?>>
                                <?= $simpleClientFieldsHtml; ?>
                            </div>
                        <? endif ?>
                    <? endif; ?>
                    <? // ******************** /User properties ***************************************************   ?>

                    <?
                    /* CAPTCHA */
                    if ($arResult["USE_CAPTCHA"] == "Y") {
                        ?>
                        <input type="hidden" name="captcha_sid" value="<?= $arResult["CAPTCHA_CODE"] ?>" />

                        <div class="ts-col-8">
                            <div class="ts-captcha ts-mb-2">
                                <div class="bx-captcha"><img src="/bitrix/tools/captcha.php?captcha_sid=<?= $arResult["CAPTCHA_CODE"] ?>" width="180" height="40" alt="CAPTCHA" /></div>
                            </div>
                            <div class="ts-input ts-mb-2 form-block<? if (in_array('WRONG_CAPTCHA', $arResult['CODE_ERRORS']) || in_array('WRONG_ENTERED_CAPTCHA', $arResult['CODE_ERRORS'])): ?> has-error<? endif ?>">
                                <label for="captcha_word" class="ts-d-none"></label>
                                <div class="placeholder"><?=GetMessage("REGISTER_CAPTCHA_TITLE")?></div>
                                <div class="validation"></div>
                                <div class="input">
                                    <input class="form__input" type="text" name="captcha_word" maxlength="50" value="">
                                </div>
                            </div>
                        </div>
                        <?
                    }
                    /* !CAPTCHA */
                    ?>

                    <!-- Submit -->
                <div class="ts-col-24 ts-col-lg-12 ts-px-0 ts-md-px-2">
                    <div class="ts-submit ts-max-width-370 ts-mx-0">
                        <input class="c-button w-100" id="reg-btn-<?= $arResult['HASH'] ?>" type="submit" name="register_submit_button" value="<?=GetMessage("AUTH_REGISTER")?>">
                    </div>
                </div>

            </div>

        </form>

        <noindex>
            <div class="ts-col-24 ts-my-3 ts-px-0 ts-md-px-2">
                <ul class="dop-pol ts-px-2">
                    <li class="ts-mb-1 ts-mt-0">
                         <a href="<?= $arParams["AUTH_URL"] ?>"><?= GetMessage("AUTH_AUTH") ?></a></li>
                    <li class="ts-mb-1 ts-mt-0"><a href="/personal/?forgot_password=yes" rel="nofollow"><?=GetMessage('AUTH_FORGOTPASSWORD')?></a></li>
                    </ul>


            </div>
        </noindex>

    </div>

<? endif ?>
</div>

<script>

    BX.ready(function () {
        
        var areaTypes = ['client', 'agent'];
        
        function initSwitcher (currentType) {
            BX.bind(BX('is-' + currentType + '-btn'), 'click', function (e) {

                for (var j = 0; j < areaTypes.length; j++) {
                    BX.addClass(BX(areaTypes[j] + '-fields-area'), 'hide');
                    BX.removeClass(BX(areaTypes[j] + '-fields-area'), 'show');
                }

                BX.addClass(BX(currentType + '-fields-area'), 'show');

            });
        }

        for (var i = 0; i < areaTypes.length; i++) {
            initSwitcher(areaTypes[i]);
        }

        var form = BX('reg-form-<?=$arResult['HASH']?>');

        var errors = [];

        function _cleanErrors() {

            BX.findChildren(form, {className: 'form-block'}, true).forEach(function (el) {

                BX.removeClass(el, 'has-error');
            });

            BX.findChildren(form, {className: 'validation'}, true).forEach(function (el) {

                BX.remove(el);
            });

        }

        function _scrollToError() {
            if(BX.findChild(form, {className: 'validation'}, true) !== null) {
                BX.scrollToNode(BX.findChild(form, {className: 'validation'}, true));
            }
        }

        function _er(code, div, message) {

            var messages = {

                REG_FORM_REGISTER_FAIL: '<?= GetMessage('REG_FORM_REGISTER_FAIL') ?>',
                REG_FORM_WRONG_EMAIL: '<?= GetMessage('REG_FORM_WRONG_EMAIL') ?>',
                REG_FORM_WRONG_ENTERED_EMAIL: '<?= GetMessage('REG_FORM_WRONG_ENTERED_EMAIL') ?>',
                REG_FORM_USER_NOT_FOUND: '<?= GetMessage('REG_FORM_USER_NOT_FOUND') ?>',
                REG_FORM_WRONG_PASSWORD: '<?= GetMessage('REG_FORM_WORNG_PASSWORD') ?>',
                REG_FORM_WRONG_ENTERED_PASSWORD: '<?= GetMessage('REG_FORM_WRONG_ENTERED_PASSWORD') ?>',
                REG_FORM_WRONG_CONFIRM_PASSWORD: '<?= GetMessage('REG_FORM_WRONG_CONFIRM_PASSWORD') ?>',
                REG_FORM_WRONG_LAST_NAME: '<?= GetMessage('REG_FORM_WRONG_LAST_NAME') ?>',
                REG_FORM_WRONG_NAME: '<?= GetMessage('REG_FORM_WRONG_NAME') ?>',
                REG_FORM_WRONG_PERSONAL_PHONE: '<?= GetMessage('REG_FORM_WRONG_PERSONAL_PHONE') ?>',
                REG_FORM_WRONG_UF_BANK_NAME: '<?= GetMessage('REG_FORM_WRONG_UF_BANK_NAME') ?>',
                REG_FORM_WRONG_UF_BANK_ADDRESS: '<?= GetMessage('REG_FORM_WRONG_UF_BANK_ADDRESS') ?>',
                REG_FORM_WRONG_UF_BANK_CODE: '<?= GetMessage('REG_FORM_WRONG_UF_BANK_CODE') ?>',
                REG_FORM_WRONG_UF_LEGAL_ADDRESS: '<?= GetMessage('REG_FORM_WRONG_UF_LEGAL_ADDRESS') ?>',
                REG_FORM_WRONG_UF_CHECKING_ACCOUNT: '<?= GetMessage('REG_FORM_WRONG_UF_CHECKING_ACCOUNT') ?>',
                REG_FORM_WRONG_UF_OKPO: '<?= GetMessage('REG_FORM_WRONG_UF_OKPO') ?>',
                REG_FORM_WRONG_UF_LEGAL_NAME: '<?= GetMessage('REG_FORM_WRONG_UF_LEGAL_NAME') ?>',
                REG_FORM_WRONG_UF_UNP: '<?= GetMessage('REG_FORM_WRONG_UF_UNP') ?>',
                REG_FORM_WRONG_CAPTCHA: '<?= GetMessage('REG_FORM_WRONG_CAPTCHA') ?>',

            }

            var tag = div === true ? 'div' : 'span';

            code = code || '';

            message = message || (typeof messages[code] !== 'undefined' ? messages[code] : '');

            return BX.create(tag, {attrs: {className: 'validation'}, text: ' ' + message});

        }

        function isValidEmailAddress(emailAddress) {
            var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
            return pattern.test(emailAddress);
        }

        function isValidPhone(phone) {
            str = phone;
            str.replace('/\s/', '');
            var pattern = new RegExp(/^\+?[0-9]{7,}$/i);
            return pattern.test(phone);
        }

        BX.bind(form, 'submit', function (e) {

            //e.preventDefault() ;

            var email = BX.findChild(this, {tagName: 'input', attribute: {name: 'REGISTER[EMAIL]'}}, true);
            var password = BX.findChild(this, {tagName: 'input', attribute: {name: 'REGISTER[PASSWORD]'}}, true);
            var name = BX.findChild(this, {tagName: 'input', attribute: {name: 'REGISTER[NAME]'}}, true);
            var lastName = BX.findChild(this, {tagName: 'input', attribute: {name: 'REGISTER[LAST_NAME]'}}, true);
            var phone = BX.findChild(this, {tagName: 'input', attribute: {name: 'REGISTER[PERSONAL_PHONE]'}}, true);
            var confirmPassword = BX.findChild(this, {tagName: 'input', attribute: {name: 'REGISTER[CONFIRM_PASSWORD]'}}, true);
            var agent = BX.findChild(this, {tagName: 'input', attribute: {name: 'IS_AGENT', id: 'is-agent-btn'}}, true);
            var captcha = BX.findChild(this, {tagName: 'input', attribute: {name: 'captcha_word'}}, true);

            var agent_info = [
                UF_LEGAL_NAME = BX.findChild(this, {tagName: 'input', attribute: {name: 'UF_LEGAL_NAME'}}, true),
                UF_LEGAL_ADDRESS = BX.findChild(this, {tagName: 'input', attribute: {name: 'UF_LEGAL_ADDRESS'}}, true),
                UF_BANK_NAME = BX.findChild(this, {tagName: 'input', attribute: {name: 'UF_BANK_NAME'}}, true),
                UF_BANK_ADDRESS = BX.findChild(this, {tagName: 'input', attribute: {name: 'UF_BANK_ADDRESS'}}, true),
                UF_BANK_CODE = BX.findChild(this, {tagName: 'input', attribute: {name: 'UF_BANK_CODE'}}, true),
                UF_CHECKING_ACCOUNT = BX.findChild(this, {tagName: 'input', attribute: {name: 'UF_CHECKING_ACCOUNT'}}, true),
                //UF_OKPO = BX.findChild(this, {tagName: 'input', attribute: {name: 'UF_OKPO'}}, true),
                UF_UNP = BX.findChild(this, {tagName: 'input', attribute: {name: 'UF_UNP'}}, true)
            ];

            /*var unp = BX.findChild(this, {tagName: 'input', attribute: {name: 'UF_INN'}}, true);
            var legal_name = BX.findChild(this, {tagName: 'input', attribute: {name: 'UF_LEGAL_NAME'}}, true);
            var contact_name = BX.findChild(this, {tagName: 'input', attribute: {name: 'UF_CONTACT_PERSON'}}, true);
            var country = BX.findChild(this, {tagName: 'input', attribute: {name: 'UF_COUNTRY'}}, true);
            var city = BX.findChild(this, {tagName: 'input', attribute: {name: 'UF_CITY'}}, true);
            var legal_address = BX.findChild(this, {tagName: 'input', attribute: {name: 'UF_LEGAL_ADDRESS'}}, true);
            var index = BX.findChild(this, {tagName: 'input', attribute: {name: 'UF_INDEX'}}, true);
            var fax = BX.findChild(this, {tagName: 'input', attribute: {name: 'UF_FAX'}}, true);*/


            _cleanErrors();

            if(email.value.length <= 0){

                BX.addClass(BX.findParent(email, {className: 'form-block'}), 'has-error');
                BX.insertAfter(_er('REG_FORM_WRONG_EMAIL'), BX.findPreviousSibling(BX.findParent(email,{className: 'input'}), {tag: 'label', attribute: {'for': 'REGISTER[EMAIL]'}}));

                e.preventDefault() ;

            } else {

                if(!isValidEmailAddress(email.value)){

                    BX.addClass(BX.findParent(email, {className: 'form-block'}), 'has-error');
                    BX.insertAfter(_er('REG_FORM_WRONG_ENTERED_EMAIL'), BX.findPreviousSibling(BX.findParent(email,{className: 'input'}), {tag: 'label', attribute: {'for': 'REGISTER[EMAIL]'}}));

                    e.preventDefault() ;

                }

            }

            if(password.value.length < 6){

                BX.addClass(BX.findParent(password, {className: 'form-block'}), 'has-error');
                BX.insertAfter(_er('REG_FORM_WRONG_PASSWORD'), BX.findPreviousSibling(BX.findParent(password,{className: 'input'}), {tag: 'label', attribute: {'for': 'REGISTER[PASSWORD]'}}));

                e.preventDefault() ;

            }
            if(confirmPassword.value != password.value){

                BX.addClass(BX.findParent(password, {className: 'form-block'}), 'has-error');
                BX.insertAfter(_er('REG_FORM_WRONG_CONFIRM_PASSWORD'), BX.findPreviousSibling(BX.findParent(password,{className: 'input'}), {tag: 'label', attribute: {'for': 'REGISTER[CONFIRM_PASSWORD]'}}));

                e.preventDefault() ;

            }
            if(phone.value.length <= 0 || !isValidPhone(phone.value)){

                BX.addClass(BX.findParent(phone, {className: 'form-block'}), 'has-error');
                BX.insertAfter(_er('REG_FORM_WRONG_PERSONAL_PHONE'), BX.findPreviousSibling(BX.findParent(phone,{className: 'input'}), {tag: 'label', attribute: {'for': 'REGISTER[PERSONAL_PHONE]'}}));

                e.preventDefault() ;

            }

            if(name.value.length < 2){

                BX.addClass(BX.findParent(name, {className: 'form-block'}), 'has-error');
                BX.insertAfter(_er('REG_FORM_WRONG_NAME'), BX.findPreviousSibling(BX.findParent(name,{className: 'input'}), {tag: 'label', attribute: {'for': 'REGISTER[NAME]'}}));

                e.preventDefault() ;

            }
            if(lastName.value.length < 2){

                BX.addClass(BX.findParent(lastName, {className: 'form-block'}), 'has-error');
                BX.insertAfter(_er('REG_FORM_WRONG_LAST_NAME'), BX.findPreviousSibling(BX.findParent(lastName,{className: 'input'}), {tag: 'label', attribute: {'for': 'REGISTER[LAST_NAME]'}}));

                e.preventDefault() ;

            }

            if(agent.value == "Y" && agent.checked) {

                for (key in agent_info){

                    if(agent_info[key].value.length <= 0) {

                        BX.addClass(BX.findParent(agent_info[key], {className: 'form-block'}), 'has-error');
                        BX.insertAfter(_er('REG_FORM_WRONG_'+agent_info[key].name), BX.findPreviousSibling(BX.findParent(agent_info[key],{className: 'input'}), {tag: 'label', attribute: {'for': agent_info[key].name}}));
                        /*BX.insertAfter(_er('REG_FORM_WRONG_'+agent_info[key].name), BX.findPreviousSibling(BX.findParent(agent_info[key], {className: 'input'}), {tag: 'DIV', attribute: {'for': agent_info[key].name}}));*/

                        e.preventDefault() ;
                    }

                }

            }
            if(typeof captcha !== "undefined" && captcha !== null && captcha.value.length <= 4){

                BX.addClass(BX.findParent(captcha, {className: 'form-block'}), 'has-error');
                BX.insertAfter(_er('REG_FORM_WRONG_CAPTCHA'), BX.findPreviousSibling(BX.findParent(captcha,{className: 'input'}), {tag: 'label', attribute: {'for': 'captcha_word'}}));

                e.preventDefault() ;
            }

            _scrollToError();


        });



    });

    function myCheckedCheckbox(node) {

        if(node.prop('checked')) {
            checkboxSaveMe.addClass('active');
        } else {
            checkboxSaveMe.removeClass('active');
        }

    }

    let input = document.querySelectorAll('.ts-input input');
    let checkboxSaveMe = $('.ts-checkbox i');
    let checkboxAgent = $('#is-agent-btn');

    if($(input).val().length != 0){
        $(input).closest('.ts-input').addClass('active');
    }

    myCheckedCheckbox(checkboxAgent);

    if(!checkboxSaveMe.attr("disabled")) {
        checkboxSaveMe.on('click', function (e) {
            $(this).toggleClass('active');
            if ($(this).hasClass('active')) {
                checkboxAgent.prop("checked", true);
                if ($("#agent-fields-area").hasClass('hide')) {
                    $("#agent-fields-area").removeClass('hide');
                    $("#agent-fields-area").addClass('show');
                    $("#client-fields-area").removeClass('show');
                }
            } else {
                checkboxAgent.prop("checked", false);
                if ($("#agent-fields-area").hasClass('show')) {
                    $("#agent-fields-area").removeClass('show');
                    $("#agent-fields-area").addClass('hide');
                    $("#client-fields-area").removeClass('show');
                }
            }

        });
    }
    $(input).on('focus',function(e){
        $(this).closest('.ts-input').addClass('active focus');

    });
    $(input).on('focusout',function(e){
        if($(this).val().length == 0){
            $(this).closest('.ts-input').removeClass('active focus');
        }
        else $(this).closest('.ts-input').removeClass('focus');

    });
    $(input).on('keyup',function(e){
        if($(this).val().length != 0){
            $(this).closest('.ts-input').addClass('active');
        }
        else{
            $(this).closest('.ts-input').removeClass('active');
        }
    });
</script>