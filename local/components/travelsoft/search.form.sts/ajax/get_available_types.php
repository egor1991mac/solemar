<?php
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define("NO_AGENT_STATISTIC",true);
define('NO_AGENT_CHECK', true);
define("PUBLIC_AJAX_MODE", true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

Bitrix\Main\Loader::includeModule("travelsoft.sts");

$arResponse = array(
    "result" => null,
    "error" => false
);

try {

    $request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();

    if(!$request->isPost())
        throw new Exception();

    $fromId = \travelsoft\sts\Utils::jsonDecode($request->getPost('fromId'), true);



    $dates = \travelsoft\sts\stores\Dates::get(array("filter" => array("UF_CITYFROM_ID" => $fromId)));

    $tourTypes = [];
    foreach ($dates as $date){
        if(!in_array($date['UF_BX_TOURTYPE_ID'], $tourTypes))
        {
            $tourTypes[] = $date['UF_BX_TOURTYPE_ID'];
        }
    }

    $arResponse['result'] = $tourTypes;

    echo \travelsoft\sts\Utils::jsonEncode($arResponse);

} catch(\Exception $e) {

    $arResponse["error"] = true;
    $arResponse["error_message"] = $e->getMessage();
    echo \travelsoft\sts\Utils::jsonEncode($arResponse);

}
