<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="bx-filter ts-mt-0 ts-mb-2 font_roboto">
    <div class="bx-filter-section container-fluid ts-px-2">

        <div class="sidebar-block">
            <h2 class="sidebar-title filter-name"><?=GetMessage('FILTER_TITLE')?></h2>
        </div>
        <form id="stsFilterForm" method='GET' action="<?= $arResult["ACTION_URL"]?>" autocomplete="off" class="smartfilter form form_filter">

            <?if(!empty($arResult["REQUEST"])):?>
                <?foreach ($arResult["REQUEST"] as $k=>$value):?>
                    <?if(!is_array($value)):?>
                        <input type="hidden" value="<?=$value?>" name="stsSearch[<?=$k?>]">
                    <?else:?>
                        <?if(!empty($arResult["REQUEST"][$k])):?>
                            <?foreach ($arResult["REQUEST"][$k] as $val):?>
                                <input type="hidden" name="stsSearch[<?=$k?>][]" value="<?=$val?>">
                            <?endforeach;?>
                        <?else:?>
                            <input type="hidden" name="stsSearch[<?=$k?>][]" value="">
                        <?endif?>
                    <?endif?>
                <?endforeach;?>
            <?endif?>



                <?if(isset($arParams["SHOW_PRICE_FILTER"]) && !empty($arParams["SHOW_PRICE_FILTER"]) && $arParams["SHOW_PRICE_FILTER"] == "Y"):?>
                    <div class="form__group form__price-group">
                        <div class="form__label"><?=GetMessage('PRICE_TITLE')?></div>
                        <div class="form__price-box">
                            <div class="form__price">
                                <input class="form__input" value="<?if(isset($arResult["REQUEST_PROP"]['priceFrom']) && !empty($arResult["REQUEST_PROP"]['priceFrom'])):?><?=$arResult["REQUEST_PROP"]['priceFrom']?><?endif?>" name="stsSearch[priceFrom]" type="text" placeholder="<?=GetMessage('PRICE_FROM')?>">
                            </div>
                            <div class="form__price">
                                <input class="form__input" value="<?if(isset($arResult["REQUEST_PROP"]['priceTo']) && !empty($arResult["REQUEST_PROP"]['priceTo'])):?><?=$arResult["REQUEST_PROP"]['priceTo']?><?endif?>" name="stsSearch[priceTo]" type="text" placeholder="<?=GetMessage('PRICE_TO')?>">
                            </div>
                        </div>
                        <div class="form__radios">
                            <div class="form__radio radio">
                                <input class="radio__input" value="BYN" type="radio" name="stsSearch[currency]" id="currency1" <?if($arResult["REQUEST_PROP"]['currency'] == "BYN"):?>checked<?endif;?>>
                                <label class="radio__label" for="currency1">BYN</label>
                            </div>
                            <div class="form__radio radio">
                                <input class="radio__input" value="EUR" type="radio" name="stsSearch[currency]" id="currency2" <?if($arResult["REQUEST_PROP"]['currency'] == "EUR"):?>checked<?endif;?>>
                                <label class="radio__label" for="currency2">EUR</label>
                            </div>
                        </div>
                    </div>
                <?endif?>

                <?if(!empty($arResult["PROPERTIES"])):?>
                    <?foreach ($arResult["PROPERTIES"] as $key => $prop):?>

                        <?if(!empty($prop["ITEMS"])):?>

                            <?$name = GetMessage($prop["NAME"]);?>

                            <?$prop_md = md5($key);?>


                            <div class="sidebar-block bx-filter-parameters-box ts-mb-2">
                                <span class="bx-filter-container-modef"></span>
                                <h2 class="collapse tour-filter__more_block bx-filter-parameters-box-title ts-pb-0" onclick="openCheckbox(this);">
                                    <span class="collapse__link ts-mx-2 bx-filter-parameters-box-hint"><?=GetMessage($prop["NAME"]."_TITLE")?></span>

                                    <div class="angle-role ts-d-xs-none">
                                        <i data-role="prop_angle" class="fa fa-angle-<?if(isset($arParams['GET_PARAMS_STS'][$name]) && !empty($arParams['GET_PARAMS_STS'][$name])):?>up<?else:?>down<?endif?>"></i>
                                    </div>
                                </h2>


                                <div class="bx-filter-block" data-role="bx_filter_block" <?if(isset($arParams['GET_PARAMS_STS'][$name]) && !empty($arParams['GET_PARAMS_STS'][$name])):?>style="display: block" <?endif?>>
                                    <div class="bx-filter-parameters-box-container">

                                    <?if($prop["NAME"] == "stars"):?>

                                        <div class="sidebar-score">

                                            <?foreach ($prop["ITEMS"] as $item):?>

                                                <?$value = (int)$item["NAME"];?>
                                                <?$item_md = md5($item["NAME"]);
                                                $item_id = "stsFilterForm_".$prop_md."_".$item_md;
                                                $item_value = $item["ID"];?>

                                                <div class="input-entry ts-mx-2 type-2 color-6">
                                                    <input class="checkbox-form<?if(in_array($item_value, $arResult["REQUEST_PROP"][GetMessage($prop["NAME"])])):?> checked<?endif?>" type="checkbox" value="<?=$item_value?>" name="stsSearch[<?=GetMessage($prop["NAME"])?>][]" id="<?=$item_id?>" <?if(in_array($item_value, $arResult["REQUEST_PROP"][GetMessage($prop["NAME"])])):?>checked<?endif?> onclick="smartFilter.click(this)">
                                                    <label class="clearfix" data-role="label_<?=$item_id?>" for="<?=$item_id?>" >

                                                        <?if(!empty($value)):?>
                                                            <span class="checkbox-text">
                                                                <?=$value?>
                                                                    <span class="rate">
                                                                    <span class="fa fa-star color-yellow"></span>
                                                                </span>
                                                            </span>
                                                        <?else:?>
                                                            <?=$item["NAME"]?>
                                                        <?endif?>

                                                       <!-- <span class="sp-check"><i class="fa fa-check"></i></span> -->
                                                    </label>
                                                </div>

                                            <?endforeach;?>

                                        </div>

                                    <?else:?>

                                        <div class="sidebar-rating">

                                            <?foreach ($prop["ITEMS"] as $item):?>

                                                <?$item_md = md5($item["NAME"]);
                                                $item_id = "stsFilterForm_".$prop_md."_".$item_md;
                                                $item_value = $item["ID"];?>

                                                <div class="input-entry color-5 ts-mx-2">
                                                    <input class="checkbox-form" id="<?=$item_id?>" type="checkbox" name="stsSearch[<?=GetMessage($prop["NAME"])?>][]" value="<?=$item_value?>" <?if(in_array($item_value, $arResult["REQUEST_PROP"][GetMessage($prop["NAME"])])):?>checked<?endif?> ">
                                                    <label class="clearfix" for="<?=$item_id?>" data-role="label_<?=$item_id?>">
                                                       <!-- <span class="sp-check"><i class="fa fa-check"></i></span> -->
                                                        <span class="checkbox-text"><?=$item["NAME"]?></span>
                                                    </label>
                                                </div>

                                            <?endforeach;?>

                                        </div>

                                    <?endif?>

                                    </div>
                                </div>

                            </div>

                        <?endif?>

                    <?endforeach;?>
                <?endif?>


            <div class="form__btns ts-d-flex ts-justify-content__space-between ts-pb-2">
                <input
                        class="form__btn form__btn-send"
                        type="submit"
                        id="set_filter"
                        name="set_filter"
                        value="<?= GetMessage("CT_BCSF_SET_FILTER") ?>"
                />
                <input
                        class="form__btn form__btn-reset"
                        type="submit"
                        id="del_filter"
                        name="del_filter"
                        value="<?= GetMessage("CT_BCSF_DEL_FILTER") ?>"
                />
            </div>
            <div class="clb"></div>
        </form>

    </div>
</div>

<script>

    function openCheckbox(elem) {

        var el = $(elem);

        if (el.find('.angle-role i').hasClass('fa-angle-down')){
            el.find('.angle-role i').removeClass('fa-angle-down').addClass('fa-angle-up');
            el.siblings('.bx-filter-block').show();
        } else {
            el.find('.angle-role i').removeClass('fa-angle-up').addClass('fa-angle-down');
            el.siblings('.bx-filter-block').hide();
        }

    }

    $(document).ready(function () {
        document.addEventListener('click',function(e){
            if($(e.target).has('.checked')){
                console.log($(e.target.closest('.input-entry ')).find('.checkbox-text').toggleClass('active'));
                //.addClass('active');
            }
        })

    })

</script>