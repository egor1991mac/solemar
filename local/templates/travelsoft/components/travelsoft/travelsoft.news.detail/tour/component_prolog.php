<?
global $APPLICATION;
$picturesID = Array();
$picturesDescription = Array();

$res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "ID" => $arParams["ELEMENT_ID"], "ACTIVE" => "Y"), false, false, Array("IBLOCK_ID", "ID", "NAME", "DETAIL_PAGE_URL"));
if ($ob = $res->GetNextElement()) {
    $arFields = $ob->GetFields();
    $arProps = $ob->GetProperties();
    $picturesID = $arProps["PICTURES"]["VALUE"];
    $picturesDescription = $arProps["PICTURES"]["DESCRIPTION"];
}

if (!empty($picturesID) && !empty($picturesDescription)) {
    ob_start(); ?>
    <div class="gallery">
        <?foreach ($picturesID as $pict):?>
            <?$picture = current(getSrc($pict, array("width"=>1100,"height"=>450), '', -1));?>
            <span style="background-image:url('<?=$picture?>');"></span>
        <?endforeach;?>
    </div>

    <?/*$APPLICATION->IncludeComponent(
        "travelsoft:travelsoft.slider",
        "",
        Array(
            "AUTO_PLAY_BIG" => "N",
            "AUTO_PLAY_HOVER_PAUSE_BIG" => "Y",
            "AUTO_PLAY_TIMEOUT_BIG" => "3000",
            "DATA_DESCRIPTION" => $picturesDescription,
            "DATA_SOURCE" => $picturesID,
            "DOT_BIG" => "N",
            "DOT_SMALL" => "N",
            "DO_NOT_INC_MAGNIFIC_POPUP" => "N",
            "DO_NOT_INC_OWL_CAROUSEL" => "N",
            "HEIGHT" => "450",
            "INC_JQUERY" => "N",
            "ITEM_COUNT_BIG" => "1",
            "ITEM_COUNT_SMALL" => "6",
            "LAZY_LOAD_BIG" => "Y",
            "LAZY_LOAD_SMALL" => "Y",
            "MARGIN_PICTURES_BIG" => "10",
            "MARGIN_PICTURES_SMALL" => "10",
            "NAV_BIG" => "Y",
            "NAV_SMALL" => "Y",
            "NO_PHOTO_PATH" => NO_PHOTO_1100_450,
            "WIDTH" => "1100"
        )
    );*/
    $this->arParams["SLIDER"] = ob_get_clean();
}

ob_start();
$declension = !empty($arProps["CN_NAME_KUDA"]["VALUE"]) ? ' '.$arProps["CN_NAME_KUDA"]["VALUE"] : '';
$db_country = CIBlockElement::GetProperty(COUNTRIES_IBLOCK_ID, $arProps["COUNTRY"]["VALUE"], array("sort" => "asc"), Array("CODE"=>"CN_NAME_KUDA"));
if($ar_country = $db_country->Fetch()){
    if(!empty($ar_country["VALUE"])){
        $declension = ' '.$ar_country["VALUE"];
    }
}
$GLOBALS["arFilterToursCountry"] = array("ACTIVE"=>"Y", "PROPERTY_COUNTRY"=>$arProps["COUNTRY"]["VALUE"], "PROPERTY_RECOMMEND_VALUE"=>"Y", "!ID"=>(int)$arFields["ID"]);
$APPLICATION->IncludeComponent(
    "travelsoft:travelsoft.news.list",
    "tours_slider",
    array(
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "ADD_SECTIONS_CHAIN" => "N",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "N",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "N",
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "A",
        "CHECK_DATES" => "Y",
        "COMPONENT_TEMPLATE" => "tours_slider",
        "DESCRIPTION_LINK" => "",
        "DESCRIPTION_NAMELINK" => "",
        "DETAIL_URL" => "",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "DISPLAY_DATE" => "Y",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "DISPLAY_TOP_PAGER" => "N",
        "FIELD_CODE" => array(
            0 => "",
            1 => "",
            2 => "",
        ),
        "FILTER_NAME" => "arFilterToursCountry",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "IBLOCK_ID" => TOURS_ID_IBLOCK,
        "IBLOCK_TYPE" => "tourpoduct",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "INCLUDE_SUBSECTIONS" => "N",
        "MESSAGE_404" => "",
        "NEWS_COUNT" => "500",
        "PAGER_BASE_LINK_ENABLE" => "N",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => ".default",
        "PAGER_TITLE" => "",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "PREVIEW_TRUNCATE_LEN" => "",
        "PROPERTY_CODE" => array(
            0 => "PICTURES",
            1 => "PREVIEW_TEXT",
            2 => "PRICE",
            3 => "CURRENCY",
            4 => "DATE",
            5 => "DAYS",
        ),
        "SET_BROWSER_TITLE" => "N",
        "SET_LAST_MODIFIED" => "N",
        "SET_META_DESCRIPTION" => "N",
        "SET_META_KEYWORDS" => "N",
        "SET_STATUS_404" => "N",
        "SET_TITLE" => "N",
        "SHOW_404" => "N",
        "SORT_BY1" => "NAME",
        "SORT_BY2" => "ID",
        "SORT_ORDER1" => "ASC",
        "SORT_ORDER2" => "ASC",
        "TEXT_DESCRIPTION" => "",
        "TEXT_TITLE" => "Рекомендованные туры".$declension,
        "FOLDER_URL" => $arParams["DETAIL_PAGE"]
    ),
    false
);
$this->arParams["TOURS_SLIDER_FOR_COUNTRY"] = ob_get_clean();

ob_start();
$GLOBALS["arFilterManagersTour"] = array("ACTIVE"=>"Y", "ID"=>$arProps["MANAGERS"]["VALUE"]);
$APPLICATION->IncludeComponent(
    "travelsoft:travelsoft.news.list",
    "managers_right",
    array(
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "ADD_SECTIONS_CHAIN" => "N",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "N",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "N",
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "A",
        "CHECK_DATES" => "Y",
        "COMPONENT_TEMPLATE" => "managers_right",
        "DESCRIPTION_LINK" => "",
        "DESCRIPTION_NAMELINK" => "",
        "DETAIL_URL" => "",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "DISPLAY_DATE" => "Y",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "DISPLAY_TOP_PAGER" => "N",
        "FIELD_CODE" => array(
            0 => "",
            1 => "",
            2 => "",
        ),
        "FILTER_NAME" => "arFilterManagersTour",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "IBLOCK_ID" => 6,
        "IBLOCK_TYPE" => "company",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "INCLUDE_SUBSECTIONS" => "N",
        "MESSAGE_404" => "",
        "NEWS_COUNT" => "3",
        "PAGER_BASE_LINK_ENABLE" => "N",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => ".default",
        "PAGER_TITLE" => "",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "PREVIEW_TRUNCATE_LEN" => "",
        "PROPERTY_CODE" => array(
            0 => "PICTURES",
            1 => "PHONE",
            2 => "SKYPE",
            3 => "LINK_FORM",
        ),
        "SET_BROWSER_TITLE" => "N",
        "SET_LAST_MODIFIED" => "N",
        "SET_META_DESCRIPTION" => "N",
        "SET_META_KEYWORDS" => "N",
        "SET_STATUS_404" => "N",
        "SET_TITLE" => "N",
        "SHOW_404" => "N",
        "SORT_BY1" => "NAME",
        "SORT_BY2" => "ID",
        "SORT_ORDER1" => "ASC",
        "SORT_ORDER2" => "ASC",
        "TEXT_DESCRIPTION" => "",
        "TEXT_TITLE" => GetMessage('MANAGERS_TITLE'),
    ),
    false
);
$this->arParams["MANAGERS_FOR_TOUR"] = ob_get_clean();

ob_start();
$APPLICATION->IncludeComponent(
    "travelsoft:travelsoft.news.list",
    "countries",
    array(
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "ADD_SECTIONS_CHAIN" => "N",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "N",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "N",
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "A",
        "CHECK_DATES" => "Y",
        "COMPONENT_TEMPLATE" => "countries",
        "DESCRIPTION_LINK" => "",
        "DESCRIPTION_NAMELINK" => "",
        "DETAIL_URL" => "",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "DISPLAY_DATE" => "Y",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "DISPLAY_TOP_PAGER" => "N",
        "FIELD_CODE" => array(
            0 => "",
            1 => "",
            2 => "",
        ),
        "FILTER_NAME" => "arFilterReviewsCountry",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "IBLOCK_ID" => COUNTRIES_IBLOCK_ID,
        "IBLOCK_TYPE" => "directories",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "INCLUDE_SUBSECTIONS" => "N",
        "MESSAGE_404" => "",
        "NEWS_COUNT" => "500",
        "PAGER_BASE_LINK_ENABLE" => "N",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => ".default",
        "PAGER_TITLE" => "",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "PREVIEW_TRUNCATE_LEN" => "",
        "PROPERTY_CODE" => array(
            0 => "FLAG",
            1 => "RECOMMEND",
            2 => "",
        ),
        "SET_BROWSER_TITLE" => "N",
        "SET_LAST_MODIFIED" => "N",
        "SET_META_DESCRIPTION" => "N",
        "SET_META_KEYWORDS" => "N",
        "SET_STATUS_404" => "N",
        "SET_TITLE" => "N",
        "SHOW_404" => "N",
        "SORT_BY1" => "SORT",
        "SORT_BY2" => "NAME",
        "SORT_ORDER1" => "ASC",
        "SORT_ORDER2" => "ASC",
        "TEXT_DESCRIPTION" => "",
        "TEXT_TITLE" => "",
    ),
    false
);
$this->arParams["COUNTRIES_LIST"] = ob_get_clean();

ob_start();
$APPLICATION->IncludeComponent(
    "bitrix:main.include",
    "",
    Array(
        "AREA_FILE_SHOW" => "file",
        "AREA_FILE_SUFFIX" => "inc",
        "EDIT_TEMPLATE" => "",
        "PATH" => SITE_DIR . "include/sidebarSubscription.php"
    )
);
$this->arParams["SIDEBAR_SUBSCRIPTION"] = ob_get_clean();

ob_start();
$APPLICATION->IncludeComponent(
    "bitrix:main.include",
    "",
    Array(
        "AREA_FILE_SHOW" => "file",
        "AREA_FILE_SUFFIX" => "inc",
        "EDIT_TEMPLATE" => "",
        "PATH" => SITE_DIR . "include/sidebarWidgets.php"
    )
);
$this->arParams["SIDEBAR_WIDGETS"] = ob_get_clean();

    ob_start();
    $APPLICATION->IncludeComponent(
        "travelsoft:search.form.sts",
        "detail",
        Array(
            "ACTION_URL" => $arFields["DETAIL_PAGE_URL"],
            "GET_PARAMS_STS" => $_REQUEST["stsSearch"],
            "SHOW_PANEL_HEADING" => "N",
            "PREDLOG" => "N",
            "COUNTRY_ID" => $arProps["COUNTRY"]["VALUE"][0]
        )
    );
    $this->arParams["SEARCH_FORM_STS"] = ob_get_clean();

    ob_start();
    $APPLICATION->IncludeComponent(
        "travelsoft:page.search.result.sts",
        "hotel",
        Array(
            "GET_PARAMS_STS" => $_REQUEST["stsSearch"],
            "BOOKING_PAGE" => "/booking/",
            "COUNTRY_ID" => $arProps["COUNTRY"]["VALUE"][0]
        )
    );
    $this->arParams["PAGE_SEARCH_RESULT_STS"] = ob_get_clean();

?>
