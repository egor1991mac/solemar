<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2014 Bitrix
 */
/**
 * Bitrix vars
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @param array $arParams
 * @param array $arResult
 * @param CBitrixComponentTemplate $this
 * @var string $templateName
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

CJSCore::Init(array('ajax'));

$arResult['HASH'] = md5(serialize($arParams) . $templateName);


function _er(string $code, bool $div = false, string $message = '') {

    $message = strlen($message) > 0 ? $message : GetMessage($code);

    if ($div) {
        echo '<div class="error">' . $message . '</div>';
    } else {
        echo '<span class="error">' . $message . '</span>';
    }
}

?>
<div class="bx-auth-reg">

    <? if ($USER->IsAuthorized()): ?>

        <p><? echo GetMessage("MAIN_REGISTER_AUTH") ?></p>

    <? else: ?>
        <?
        if (count($arResult["ERRORS"]) > 0):
            foreach ($arResult["ERRORS"] as $key => $error)
                if (intval($key) == 0 && $key !== 0)
                    $arResult["ERRORS"][$key] = str_replace("#FIELD_NAME#", "&quot;" . GetMessage("REGISTER_FIELD_" . $key) . "&quot;", $error);

            ShowError(implode("<br />", $arResult["ERRORS"]));

        elseif ($arResult["USE_EMAIL_CONFIRMATION"] === "Y"):
            ?>
            <p><? echo GetMessage("REGISTER_EMAIL_WILL_BE_SENT") ?></p>
        <? endif ?>

        <form method="post" action="<?= POST_FORM_ACTION_URI ?>" name="regform" id="reg-form-<?=$arResult['HASH']?>" enctype="multipart/form-data">
            <input name="HASH" type="hidden" value="<?= $arResult['HASH'] ?>">
            <input type="hidden" name="REGISTER[LOGIN]" value="temp_login">
            <?
            if ($arResult["BACKURL"] <> ''):
                ?>
                <input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>" />
                <?
            endif;
            ?>
            <fieldset>
                <legend><b><?= GetMessage("AUTH_REGISTER") ?></b></legend>
                <div class="contact-info-block">
                    <? foreach ($arResult["SHOW_FIELDS"] as $FIELD): ?>
                        <?
                        /* if ($FIELD == "AUTO_TIME_ZONE" && $arResult["TIME_ZONE_ENABLED"] == true): ?>
                          <tr>
                          <td><? echo GetMessage("main_profile_time_zones_auto") ?><? if ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y"): ?><span class="starrequired">*</span><? endif ?></td>
                          <td>
                          <select name="REGISTER[AUTO_TIME_ZONE]" onchange="this.form.elements['REGISTER[TIME_ZONE]'].disabled = (this.value != 'N')">
                          <option value=""><? echo GetMessage("main_profile_time_zones_auto_def") ?></option>
                          <option value="Y"<?= $arResult["VALUES"][$FIELD] == "Y" ? " selected=\"selected\"" : "" ?>><? echo GetMessage("main_profile_time_zones_auto_yes") ?></option>
                          <option value="N"<?= $arResult["VALUES"][$FIELD] == "N" ? " selected=\"selected\"" : "" ?>><? echo GetMessage("main_profile_time_zones_auto_no") ?></option>
                          </select>
                          </td>
                          </tr>
                          <tr>
                          <td><? echo GetMessage("main_profile_time_zones_zones") ?></td>
                          <td>
                          <select name="REGISTER[TIME_ZONE]"<? if (!isset($_REQUEST["REGISTER"]["TIME_ZONE"])) echo 'disabled="disabled"' ?>>
                          <? foreach ($arResult["TIME_ZONE_LIST"] as $tz => $tz_name): ?>
                          <option value="<?= htmlspecialcharsbx($tz) ?>"<?= $arResult["VALUES"]["TIME_ZONE"] == $tz ? " selected=\"selected\"" : "" ?>><?= htmlspecialcharsbx($tz_name) ?></option>
                          <? endforeach ?>
                          </select>
                          </td>
                          </tr>
                          <? else: */
                        if ($FIELD == 'LOGIN') {
                            continue;
                        }
                        ?>
                        <div class="form-group <? if (in_array('WRONG_'.$FIELD, $arResult['CODE_ERRORS']) || in_array('USER_NOT_FOUND', $arResult['CODE_ERRORS']) || in_array('WRONG_ENTERED_'.$FIELD, $arResult['CODE_ERRORS'])): ?>has-error<? endif ?>">
                            <label for="REGISTER[<?= $FIELD ?>]"><?= GetMessage("REGISTER_FIELD_" . $FIELD) ?>:</label>
                            <?
                            switch ($FIELD) {

                                case "EMAIL":
                                    ?>
                                    <?
                                    if (in_array('WRONG_'.$FIELD, $arResult['CODE_ERRORS'])) {
                                        _er('REG_FORM_WRONG_'.$FIELD);
                                    }
                                    if (in_array('USER_NOT_FOUND', $arResult['CODE_ERRORS'])) {
                                        _er('REG_FORM_USER_NOT_FOUND');
                                    }
                                    if (in_array('REGISTER_FAIL', $arResult['CODE_ERRORS'])) {
                                        _er('REG_FORM_REGISTER_FAIL');
                                    }
                                    ?>
                                    <input <?
                                    /*if ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y") {
                                        echo 'required=""';
                                    }*/
                                    ?> class="form-control" type="email" name="REGISTER[<?= $FIELD ?>]" value="<?= $arResult["VALUES"][$FIELD] ?>">
                                    <?
                                    break;

                                case "PERSONAL_PHONE":
                                    ?>
                                    <?
                                    if (in_array('WRONG_'.$FIELD, $arResult['CODE_ERRORS'])) {
                                        _er('REG_FORM_WRONG_'.$FIELD);
                                    }
                                    ?>
                                    <input <?
                                    /*if ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y") {
                                        echo 'required=""';
                                    }*/
                                    ?> class="form-control" size="30" type="tel" name="REGISTER[<?= $FIELD ?>]" value="<?= $arResult["VALUES"][$FIELD] ?>">
                                    <?
                                    break;

                                case "PASSWORD":
                                    ?>
                                    <?
                                    if (in_array('WORNG_'.$FIELD, $arResult['CODE_ERRORS'])) {
                                        _er('WORNG_'.$FIELD, false, $arResult['SYSTEM_ERRORS_MESSAGES']['WORNG_PASSWORD']);
                                    }
                                    if (in_array('WRONG_ENTERED_'.$FIELD, $arResult['CODE_ERRORS'])) {
                                        _er('REG_FORM_WRONG_ENTERED_'.$FIELD);
                                    }
                                    ?>
                                    <input <?
                                    /*if ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y") {
                                        echo 'required=""';
                                    }*/
                                    ?> class="form-control" size="30" type="password" name="REGISTER[<?= $FIELD ?>]" value="<?= $arResult["VALUES"][$FIELD] ?>" autocomplete="off">
                                    <? /* if ($arResult["SECURE_AUTH"]): ?>
                                      <span class="bx-auth-secure" id="bx_auth_secure" title="<? echo GetMessage("AUTH_SECURE_NOTE") ?>" style="display:none">
                                      <div class="bx-auth-secure-icon"></div>
                                      </span>
                                      <noscript>
                                      <span class="bx-auth-secure" title="<? echo GetMessage("AUTH_NONSECURE_NOTE") ?>">
                                      <div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
                                      </span>
                                      </noscript>
                                      <script type="text/javascript">
                                      document.getElementById('bx_auth_secure').style.display = 'inline-block';
                                      </script>
                                      <? endif */ ?>
                                    <?
                                    break;
                                case "CONFIRM_PASSWORD":
                                    ?>
                                    <?
                                    if (in_array('WRONG_'.$FIELD, $arResult['CODE_ERRORS'])) {
                                        _er('REG_FORM_WRONG_'.$FIELD);
                                    }
                                    ?>
                                    <input <?
                                    /*if ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y") {
                                        echo 'required=""';
                                    }*/
                                    ?> size="30" type="password" name="REGISTER[<?= $FIELD ?>]" value="<?= $arResult["VALUES"][$FIELD] ?>" autocomplete="off" class="form-control"><?
                                    break;

                                case "NAME":
                                    ?>
                                    <?
                                    if (in_array('WRONG_'.$FIELD, $arResult['CODE_ERRORS'])) {
                                        _er('REG_FORM_WRONG_'.$FIELD);
                                    }
                                    ?>
                                    <input <?
                                    /*if ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y") {
                                        echo 'required=""';
                                    }*/
                                    ?> size="30" type="password" name="REGISTER[<?= $FIELD ?>]" value="<?= $arResult["VALUES"][$FIELD] ?>" autocomplete="off" class="form-control"><?
                                    break;

                                case "LAST_NAME":
                                    ?>
                                    <?
                                    if (in_array('WRONG_'.$FIELD, $arResult['CODE_ERRORS'])) {
                                        _er('REG_FORM_WRONG_'.$FIELD);
                                    }
                                    ?>
                                    <input <?
                                    /*if ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y") {
                                        echo 'required=""';
                                    }*/
                                    ?> size="30" type="password" name="REGISTER[<?= $FIELD ?>]" value="<?= $arResult["VALUES"][$FIELD] ?>" autocomplete="off" class="form-control"><?
                                    break;

                                /* case "PERSONAL_GENDER":
                                  ?><select name="REGISTER[<?= $FIELD ?>]">
                                  <option value=""><?= GetMessage("USER_DONT_KNOW") ?></option>
                                  <option value="M"<?= $arResult["VALUES"][$FIELD] == "M" ? " selected=\"selected\"" : "" ?>><?= GetMessage("USER_MALE") ?></option>
                                  <option value="F"<?= $arResult["VALUES"][$FIELD] == "F" ? " selected=\"selected\"" : "" ?>><?= GetMessage("USER_FEMALE") ?></option>
                                  </select><?
                                  break; */

                                case "PERSONAL_COUNTRY":
                                case "WORK_COUNTRY":
                                    ?><select <?
                                    if ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y") {
                                        echo 'required=""';
                                    }
                                    ?> class="form-control" name="REGISTER[<?= $FIELD ?>]"><?
                                    foreach ($arResult["COUNTRIES"]["reference_id"] as $key => $value) {
                                        ?><option value="<?= $value ?>"<? if ($value == $arResult["VALUES"][$FIELD]): ?> selected="selected"<? endif ?>><?= $arResult["COUNTRIES"]["reference"][$key] ?></option>
                                        <?
                                    }
                                    ?></select><?
                                    break;

                                case "PERSONAL_PHOTO":
                                case "WORK_LOGO":
                                    ?><input <?
                                    if ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y") {
                                        echo 'required=""';
                                    }
                                    ?> class="form-control" size="30" type="file" name="REGISTER_FILES_<?= $FIELD ?>"><?
                                    break;

                                case "PERSONAL_NOTES":
                                case "WORK_NOTES":
                                    ?><textarea <?
                                    if ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y") {
                                        echo 'required=""';
                                    }
                                    ?> class="form-control" cols="30" rows="5" name="REGISTER[<?= $FIELD ?>]"><?= $arResult["VALUES"][$FIELD] ?></textarea><?
                                    break;
                                default:
                                    if ($FIELD == "PERSONAL_BIRTHDAY"):
                                        ?><small><?= $arResult["DATE_FORMAT"] ?></small><br /><? endif;
                                    ?><input <?
                                    if ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y") {
                                        echo 'required=""';
                                    }
                                    ?> class="form-control" size="30" type="text" name="REGISTER[<?= $FIELD ?>]" value="<?= $arResult["VALUES"][$FIELD] ?>"><?
                                    if ($FIELD == "PERSONAL_BIRTHDAY")
                                        $APPLICATION->IncludeComponent(
                                            'bitrix:main.calendar', '', array(
                                            'SHOW_INPUT' => 'N',
                                            'FORM_NAME' => 'regform',
                                            'INPUT_NAME' => 'REGISTER[PERSONAL_BIRTHDAY]',
                                            'SHOW_TIME' => 'N'
                                        ), null, array("HIDE_ICONS" => "Y")
                                        );
                                    ?><? }
                            ?>
                            <? // endif  ?>
                        </div>
                    <? endforeach ?>
                    <? // ********************* User properties ***************************************************?>
                    <?
                    if ($arResult["USER_PROPERTIES"]["SHOW"] == "Y"):
                        $arAgentFields = array(
                            0 => "UF_INN",
                            1 => "UF_COUNTRY",
                            2 => "UF_CITY",
                            3 => "UF_LEGAL_ADDRESS",
                            4 => "UF_INDEX",
                            5 => "UF_FAX",
                            6 => "UF_LEGAL_NAME",
                            7 => "UF_CONTACT_PERSON",
                        );
                        $agentFieldsHtml = $simpleClientFieldsHtml = $html = '';
                        ?>
                        <?
                        foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField):

                            $html = '<div class="form-group">
                            <label>' . $arUserField["EDIT_FORM_LABEL"] . ':';
                            if ($arUserField["MANDATORY"] == "Y") {
                                $html .= '<span class="starrequired">*</span>';
                            }

                            $html .= '</label>';

                            ob_start();
                            $APPLICATION->IncludeComponent(
                                "bitrix:system.field.edit", $arUserField["USER_TYPE"]["USER_TYPE_ID"], array("bVarsFromForm" => $arResult["bVarsFromForm"], "arUserField" => $arUserField, "form_name" => "regform"), null, array("HIDE_ICONS" => "Y"));

                            $html .= ob_get_contents();
                            ob_end_clean();
                            $html .= '</div>';

                            if (in_array($FIELD_NAME, $arAgentFields)) {

                                $agentFieldsHtml .= $html;
                            } else {

                                $simpleClientFieldsHtml .= $html;
                            }
                        endforeach;
                        ?>
                        <? if (strlen($agentFieldsHtml) > 0): ?>
                        <div class="checkbox">
                            <label><input <?
                                if ($_POST['IS_AGENT'] != 'Y') {
                                    echo 'checked=""';
                                }
                                ?> id="is-client-btn" type="radio" name="IS_AGENT" value="N"> <b>Я клиент</b></label>
                            <label><input <?
                                //if ($_POST['IS_AGENT'] == 'Y') {
                                echo 'checked=""';
                                //}
                                ?> id="is-agent-btn" type="radio" name="IS_AGENT" value="Y"> <b>Я агент</b></label>
                        </div>
                        <? //= strlen(trim($arParams["USER_PROPERTY_NAME"])) > 0 ? $arParams["USER_PROPERTY_NAME"] : GetMessage("USER_TYPE_EDIT_TAB")  ?>
                        <div id="agent-fields-area" class="show">
                            <?= $agentFieldsHtml; ?>
                        </div>
                        <div id="client-fields-area" <? if ($_POST['IS_AGENT'] != 'Y'): ?>class="show"<? else: ?>class="hide"<? endif ?>>
                            <?= $simpleClientFieldsHtml; ?>
                        </div>
                    <? endif ?>
                    <? endif; ?>
                    <? // ******************** /User properties ***************************************************   ?>
                    <?
                    /* CAPTCHA */
                    if ($arResult["USE_CAPTCHA"] == "Y") {
                        ?>
                        <div class="form-group">
                            <label><b><?= GetMessage("REGISTER_CAPTCHA_TITLE") ?></b></label><br>
                            <input type="hidden" name="captcha_sid" value="<?= $arResult["CAPTCHA_CODE"] ?>" />
                            <img src="/bitrix/tools/captcha.php?captcha_sid=<?= $arResult["CAPTCHA_CODE"] ?>" width="180" height="40" alt="CAPTCHA" /><br>
                            <label><?= GetMessage("REGISTER_CAPTCHA_PROMT") ?>:<span class="starrequired">*</span></label>
                            <input type="text" name="captcha_word" maxlength="50" value="" class="form-control" />
                        </div>
                        <?
                    }
                    /* !CAPTCHA */
                    ?>
                    <div class="form-group">
                        <input class="button" id="reg-btn-<?= $arResult['HASH'] ?>" type="submit" name="register_submit_button" value="<?= GetMessage("AUTH_REGISTER") ?>" />
                    </div>
                    <p><? echo $arResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"]; ?></p>
                    <p><span class="starrequired">*</span><?= GetMessage("AUTH_REQ") ?></p>
                    <p><a href="<?= $arParams["AUTH_URL"] ?>"><b><?= GetMessage("AUTH_AUTH") ?></b></a></p>
                </div>
            </fieldset>
        </form>
    <? endif ?>
</div>

<script>
    BX.ready(function () {

        var areaTypes = ['client', 'agent'];

        function initSwitcher (currentType) {
            BX.bind(BX('is-' + currentType + '-btn'), 'click', function (e) {

                for (var j = 0; j < areaTypes.length; j++) {
                    BX.addClass(BX(areaTypes[j] + '-fields-area'), 'hide');
                    BX.removeClass(BX(areaTypes[j] + '-fields-area'), 'show');
                }

                BX.addClass(BX(currentType + '-fields-area'), 'show');

            });
        }



        for (var i = 0; i < areaTypes.length; i++) {
            initSwitcher(areaTypes[i]);
        }



        var form = BX('reg-form-<?=$arResult['HASH']?>');

        function _cleanErrors() {

            BX.findChildren(form, {className: 'form-group'}, true).forEach(function (el) {

                BX.removeClass(el, 'has-error');
            });

            BX.findChildren(form, {className: 'error'}, true).forEach(function (el) {

                BX.remove(el);
            });

        }

        function _scrollToError() {
            BX.scrollToNode(BX.findChild(form, {className: 'error'}, true));
        }

        function _er(code, div, message) {

            var messages = {

                REG_FORM_REGISTER_FAIL: '<?= GetMessage('REG_FORM_REGISTER_FAIL') ?>',
                REG_FORM_WRONG_EMAIL: '<?= GetMessage('REG_FORM_WRONG_EMAIL') ?>',
                REG_FORM_USER_NOT_FOUND: '<?= GetMessage('REG_FORM_USER_NOT_FOUND') ?>',
                REG_FORM_WRONG_PASSWORD: '<?= GetMessage('REG_FORM_WORNG_PASSWORD') ?>',
                REG_FORM_WRONG_ENTERED_PASSWORD: '<?= GetMessage('REG_FORM_WRONG_ENTERED_PASSWORD') ?>',
                REG_FORM_WRONG_CONFIRM_PASSWORD: '<?= GetMessage('REG_FORM_WRONG_CONFIRM_PASSWORD') ?>',
                REG_FORM_WRONG_LAST_NAME: '<?= GetMessage('REG_FORM_WRONG_LAST_NAME') ?>',
                REG_FORM_WRONG_NAME: '<?= GetMessage('REG_FORM_WRONG_NAME') ?>',
                REG_FORM_WRONG_PERSONAL_PHONE: '<?= GetMessage('REG_FORM_WRONG_PERSONAL_PHONE') ?>',

            }

            var tag = div === true ? 'div' : 'span';

            code = code || '';

            message = message || (typeof messages[code] !== 'undefined' ? messages[code] : '');

            return BX.create(tag, {attrs: {className: 'error'}, text: ' ' + message});

        }

        // AJAX SUBMIT FORM

        BX.bind(form, 'submit', function (e) {

            var email = BX.findChild(this, {tagName: 'input', attribute: {name: 'REGISTER[EMAIL]'}}, true);
            var password = BX.findChild(this, {tagName: 'input', attribute: {name: 'REGISTER[PASSWORD]'}}, true);
            var name = BX.findChild(this, {tagName: 'input', attribute: {name: 'REGISTER[NAME]'}}, true);
            var lastName = BX.findChild(this, {tagName: 'input', attribute: {name: 'REGISTER[LAST_NAME]'}}, true);
            var phone = BX.findChild(this, {tagName: 'input', attribute: {name: 'REGISTER[PERSONAL_PHONE]'}}, true);
            var confirmPassword = BX.findChild(this, {tagName: 'input', attribute: {name: 'REGISTER[CONFIRM_PASSWORD]'}}, true);

            console.log(111);
            e.preventDefault() ;
            e.stopPropagation();

        });


        // FORM VALIDATION
        /*top.BX.bind(form, 'submit', function () {

         console.log(123);

         var email = top.BX.findChild(this, {tagName: 'input', attribute: {name: 'REGISTER[EMAIL]'}}, true);
         var password = top.BX.findChild(this, {tagName: 'input', attribute: {name: 'REGISTER[PASSWORD]'}}, true);
         var name = top.BX.findChild(this, {tagName: 'input', attribute: {name: 'REGISTER[NAME]'}}, true);
         var lastName = top.BX.findChild(this, {tagName: 'input', attribute: {name: 'REGISTER[LAST_NAME]'}}, true);
         var phone = top.BX.findChild(this, {tagName: 'input', attribute: {name: 'REGISTER[PERSONAL_PHONE]'}}, true);
         var confirmPassword = top.BX.findChild(this, {tagName: 'input', attribute: {name: 'REGISTER[CONFIRM_PASSWORD]'}}, true);


         top.BX.ajax.post(this.action,
         (top.BX.ajax.prepareForm(this, {
         EMAIL: email.value,
         PERSONAL_PHONE: phone.value

         })).data, function (resp) {

         console.log(resp);

         resp = JSON.parse(resp);

         if (typeof resp.success !== 'undefined' && resp.success === true) {
         // TEMPORARY SOLUTION
         top.BX.scrollToNode(form);
         top.BX.remove(top.BX('reg-btn'));
         top.BX.remove(top.BX.findChildByClassName(form, 'contact-info-block', true));

         } else if (typeof resp.errors !== 'undefined' && top.BX.type.isArray(resp.errors)) {

         _cleanErrors();

         for (var i = 0; i < resp.errors.length; i++) {

         switch (resp.errors[i]) {

         case 'WRONG_EMAIL':
         case 'REGISTER_FAIL':

         top.BX.addClass(top.BX.findParent(email, {className: 'form-group'}), 'has-error');
         top.BX.insertAfter(_er('REG_FORM_' + resp.errors[i]), top.BX.findPreviousSibling(email, {tag: 'LABEL', attribute: {'for': 'REGISTER[EMAIL]'}}));

         break;

         case 'USER_NOT_FOUND':

         top.BX.addClass(top.BX.findParent(email, {className: 'form-group'}), 'has-error');
         top.BX.insertAfter(_er('REG_FORM_USER_NOT_FOUND'), top.BX.findPreviousSibling(email, {tag: 'LABEL', attribute: {'for': 'REGISTER[EMAIL]'}}));

         break;

         case 'WRONG_PASSWORD':

         top.BX.addClass(top.BX.findParent(password, {className: 'form-group'}), 'has-error');
         top.BX.insertAfter(_er('REG_FORM_WRONG_PASSWORD', false, typeof resp.system_errors_messages.WRONG_PASSWORD !== 'undefined' ? resp.system_errors_messages.WRONG_PASSWORD : null), top.BX.findPreviousSibling(password, {tag: 'LABEL', attribute: {'for': 'REGISTER[PASSWORD]'}}));

         break;

         case 'WRONG_ENTERED_PASSWORD':

         top.BX.addClass(top.BX.findParent(password, {className: 'form-group'}), 'has-error');
         top.BX.insertAfter(_er('REG_FORM_WRONG_ENTERED_PASSWORD'), top.BX.findPreviousSibling(password, {tag: 'LABEL', attribute: {'for': 'REGISTER[PASSWORD]'}}));


         break;

         case 'WRONG_CONFIRM_PASSWORD':

         top.BX.addClass(top.BX.findParent(confirmPassword, {className: 'form-group'}), 'has-error');
         top.BX.insertAfter(_er('REG_FORM_WRONG_CONFIRM_PASSWORD'), top.BX.findPreviousSibling(confirmPassword, {tag: 'LABEL', attribute: {'for': 'REGISTER[CONFIRM_PASSWORD]'}}));

         break;

         case 'WRONG_LAST_NAME':

         top.BX.addClass(top.BX.findParent(lastName, {className: 'form-group'}), 'has-error');
         top.BX.insertAfter(_er('REG_FORM_WRONG_LAST_NAME'), top.BX.findPreviousSibling(lastName, {tag: 'LABEL', attribute: {'for': 'REGISTER[LAST_NAME]'}}));
         break;

         case 'WRONG_NAME':

         top.BX.addClass(top.BX.findParent(name, {className: 'form-group'}), 'has-error');
         top.BX.insertAfter(_er('REG_FORM_WRONG_NAME'), top.BX.findPreviousSibling(name, {tag: 'LABEL', attribute: {'for': 'REGISTER[NAME]'}}));
         break;

         case 'WRONG_PERSONAL_PHONE':

         top.BX.addClass(top.BX.findParent(phone, {className: 'form-group'}), 'has-error');
         top.BX.insertAfter(_er('REG_FORM_WRONG_PERSONAL_PHONE'), top.BX.findPreviousSibling(phone, {tag: 'LABEL', attribute: {'for': 'REGISTER[PERSONAL_PHONE]'}}));
         break;

         }
         }

         _scrollToError();
         return false;
         }

         });

         return top.BX.PreventDefault();

         });*/






    });
</script>