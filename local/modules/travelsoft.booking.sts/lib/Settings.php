<?php

namespace travelsoft\booking\sts;

use Bitrix\Main\Config\Option;

/**
 * Класс настроек модуля поискс туров
 *
 * @author juliya.sharlova
 * @copyright (c) 2017, travelsoft
 */
class Settings {

    const SECOND_IN_DAY = 86400;

    /**
     * Возвращает id таблицы операторов
     * @return int
     */
    public static function operatorsStoreId(): int {

        return (int) self::get("OPERATORS_HL");
    }

    /**
     * Возвращает id инфоблока отелей
     * @return int
     */
    public static function hotelsStoreId(): int {

        return (int) self::get("HOTELS_HL");
    }

    /**
     * Возвращает ID группы агентов
     * @return int
     */
    public static function agentsGroupId(): int {

        return (string) self::get("AGENT_GROUP_ID");
    }

    /**
     * Возвращает ID группы клиентов
     * @return int
     */
    public static function clientGroupId(): int {

        return (string) self::get("CLIENT_GROUP_ID");
    }

    /**
     * Возвращает признак автоматической активации при регистрации
     * @return boolean
     */
    public static function autoActivationAgents(): bool {

        return (string) self::get("AUTO_ACTIVATION_AGENTS") === "Y";
    }

    /**
     * Возвращает ID шаблона письма активации агента
     * @return int
     */
    public static function agentActiveMailTemplate(): int {

        return (string) self::get("AGENT_ACTIVE_MAIL_TEMPLATE");
    }

    /**
     * Возвращает ID шаблона письма регистрации нового агента
     * @return int
     */
    public static function agentRegisterMailTemplate(): int {

        return (string) self::get("AGENT_REGISTER_MAIL_TEMPLATE");
    }

    /**
     * Возвращает ID шаблона письма регистрации нового агента для агента для менеджера
     * @return int
     */
    public static function agentRegisterMailTemplateForManager(): int {

        return (string) self::get("AGENT_REGISTER_MAIL_TEMPLATE_FOR_MANAGER");
    }

    /**
     * Возвращает адрес шлюза ключевого оператора
     * @return string
     */
    public static function urlOperator(): string {

        return (string) self::get("URL_OPERATOR");
    }

    /**
     * @param string $name
     * @return string
     */
    protected static function get(string $name): string {

        return (string) Option::get("travelsoft.booking.sts", $name);
    }

}
