/**
 * Created by julia on 28.09.17.
 */
function StsApp (options, $, progress, PageNavigator, templater) {

    var result = [], resultDetail = [], resultDetail10 = [];
    var pageNavigator = null;
    var detailNavigator = {};
    var _cache = new Cache();
    var _this = this;


    var html_search_hotels =
        '<div class="col-lg-12 bx-filter-parameters-box bx-active">'+
        '<span class="bx-filter-container-modef"></span>'+
        '<div class="bx-filter-parameters-box-title">'+
        '<div style="cursor:pointer">Поиск по названию отеля<i data-role="prop_angle" class="fa fa-angle-up"></i>'+
        '</div>'+
        '</div>'+
        '<div class="bx-filter-block" data-role="bx_filter_block">'+
        '<div class="row bx-filter-parameters-box-container box-hotels">'+
        '<div class="col-xs-12">'+
        '<input class="form-control" id="name_hotel" onkeyup="app.searchHotels(this)" onpaste="app.searchHotels(this)" value="" name="stsFilter[HOTEL_NAME]">'+
        '</div>'+
        '</div>'+
        '<div style="clear: both"></div>'+
        '</div>'+
        '</div>';

    if (typeof options.request !== "object") {

        throw new Error("Не указаны параметры поиска");
        return false;
    }

    if (!$.isArray(options.operators) && options.operators.length <= 0) {

        throw new Error("Не заданы операторы поиска");
        return false;
    }

    if (typeof options.url !== "string" && options.url.length <= 0) {

        throw new Error("Не указан адрес запроса поиска");
        return false;
    }

    this.getItemsList = function () {

        var timerId = null;
        var _this = this;
        var pull_operators = 0;

        if(typeof options.request.operators === "undefined") {
            options.request.operators = options.operators;
        }

        /*progress.render(pull_operators);*/

        options.operators.forEach(function (id, index) {

            options.request.operator_id = id;

            $.ajax({
                method: "get",
                url: options.url,
                dataType: 'json',
                data: options.request,
                timeout: 20000,
                complete: function (jqXHR, textStatus) {

                    if(textStatus == "timeout") {

                        document.getElementById(options.mount).innerHTML = '<p>Ошибка сервера. Приносим свои извинения.</p>';

                    }
                    else if(textStatus == "success") {

                        var er = jqXHR.responseJSON;

                        if(er.error == true){
                            document.getElementById(options.mount).innerHTML = '<p>Ошибка сервера. Приносим свои извинения.</p>';
                        }

                    }

                },
                success: function (data) {

                    //console.log(data,123);

                    pull_operators++;
                    /*progress.render(pull_operators);*/

                    if(data.error !== true && typeof data === "object" && data.result !== null) {
                        data.result.forEach(function (el) {
                            result.push(el);
                        });
                    }

                    if(pull_operators === options.operators.length){

                        setTimeout(function () {

                            //модицикация результативного массива. группировка по отелям от операторов
                            result = _this.getSortList(_this.getGroupResultBxId(result));

                            pageNavigator = new PageNavigator(app.getResult(), 10, '<a class="pagination__link pagination__btn-link" onclick="app.onClickPageBtn(#page#)" href="javascript:void(0)">#content#</a>');

                            if(app.getResult().length > 0){
                                clearInterval(timerId);

                               /* var textAr = ["объект", "объекта", "объектов"];
                                $("#cnt_objects").html('<div><span>Найдено: <span class="cnt">' + app.getResult().length + '</span></span> ' + numWord(app.getResult().length,textAr) + '</div>').css("display","block");*/

                                _this.render(_this.getResultByPage(1), 1);

                                //TODO: поиск по названию отеля
                                /*document.getElementById("search_hotels_name").innerHTML = html_search_hotels;*/
                            }
                            else{
                                $("#cnt_objects").html('');

                                var html_m = $('#' + options.mount).html();

                                if(typeof html_m !== "undefined" && html_m.length > 0 && html_m.indexOf("Ошибка") > -1){

                                } else {
                                    /*document.getElementById(progress.getMountPoint()).innerHTML = '<p>Поиск не дал результатов. Попробуйте изменить параметры запроса.</p>';*/
                                    document.getElementById(options.mount).innerHTML = '<p>Поиск не дал результатов. Попробуйте изменить параметры запроса.</p>';
                                }
                            }


                        },1000);

                    }

                },
                error: function () {

                    pull_operators++;
                    /*progress.render(pull_operators);*/
                    if(pull_operators === options.operators.length){

                        setTimeout(function () {

                            //модицикация результативного массива. группировка по отелям от операторов

                            result = _this.getSortList(_this.getGroupResultBxId(result));

                            pageNavigator = new PageNavigator(app.getResult(), 10, '<a onclick="app.onClickPageBtn(#page#)" href="javascript:void(0)">#content#</a>');

                            if(app.getResult().length > 0){
                                clearInterval(timerId);
                                _this.render(_this.getResultByPage(1), 1);
                            }
                            else{
                                /*document.getElementById(progress.getMountPoint()).innerHTML = '<p>Поиск не дал результатов. Попробуйте изменить параметры запроса.</p>';*/
                                document.getElementById(options.mount).innerHTML = '<p>Поиск не дал результатов. Попробуйте изменить параметры запроса.</p>';
                            }



                        },1000);

                    }

                }

            });

        });

        timerId = setInterval(function() {
            if(result.length > 0){
                result = _this.getSortList(_this.getGroupResultBxId(result));
                _this.render(result, null);
            }
        }, 1000);

    };

    this.getItemsDetail = function (request, mount) {

        var request_ = options.request;
        var timerIdDetail = null;
        var pull_operators_detail = 0;
        var result_end_num = 0;

        if (typeof _cache.get(mount + 1) !== 'string') {

            resultDetail = [];

            document.getElementById(mount).innerHTML = '<div class="detail-preloader"></div>';

            if(typeof request === "object" && request){
                for(var key in request){
                    if(request.hasOwnProperty(key)){
                        request_[key] = request[key]
                    }
                }
            }

            request_['disablegroup'] = 'true';

            if(typeof request_.operators === "undefined") {
                request_.operators = options.operators;
            }

            request_.operators.forEach(function (id, index) {

                options.request.operator_id = id;

                $.ajax({
                    method: "get",
                    url: options.url,
                    dataType: 'json',
                    timeout: 20000,
                    data: request_,
                    complete: function (jqXHR, textStatus) {
                        if(textStatus == "timeout") {

                            document.getElementById(mount).innerHTML = '<p>Ошибка сервера. Приносим свои извинения.</p>';

                        }
                    },
                    success: function (data) {

                        pull_operators_detail++;

                        if(data.error !== true && typeof data === "object" && data.result !== null) {
                            data.result.forEach(function (el) {
                                resultDetail.push(el);
                            });
                        }


                        if(pull_operators_detail === request_.operators.length){

                            $('#'+mount).closest(".hotel-item").find(".thm-btn.detail").css({"display":"none"});
                            $('#'+mount).closest(".hotel-item").find(".thm-btn.dis").css({"display":"block"});
                            $('#'+mount).closest(".hotel-item").find(".more-btn").css({"display":"block"});

                            if(resultDetail.length > 0){

                                //сортировка
                                resultDetail = _this.getSortList(resultDetail);
                                //resultDetail = _this.getSortListRoomType(resultDetail);
                                //resultDetail = _this.getSortListDateFrom(resultDetail);

                                detailNavigator[request_.hotels[0]] = new PageNavigator(resultDetail, 10);

                                document.getElementById(mount).innerHTML = '';

                                _this.onClickDetailMoreBtn(mount,1,request_.hotels[0]);

                            } else {
                                document.getElementById(mount).innerHTML = '<p>Поиск не дал результатов.</p>';
                            }

                            $('#'+mount).css({"display":"block"});

                        }
                    },
                    error: function () {

                        pull_operators_detail++;
                        $('#'+mount).closest(".hotel-item").find(".thm-btn.detail").css({"display":"none"});
                        $('#'+mount).closest(".hotel-item").find(".thm-btn.dis").css({"display":"block"});
                        if(resultDetail.length > 0){

                            //сортировка
                            resultDetail = _this.getSortList(resultDetail);
                            //resultDetail = _this.getSortListRoomType(resultDetail);
                            //resultDetail = _this.getSortListDateFrom(resultDetail);

                            detailNavigator[request_.hotels[0]] = new PageNavigator(resultDetail, 10);

                            document.getElementById(mount).innerHTML = '';

                            _this.onClickDetailMoreBtn(mount,1,request_.hotels[0]);

                        } else {
                            document.getElementById(mount).innerHTML = '<p>Поиск не дал результатов.</p>';
                        }

                        $('#'+mount).css({"display":"block"});

                    }

                });

            });

        } else {

            $('#'+mount).closest(".hotel-item").find(".thm-btn.detail").css({"display":"none"});
            $('#'+mount).closest(".hotel-item").find(".thm-btn.dis").css({"display":"block"});
            $('#'+mount).closest(".hotel-item").find(".more-btn").css({"display":"block"});
            document.getElementById(mount).innerHTML += _cache.get(mount + 1);
            renderMoreBtnDetail(2,mount,request_.hotels[0]);
            $('#'+mount).css({"display":"block"});
        }

    };

    this.getItemsDetailHotel = function (mount) {

        var request_ = options.request;
        var timerIdDetail = null;
        var pull_operators_detail = 0;
        var result_end_num = 0;

        if (typeof _cache.get(mount + 1) !== 'string') {

            //document.getElementById(mount).innerHTML = '<div class="detail-preloader"></div>';

            request_['disablegroup'] = 'true';

            if(typeof request_.operators === "undefined") {
                request_.operators = options.operators;
            }

            request_.operators.forEach(function (id, index) {

                options.request.operator_id = id;

                $.ajax({
                    method: "get",
                    url: options.url,
                    dataType: 'json',
                    timeout: 20000,
                    data: request_,
                    complete: function (jqXHR, textStatus) {

                        if(textStatus == "timeout") {

                            pull_operators_detail++;

                            if(pull_operators_detail === request_.operators.length){

                                if(resultDetail.length > 0){

                                    //сортировка
                                    /*resultDetail = _this.getSortList(resultDetail);*/
                                    //resultDetail = _this.getSortListRoomType(resultDetail);
                                    //resultDetail = _this.getSortListRoomTypeCatType(resultDetail);
                                    resultDetail = _this.getSortListDateFrom(resultDetail);

                                    detailNavigator[request_.country] = new PageNavigator(resultDetail, 6);

                                    document.getElementById(mount).innerHTML = '';

                                    _this.onClickDetailMoreBtn(mount,1,request_.country,'hotel');

                                } else {

                                    var parentMount = document.getElementById(mount).closest(".hotel-item");
                                    parentMount.innerHTML = '<p>К сожалению, поиск не дал результатов.</br>Вы можете <a href="/online/">поискать другие варианты туров</a> или позвонить нам по телефону +375173881616.</p>';
                                }


                            }

                        }
                    },
                    success: function (data) {

                        pull_operators_detail++;

                        if(data.error !== true && typeof data === "object" && data.result !== null) {
                            data.result.forEach(function (el) {
                                resultDetail.push(el);
                            });
                        }

                        if(pull_operators_detail === request_.operators.length){

                            if(resultDetail.length > 0){

                                //сортировка
                                /*resultDetail = _this.getSortList(resultDetail);*/
                                //resultDetail = _this.getSortListRoomType(resultDetail);
                                //resultDetail = _this.getSortListRoomTypeCatType(resultDetail);
                                resultDetail = _this.getSortListDateFrom(resultDetail);

                                detailNavigator[request_.country] = new PageNavigator(resultDetail, 6);

                                document.getElementById(mount).innerHTML = '';

                                _this.onClickDetailMoreBtn(mount,1,request_.country,'hotel');

                            } else {

                                var parentMount = document.getElementById(mount).closest(".hotel-item");
                                parentMount.innerHTML = '<p>К сожалению, поиск не дал результатов.</br>Вы можете <a href="/online/">поискать другие варианты туров</a> или позвонить нам по телефону +375173881616.</p>';
                            }


                        }
                    },
                    error: function () {

                        pull_operators_detail++;
                        if(pull_operators_detail === request_.operators.length){

                            if(resultDetail.length > 0){

                                //сортировка
                                /*resultDetail = _this.getSortList(resultDetail);*/
                                //resultDetail = _this.getSortListRoomType(resultDetail);
                                //resultDetail = _this.getSortListRoomTypeCatType(resultDetail);
                                resultDetail = _this.getSortListDateFrom(resultDetail);

                                detailNavigator[request_.country] = new PageNavigator(resultDetail, 6);

                                document.getElementById(mount).innerHTML = '';

                                _this.onClickDetailMoreBtn(mount,1,request_.country,'hotel');

                            } else {

                                var parentMount = document.getElementById(mount).closest(".hotel-item");
                                parentMount.innerHTML = '<p>К сожалению, поиск не дал результатов.</br>Вы можете <a href="/online/">поискать другие варианты туров</a> или позвонить нам по телефону +375173881616.</p>';
                            }


                        }

                    }

                });

            });

        } else {
            document.getElementById(mount).innerHTML = _cache.get(mount + 1);
            renderMoreBtnDetail(2,mount,request_.country,"hotel");
            $('#'+mount).css({"display":"block"});
        }

    };

    //render result
    this.render = function (result, page, mount, templateName) {

        var mount_ = mount || options.mount;

        var htmlList = templater.generate(templateName || 'list',{items: result, services: options.services, bookingPage: ''});

        document.getElementById(mount_).innerHTML = htmlList;
        options.renderCallback();
        if (page) {
            document.getElementById(options.paginatorMount).innerHTML = pageNavigator.page(page).getHtml();
        }

    };

    //render detail result
    this.renderDetail = function (result, mount, callback, templateName) {

        var htmlDetailList = templater.generate(templateName || 'detail',{items: result, services: null, bookingPage: options.bookingPage});

        document.getElementById(mount).innerHTML += htmlDetailList;
        options.renderCallback();
        if(typeof callback === "function") {
            callback(htmlDetailList);
        }

    };

    this.filterByHotelName = function (result, name) {

        var resultHotels = [];

        result.forEach(function (obj) {

            name_h = new RegExp(name,'ig');
            if(name_h.test(obj["hotel"]["viewName"])){

                resultHotels.push(obj);

            }

        });

        return resultHotels;

    };

    this.searchHotels = function (hotel) {

        setTimeout(function () {

            var hotelName = hotel.value.toUpperCase();
            var resultHotels = !hotelName.length ? result : _this.filterByHotelName(result,hotelName);

            if(!resultHotels.length){
                document.getElementById(options.paginatorMount).innerHTML = '';
                document.getElementById(options.mount).innerHTML = '<div class="hotel-list-content"><p>Поиск не дал результатов. Попробуйте сменить параметры поиска.</p></div>';
            } else {
                pageNavigator = new PageNavigator(resultHotels, 10, '<a class="pagination__link pagination__btn-link c-button b-40 bg-dr-blue-2 hv-dr-blue-2-o" onclick="app.onClickPageBtn(#page#)" href="javascript:void(0)">#content#</a>');
                _this.render(_this.getResultByPage(1), 1);
            }

        }, 200);

    };

    this.getGroupResultBxId = function (result) {

        var groupResult = [], _result = [];

        result.forEach(function (obj, index) {

            if(typeof obj["hotel"]["id_bx"] !== "undefined" && obj["hotel"]["id_bx"] != 0){

                if(typeof groupResult[obj["hotel"]["id_bx"]] !== "undefined"){

                    if(obj["priceCurrency"]["price"] < _result[groupResult[obj["hotel"]["id_bx"]]]["priceCurrency"]["price"]) {

                        _result[groupResult[obj["hotel"]["id_bx"]]] = obj;

                    }

                } else {

                    groupResult[obj["hotel"]["id_bx"]] = _result.push(obj) - 1;

                }

            } else {

                _result.push(obj);

            }

        });

        return _result;

    };

    this.getSortList = function (result) {

        return result.sort(comparePrice)

    };

    this.getSortListRoomType = function (result) {

        result = result.sort(compareRoomType);
        return result.reverse();

    };

    this.getSortListRoomTypeCatType = function (result) {

        result = result.sort(compareRoomTypeCatType);
        return result.reverse();

    };

    this.getSortListDateFrom = function (result) {

        result = result.sort(compareDateFrom);
        return result;

    };

    this.getResultByPage = function (page) {

        $("div#progressBox").remove();
        return pageNavigator.page(page).getItems();

    };

    this.onClickPageBtn = function (page) {

        _cache.remove("poolDetailListBtns");
        this.render(this.getResultByPage(page), page);
        $("html, body").animate({scrollTop: $('#scrollToBox').offset().top - 70}, 600);

    };

    //get result
    this.getResult = function () {

        return result;

    };

    this.displayBtn = function  (mount) {

        $('#'+mount).closest(".hotel-item").find(".thm-btn.dis").css({"display":"none"});
        $('#'+mount).css({"display":"none"});
        $('#'+mount).closest(".hotel-item").find(".thm-btn.detail").css({"display":"block"});
        /*$('#'+mount).closest(".hotel-item").find(".more-btn").innerHTML += '';*/
        $('#'+mount).closest(".hotel-item").find(".more-btn").css({"display":"none"});

    };

    this.displayBtnDetailHotel = function  (mount) {

        $('#'+mount).closest(".hotel-item").find(".more-btn").css({"display":"none"});

    };

    this.onClickDetailMoreBtn = function (mount, page, hotelId, templateName) {

        if(typeof templateName == "undefined"){
            templateName = '';
        }
        if(typeof _cache.get(mount + page) === "string"){
            document.getElementById(mount).innerHTML += _cache.get(mount + page);
            if((page+1) <= detailNavigator[hotelId].pageCount()) {
                renderMoreBtnDetail(page+1, mount, hotelId, templateName);
            }
            else {
                getCacheMoreBtn(mount).html('');
            }
            return false;
        }

        var callback = function (data) { _cache.set(mount + page, data); return false;};

        if(!page) return false;

        if((page+1) <= detailNavigator[hotelId].pageCount()) {
            callback = function (data) {
                renderMoreBtnDetail(page+1, mount, hotelId, templateName);
                _cache.set(mount + page, data);
            }
        }
        else {
            getCacheMoreBtn(mount).html('');
        }

        this.renderDetail(detailNavigator[hotelId].page(page).getItems(), mount, callback, templateName);

    };

    function comparePrice(objA, objB) {
        return objA.priceCurrency.price - objB.priceCurrency.price;
    }

    function compareRoomType(objA, objB) {

        if (objA.roomCatType.viewName > objB.roomCatType.viewName) {
            return 1;
        }
        if (objA.roomCatType.viewName < objB.roomCatType.viewName) {
            return -1;
        }

        return 0;

        //return objA.roomCatType.viewName > objB.roomCatType.viewName;
    }

    function compareRoomTypeCatType(objA, objB) {

        var room_cat_type_A = objA.roomType.viewName + ' ' + objA.roomCatType.viewName;
        var room_cat_type_B = objB.roomType.viewName + ' ' + objB.roomCatType.viewName;

        if (room_cat_type_A > room_cat_type_B) {
            return 1;
        }
        if (room_cat_type_A < room_cat_type_B) {
            return -1;
        }

        return 0;

        //return objA.roomCatType.viewName > objB.roomCatType.viewName;
    }

    function compareDateFrom(objA, objB) {

        if (objA.tourDate.dateFrom > objB.tourDate.dateFrom) {
            return 1;
        }
        if (objA.tourDate.dateFrom < objB.tourDate.dateFrom) {
            return -1;
        }

        return 0;

        //return objA.roomCatType.viewName > objB.roomCatType.viewName;
    }

    function renderMoreBtnDetail (page, mount, hotelId, templateName) {

        templateName = templateName || '';
        getCacheMoreBtn(mount).html('<div onclick="app.onClickDetailMoreBtn(\'' + mount + '\', ' + page + ', ' + hotelId + ', \'' + templateName + '\')" class="more-btn-text"><span>показать еще</span></div>');

    }

    function getCacheMoreBtn(mount) {

        var poolDetailListBtns = _cache.get('poolDetailListBtns') || {};

        if(typeof poolDetailListBtns[mount + "more-btn"] === "object"){

            return poolDetailListBtns[mount + "more-btn"];

        }

        poolDetailListBtns[mount + "more-btn"] = $('#'+mount).closest('.hotel-item').find('.more-btn');

        _cache.set("poolDetailListBtns", poolDetailListBtns);

        return poolDetailListBtns[mount + "more-btn"];

    }

    function numWord(num, text, show_num) {

        num_text = '';
        num = num % 100;
        if (num > 19) {
            num = num % 10;
        }

        if(show_num){
            num_text = num + ' ';
        }

        switch (num) {
            case 1: {
                return num_text + text[0];
            }
            case 2:
            case 3:
            case 4: {
                return num_text + text[1];
            }
            default: {
                return num_text + text[2];
            }
        }

    }


}