<?php

/*
 * header 404 если не POST запрос со страницы
 * 
 * Статусы ответов:
 *                  0 - не удалось удалить позицию в корзине
 *                  1 - удаление позиции прошло удачно
 *                  2 - введён некорректный email
 *                  3 - удачная авторизация
 *                  4 - ошибка авторизации
 *                  5 - email и подтверждение email не совпадают
 *                  6 - успешная регистрация
 *                  7 - ошибка регистрации
 *                  8 - пользователя с введенным $email не существует
 *                  9 - пользователь с введенным $email существует
 */

define("STOP_STATISTICS", true);
define("NO_KEEP_STATISTIC", "Y");
define("NO_AGENT_STATISTIC", "Y");
define("DisableEventsCheck", true);
define("BX_SECURITY_SHOW_MESSAGE", true);

$documentRoot = filter_input(INPUT_SERVER, 'DOCUMENT_ROOT');
require_once($documentRoot . '/bitrix/modules/main/include/prolog_before.php');

use Bitrix\Main\Web\Json;

$request = Bitrix\Main\Application::getInstance()->getContext()->getRequest();

if (!check_bitrix_sessid() || !$request->isPost()) {
    $protocol = filter_input(INPUT_SERVER, 'SERVER_PROTOCOL');
    header($protocol . " 404 Not Found");
    exit;
}

try {
    
    $email = filter_var($request->getPost("email"), FILTER_VALIDATE_EMAIL);

    if ($request->getPost("action") === "check_auth") {

        if ($email) {

            if (bx_user_exists($email)) {

                throw new Exception(Json::encode(array("message" => "", "status" => 9)));
            }

            throw new Exception(Json::encode(array("message" => "", "status" => 8)));
        }

        throw new Exception(Json::encode(array("message" => "Wrong email", "status" => 2)));
    } elseif ($request->getPost("action") === "do_authorize") {

        $error = null;
        if (bx_authorize($email, $password, $error)) {
            throw new Exception(Json::encode(array("message" => null, "status" => 3)));
        }

        throw new Exception(Json::encode(array("message" => strip_tags($error), "status" => 4)));
    } elseif ($request->getPost("action") === "do_registration") {

        $error = null;

        $confirm_email = filter_var($request->getPost("confirm_email"), FILTER_VALIDATE_EMAIL);

        if ((string) $email !== (string) $confirm_email) {

            throw new Exception(Json::encode(array("message" => "emails not equals", "status" => 5)));
        }

        if (bx_user_register($email, $error, $message)) {

            throw new Exception(Json::encode(array("message" => strip_tags($message), "status" => 6)));
        }

        throw new Exception(Json::encode(array("message" => strip_tags($error), "status" => 7)));
    }
} catch (\Exception $e) {

    header('Content-Type: application/json; charset=' . SITE_CHARSET);
    echo Json::encode($e->getMessage());
    exit;
}


/* проверка существования пользователя по email */

function bx_user_exists($email) {

    $dbres = Bitrix\Main\UserTable::getList(
                    array(
                        'select' => array('ID'),
                        'filter' => array('EMAIL' => $email)
                    )
            )->fetch();
    if ($dbres["ID"] > 0) {
        return true;
    }

    return false;
}

/** регистрируем нового пользователя * */
function bx_user_register($email, &$error, &$message) {

    $result = $GLOBALS['USER']->SimpleRegister($email);
    if ($result["TYPE"] != "ERROR") {
        $message = $result["MESSAGE"];
        return true;
    }

    $error = $result["MESSAGE"];
    return false;
}

/** авторизация пользователя * */
function bx_authorize($email, $password, &$error) {

    $result = $GLOBALS['USER']->Login($email, $password, "Y", "Y");

    if ($result === true) {
        return true;
    }

    $error = $result["MESSAGE"];

    return false;
}
