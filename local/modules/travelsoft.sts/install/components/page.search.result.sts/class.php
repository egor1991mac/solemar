<?php

use Bitrix\Highloadblock\HighloadBlockTable as HL;
use Bitrix\Main\Config\Option;

Bitrix\Main\Loader::includeModule('travelsoft.sts');
Bitrix\Main\Loader::includeModule('highloadblock');

class  TravelsoftSearchResultComponent extends CBitrixComponent {

    public $dr;

    // include component_prolog.php
    public function includeComponentProlog()
    {
        $file = "component_prolog.php";

        $template_name = $this->GetTemplateName();

        if ($template_name == "")
            $template_name = ".default";

        $relative_path = $this->GetRelativePath();

        $this->dr = Bitrix\Main\Application::getDocumentRoot();

        $file_path = $this->dr . SITE_TEMPLATE_PATH . "/components" . $relative_path . "/" . $template_name . "/" . $file;

        $arParams = &$this->arParams;

        if(file_exists($file_path))
            require $file_path;
        else {

            $file_path = $this->dr . "/bitrix/templates/.default/components" . $relative_path . "/" . $template_name . "/" . $file;

            if(file_exists($file_path))
                require $file_path;
            else {
                $file_path = $this->dr . $this->__path . "/templates/" . $template_name . "/" . $file;
                if(file_exists($file_path))
                    require $file_path;
                else {

                    $file_path = $this->dr . "/local/components" . $relative_path . "/templates/" . $template_name . "/" . $file;

                    if(file_exists($file_path))
                        require $file_path;
                }

            }
        }
    }

    /**
     * Проверка параметров компонента
     * @throws Exception
     */
    protected function checkInputParameters () {

        $this->arParams['GET_PARAMS_STS'] = (array)$this->arParams['GET_PARAMS_STS'];

        if ($this->arParams['COUNTRY_ID'] != "") {
            $this->arParams['COUNTRY_ID'] = (int)$this->arParams['COUNTRY_ID'];
        }

        if ($this->arParams['CITY_ID'] != "") {
            $this->arParams['CITY_ID'] = (int)$this->arParams['CITY_ID'];
        }

        if ($this->arParams['HOTEL_ID'] != "") {
            $this->arParams['HOTEL_ID'] = (int)$this->arParams['HOTEL_ID'];
        }

    }

    protected function getDatesForSearch () {

        $filter = [
            "filter" => [
                "UF_CITYFROM_ID" => $this->arResult["REQUEST"]["cityFrom"],
                "UF_COUNTRY_ID" => $this->arResult["REQUEST"]["country"],
            ]
        ];

        if(isset($this->arResult["REQUEST"]["tourTypes"]) && !empty($this->arResult["REQUEST"]["tourTypes"])){
            $filter["filter"]["UF_BX_TOURTYPE_ID"] = $this->arResult["REQUEST"]["tourTypes"];
        }

        $result = \travelsoft\sts\stores\Dates::get($filter);

        if (count($result) > 0) {

            if(!isset($this->arResult["REQUEST"]["tourTypes"]) || empty($this->arResult["REQUEST"]["tourTypes"])){
                $result = array_slice($result, 0, 1);
            }
            $today = MakeTimeStamp((string)date("d.m.Y"), "DD.MM.YYYY");

            $arResultBlock = array();
            $arResultNights = array();
            foreach ($result as $item) {

                //даты
                $date = unserialize($item["UF_DATES"]);

                $arResultBlock = array_merge($arResultBlock, $date);
                $gg = array_unique($arResultBlock);
                array_walk($gg, function (&$item_) {
                    $item_ = MakeTimeStamp((string)$item_, "DD.MM.YYYY");
                });
                sort($gg);
                array_walk($gg, function (&$item_) {
                    $item_ = date("d.m.Y", $item_);
                });
                $arResultBlock = $gg;

                //ночи
                $nights = unserialize($item["UF_NIGHTS"]);
                $arResultNights = array_merge($arResultNights, $nights);
                $ggn = array_unique($arResultNights);
                sort($ggn);
                $arResultNights = $ggn;

            }

            if (!empty($arResultBlock)) {
                $this->arResult["REQUEST"]["dateFrom"] = $arResultBlock[0];
                $date_from = MakeTimeStamp((string)$this->arResult["REQUEST"]["dateFrom"], "DD.MM.YYYY");
                $date_to = MakeTimeStamp((string)$this->arResult["REQUEST"]["dateTo"], "DD.MM.YYYY");
                if($date_from < $today) {
                    $date_from = $today;
                    $this->arResult["REQUEST"]["dateFrom"] = date("d.m.Y", $date_from);
                }
                if($date_to < $date_from) {
                    $date_to = AddToTimeStamp(array("DD"=>$this->arResult["PERIOD_DATE"]), $date_from);
                    $this->arResult["REQUEST"]["dateTo"] = date("d.m.Y", $date_to);
                }
            }
            if (!empty($arResultNights)) {
                $arResponse["result"]["nights"] = $arResultNights;
                if(!in_array($this->arResult["REQUEST"]["nightFrom"],$arResultNights)){
                    $this->arResult["REQUEST"]["nightFrom"] = $arResultNights[0];
                }
                if(!in_array($this->arResult["REQUEST"]["nightTo"],$arResultNights)){
                    $this->arResult["REQUEST"]["nightTo"] = end($arResultNights);
                }
            }
        }

    }


    public function executeComponent () {

        $this->includeComponentProlog();

        $this->checkInputParameters();

        $this->arResult["REQUEST"] = array();

        $period = unserialize(\travelsoft\sts\Settings::get("DATE_RANGE"));
        $this->arResult["PERIOD_DATE"] = $period[1];

        if(!empty($this->arParams['GET_PARAMS_STS'])){

            $this->arResult["REQUEST"] = array(

                "cityFrom" => (int)$this->arParams['GET_PARAMS_STS']['cityFrom'],
                "country" => (int)$this->arParams['GET_PARAMS_STS']['country'],
                "dateFrom" => $this->arParams['GET_PARAMS_STS']['dateFrom'],
                "dateTo" => $this->arParams['GET_PARAMS_STS']['dateTo'],
                "nightFrom" => (int)$this->arParams['GET_PARAMS_STS']['nightFrom'],
                "nightTo" => (int)$this->arParams['GET_PARAMS_STS']['nightTo'],
                "adults" => (int)$this->arParams['GET_PARAMS_STS']['adults'],
                "children" => $this->arParams['GET_PARAMS_STS']['children'] && !is_null($this->arParams['GET_PARAMS_STS']['children']) ? (int)$this->arParams['GET_PARAMS_STS']['children'] : \travelsoft\sts\Settings::getDefaultChildren(),
                "tourTypes" => isset($this->arParams['GET_PARAMS_STS']['tourTypes']) ? (array)$this->arParams['GET_PARAMS_STS']['tourTypes'] : array(),
                "tours" => isset($this->arParams['GET_PARAMS_STS']['tours']) ? (array)$this->arParams['GET_PARAMS_STS']['tours'] : array(),
                "cities" => isset($this->arParams['GET_PARAMS_STS']['cities']) ? (array)$this->arParams['GET_PARAMS_STS']['cities'] : array(),
                "hotels" => isset($this->arParams['GET_PARAMS_STS']['hotels']) ? (array)$this->arParams['GET_PARAMS_STS']['hotels'] : array(),
                "stars" => isset($this->arParams['GET_PARAMS_STS']['stars']) ? (array)$this->arParams['GET_PARAMS_STS']['stars'] : array(),
                "meals" => isset($this->arParams['GET_PARAMS_STS']['meals']) ? (array)$this->arParams['GET_PARAMS_STS']['meals'] : array(),

            );

            if(isset($this->arParams['GET_PARAMS_STS']["priceFrom"]) && !empty($this->arParams['GET_PARAMS_STS']["priceFrom"]) && isset($this->arParams['GET_PARAMS_STS']["currency"]) && !empty($this->arParams['GET_PARAMS_STS']["currency"])){

                $priceTo = $this->arParams['GET_PARAMS_STS']['priceTo'] && !empty($this->arParams['GET_PARAMS_STS']['priceTo']) ? $this->arParams['GET_PARAMS_STS']['priceTo'] : $this->arParams['GET_PARAMS_STS']["priceFrom"];
                $this->arResult["REQUEST"]["price"] = array($this->arParams['GET_PARAMS_STS']["priceFrom"],$priceTo,$this->arParams['GET_PARAMS_STS']["currency"]);

            } elseif(isset($this->arParams['GET_PARAMS_STS']["priceTo"]) && !empty($this->arParams['GET_PARAMS_STS']["priceTo"]) && isset($this->arParams['GET_PARAMS_STS']["currency"]) && !empty($this->arParams['GET_PARAMS_STS']["currency"])) {

                $priceFrom = $this->arParams['GET_PARAMS_STS']['priceFrom'] && !empty($this->arParams['GET_PARAMS_STS']['priceFrom']) ? $this->arParams['GET_PARAMS_STS']['priceFrom'] : $this->arParams['GET_PARAMS_STS']["priceTo"];
                $this->arResult["REQUEST"]["price"] = array($priceFrom,$this->arParams['GET_PARAMS_STS']["priceTo"],$this->arParams['GET_PARAMS_STS']["currency"]);

            }

        }
        else {

            $date = \travelsoft\sts\Settings::getDefaultSearchDate();
            $night = \travelsoft\sts\Settings::getDefaultSearchNight();

            $this->arResult["REQUEST"] = array(

                "cityFrom" => \travelsoft\sts\Settings::getDefaultCityFrom(),
                "country" => \travelsoft\sts\Settings::getDefaultCountry(),
                "dateFrom" => $date['dateFrom'],
                "dateTo" => $date['dateTo'],
                "nightFrom" => $night['nightFrom'],
                "nightTo" => $night['nightTo'],
                "adults" => \travelsoft\sts\Settings::getDefaultAdults(),
                "children" => \travelsoft\sts\Settings::getDefaultChildren(),
                "tourTypes" => array(),
                "tours" => array(),
                "cities" => array(),
                "hotels" => array(),
                "stars" => array(),
                "meals" => array(),

            );

        }

        if($this->arResult["REQUEST"]["children"] > 0){

            if($this->arResult["REQUEST"]["children"] == 1){
                $this->arResult["REQUEST"]["age1"] = $this->arParams['GET_PARAMS_STS']['age1'] && !empty($this->arParams['GET_PARAMS_STS']['age1']) ? (int)$this->arParams['GET_PARAMS_STS']['age1'] : 6;
            } elseif ($this->arResult["REQUEST"]["children"] >= 1) {
                $this->arResult["REQUEST"]["age1"] = $this->arParams['GET_PARAMS_STS']['age1'] && !empty($this->arParams['GET_PARAMS_STS']['age1']) ? (int)$this->arParams['GET_PARAMS_STS']['age1'] : 6;
                $this->arResult["REQUEST"]["age2"] = $this->arParams['GET_PARAMS_STS']['age2'] && !empty($this->arParams['GET_PARAMS_STS']['age2']) ? (int)$this->arParams['GET_PARAMS_STS']['age2'] : 6;
            }

        }

        if(!empty($this->arParams['COUNTRY_ID'])) {
            $this->arResult["REQUEST"]["country"] = $this->arParams['COUNTRY_ID'];
        }
        if(!empty($this->arParams['CITY_ID'])) {
            $this->arResult["REQUEST"]["cities"] = (array)$this->arParams['CITY_ID'];
        }
        if(!empty($this->arParams['HOTEL_ID'])) {
            $this->arResult["REQUEST"]["hotels"] = (array)$this->arParams['HOTEL_ID'];
        }
        if(!empty($this->arParams['TOUR_TYPE_ID'])) {
            $this->arResult["REQUEST"]["tourTypes"] = (array)$this->arParams['TOUR_TYPE_ID'];
        }

        $this->getDatesForSearch();

        $this->arResult["OPERATORS_TOTAL"] = \travelsoft\sts\Utils::getOperators_();
        $this->arResult["OPERATORS"] = array_keys($this->arResult["OPERATORS_TOTAL"]);

        $this->arResult["SERVICES"] = services();

        $this->arResult["TEMPLATES"] = array();


        $html_path = __DIR__."/html.templates";
        $arFiles = array(
            "list" => array(
                "container" => "container.php",
                "list" => "list.php",
                "stars" => "stars.php",
                "star" => "star.php",
                "star_" => "star-o.php",
                "country" => "country.php",
                "city" => "city.php",
                "services" => "services.php",
                "service_pic" => "service-pic.php",
                "service_icon" => "service-icon.php"
            ),
            "detail" => array(
                "container" => "container.php",
                "list" => "list.php",
                "food_full_name" => "food-full-name.php",
                "food_desc" => "food-desc.php"
            ),
            "hotel" => array(
                "container" => "container.php",
                "list" => "list.php",
                "food_full_name" => "food-full-name.php",
                "food_desc" => "food-desc.php"
            ),
        );

        foreach ($arFiles as $key=>$templ){

            foreach ($templ as $k=>$file) {

                ob_start();
                include $html_path . "/" . $key . "/" . $file;
                $this->arResult["TEMPLATES"][$key][$k] = str_replace(array("\r", "\n"), array("", ""), ob_get_clean());

            }

        }

        $this->IncludeComponentTemplate();

    }

}