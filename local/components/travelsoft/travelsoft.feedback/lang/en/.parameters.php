<?
$MESS ['MFP_CAPTCHA'] = "Use CAPTCHA for Unauthorized Visitors";
$MESS ['MFP_OK_MESSAGE'] = "Text To Show Upon Sending";
$MESS ['MFP_OK_TEXT'] = "Thank you. Your message is now being processed.";
$MESS ['MFP_EMAIL_TO'] = "Destination E-mail Address";
$MESS ['MFP_REQUIRED_FIELDS'] = "Required fields";
$MESS ['MFP_ALL_REQ'] = "(all non-required)";
$MESS ['MFP_NAME'] = "Name";
$MESS ['MFP_MESSAGE'] = "Message";
$MESS ['MFP_EMAIL_TEMPLATES'] = "E-mail Templates";
$MESS["IBLOCK_TYPE"] = "Type iblock";
$MESS["IBLOCK_IBLOCK"] = "Infoblock";
$MESS["IBLOCK_PROPERTY"] = "Properties";
$MESS["IBLOCK_PROPERTY_REQUIRED"] = "Properties required";
$MESS["MFP_TITLE"] = "Form Header";
?>