<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="hotel-offers-info">
    <?if(isset($arResult["ITEMS"]) && !empty($arResult["ITEMS"])):?>

            <? foreach ($arResult["ITEMS"] as $arItem): ?>
                <?$name = !empty($arItem["hotel"]["name"]) ? $arItem["hotel"]["name"] : $arItem["hotel"]["originalName"];
                $city = !empty($arItem["city"]["name"]) ? $arItem["city"]["name"] : $arItem["city"]["originalName"];
                $food = !empty($arItem["food"]["name"]) ? $arItem["food"]["name"] : $arItem["food"]["originalName"];
                $placement = !empty($arItem["placement"]["name"]) ? $arItem["placement"]["name"] : $arItem["placement"]["originalName"];
                $roomType = !empty($arItem["roomType"]["name"]) ? $arItem["roomType"]["name"] : $arItem["roomType"]["originalName"];
                $roomCatType = !empty($arItem["roomCatType"]["name"]) ? $arItem["roomCatType"]["name"] : $arItem["roomCatType"]["originalName"];
                ?>
                <div class="hotel-offer">
                    <div class="hotel-offer_grouped-tours">
                        <div class="hotel-offer_grouped-dates">
                            <div class="hotel-offer-text hotel-offer-text_bold hotel-offer-text-dates">
                                <?=$arItem["tourDate"]["dateFrom"]?> - <?=$arItem["tourDate"]["dateTo"]?>
                            </div>
                            <div class="hotel-offer-text hotel-offer-text-night text-grey">
                                <?=num2word($arItem["night"],array("ночь","ночи","ночей"))?>
                            </div>
                            <!--<div class="hotel-offer-tooltip">
                                <div class="hotel-offer-name">
                                    <?/*=$city*/?>, <?/*=$name*/?>
                                </div>
                            </div>-->
                        </div>
                        <div class="hotel-offer_grouped-accomodation">
                            <div class="hotel-offer-text-food hotel-offer-text">
                                <span class="hotel-offer-text-food-code hotel-offer-text_bold"><?=$food?></span>
                                <?if(!empty($arItem["food"]["fullName"])):?>
                                    <span class="hotel-offer-text-food-desc text-grey" title="<?=$arItem["food"]["fullName"]?>"> — <?=$arItem["food"]["fullName"]?></span>
                                <?endif?>
                                <?if(!empty($arItem["food"]["desc"])):?>
                                    <i class="fa fa-info hotel-offer-text-food_info" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?=$arItem["food"]["desc"]?>"></i>
                                    <!--<div class="hotel-offer-text-food_info" data-toggle="tooltip" data-placement="top" title="<?/*=$arItem["food"]["desc"]*/?>" data-original-title="<?/*=$arItem["food"]["desc"]*/?>"></div>-->
                                <?endif?>
                            </div>
                            <div class="hotel-offer_grouped-room">
                                <div class="hotel-offer-text-room hotel-offer-text">
                                    <span class="hotel-offer-text-room-code hotel-offer-text_bold"><?=$placement?></span>
                                    <span class="hotel-offer-text-room-desc text-grey"> — <?=$roomType?> <?=$roomCatType?></span>
                                </div>
                            </div>
                        </div>
                        <!--<div class="hotel-offer_grouped-operator">
                            <div class="hotel-offer_grouped-logo">
                                <?/*if(!empty($arResult["OPERATORS"][$arItem["operator_id"]]["UF_LOGO"])):*/?>
                                    <img src="<?/*=getSrcImage($arResult["OPERATORS"][$arItem["operator_id"]]["UF_LOGO"],array("width"=>110,"height"=>40), null, 'BX_RESIZE_IMAGE_PROPORTIONAL_ALT')*/?>" alt="<?/*=$arResult["OPERATORS"][$arItem["operator_id"]]["UF_NAME"]*/?>">
                                <?/*else:*/?>
                                    <?/*=$arResult["OPERATORS"][$arItem["operator_id"]]["UF_NAME"]*/?>
                                <?/*endif*/?>
                            </div>
                        </div>-->
                        <div class="hotel-offer_grouped-price">
                            <div class="hotel-offer-text-price hotel-offer-text">
                                <span class="hotel-offer-price_and_currency"><?= \travelsoft\currency\Converter::getInstance()->convert($arItem["prices"], $arItem["defCurrency"])->getResult() ?></span>
                            </div>
                        </div>
                        <div class="hotel-offer_grouped-btn">
                            <div class="hotel-offer-text-btn hotel-offer-text">
                                <span class="hotel-offer-detail thm-btn">Заказать</span>
                            </div>
                        </div>
                    </div>
                </div>
            <?endforeach;?>

    <?else:?>
        <p class="hotel-offer">Нет других вариантов размещения.</p>
    <?endif?>
</div>