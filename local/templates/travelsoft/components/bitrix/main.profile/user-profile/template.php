<?
/**
 * @global CMain $APPLICATION
 * @param array $arParams
 * @param array $arResult
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
?>

<div class="my-profile">

    <!--<h3><?/*=GetMessage('PROFILE')*/?></h3>-->


<?ShowError($arResult["strProfileError"]);?>
<?
if ($arResult['DATA_SAVED'] == 'Y')
	ShowNote(GetMessage('PROFILE_DATA_SAVED'));
?>
<script type="text/javascript">
<!--
var opened_sections = [<?
$arResult["opened"] = $_COOKIE[$arResult["COOKIE_PREFIX"]."_user_profile_open"];
$arResult["opened"] = preg_replace("/[^a-z0-9_,]/i", "", $arResult["opened"]);
if (strlen($arResult["opened"]) > 0)
{
	echo "'".implode("', '", explode(",", $arResult["opened"]))."'";
}
else
{
	$arResult["opened"] = "reg";
	echo "'reg'";
}
?>];
//-->

var cookie_prefix = '<?=$arResult["COOKIE_PREFIX"]?>';
</script>
<?//dm($arResult);?>
<!-- Контейнер -->

<div class="ts-theme">
<!-- Форма -->
<form method="post" name="form1" action="<?=$arResult["FORM_TARGET"]?>" enctype="multipart/form-data" class="ts-wrap ts-px-0 ts-px-md-2">
<?=$arResult["BX_SESSION_CHECK"]?>
<input type="hidden" name="lang" value="<?=LANG?>" />
<input type="hidden" name="ID" value=<?=$arResult["ID"]?> />
<input type="hidden" name="LOGIN" value="<? echo $arResult["arUser"]["LOGIN"]?>" />
    <div class="ts-title ts-px-0 ts-py-0 ts-mx-0 ts-md-mx-2">
        <h2 >Информация о пользовотеле</h2>
        <button class="ts-d-none">x</button>
    </div>

    <!-- Контент -->
    <div class="ts-content">

        <!-- Логин -->
        <div class="ts-col-24 ts-col-lg-12  ts-mb-2 ts-px-0 ts-md-px-2">
            <div class="ts-input">
                <label for="" class="ts-d-none"></label>
                <div class="placeholder"><?=GetMessage("NAME")?></div>
                <div class="validation"></div>
                <div class="input">
                    <input type="text" name="NAME" maxlength="50" value="<?=$arResult["arUser"]["NAME"]?>" />
                </div>
            </div>
        </div>
        <!-- Логин -->
        <div class="ts-col-24 ts-col-lg-12  ts-mb-2 ts-px-0 ts-md-px-2">
            <div class="ts-input">
                <label for="" class="ts-d-none"></label>
                <div class="placeholder"><?=GetMessage("LAST_NAME")?></div>
                <div class="validation"></div>
                <div class="input">
                    <input type="text" name="LAST_NAME" maxlength="50" value="<?=$arResult["arUser"]["LAST_NAME"]?>" />
                </div>
            </div>
        </div>
        <!-- Логин -->
        <div class="ts-col-24 ts-col-lg-12  ts-mb-2 ts-px-0 ts-md-px-2">
            <div class="ts-input">
                <label for="" class="ts-d-none"></label>
                <div class="placeholder"><?=GetMessage("SECOND_NAME")?></div>
                <div class="validation"></div>
                <div class="input">
                    <input type="text" name="SECOND_NAME" maxlength="50" value="<?=$arResult["arUser"]["SECOND_NAME"]?>" />
                </div>
            </div>
        </div>
        <!-- Логин -->
        <div class="ts-col-24 ts-col-lg-12  ts-mb-2 ts-px-0 ts-md-px-2">
            <div class="ts-input">
                <label for="" class="ts-d-none"></label>
                <div class="placeholder"><?=GetMessage("USER_BIRTHDAY_DT")?></div>
                <div class="validation"></div>
                <div class="input">
                    <input type="text" name="PERSONAL_BIRTHDAY" value="<?=$arResult["arUser"]["PERSONAL_BIRTHDAY"]?>" />
                </div>
            </div>
        </div>
        <!-- Логин -->
        <div class="ts-col-24 ts-col-lg-12  ts-mb-2 ts-px-0 ts-md-px-2">
            <div class="ts-input">
                <label for="" class="ts-d-none"></label>
                <div class="placeholder"><?=GetMessage("EMAIL")?></div>
                <div class="validation"></div>
                <div class="input">
                    <?/*if($arResult["EMAIL_REQUIRED"]):*/?><!----><?/*endif*/?>
                    <input type="text" name="EMAIL" maxlength="50" value="<? echo $arResult["arUser"]["EMAIL"]?>" />
                </div>
            </div>
        </div>
        <?if($arResult["arUser"]["EXTERNAL_AUTH_ID"] == ''):?>
            <div class="ts-col-24 ts-col-lg-12  ts-mb-2 ts-px-0 ts-md-px-2">
                <div class="ts-input">
                    <label for="" class="ts-d-none"></label>
                    <div class="placeholder"><?=GetMessage("NEW_PASSWORD_REQ")?></div>
                    <div class="validation"></div>
                    <div class="input">
                        <input type="password" name="NEW_PASSWORD" maxlength="50" value="" autocomplete="off" class="bx-auth-input" />
                    </div>
                    <?if($arResult["SECURE_AUTH"]):?>
                        <span class="bx-auth-secure" id="bx_auth_secure" title="<?echo GetMessage("AUTH_SECURE_NOTE")?>" style="display:none">
                                <div class="bx-auth-secure-icon"></div>
                            </span>
                        <noscript>
                                <span class="bx-auth-secure" title="<?echo GetMessage("AUTH_NONSECURE_NOTE")?>">
                                    <div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
                                </span>
                        </noscript>
                        <script type="text/javascript">
                            document.getElementById('bx_auth_secure').style.display = 'inline-block';
                        </script>
                    <?endif?>
                </div>
            </div>
            <div class="ts-col-24 ts-col-lg-12  ts-mb-2 ts-px-0 ts-md-px-2">
                <div class="ts-input">
                    <label for="" class="ts-d-none"></label>
                    <div class="placeholder"><?=GetMessage("NEW_PASSWORD_CONFIRM")?></div>
                    <div class="validation"></div>
                    <div class="input">
                        <input type="password" name="NEW_PASSWORD_CONFIRM" maxlength="50" value="" autocomplete="off" />
                    </div>
                </div>
            </div>
        <?endif;?>
        <?if($arResult["TIME_ZONE_ENABLED"] == true):?>
            <!-- Заголовок -->
            <div class="ts-title ts-px-1 ts-py-1">
                <h2 class="ts-h2 ts-reset ts-color-white"><?=GetMessage("main_profile_time_zones")?></h2>
            </div>
            <div class="ts-col-24 ts-col-lg-12  ts-mb-2 ts-px-0 ts-md-px-2">
                <div class="ts-select">
                    <label for="" class="ts-d-none"></label>
                    <div class="placeholder"><?=GetMessage("main_profile_time_zones_auto")?></div>
                    <div class="validation"></div>
                    <div class="input">
                        <select name="AUTO_TIME_ZONE" onchange="this.form.TIME_ZONE.disabled=(this.value != 'N')" class="">
                            <option value=""><?echo GetMessage("main_profile_time_zones_auto_def")?></option>
                            <option value="Y"<?=($arResult["arUser"]["AUTO_TIME_ZONE"] == "Y"? ' SELECTED="SELECTED"' : '')?>><?echo GetMessage("main_profile_time_zones_auto_yes")?></option>
                            <option value="N"<?=($arResult["arUser"]["AUTO_TIME_ZONE"] == "N"? ' SELECTED="SELECTED"' : '')?>><?echo GetMessage("main_profile_time_zones_auto_no")?></option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="ts-col-24 ts-col-lg-12  ts-mb-2 ts-px-0 ts-md-px-2">
                <div class="ts-select">
                    <label for="" class="ts-d-none"></label>
                    <div class="placeholder"><?=GetMessage("main_profile_time_zones_zones")?></div>
                    <div class="validation"></div>
                    <div class="input">
                        <select name="TIME_ZONE"<?if($arResult["arUser"]["AUTO_TIME_ZONE"] <> "N") echo ' disabled="disabled"'?> class="form-control">
                            <?foreach($arResult["TIME_ZONE_LIST"] as $tz=>$tz_name):?>
                                <option value="<?=htmlspecialcharsbx($tz)?>"<?=($arResult["arUser"]["TIME_ZONE"] == $tz? ' SELECTED="SELECTED"' : '')?>><?=htmlspecialcharsbx($tz_name)?></option>
                            <?endforeach?>
                        </select>
                    </div>
                    </div>
                </div>
            </div>
        <?endif;?>
    <noindex>
        <div class="ts-col-24 ts-col-lg-12  ts-mb-2 ts-px-0 ts-md-px-2 ts-mx-0">
            <div class="ts-text">
                <p><?echo $arResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"];?></p>
            </div>
        </div>
    </noindex>
        <!-- Submit -->
        <div class="ts-col-24 ts-col-lg-12  ts-mb-2 ts-px-0 ts-md-px-2">
            <div class="ts-submit ts-max-width-370 ts-mx-0">
                <input type="submit" name="save"  value="<?=(($arResult["ID"]>0) ? GetMessage("MAIN_SAVE") : GetMessage("MAIN_ADD"))?>">
            </div>
        </div>

    </div>

</form>



</div>

<?
if($arResult["SOCSERV_ENABLED"])
{
	$APPLICATION->IncludeComponent("bitrix:socserv.auth.split", ".default", array(
			"SHOW_PROFILES" => "Y",
			"ALLOW_DELETE" => "Y"
		),
		false
	);
}
?>
</div>


<?$APPLICATION->AddHeadScript("https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js", true);?>

<script>
(function ($) {
    let input = document.querySelectorAll('.ts-input input');
    let checkboxSaveMe = $('.ts-checkbox i');
    [].slice.call(input).forEach(item=>{
        if($(item).val().length != 0){
            $(item).closest('.ts-input').addClass('active');
        }
    })


    checkboxSaveMe.on('click',function(e){
        $(this).toggleClass('active');
    });
    $(input).on('focus',function(e){
        $(this).closest('.ts-input').addClass('active focus');

    });
    $(input).on('focusout',function(e){
        if($(this).val().length == 0){
            $(this).closest('.ts-input').removeClass('active focus');
        }
        else $(this).closest('.ts-input').removeClass('focus');

    });
    $(input).on('keyup',function(e){
        if($(this).val().length != 0){
            $(this).closest('.ts-input').addClass('active');
        }
        else{
            $(this).closest('.ts-input').removeClass('active');
        }
    });
    $("input[name='PERSONAL_BIRTHDAY']").mask("99.99.9999");
})(jQuery);
</script>