<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if (!empty($arResult["PROPERTIES"]["MAP"]["VALUE"])) {
    $LATLNG = explode(",", $arResult["PROPERTIES"]["MAP"]["VALUE"]);
    $arResult['MAP_SCALE'] = $arResult["PROPERTIES"]["MAP_SCALE"]["VALUE"] ? $arResult["PROPERTIES"]["MAP_SCALE"]["VALUE"] : 13;
    $arResult['ROUTE_INFO'] = array(
        "lat" => $LATLNG[0],
        "lng" => $LATLNG[1],
        "title" => $arResult['NAME']
    );
}


$cities_db = \CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => CITIES_IBLOCK_ID, "ACTIVE" => "Y", "PROPERTY_COUNTRY" => $arResult["ID"]), false, false, Array("ID", "NAME", "IBLOCK_ID"));
if ($cities_db->SelectedRowsCount() >= 1) {
    $arResult["CITIES"] = true;
}

$hotels_db = \CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => HOTELS_IBLOCK_ID, "ACTIVE" => "Y", "PROPERTY_COUNTRY" => $arResult["ID"]), false, false, Array("ID", "NAME", "IBLOCK_ID"));
if ($hotels_db->SelectedRowsCount() >= 1) {
    $arResult["HOTELS"] = true;
}

$visas_db = \CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => VISAS_IBLOCK_ID, "ACTIVE" => "Y", "PROPERTY_COUNTRY" => $arResult["ID"]), false, false, Array("ID", "NAME", "IBLOCK_ID"));
if ($visas_db->SelectedRowsCount() >= 1) {
    $arResult["VISAS"] = true;
}

$pages_db = \CIBlockElement::GetList(Array(), Array("IBLOCK_ID"=>DETAIL_PAGES_ID_IBLOCK, "ACTIVE"=>"Y", "PROPERTY_COUNTRY"=>$arResult["ID"]), false, false, Array("ID","NAME","IBLOCK_ID","CODE","PROPERTY_BUTTON_NAME"));
if($pages_db->SelectedRowsCount() >= 1){

    while($ob = $pages_db->GetNext()){

        $arResult["PAGES_INFO"][] = array(
            "ID" => $ob["ID"],
            "NAME" => $ob["NAME"],
            "CODE" => $ob["CODE"],
            "BUTTON_NAME" => $ob["PROPERTY_BUTTON_NAME_VALUE"]
        );

    }
}

$arResult["DIRECTIONS"] = false;
$directions = \travelsoft\sts\stores\RelationDirections::get();
foreach ($directions as $direction) {

    if(in_array($arResult["ID"],$direction["UF_COUNTRY_BX_ID"])){
        $arResult["DIRECTIONS"] = true;
        break;
    }

}

$cp = $this->__component; // объект компонента

if (is_object($cp))
{
    $cp->arResult['DIRECTIONS'] = $arResult["DIRECTIONS"];
    $cp->arResult['DETAIL_PAGE_URL'] = $arResult["DETAIL_PAGE_URL"];
    $cp->arResult['DISPLAY_PROPERTIES'] = $arResult["DISPLAY_PROPERTIES"];
    $cp->SetResultCacheKeys(array('DISPLAY_PROPERTIES','DETAIL_PAGE_URL','DIRECTIONS'));
}