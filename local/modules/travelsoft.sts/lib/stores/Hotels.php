<?php

namespace travelsoft\sts\stores;

use travelsoft\sts\adapters\Iblock;
use travelsoft\sts\Utils;
use travelsoft\sts\Settings;

/**
 * Класс для работы с инфоблоком отелей
 *
 * @author juliya.sharlova
 * @copyright (c) 2017, travelsoft
 */
class Hotels extends Iblock
{
    protected static $storeName = 'hotels';

    public function getImage(int $id): string {

        $arData = self::get(array("filter" => array("ID" => $id)));

        if(!empty($arData[$id]["PROPERTIES"]["PICTURES"]["VALUE"])){

            return (string) Utils::getPath($arData[$id]["PROPERTIES"]["PICTURES"]["VALUE"][0], array("width"=>222,"height"=>182));

        }
        else {

            return Settings::pathNoPhoto();

        }

    }

    public function getElementLink(int $id): string {

        $arData = self::get(array("filter" => array("ID" => $id),"select" => array("ID", "DETAIL_PAGE_URL")));
        return (string) $arData[$id]["DETAIL_PAGE_URL"];

    }

    public function getElementDesc(int $id): string {

        $arData = self::get(array("filter" => array("ID" => $id)));
        if(!empty($arData[$id]["PROPERTIES"]["PREVIEW_TEXT"]["VALUE"]["TEXT"])) {
            return (string) $arData[$id]["PROPERTIES"]["PREVIEW_TEXT"]["~VALUE"]["TEXT"];
        }
        elseif(!empty($arData[$id]["PROPERTIES"]["DESC"]["VALUE"]["TEXT"])) {
            return (string) $arData[$id]["PROPERTIES"]["DESC"]["~VALUE"]["TEXT"];
        }
        else {
            return '';
        }


    }

    public function getElementServices(int $id) {

        $arData = self::get(array("filter" => array("ID" => $id)));
        if(!empty($arData[$id]["PROPERTIES"]["SERVICES"]["VALUE"])) {
            return (array) $arData[$id]["PROPERTIES"]["SERVICES"]["VALUE"];
        }
        else {
            return "";
        }

    }

}