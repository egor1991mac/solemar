var jshover = function()
{
	var menuDiv = document.getElementById("horizontal-multilevel-menu")
	if (!menuDiv)
		return;

	var sfEls = menuDiv.getElementsByTagName("li");
	for (var i=0; i<sfEls.length; i++) 
	{
		sfEls[i].onmouseover=function()
		{
			this.className+=" jshover";
		}
		sfEls[i].onmouseout=function() 
		{
			this.className=this.className.replace(new RegExp(" jshover\\b"), "");
		}
	}
}

if (window.attachEvent) 
	window.attachEvent("onload", jshover);

/*
$(document).ready(function (e) {
	$('span.arrow').on('click',function(e){
		console.log($('span.arrow'));
		console.log($(this).closest('.ts-dropdown').addClass('ts-dropdown__drop'));

		if(that.hasClass('active')){
			if(that.hasClass('ts-dropdown__drop')){
				that.toggleClass('active ts-dropdown__drop');
			}
			else{
				that.toggleClass('ts-dropdown__drop');
			}

		}
		else{
			that.toggleClass('active ts-dropdown__drop');
		}

	})*/
	/*document.addEventListener('click',function(e){
		console.log(!e.target.closest('.ts-dropdown'));
		if(!e.target.closest('.ts-dropdown')){
			$('.ts-dropdown').removeClass('active ts-dropdown__drop');
		}
	})
});*/
