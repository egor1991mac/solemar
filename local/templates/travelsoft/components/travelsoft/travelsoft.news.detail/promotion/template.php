<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?$properties = $arResult["DISPLAY_PROPERTIES"];?>

<div class="container post col2">
    <h1><?=$arResult["NAME"]?></h1>
    <div class="sidebar">
        <ul class="tours">
            <?=$arParams["TOURS_RIGHT_FOR_COUNTRY"]?>
        </ul>
    </div>
    <?if($properties["PICTURES"]["VALUE"]):?>
        <?=$arParams["SLIDER"]?>
    <?endif?>
    <div class="content">
        <?if(!empty($properties["DESC"]["VALUE"]["TEXT"])):?>
            <?=$properties["DESC"]["DISPLAY_VALUE"]?>
        <?endif?>
        <div style="clear: both"></div>
    </div>
</div>
<?=$arParams["PROMOTIONS_LIST"]?>
<?=$arParams["COUNTRIES_LIST"]?>