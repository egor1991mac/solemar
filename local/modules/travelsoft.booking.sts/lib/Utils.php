<?php

namespace travelsoft\booking\sts;

\Bitrix\Main\Loader::includeModule('travelsoft.currency');

/**
 * Класс с различными утилитами
 *
 * @author juliya.sharlova
 */
class Utils {

    /**
     * Объект запроса
     * @var \Bitrix\Main\HttpRequest
     */
    static protected $request = null;
    static public $group_agent = false;
    static public $user_active = false;

    /**
     * Возвращает результат конвертации массива в строку
     * @param array $arFields
     * @return string
     */
    public static function ats(array $arFields): string {
        return base64_encode(gzcompress(serialize($arFields), 9));
    }

    /**
     * Возвращает результат конвертации строки в массив
     * @param string $str
     * @return array
     */
    public static function sta(string $str): array {
        return (array) unserialize(gzuncompress(base64_decode($str)));
    }

    public static function hash ($value, $salt) {
        return md5($value . $salt);
    }

    public static function checkhash ($value, $hash, $salt) {
        return md5 ($value . $salt) === $hash;
    }

    /**
     * @global CMain $APPLICATION
     * @global CUser $USER
     * @var array $arParams
     * @var array $arResult
     * @var CatalogSectionComponent $component
     * @var CBitrixComponentTemplate $this
     * @var string $templateName
     * @var string $componentPath
     */
    public static function _er(string $code, bool $div = false, string $message = '') {

        $text = strlen($message) > 0 ? $message : GetMessage($code);

        if ($div) {
            echo '<div class="error">' . $text . '</div>';
        } else {
            echo '<span class="error">' . $text . '</span>';
        }
    }

    /**
     * Обертка метода self::_er()
     * @param array $arErrors
     * @param string $code
     * @param bool $div
     * @param string $message
     */
    public static function showError(array $arErrors = array(), string $code, bool $div = false, string $message = '') {

        if (in_array($code, $arErrors)) {
            Utils::_er($code, $div);
        }
    }

    /**
     * Склеивает элементы массива в строку
     * @param array $array
     * @return string
     */
    public static function gluingAnArray(array $array, string $delemiter = ' '): string {

        return implode($delemiter, array_filter($array, function ($item) {
            return strlen($item) > 0;
        }));
    }
    
    /**
     * Возвращает массив текущей валюты
     * @return array
     */
    public static function getCurrentCurrency() {
        return \travelsoft\Currency::getInstance()->get("current_currency");
    }

    /**
     * Возвращает массив всех валют
     * @return array
     */
    public static function getAllCurrency() {
        return \travelsoft\Currency::getInstance()->get("currency");
    }

    /**
     * Конвертирует валюту
     * @param type $price
     * @param type $in
     * @param type $out
     * @param type $onlyN
     * @return mixed
     */
    public static function convertCurrency($price = null, $in = null, $out = null, $onlyN = false) {
        return \travelsoft\Currency::getInstance()->convertCurrency($price, $in, $out, $onlyN);
    }

    /**
     * Выполянет действия для всех операторов
     */
    public function operatorsIterator ($callback) {

        foreach (\travelsoft\booking\sts\stores\Operators::get() as $operator) {
            $callback($operator);
        }

    }

    /**
     *  Основной оператор
     *  @return array
     */
    public static function getMainOperator() {

        return current(\travelsoft\booking\sts\stores\Operators::get(array("filter" => array("UF_MAIN_OPERATOR" => 1))));

    }

    /**
     * Новый пользователь
     * @return bool
     */
    public static function createUser($parameters) {

        $operator = self::getMainOperator();

        // создаём пользователя у операторов
        $response = (new $operator["UF_CLASS"])->createUser(array(
            "address" => $operator["UF_GATEWAY"],
            "is_agent" => $parameters['is_agent'] ? 1 : 0,
            "update" => 0,
            "operator_id" => $operator["ID"],
            "user_info" => array(
                "email" => $parameters['email'],
                "login" => $parameters['email'],
                "password" => $parameters['password'],
                "activated" => $parameters['activated'] ? 1 : 0,
                "username" => $parameters["username"],
                "company" => $parameters["company"],
                "address" => $parameters["address"],
                "inn" => $parameters["inn"]
            )
        ));

        return $response;

    }

    /**
     * Редактирование пользователя
     * @return bool
     */
    public static function updateUser($parameters) {

        $result = false;
        $operator = self::getMainOperator();

        // редактирование пользователя у операторов
        $response = (new $operator["UF_CLASS"])->updateUser(array(
            "address" => $operator["UF_GATEWAY"],
            "is_agent" => $parameters['is_agent'] ? 1 : 0,
            "update" => 1,
            "operator_id" => $operator["ID"],
            "user_info" => $parameters["user_info"]
        ));

        if (isset($response["result"]["result"])) {
            $result = true;
        }

        return $result;

    }

    /**
     * Делаем LOGIN = EMAIL
     * @param array $arFields
     */
    public static function setLoginEqlEmail(&$arFields) {
        $arFields['LOGIN'] = $arFields['EMAIL'];
    }

    /**
     * Сохранение/удаление/получение пароля в массиве $GLOBALS
     * @param string $action
     * @param string $password
     * @return string|null
     */
    public static function passWorker($action, $password) {

        if ($action == "save") {
            $GLOBALS["PASSWORD"] = $password;
        } elseif ($action == "delete") {
            unset($GLOBALS["PASSWORD"]);
        } else {
            return $GLOBALS["PASSWORD"];
        }
    }

    /**
     * Проверка что запрос исходит от агента
     * @return boolean
     */
    public static function isAgentRequest() {

        self::$request = self::getRequest();

        if (self::$request->isPost() && self::$request->getPost('IS_AGENT') == "Y") {
            return true;
        }

        return false;
    }

    /**
     * Возвращает объект запроса
     * @return \Bitrix\Main\HttpRequest
     */
    public static function getRequest() {
        return \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
    }

}
