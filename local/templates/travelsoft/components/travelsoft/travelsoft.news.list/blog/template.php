<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if(!empty($arResult["ITEMS"])):?>

    <ul class="posts">

        <?foreach($arResult["ITEMS"] as $arItem):?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <?$arImg = getSrc($arItem["DISPLAY_PROPERTIES"]["PICTURES"]["VALUE"], array("width"=>267,"height"=>187), NO_PHOTO_267_187, 1);?>

            <li id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="img" style="background-image:url('<?= $arImg[0] ?>')"></a>
                <div class="info">
                    <span class="date"><?= $arItem["DISPLAY_ACTIVE_FROM"] ?></span>
                    <h2><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?= $arItem["NAME"] ?></a></h2>
                    <?if(!empty($arItem["DISPLAY_PROPERTIES"]["PREVIEW_TEXT"]["VALUE"]["TEXT"])):?>
                        <p><?= substr2($arItem["DISPLAY_PROPERTIES"]["PREVIEW_TEXT"]["DISPLAY_VALUE"], 500) ?></p>
                    <?elseif(!empty($arItem["DISPLAY_PROPERTIES"]["DESC"]["VALUE"]["TEXT"])):?>
                        <p><?= substr2($arItem["DISPLAY_PROPERTIES"]["DESC"]["DISPLAY_VALUE"], 500) ?></p>
                    <?endif?>
                    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=GetMessage('READ_MORE')?></a>
                </div>
            </li>

        <?endforeach;?>

    </ul>
    <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
        <div style="clear: both"></div>
        <?=$arResult["NAV_STRING"]?>
    <?endif;?>
<?endif?>