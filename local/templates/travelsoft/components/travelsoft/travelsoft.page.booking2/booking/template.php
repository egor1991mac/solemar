<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$this->addExternalCss($templateFolder . "/webui-popover/jquery.webui-popover.min.css");
$this->addExternalJs($templateFolder . "/webui-popover/jquery.webui-popover.min.js");
$this->addExternalJs($templateFolder . "/jquery.magnific-popup.min.js");
$this->addExternalCss($templateFolder."/dist/select2.min.css");

$this->addExternalJs($templateFolder."/dist/select2.min.js", true);

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

// show alert
function _sa($message, $return = false) {

    $stringTpl = "<div role=\"alert\" class=\"alert alert-danger\">#MESSAGE#</div></div>";

    $icon = "<i class=\"fa fa-info-circle\"></i>";

    if (is_array($message)) {

        $stack = array();
        foreach ($message as $m) {
            $stack[] = "<div>" . $icon . " " . $m . "</div>";
        }

        $string = str_replace("#MESSAGE#", implode("", $stack), $stringTpl);
    } else {

        $string = str_replace("#MESSAGE#", "<div>" . $icon . " " . $message . "</div>", $stringTpl);
    }

    if ($return) {
        return $string;
    }

    echo $string;
}

// show error
function _se($message, $br = true) {
    echo "<div class='validation'>" . ($br ? "<br>" : "") . $message . ($br ? "<br>" : "") . "</div>";
}

//show hotel
function showHotel($hotel){

    $html = '';

    $html.= '<tr>';
        $html.= '<td valign="top" data-label="'.GetMessage('PROGRAM_ACCM_DATE').'" class="day">';
            $html.= $hotel["dateBegin"].' — '.$hotel["dateEnd"].'<br>';
            $html.= num2word($hotel["night"],array("ночь","ночи","ночей"));
        $html.= '</td>';
        $html.= '<td valign="top" data-label="'.GetMessage('PROGRAM_ACCM_HOTEL').'" class="day">';
            $html.= $hotel["hotelName"].' '.$hotel["hotelStars"].'<br>';
            $html.= '<small>'.$hotel["city"].'</small>';
        $html.= '</td>';
        $html.= '<td valign="top" data-label="'.GetMessage('PROGRAM_ACCM_PLACEMENT').'" class="day">';
            $html.= $hotel["accm"]["placement"].' '.$hotel["accm"]["roomType"].' '.$hotel["accm"]["roomCatType"];
        $html.= '</td>';
        $html.= '<td valign="top" data-label="'.GetMessage('PROGRAM_ACCM_FOOD').'" class="day">';
            $html.= $hotel["food"];
        $html.= '</td>';
    $html.= '</tr>';

    return $html;

}

//dm($arResult,false,false,false);

if (empty($arResult['ORDER'])) {
    _sa(Loc::getMessage("EMPTY_CART"));
    return;
}
?>


<div class="step ts-d-none ts-d-lg-block">
    <ul class="payment-step text-center clearfix ts-mx-0">
        <li class="step-select">
            <span>1</span>
            <p><?= Loc::getMessage("SERVICE_CHOOSE") ?></p>
        </li>
        <li class="step-part">
            <span>2</span>
            <p><?= Loc::getMessage("YOUR_ORDER_AND_PAY") ?></p>
        </li>
        <li>
            <span>3</span>
            <p><?= Loc::getMessage("BOOKING_IS_DONE") ?></p>
        </li>
    </ul>
</div>

<div class="separator ts-d-none ts-d-lg-block"></div>

<div class="basket-title ts-mb-3 ts-mb-lg-0">
    <h3 class="ts-text-center">
        <?if(!empty($arResult["PROGRAM_TOUR"])):?><?= GetMessage('PROGRAM_BASKET_TITLE')?><?else:?><?= GetMessage('BASKET_TITLE') ?><?endif;?>
    </h3>
</div>

    <div class="box_map">
        <div class="name_block ts-pb-1">
            <i class="fas fa-home"></i>
            <div class="title_box"><h4><?=GetMessage('PROGRAM_ACCM_TITLE')?></h4></div>
        </div>
        <table class="booking">
            <thead>
            <tr>
                <th class="dow"><?=GetMessage('PROGRAM_ACCM_DATE')?></th>
                <th class="dow"><?=GetMessage('PROGRAM_ACCM_HOTEL')?></th>
                <th class="dow"><?=GetMessage('PROGRAM_ACCM_PLACEMENT')?></th>
                <th class="dow"><?=GetMessage('PROGRAM_ACCM_FOOD')?></th>
            </tr>
            </thead>
            <tbody>
                <?=showHotel($arResult["ORDER"])?>
            </tbody>
        </table>
    </div>


    <?if($arResult["PROGRAM_TOUR"]["avia"]):?>

        <div class="box_map">
            <div class="name_block ts-pb-1">
                <i class="fas fa-plane-departure"></i>
                <div class="title_box"><h4><?=GetMessage('PROGRAM_AVIA_TITLE')?></h4></div>
            </div>
            <table class="booking">
                <thead>
                <tr>
                    <th class="dow"><?=GetMessage('PROGRAM_AVIA_FLIGHT')?></th>
                    <th class="dow"><?=GetMessage('PROGRAM_AVIA_AIRLINE')?></th>
                    <th class="dow"><?=GetMessage('PROGRAM_AVIA_PLACES')?></th>
                    <th class="dow"><?=GetMessage('PROGRAM_AVIA_AIRPORTDEP')?></th>
                    <th class="dow"><?=GetMessage('PROGRAM_AVIA_DATETIMEDEP')?></th>
                    <th class="dow"><?=GetMessage('PROGRAM_AVIA_AIRPORTARR')?></th>
                    <th class="dow"><?=GetMessage('PROGRAM_AVIA_DATETIMEARR')?></th>
                </tr>
                </thead>
                <tbody>
                    <?foreach($arResult["PROGRAM_TOUR"]["avia"] as $avia):?>
                        <tr>
                            <td valign="top" data-label="<?=GetMessage('PROGRAM_AVIA_FLIGHT')?>" class="day">
                                <?=$avia["flight"]?>
                            </td>
                            <td valign="top" data-label="<?=GetMessage('PROGRAM_AVIA_AIRLINE')?>" class="day">
                                <?=$avia["airline"]?>
                            </td>
                            <td valign="top" data-label="<?=GetMessage('PROGRAM_AVIA_PLACES')?>" class="day">
                                <?=$avia["places"]?>
                            </td>
                            <td valign="top" data-label="<?=GetMessage('PROGRAM_AVIA_AIRPORTDEP')?>" class="day">
                                <?=$avia["airportDep"]?>
                            </td>
                            <td valign="top" data-label="<?=GetMessage('PROGRAM_AVIA_DATETIMEDEP')?>" class="day">
                                <?=$avia["dateFrom"]?>
                            </td>
                            <td valign="top" data-label="<?=GetMessage('PROGRAM_AVIA_AIRPORTARR')?>" class="day">
                                <?=$avia["airportArr"]?>
                            </td>
                            <td valign="top" data-label="<?=GetMessage('PROGRAM_AVIA_DATETIMEARR')?>" class="day">
                                <?=$avia["dateTo"]?>
                            </td>
                        </tr>
                    <?endforeach;?>
                </tbody>
            </table>
        </div>

    <?endif?>

    <?if($arResult["PROGRAM_TOUR"]["transfers"]):?>

        <div class="box_map">
            <div class="name_block ts-pb-1">
                <i class="fas fa-exchange-alt"></i>
                <div class="title_box"><h4><?=GetMessage('PROGRAM_TRANSFER_TITLE')?></h4></div>
            </div>
            <table class="booking">
                <thead>
                <tr>
                    <th class="dow"><?=GetMessage('PROGRAM_TRANSFER_DATE')?></th>
                    <th class="dow"><?=GetMessage('PROGRAM_TRANSFER_CITY')?></th>
                    <th class="dow"><?=GetMessage('PROGRAM_TRANSFER_ROUTE')?></th>
                    <th class="dow"><?=GetMessage('PROGRAM_TRANSFER_TRANSPORT')?></th>
                </tr>
                </thead>
                <tbody>
                <?foreach($arResult["PROGRAM_TOUR"]["transfers"] as $transfer):?>
                    <tr>
                        <td valign="top" data-label="<?=GetMessage('PROGRAM_TRANSFER_DATE')?>" class="day">
                            <?=$transfer["dateFrom"]?>
                        </td>
                        <td valign="top" data-label="<?=GetMessage('PROGRAM_TRANSFER_CITY')?>" class="day">
                            <?=$transfer["city"]?>
                        </td>
                        <td valign="top" data-label="<?=GetMessage('PROGRAM_TRANSFER_ROUTE')?>" class="day">
                            <?=$transfer["route"]?>
                        </td>
                        <td valign="top" data-label="<?=GetMessage('PROGRAM_TRANSFER_TRANSPORT')?>" class="day">
                            <?=$transfer["transport"]?>
                        </td>
                    </tr>
                <?endforeach;?>
                </tbody>
            </table>
        </div>

    <?endif?>

    <?if($arResult["PROGRAM_TOUR"]["excursions"]):?>

        <div class="box_map">
            <div class="name_block ts-pb-1">
                <div class="img_box"><img src="<?=SITE_TEMPLATE_PATH?>/img/ex_icon.png"></div>
                <div class="title_box"><h4><?=GetMessage('PROGRAM_EXCURSION_TITLE')?></h4></div>
            </div>
            <table class="booking">
                <thead>
                <tr>
                    <th class="dow"><?=GetMessage('PROGRAM_EXCURSION_DATE')?></th>
                    <th class="dow"><?=GetMessage('PROGRAM_EXCURSION_CITY')?></th>
                    <th class="dow"><?=GetMessage('PROGRAM_EXCURSION_ROUTE')?></th>
                    <th class="dow"><?=GetMessage('PROGRAM_EXCURSION_TRANSPORT')?></th>
                </tr>
                </thead>
                <tbody>
                <?foreach($arResult["PROGRAM_TOUR"]["excursions"] as $excursion):?>
                    <tr>
                        <td valign="top" data-label="<?=GetMessage('PROGRAM_EXCURSION_DATE')?>" class="day">
                            <?=$excursion["dateFrom"]?>
                        </td>
                        <td valign="top" data-label="<?=GetMessage('PROGRAM_EXCURSION_CITY')?>" class="day">
                            <?=$excursion["city"]?>
                        </td>
                        <td valign="top" data-label="<?=GetMessage('PROGRAM_EXCURSION_ROUTE')?>" class="day">
                            <?=$excursion["route"]?>
                        </td>
                        <td valign="top" data-label="<?=GetMessage('PROGRAM_EXCURSION_TRANSPORT')?>" class="day">
                            <?=$excursion["transport"]?>
                        </td>
                    </tr>
                <?endforeach;?>
                </tbody>
            </table>
        </div>

    <?endif?>

    <?if($arResult["PROGRAM_TOUR"]["visas"]):?>

        <div class="box_map">
            <div class="name_block">
                <div class="img_box"><img src="<?=SITE_TEMPLATE_PATH?>/img/visa_icon.png"></div>
                <div class="title_box"><h4><?=GetMessage('PROGRAM_VISA_TITLE')?></h4></div>
            </div>
            <table class="booking">
                <thead>
                <tr>
                    <th class="dow"><?=GetMessage('PROGRAM_VISA_DATE')?></th>
                    <th class="dow"><?=GetMessage('PROGRAM_VISA_COUNTRY')?></th>
                    <th class="dow"><?=GetMessage('PROGRAM_VISA_NAME')?></th>
                    <th class="dow"><?=GetMessage('PROGRAM_VISA_DAYS')?></th>
                </tr>
                </thead>
                <tbody>
                <?foreach($arResult["PROGRAM_TOUR"]["visas"] as $visa):?>
                    <tr>
                        <td valign="top" data-label="<?=GetMessage('PROGRAM_VISA_DATE')?>" class="day">
                            <?=$visa["dateFrom"]?>
                        </td>
                        <td valign="top" data-label="<?=GetMessage('PROGRAM_VISA_COUNTRY')?>" class="day">
                            <?=$visa["country"]?>
                        </td>
                        <td valign="top" data-label="<?=GetMessage('PROGRAM_VISA_NAME')?>" class="day">
                            <?=$visa["name"]?>
                        </td>
                        <td valign="top" data-label="<?=GetMessage('PROGRAM_VISA_DAYS')?>" class="day">
                            <?=num2word($visa["days"],array("день","дня","дней"))?>
                        </td>
                    </tr>
                <?endforeach;?>
                </tbody>
            </table>
        </div>

    <?endif?>

    <?if($arResult["PROGRAM_TOUR"]["insurances"]):?>

        <div class="box_map">
            <div class="name_block">
                <i class="fas fa-file-alt"></i>
                <div class="title_box"><h4><?=GetMessage('PROGRAM_INSURANCE_TITLE')?></h4></div>
            </div>
            <?foreach($arResult["PROGRAM_TOUR"]["insurances"] as $insurances):?>
                <?$str = GetMessage('PROGRAM_INSURANCE_TEXT');
                $str = str_replace("#INSURANCE_NAME#", $insurances["name"], $str);
                $str = str_replace("#INSURANCE_DATE#", $insurances["dateFrom"], $str);
                $str = str_replace("#INSURANCE_DAYS#", num2word($insurances["days"],array("день","дня","дней")), $str);?>
                <p><?=$str?></p>
            <?endforeach;?>
        </div>

    <?endif?>

    <div class="box_map">
        <div class="name_block">
            <i class="fas fa-dollar-sign"></i>
            <div class="title_box"><h4><?=GetMessage('PROGRAM_PRICE_TITLE')?></h4></div>
        </div>
        <div id="total-cost-info">
            <div><small><?= Loc::getMessage("TOTAL_PEOPLE")?>:</small>&nbsp;<span id="people"><?= $arResult['ORDER']["nmen"]?></span></div>
            <div><small><?= Loc::getMessage("TOTAL_COST")?>:</small>&nbsp;<span id="total-area"><?= $arResult['ORDER']["price"]?></span></div>
        </div>
       <!-- <table id="total-cost-info" class="booking">
            <tbody>
            <tr><td><?/*= Loc::getMessage("TOTAL_PEOPLE")*/?>:&nbsp; </td><td><?/*= $arResult['ORDER']["nmen"]*/?></td></tr>
            <tr><td><?/*= Loc::getMessage("TOTAL_COST")*/?>:&nbsp; </td><td id="total-area"><?/*= $arResult['ORDER']["price"]*/?></td></tr>
            </tbody>
        </table>-->
    </div>

<div class="row top-m">
    <div class="">
        <div class="main-cn bg-white clearfix">
            <?
            if ($arResult["ERRORS"]["BOOKING"]) {
                _sa(Loc::getMessage("BOOKING_ERROR"));
            } else if ($arResult["ERRORS"]["FORM"]) {
                foreach ($arResult["ERRORS"]["FORM"] as $key => $val) {
                    if ((string) $key != "TOURIST") {
                        $stack[] = Loc::getMessage($val);
                    } else {
                        $stack[] = Loc::getMessage("WRONG_SOME_TOURIST");
                    }
                }
                _sa($stack);
            }

            $max_people = 0;
            $max_people = $arResult["ORDER"]["nmen"] > $max_people ? $arResult["ORDER"]["nmen"] : $max_people;

            /*if(isset($arResult["ORDER"])){
                $active_service_count = 1;
            }*/
            /*$max_people = 0;
            while ($basketItem = $arResult["ORDER"]->fetch()) :
                $arItem = $basketItem["item"]->getPropertiesLikeArray();
                $arItem["position"] = $basketItem["position"];
                if ($arItem["can_buy"]) {
                    $people = $arItem["adults"] + $arItem["children"];
                    $total_people_count += $people;
                    $max_people = $people > $max_people ? $people : $max_people;
                    $active_service_count++;
                }
                */?><!--
                <div class="payment-room mb-10 pos-rel item-cart">
                    <div class="row">
                        <div class="col-lg-12 payment-room-priceservice blueborder">
                            <h2 class="mt-none"><?/*= $arResult["PARENT_ELEMENTS"][$arItem["type"]][$arResult["SERVICES"][$arItem["type"]][$arItem["service_id"]]["UF_IBLOCK_ELEMENT_ID"]]["NAME"] */?></h2>
                            <div class="payment-price">
                                <div class="col-lg-6">
                                    <?/* if (!empty($arResult["SERVICES"][$arItem["type"]][$arItem["service_id"]]["PICTURE"]['src'])): */?>
                                        <figure>
                                            <img src="<?/*= $arResult["SERVICES"][$arItem["type"]][$arItem["service_id"]]["PICTURE"]['src'] */?>" alt="<?/*= $arItem["UF_NAME" . POSTFIX_PROPERTY] */?>">
                                        </figure>
                                    <?/* endif; */?>
                                    <div class="mt-5">
                                        <b><?/*= Loc::getMessage("NAME") */?>:</b> <?/*= $arResult["SERVICES"][$arItem["type"]][$arItem["service_id"]]["UF_NAME" . POSTFIX_PROPERTY] */?>
                                    </div>
                                    <div class="mt-5">
                                        <b><?/*= Loc::getMessage("RATE") */?>:</b> <?/*= $arResult["RATES"][$arItem["type"]][$arItem["rate_id"]]["UF_NAME" . POSTFIX_PROPERTY] */?>
                                    </div>
                                    <?/*
                                    $food = null;
                                    for ($i = 0, $cnt = count($arResult["RATES"][$arItem["type"]][$arItem["rate_id"]]["UF_FOOD_ID"]); $i < $cnt; $i++) {
                                        $food[] = $arResult["FOOD"][$arResult["RATES"][$arItem["type"]][$arItem["rate_id"]]["UF_FOOD_ID"][$i]];
                                    }
                                    */?>
                                    <?/*
                                    $food = array_filter($food, function ($it) {
                                        return $it > 0;
                                    });
                                    if ($food):
                                        */?>
                                        <div class="mt-5">
                                            <b><?/*= Loc::getMessage("FOOD") */?>:</b> <?/*= implode(", ", $food) */?>
                                        </div>
                                    <?/* endif */?>
                                </div>
                                <div class="col-lg-3">
                                    <?/*
                                    if (!$arItem["can_buy"]) {
                                        _sa(Loc::getMessage("CAN'T_BOOKING"));
                                    }
                                    $date_from = date("d.m.Y", $arItem["date_from"]);
                                    $date_to = date("d.m.Y", $arItem["date_to"]);
                                    */?>
                                    <div class="mt-5"><b><?/*= Loc::getMessage("DATE_FROM") */?>:</b> <?/*= $date_from */?></div>
                                    <?/* if ($arItem["date_to"]) : */?>
                                        <div class="mt-5"><b><?/*= Loc::getMessage("DATE_TO") */?>:</b> <?/*= $date_to */?></div>
                                    <?/* endif */?>
                                    <?/* if ($arItem["duration"]): */?>
                                        <div class="mt-5"><b><?/*= Loc::getMessage("DAYS") */?>:</b> <?/*= $arItem["duration"]; */?></div>
                                    <?/* endif */?>
                                    <div class="mt-5"><b><?/*= Loc::getMessage("ADULTS") */?>:</b> <?/*= $arItem["adults"] */?></div>
                                    <?/* if ($arItem["children"] > 0): */?>
                                        <div class="mt-5"><b><?/*= Loc::getMessage("CHILDREN") */?>:</b> <?/*= $arItem["children"] */?></div>
                                    <?/* endif */?>
                                </div>
                                <div class="col-lg-3">
                                    <div class="price-in-basket">
                                        <b><?/*= Loc::getMessage("PRICE") */?>: </b><?/*= \travelsoft\Currency::getInstance()->convertCurrency($arItem["price"], $arItem["currency"]) */?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            --><?/* endwhile; */?>
            <?
            if (count($arResult['POST']['tourist']) > $max_people) {
                $max_people = count($arResult['POST']['tourist']);
            }
            if ($max_people):
                //$arCitizenship = \travelsoft\booking\sts\Utils::getCitizenship();
                $arCitizenship = array("Россия","Украина","Беларусь");
                ?>

            <!-- Контейнер -->
            <div class="ts-theme">
                <!-- Форма -->

                <form id="booking" action="<?= $APPLICATION->GetCurPage(false) ?>" method="POST" class="simple-from ts-wrap ts-px-4 ts-py-lg-2 ts-bg-color">
                    <div id="form-shading"></div>
                    <?= bitrix_sessid_post() ?>
                    <input type="hidden" value="<?=$arResult["ORDER"]["priceKey"]?>" name="make_booking[priceKey]">
                    <input type="hidden" value="<?=$arResult["ORDER"]["operatorId"]?>" name="make_booking[operatorId]">


                        <div class="ts-row tourist-row">
                            <?
                            $tag_open = true;
                            $er = $arResult["ERRORS"]["FORM"]["TOURIST"];
                            for ($i = 0; $i < $max_people; $i++):
                                ?>

                                <div class="ts-col-24 ts-col-lg-12 tourist-block ts-mb-2">
                                    <div class="sidber-box">

                                        <!-- Заголовок -->
                                        <div class="ts-title ts-px-0 ts-py-1">
                                            <h4 class="ts-reset"><?= Loc::getMessage("TOURIST") ?> <?= $i + 1 ?></h4>
                                        </div>

                                        <div class="ts-content popular-post-inner ts-px-0 ts-pt-2 ts-pb-3">

                                            <div class="ts-col-24 ts-mb-2 form-block">
                                                <div class="ts-input<?if(in_array("WRONG_NAME", $er[$i])):?> has-error<?endif?>">
                                                    <label for="" class="ts-d-none"></label>
                                                    <div class="placeholder"><span><?= Loc::getMessage("PLACEHOLDER_NAME") ?></span></div>
                                                    <?
                                                    if (in_array("WRONG_NAME", $er[$i])) {
                                                        _se(Loc::getMessage("WRONG_NAME"),false);
                                                    }
                                                    ?>
                                                    <div class="input">
                                                        <input data-error-type="WRONG_NAME" value="<?= $arParams["_POST"]["tourist"][$i]['name'] ?>" type="text" name="make_booking[tourist][<?= $i ?>][name]" class="field-input to-validate">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="ts-col-24 ts-mb-2 form-block">
                                                <div class="ts-input<?if(in_array("WRONG_LAST_NAME", $er[$i])):?> has-error<?endif?>">
                                                    <label for="" class="ts-d-none"></label>
                                                    <div class="placeholder"><span><?= Loc::getMessage("PLACEHOLDER_LAST_NAME") ?></span></div>
                                                    <?
                                                    if (in_array("WRONG_LAST_NAME", $er[$i])) {
                                                        _se(Loc::getMessage("WRONG_LAST_NAME"),false);
                                                    }
                                                    ?>
                                                    <div class="input">
                                                        <input  data-error-type="WRONG_LAST_NAME" value="<?= $arParams["_POST"]["tourist"][$i]['last_name'] ?>" type="text" name="make_booking[tourist][<?= $i ?>][last_name]" placeholder="" class="field-input to-validate">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="ts-col-24 ts-mb-2 form-block">
                                                <div class="ts-select<?if(in_array("WRONG_MALE", $er[$i])):?> has-error<?endif?>">
                                                    <label for="" class="ts-d-none"></label>
                                                    <div class="placeholder"><span><?= Loc::getMessage("SEX") ?></span></div>
                                                    <?
                                                    if (in_array("WRONG_MALE", $er[$i])) {
                                                        _se(Loc::getMessage("WRONG_MALE"),false);
                                                    }
                                                    ?>

                                                        <select data-error-type="WRONG_MALE" class="to-validate js-select2 w-50" name="make_booking[tourist][<?= $i ?>][male]">
                                                            <? foreach ($arResult["MALE"] as $val => $title): ?>
                                                                <option <? if ($arParams["_POST"]["tourist"][$i]['male'] == $val): ?>selected<? endif ?> value="<?= $val ?>"><?= Loc::getMessage($title) ?></option>
                                                            <? endforeach ?>
                                                        </select>

                                                </div>
                                            </div>
                                            <div class="ts-col-24 ts-mb-2 form-block">
                                                <div class="ts-input<?if(in_array("WRONG_BIRTHDATE", $er[$i])):?> has-error<?endif?>">
                                                    <label for="" class="ts-d-none"></label>
                                                    <div class="placeholder"><span><?= Loc::getMessage("PLACEHOLDER_BIRTHDATE") ?></span></div>
                                                    <?
                                                    if (in_array("WRONG_BIRTHDATE", $er[$i])) {
                                                        _se(Loc::getMessage("WRONG_BIRTHDATE"),false);
                                                    }
                                                    ?>
                                                    <div class="input">
                                                        <input data-error-type="WRONG_BIRTHDATE" value="<?= $arParams["_POST"]['tourist'][$i]["birthdate"] ?>" pattern="<?= $arResult["PATTERNS"]["birthdate"] ?>" name="make_booking[tourist][<?= $i ?>][birthdate]" type="text" placeholder="<?= Loc::getMessage("DDMMYYYY") ?>" class="birthdate-field field-input to-validate">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="ts-col-24 ts-mb-2 form-block">
                                                <div class="ts-input<?if(in_array("WRONG_PASSPORT", $er[$i])):?> has-error<?endif?>">
                                                    <label for="" class="ts-d-none"></label>
                                                    <div class="placeholder"><span><?= Loc::getMessage("PLACEHOLDER_PASSPORT") ?></span></div>
                                                    <?
                                                    if (in_array("WRONG_PASSPORT", $er[$i])) {
                                                        _se(Loc::getMessage("WRONG_PASSPORT"),false);
                                                    }
                                                    ?>
                                                    <div class="input">
                                                        <input data-error-type="WRONG_PASSPORT" value="<?= $arParams["_POST"]['tourist'][$i]["passport"] ?>" name="make_booking[tourist][<?= $i ?>][passport]" type="text" placeholder="" class="field-input to-validate">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="ts-col-24 ts-mb-2 form-block">
                                                <div class="ts-input<?if(in_array("WRONG_PASSPORT_DATE", $er[$i])):?> has-error<?endif?>">
                                                    <label for="" class="ts-d-none"></label>
                                                    <div class="placeholder"><span><?= Loc::getMessage("PLACEHOLDER_PASSPORT_DATE") ?></span></div>
                                                    <?
                                                    if (in_array("WRONG_PASSPORT_DATE", $er[$i])) {
                                                        _se(Loc::getMessage("WRONG_PASSPORT_DATE"),false);
                                                    }
                                                    ?>
                                                    <div class="input">
                                                        <input data-error-type="WRONG_PASSPORT_DATE" value="<?= $arParams["_POST"]['tourist'][$i]["passport_date"] ?>" pattern="<?= $arResult["PATTERNS"]["passport_date"] ?>" name="make_booking[tourist][<?= $i ?>][passport_date]" type="text" placeholder="<?= Loc::getMessage("DDMMYYYY") ?>" class="birthdate-field field-input to-validate">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="ts-col-24 ts-mb-2 form-block">
                                                <div class="ts-select<?if(in_array("WRONG_CITIZENSHIP", $er[$i])):?> has-error<?endif?>">
                                                    <label for="" class="ts-d-none"></label>
                                                    <div class="placeholder"><span><?= Loc::getMessage("PLACEHOLDER_CITIZEN") ?></span></div>
                                                    <?
                                                    if (in_array("WRONG_CITIZENSHIP", $er[$i])) {
                                                        _se(Loc::getMessage("WRONG_CITIZENSHIP"),false);
                                                    }
                                                    ?>

                                                        <select data-error-type="WRONG_CITIZENSHIP" class="to-validate citizenship js-select2  w-50" name="make_booking[tourist][<?= $i ?>][citizenship]">
                                                            <? foreach ($arCitizenship as $ID => $name) { ?>
                                                                <option <?
                                                                if ($arParams["_POST"]['tourist'][$i]["citizenship"] == $name) {
                                                                    echo "selected";
                                                                }
                                                                ?> value="<?= $name ?>"><?= $name ?></option>
                                                            <? } ?>
                                                        </select>

                                                </div>
                                            </div>
                                            <div class="ts-col-24 form-block">
                                                <div class="ts-input<?if(in_array("WRONG_PASSPORT_ID", $er[$i])):?> has-error<?endif?>">
                                                    <label for="" class="ts-d-none"></label>
                                                    <div class="placeholder"><span><?= Loc::getMessage("PLACEHOLDER_PASSPORT_ID") ?></span></div>
                                                    <?
                                                    if (in_array("WRONG_PASSPORT_ID", $er[$i])) {
                                                        _se(Loc::getMessage("WRONG_PASSPORT_ID"),false);
                                                    }
                                                    ?>
                                                    <div class="input">
                                                        <input data-error-type="WRONG_PASSPORT_ID" value="<?= $arParams["_POST"]["tourist"][$i]['passport_id'] ?>" type="text" name="make_booking[tourist][<?= $i ?>][passport_id]" placeholder="<?= Loc::getMessage('PLACEHOLDER_PASSPORT_ID_ADDTEXT')?>" class="field-input to-validate">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?
                                if (($i + 1) % 2 === 0):
                                    $tag_open = false;
                                    ?>
                                    </div>
                                    <?
                                    if (($i + 1) < $max_people):
                                        $tag_open = true;
                                        ?>
                                        <div class="row form bg-none tourist-row">
                                    <? endif ?>
                                <? endif ?>
                            <? endfor ?>
                            <? if ($tag_open): ?>
                                <span class="additional-tourist-area"></span>
                                </div>
                            <? endif ?>
                            <?if ($total_people_count > $max_people): ?>
                                <div class="row form bg-none text-right">
                                    <div class="col-md-12">
                                        <button style="padding: 10px" type="button" id="add-tourist-btn" class="awe-btn awe-btn-1 c-button b-40 bg-grey-3-t hv-grey-3-t b-1">
                                        <?= Loc::getMessage('ADD_TOURIST_BTN_TITLE')?></button>
                                    </div>

                                </div>
                            <?endif?>

                            <!-- типо текстареа -> ваши пожелания 1 -->

                                <div class="ts-row ts-bg-color  ts-pb-2">
                                    <div class="ts-col-24 ts-px-4">
                                    <div class="ts-input form-block">
                                        <label for="" class="ts-d-none"></label>
                                        <div class="placeholder"><?= Loc::getMessage("COMMENT") ?></div>
                                        <div class="input">
                                            <input value="<?= $arParams["_POST"]["comment"] ?>" type="text" name="make_booking[comment]" placeholder="<?= Loc::getMessage("PLACEHOLDER_COMMENT") ?>" class="field-input">
                                        </div>
                                    </div>
                                    </div>
                                </div>

                                <div class="ts-row info-form alert-box ts-bg-color ts-mb-2 form" style="border:0">
                                    <div class="insert-after ts-col-24 ts-col-lg-12">
                                        <div class="ts-width-100 ts-mb-3 ">
                                            <div class="ts-text">
                                                <p class="input-description"><?= Loc::getMessage("EMAIL_DESCRIPTION") ?></p>
                                            </div>
                                            <div class="ts-input form-block<?if(array_search("WRONG_EMAIL", $arResult["ERRORS"]["FORM"]) !== null && array_search("WRONG_EMAIL", $arResult["ERRORS"]["FORM"]) !== false):?> has-error<?endif?>">
                                                <label for="" class="ts-d-none"></label>
                                                <div class="placeholder"><?= Loc::getMessage("PLACEHOLDER_YOUR_EMAIL") ?></div>
                                                <?
                                                $sr = array_search("WRONG_EMAIL", $arResult["ERRORS"]["FORM"]);
                                                if ($sr !== null && $sr !== false) {
                                                    _se(Loc::getMessage("WRONG_EMAIL"), false);
                                                }
                                                ?>
                                                <div class="input">
                                                    <input data-error-type="WRONG_EMAIL" <? if ($arResult["USER"]["email"]): ?>value="<?= $arResult["USER"]["email"] ?>" readonly<? endif ?>  name="make_booking[email]" id="main-email" type="email" placeholder="mail@example.com" class="field-input to-validate">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="ts-col-24 ts-col-lg-12 ts-mb-2">
                                        <div class="ts-text">
                                            <p class="input-description"><?= Loc::getMessage("PHONE_DESCRIPTION") ?></p>
                                        <div class="ts-input form-block<?if(array_search("WRONG_PHONE", $arResult["ERRORS"]["FORM"]) !== null && array_search("WRONG_PHONE", $arResult["ERRORS"]["FORM"]) !== false):?> has-error<?endif?>">
                                            <label for="" class="ts-d-none"></label>
                                            <div class="placeholder"><?= Loc::getMessage("PLACEHOLDER_YOUR_PHONE") ?></div>
                                            <?
                                            $sr = array_search("WRONG_PHONE", $arResult["ERRORS"]["FORM"]);
                                            if ($sr !== null && $sr !== false) {
                                                _se(Loc::getMessage("WRONG_PHONE"), false);
                                            }
                                            ?>
                                            <div class="input">
                                                <input data-error-type="WRONG_PHONE" value="<?= $arParams["_POST"]['phone'] ?>" pattern="<?= $arResult["PATTERNS"]["phone"] ?>" name="make_booking[phone]" id="phone-email" type="text" placeholder="+375 XX XXXXXXX" class="field-input to-validate">
                                            </div>
                                        </div>
                                    </div>

                                <? //if ($USER->IsAdmin()):?>
                                <!--<div class="row">
                                    <div class="col-md-12">
                                        <table id="total-cost-info2">
                                            <tbody>
                                                <tr><td><?/*= Loc::getMessage("TOTAL_PEOPLE")*/?>:&nbsp; </td><td><?/*= $arResult['ORDER']["nmen"]*/?></td></tr>
                                                <tr><td><?/*= Loc::getMessage("TOTAL_COST")*/?>:&nbsp; </td><td id="total-area"><?/*= $arResult['ORDER']["price"]*/?></td></tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>-->
                                <? //endif?>
                            </div>
                            <? $arGroups = $USER->GetUserGroupArray(); ?>
                            <? if (!$USER->IsAuthorized() || !in_array(\Bitrix\Main\Config\Option::get("travelsoft.booking.sts", "AGENT_GROUP_ID"), $arGroups)): ?>
                                <div class="ts-col-24 ts-mb-2 info-form submit" >
                                    <div class="ts-checkbox ts-w-100 checkbox<?if(array_search("WRONG_ACCEPT", $arResult["ERRORS"]["FORM"]) !== null && array_search("WRONG_ACCEPT", $arResult["ERRORS"]["FORM"]) !== false):?> has-error<?endif?>">


                                        <label for="accept" class="ts-pl-1 checkbox__label ts-py-0 ts-mx-1"><a id="show-popup" href="/popup/agreement.html">
                                                <span class="checkbox-text"><?= Loc::getMessage("ACCEPT") ?></span></a></label>
                                        <?
                                        $sr = array_search("WRONG_ACCEPT", $arResult["ERRORS"]["FORM"]);
                                        if ($sr !== null && $sr !== false) {
                                            _se(Loc::getMessage("WRONG_ACCEPT"), false);
                                        }
                                        ?>
                                        <input value="0" name="make_booking[accept]" type="hidden" id="notcheckedaccept">
                                        <input <?if($arParams["_POST"]['accept']):?>checked<?endif?> class="checkbox__input to-validate checkbox-form ts-d-none" data-error-type="WRONG_ACCEPT" value="1" name="make_booking[accept]" type="checkbox" id="accept">
                                    </div>
                                    <div class="ts-text">
                                        <p><?= Loc::getMessage("NOTIFY"); ?></p>
                                    </div>
                                </div>
                            <? endif ?>
                            <!--<div class="info-form submit text-center">
                                <?/* $arGroups = $USER->GetUserGroupArray(); */?>
                                <?/* if (!$USER->IsAuthorized() || !in_array(\Bitrix\Main\Config\Option::get("travelsoft.booking.sts", "AGENTS_GROUP_ID"), $arGroups)): */?>
                                    <div class="confirm-terms radio-checkbox">
                                        <div class="input-entry color-3 form__checkbox checkbox">
                                            <input class="checkbox__input to-validate checkbox-form" data-error-type="WRONG_ACCEPT" name="make_booking[accept]" type="checkbox" id="accept">
                                            <label for="accept" class="checkbox__label bx-filter-param-label clearfix">
                                                <span class="sp-check"><i class="fa fa-check"></i></span>
                                                <a id="show-popup" href="/popup/agreement.html"><span class="checkbox-text"><?/*= Loc::getMessage("ACCEPT") */?></span></a>
                                            </label>
                                        </div>
                                        <p><?/*= Loc::getMessage("NOTIFY"); */?></p>
                                    </div>
                                <?/* endif */?>
                            </div>-->

                                <div class="ts-col-24">
                                    <div class="ts-submit ts-max-width-370">
                                        <input name="make_booking[submit]" id="booking_now" value="<?= Loc::getMessage("BOOKING_BTN") ?>" type="submit" class="form__btn-send c-button w-100" >
                                    </div>
                                </div>
                            </div>
                </div>


                </form>

            </div>
            <? endif ?>
        </div>
    </div>
</div>
<script>



        select2form = $('#booking select.js-select2');

        /**
         * инициализируем select2
         * @param {object} options
         */
        __initSelect2 = function (options) {

            // инициализируем select
            function select2 (obj) {

                obj.select2({
                    theme: "material",
                    //allowClear: true,
                    formatNoMatches: function () {
                        return "<?= GetMessage("NOMATCHES")?>";
                    },
                    minimumResultsForSearch: -1
                });

                $(options).each(function(e){
                    if($(this).val().length != 0) {
                        $(this).closest('.ts-select').addClass('active');

                    }
                });
                $(options).on('select2:select', function (e) {
                    var data = e.params.data;
                    if(data.element.value.length != 0){
                        let elem = data.element.closest('.ts-select');
                        $(elem).addClass('active');

                    }
                });

            }

            select2(options);
            $(".select2-selection__arrow")
                .addClass("arrow-drop");

            $(options).each(function(e){
                var select = $(this).closest('.ts-select').children()[0];

                if($(this).val().length != 0) {
                    $(this).closest('.ts-select').addClass('active');

                }
            });


            $(options).on('select2:select', function (e) {
                var data = e.params.data;
                if(data.element.value.length != 0){
                    let elem = data.element.closest('.ts-select');
                    $(elem).addClass('active');

                }
            });

        };



    (function ($) {

        $(window.document).ready(function () {

            __initSelect2(select2form);

            let input = document.querySelectorAll('.ts-input input');
            let checkboxSaveMe = $('.ts-checkbox i');
            let checkboxAccept = $('#accept');

            console.log(typeof checkboxAccept);

            function myCheckedCheckbox(node) {

                if(node.prop('checked')) {
                    checkboxSaveMe.addClass('active');
                } else {
                    checkboxSaveMe.removeClass('active');
                }

            }

            [].slice.call(input).forEach(item=>{
                if($(item).val().length != 0){
                    $(item).closest('.ts-input').addClass('active');
                }
            });

            if(typeof checkboxAccept !== "undefined") {

                myCheckedCheckbox(checkboxAccept);

                checkboxSaveMe.on('click',function(e){
                    $(this).toggleClass('active');
                    if ($(this).hasClass('active')) {
                        checkboxAccept.prop("checked", true);
                    } else {
                        checkboxAccept.prop("checked", false);
                        checkboxSaveMe.parent().addClass('has-error');
                    }
                });

            }

            $(input).on('focus',function(e){
                $(this).closest('.ts-input').addClass('active focus');

            });
            $(input).on('focusout',function(e){
                if($(this).val().length == 0){
                    $(this).closest('.ts-input').removeClass('active focus');
                }
                else $(this).closest('.ts-input').removeClass('focus');

            });
            $(input).on('keyup',function(e){
                if($(this).val().length != 0){
                    $(this).closest('.ts-input').addClass('active');
                }
                else{
                    $(this).closest('.ts-input').removeClass('active');
                }
            });

            $.make_booking_component_dialog({
                email_input_id: "main-email",
                mount_point_id: "phone-email",
                booking_form_id: "booking",
                booking_btn_id: "booking_now",
                empty_cart_message: '<?= _sa(Loc::getMessage("EMPTY_CART"), $alertMessageTpl) ?>',
                //active_service_count: <?= $active_service_count > 0 ? $active_service_count : 0 ?>,
                cart_item_class_container: "item-cart",
                cart_items_count: <? $cnt = 1; echo($cnt > 0 ? $cnt : 0)?>,
                preloader: "<?= $templateFolder ?>/25.gif",
                del_pos: <?= Bitrix\Main\Web\Json::encode($arDelPos) ?>,
                sessid: "<?= bitrix_sessid() ?>",
                <?
                // промо
                if ($cnt > 0): ?>
                totalAreaSelector: "#total-area",
                //discountAreaSelector: "#discount-area",
                <? endif;?>
                <?if ($total_people_count > $max_people): ?>
                additional_tourists: {
                    count: <?= $total_people_count - $max_people?>,
                    index: <?= $max_people?>,
                    template: '<div class="col-md-6 tourist-block">' +
                        '<h3><?= Loc::getMessage("TOURIST") ?> #number#</h3>' +

                        '<div class="col-xs-12 col-sm-12"><div class="form-block type-2 clearfix"><div class="form-label color-dark-2"><?= Loc::getMessage("PLACEHOLDER_NAME") ?></div>' +
                        '<input data-error-type="WRONG_NAME" value="" type="text" name="make_booking[tourist][#index#][name]" placeholder="" class="field-input to-validate">' +
                        '</div></div>' +
                        '<div class="col-xs-12 col-sm-12"><div class="form-block type-2 clearfix"><div class="form-label color-dark-2"><?= Loc::getMessage("PLACEHOLDER_LAST_NAME") ?></div>' +
                        '<input  data-error-type="WRONG_LAST_NAME" value="" type="text" name="make_booking[tourist][#index#][last_name]" placeholder="" class="field-input to-validate">' +
                        '</div></div>' +
                        '<div class="col-xs-12 col-sm-12"><div class="form-block type-2 clearfix"><div class="form-label color-dark-2"><?= Loc::getMessage("SEX") ?></div>' +
                        '<select data-error-type="WRONG_MALE" class="to-validate" name="make_booking[tourist][#index#][male]">' +
                        <?
                        $options = '';
                        foreach ($arResult["MALE"] as $val => $title) {
                            $options .= '<option value="' . $val . '">' . Loc::getMessage($title) . '</option>';
                        }?>
                        '<?= $options?>' +
                        '</select>' +
                        '</div></div>' +
                        '<div class="col-xs-12 col-sm-12"><div class="form-block type-2 clearfix"><div class="form-label color-dark-2"><?= Loc::getMessage("PLACEHOLDER_BIRTHDATE") ?></div>' +
                        '<input data-error-type="WRONG_BIRTHDATE" value="" pattern="<?= str_replace('\\', '\\\\', $arResult["PATTERNS"]["birthdate"]) ?>" name="make_booking[tourist][#index#][birthdate]" type="text" placeholder="<?= Loc::getMessage("DDMMYYYY") ?>" class="birthdate-field field-input to-validate">' +
                        '</div></div>' +
                        '<div class="col-xs-12 col-sm-12"><div class="form-block type-2 clearfix"><div class="form-label color-dark-2"><?= Loc::getMessage("PLACEHOLDER_PASSPORT") ?></div>' +
                        '<input data-error-type="WRONG_PASSPORT" value="" name="make_booking[tourist][#index#][passport]" type="text" placeholder="" class="field-input to-validate">' +
                        '</div></div>' +
                        '<div class="col-xs-12 col-sm-12"><div class="form-block type-2 clearfix"><div class="form-label color-dark-2"><?= Loc::getMessage("PLACEHOLDER_CITIZEN") ?></div>' +
                        '<select data-error-type="WRONG_CITIZENSHIP" class="to-validate" name="make_booking[tourist][#index#][citizenship]">' +
                        <?
                        $options = '';
                        foreach ($arCitizenship as $ID => $name) {

                            $options .= '<option value=\"' . $ID . '\">' . $name . '</option>';
                        }?>
                        "<?= $options?>" +
                        '</select>' +
                        '</div></div>' +
                        '</div>'
                },
                <?endif?>
                is_authorized: <? if ($arResult["USER"]["is_authorized"]): ?>true<? else: ?>false<? endif ?>,
                forgot_password_link: "/personal/index.php?forgot_password=yes",
                messages: {
                    booking_btn: "<?= Loc::getMessage("BOOKING_BTN") ?>",
                    empty_email: "<?= Loc::getMessage("DIALOG_EMPTY_EMAIL") ?>",
                    confirm_email_error: "<?= Loc::getMessage("DIALOG_CONFIRM_EMAIL_ERROR") ?>",
                    confirm_email_not_equal_email: "<?= Loc::getMessage("DIALOG_CONFIRM_EMAIL_NOT_EQUAL_EMAIL") ?>",
                    placeholder_confirm_email: "<?= Loc::getMessage("DIALOG_PLACEHOLDER_CONFIRM_EMAIL") ?>",
                    placeholder_password: "<?= Loc::getMessage("DIALOG_PLACEHOLDER_PASSWORD") ?>",
                    do_registration_button: "<?= Loc::getMessage("DIALOG_DO_REGISTRATION_BTN") ?>",
                    do_authorize_button: "<?= Loc::getMessage("DIALOG_DO_AUTHORIZE_BTN") ?>",
                    forgot_password: "<?= Loc::getMessage("DIALOG_FORGOT_PASSWORD") ?>",
                    enter_the_password: "<?= Loc::getMessage("DIALOG_ENTER_THE_PASSWORD") ?>",
                    _404: "<?= Loc::getMessage("DIALOG_404") ?>",
                    _500: "<?= Loc::getMessage("DIALOG_505") ?>",
                    default_error_text: "<?= Loc::getMessage("DIALOG_DEFAULT_ERROR_TEXT") ?>",
                    status_0: "<?= Loc::getMessage("DIALOG_STATUS_0") ?>",
                    status_1: "<?= Loc::getMessage("DIALOG_STATUS_1") ?>",
                    status_2: "<?= Loc::getMessage("DIALOG_STATUS_2") ?>",
                    status_3: "<?= Loc::getMessage("DIALOG_STATUS_3") ?>",
                    status_4: "<?= Loc::getMessage("DIALOG_STATUS_4") ?>",
                    status_5: "<?= Loc::getMessage("DIALOG_STATUS_5") ?>",
                    status_6: "<?= Loc::getMessage("DIALOG_STATUS_6") ?>",
                    status_7: "<?= Loc::getMessage("DIALOG_STATUS_7") ?>",
                    status_8: "<?= Loc::getMessage("DIALOG_STATUS_8") ?>",
                    status_9: "<?= Loc::getMessage("DIALOG_STATUS_9") ?>",
                    <?if ($cnt > 0 && $USER->IsAdmin()):?>
                    status_10: "<?= Loc::getMessage("DIALOG_STATUS_10") ?>",
                    status_11: "<?= Loc::getMessage("DIALOG_STATUS_11") ?>",
                    status_12: "<?= Loc::getMessage("DIALOG_STATUS_12") ?>",
                    status_13: "<?= Loc::getMessage("DIALOG_STATUS_13") ?>",
                    status_14: "<?= Loc::getMessage("DIALOG_STATUS_14") ?>",
                    status_15: "<?= Loc::getMessage("DIALOG_STATUS_15") ?>",
                    status_16: "<?= Loc::getMessage("DIALOG_STATUS_16") ?>",
                    status_17: "<?= Loc::getMessage("DIALOG_STATUS_17") ?>",
                    status_18: "<?= Loc::getMessage("DIALOG_STATUS_18") ?>",
                    <?endif?>
                },
                sendBeforeForm: function (data) {

                    function scrollTo(element) {
                        $('html, body').animate({
                            scrollTop: $(element).offset().top - 110
                        }, 1000);
                    }

                    var errorsContainer = null;

                    if (!data.is_authorized) {
                        alert("<?= Loc::getMessage("MUST_BE_AUTHORIZE_NOTIFY") ?>");
                        return false;
                    }

                    // VALIDATION FIELDS
                    $(".error-container").remove();
                    $(".to-validate").each(function () {

                        var $this = $(this), val = $(this).val(), errorType = $this.data("error-type"), error = null;

                        console.log($this, 1);
                        console.log(errorType, 2);

                        if (errorType == "WRONG_NAME") {
                            if (val.length < 3) {
                                error = "<? _se(Loc::getMessage("WRONG_NAME")) ?>";
                            }
                        } else if (errorType == "WRONG_LAST_NAME") {
                            if (val.length < 3) {
                                error = "<? _se(Loc::getMessage("WRONG_LAST_NAME")) ?>";
                            }
                        } else if (errorType == "WRONG_MALE") {
                            if (!val) {
                                error = "<? _se(Loc::getMessage("WRONG_MALE")) ?>";
                            }
                        } else if (errorType == "WRONG_BIRTHDATE") {
                            if (!val) {
                                error = "<? _se(Loc::getMessage("WRONG_BIRTHDATE")) ?>";
                            }
                        } else if (errorType == "WRONG_PASSPORT") {
                            if (!val) {
                                error = "<? _se(Loc::getMessage("WRONG_PASSPORT")) ?>";
                            }
                        } else if (errorType == "WRONG_PASSPORT_DATE") {
                            if (!val) {
                                error = "<? _se(Loc::getMessage("WRONG_PASSPORT_DATE")) ?>";
                            }
                        } else if (errorType == "WRONG_CITIZENSHIP") {
                            if (!val) {
                                error = "<? _se(Loc::getMessage("WRONG_CITIZENSHIP")) ?>"
                            }
                        } else if (errorType == "WRONG_PASSPORT_ID") {
                            var citizenship = $this.closest('.popular-post-inner').find('.citizenship').val();
                            if (citizenship == "Беларусь" && !val) {
                                error = "<? _se(Loc::getMessage("WRONG_PASSPORT_ID")) ?>";
                            }
                        } else if (errorType == "WRONG_EMAIL") {
                            if (!val) {
                                error = "<? _se(Loc::getMessage("WRONG_EMAIL"), false) ?>";
                            }
                        } else if (errorType == "WRONG_PHONE") {
                            if (!val) {
                                error = "<? _se(Loc::getMessage("WRONG_PHONE"), false) ?>";
                            }
                        } else if (errorType == "WRONG_ACCEPT") {
                            if (!$this.is(":checked")) {
                                error = "<? _se(Loc::getMessage("WRONG_ACCEPT")) ?>";
                            }
                        }

                        console.log(error);

                        if (error) {
                            $this.before(error);
                        }

                    });

                    errorsContainer = $(".error-container");

                    if (errorsContainer.length) {
                        scrollTo(errorsContainer.get(0));
                        return false;
                    }


                    $('#form-shading').show();

                    return true;
                }
            });

            <? if ($arParams["INC_JQUERY_MASKEDINPUT"] == "Y"): ?>
            $(".birthdate-field").mask("99.99.9999");
            <? endif ?>

            $("#show-popup").magnificPopup({
                type: "ajax",
                midClick: true
            });

            $('.hint').each(function () {
                $(this).webuiPopover({
                    content: $(this).data('hint'),
                    width: $(window).outerWidth() <= 390 ? "200px" : "300px",
                    trigger: $(window).outerWidth() <= 990 ? 'click' : 'hover',
                    placement: "auto"
                })
            });

            /* $('.have-promo').on('click', function () {
                 var $this = $(this);
                 if ($this.is(":checked")) {
                     $('.promo-input-area').removeClass("hidden");
                 } else {
                     $('.promo-input-area').addClass("hidden");
                 }
             })*/

        });

                })(jQuery);
</script>