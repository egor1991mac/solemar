<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => GetMessage("HBLOCK_DESC_LIST"),
    "DESCRIPTION" => GetMessage("HBLOCK_DESC_LIST_DESC"),
    "ICON" => "/images/form.gif",
    "SORT" => 10,
    "COMPLEX" => "N",
    "CACHE_PATH" => "Y",
    "PATH" => array(
        "ID" => "travelsoft",
        "CHILD" => array(
            "ID" => "mastertour",
            "NAME" => "Мастер-Тур",
        ),
    ),
);
?>