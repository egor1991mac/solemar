<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
$country_db = \CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => COUNTRIES_IBLOCK_ID, "ACTIVE" => "Y", "ID" => $arParams["COUNTRY_ID"]), false, false, Array("ID", "NAME", "CODE", "IBLOCK_ID", "PROPERTY_FLAG", "PROPERTY_TOURIST_GUIDE"));
while($ar_country = $country_db->GetNext())
{
    $arResult["COUNTRY"] = $ar_country;
}
$cities_db = \CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => CITIES_IBLOCK_ID, "ACTIVE" => "Y", "PROPERTY_COUNTRY" => $arParams["COUNTRY_ID"]), false, false, Array("ID", "NAME", "IBLOCK_ID"));
if ($cities_db->SelectedRowsCount() >= 1) {
    $arResult["CITIES"] = true;
}
$hotels_db = \CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => HOTELS_IBLOCK_ID, "ACTIVE" => "Y", "PROPERTY_COUNTRY" => $arParams["COUNTRY_ID"]), false, false, Array("ID", "NAME", "IBLOCK_ID"));
if ($hotels_db->SelectedRowsCount() >= 1) {
    $arResult["HOTELS"] = true;
}

$visas_db = \CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => VISAS_IBLOCK_ID, "ACTIVE" => "Y", "PROPERTY_COUNTRY" => $arParams["COUNTRY_ID"]), false, false, Array("ID", "NAME", "IBLOCK_ID"));
if ($visas_db->SelectedRowsCount() >= 1) {
    $arResult["VISAS"] = true;
}


if(!empty($arResult["ITEMS"])) {

    $arResult["REGIONS"] = array();
    $arResult["RESORTS_TYPE"] = array();
    $arResult["NEW_ITEMS"] = array();
    $regions = array();
    $resorts_type = array();

    foreach ($arResult["ITEMS"] as $k => $arItem) {

        if (!empty($arItem["DISPLAY_PROPERTIES"]["RESORT_TYPE"]["VALUE"])) {

            if(!isset($arResult["NEW_ITEMS"][$arItem["DISPLAY_PROPERTIES"]["RESORT_TYPE"]["VALUE"]])){
                $arResult["NEW_ITEMS"][$arItem["DISPLAY_PROPERTIES"]["RESORT_TYPE"]["VALUE"]] = array();
                $resorts_type[] = $arItem["DISPLAY_PROPERTIES"]["RESORT_TYPE"]["VALUE"];
            }
            if (!empty($arItem["DISPLAY_PROPERTIES"]["REGION"]["VALUE"])) {

                if(!isset($arResult["NEW_ITEMS"][$arItem["DISPLAY_PROPERTIES"]["RESORT_TYPE"]["VALUE"]][$arItem["DISPLAY_PROPERTIES"]["REGION"]["VALUE"]])){
                    $arResult["NEW_ITEMS"][$arItem["DISPLAY_PROPERTIES"]["RESORT_TYPE"]["VALUE"]][$arItem["DISPLAY_PROPERTIES"]["REGION"]["VALUE"]]["ITEMS"] = array();
                    $regions[] = $arItem["DISPLAY_PROPERTIES"]["REGION"]["VALUE"];
                }


            }

            $arResult["NEW_ITEMS"][$arItem["DISPLAY_PROPERTIES"]["RESORT_TYPE"]["VALUE"]][$arItem["DISPLAY_PROPERTIES"]["REGION"]["VALUE"]]["ITEMS"][] = $arItem;

        }


    }

    if(!empty($regions)){
        $res = CIBlockElement::GetList(Array("SORT"=>"ASC"), Array("IBLOCK_ID"=>REGIONS_ID_IBLOCK, "ACTIVE"=>"Y", "ID"=>$regions), false, false, Array("IBLOCK_ID","ID","NAME"));
        while($ar_fields = $res->GetNext())
        {
            $arResult["REGIONS"][$ar_fields["ID"]] = $ar_fields;
        }
    }
    if(!empty($resorts_type)){
        $res = CIBlockElement::GetList(Array("SORT"=>"ASC"), Array("IBLOCK_ID"=>RESORTS_TYPE_ID_IBLOCK, "ACTIVE"=>"Y", "ID"=>$resorts_type), false, false, Array("IBLOCK_ID","ID","NAME"));
        while($ar_fields = $res->GetNext())
        {
            $arResult["RESORTS_TYPE"][$ar_fields["ID"]] = $ar_fields;
        }
    }

    if(!empty($arResult["NEW_ITEMS"])){

        $arResult["ITEMS"] = array();

        foreach ($arResult["RESORTS_TYPE"] as $type){

            $arResult["ITEMS"][$type["ID"]]["NAME"] = $type["NAME"];
            $arResult["ITEMS"][$type["ID"]]["ID"] = $type["ID"];

            foreach ($arResult["REGIONS"] as $region){

                if(isset($arResult["NEW_ITEMS"][$type["ID"]][$region["ID"]])) {
                    $arResult["ITEMS"][$type["ID"]]["ITEMS"][$region["ID"]]["NAME"] = $region["NAME"];
                    $arResult["ITEMS"][$type["ID"]]["ITEMS"][$region["ID"]]["ID"] = $region["ID"];

                    foreach ($arResult["NEW_ITEMS"][$type["ID"]][$region["ID"]] as $item) {

                        $arResult["ITEMS"][$type["ID"]]["ITEMS"][$region["ID"]]["ITEMS"] = $item;

                    }
                }

            }


        }

    }

}