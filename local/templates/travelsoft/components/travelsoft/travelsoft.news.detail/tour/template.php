<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$this->addExternalJS(SITE_TEMPLATE_PATH . "/js/MapAdapter/MapAdapter.js");

/*\Bitrix\Main\Loader::includeModule('travelsoft.currency');
$converter = \travelsoft\currency\factory\Converter::getInstance();*/
$htmlMapID = 'map';
?>
<?$properties = $arResult["DISPLAY_PROPERTIES"];?>
<!--<div class="fast-bar">
    <div class="container">
        <div id="curr">
            <ul>
                <li class="text">Цены в:</li>
                <li class="byn"><a href="https://solemare.by/tours/italy/about#byn" class="flashlink"><span>BYN</span></a></li>
                <li class="usd"><a href="https://solemare.by/tours/italy/about#usd" class="flashlink"><span>USD</span></a></li>
                <li class="eur"><a href="https://solemare.by/tours/italy/about#eur" class="flashlink"><span>EUR</span></a></li>
                <li class="gbp"><a href="https://solemare.by/tours/italy/about#gbp" class="flashlink"><span>GBP</span></a></li>
            </ul>
        </div>
        <ul>
        </ul>
    </div>
</div>-->
<div class="container tour col2">
    <h1 class=""><?=$arResult["NAME"]?></h1>
    <?if(!empty($properties["ROUTE"]["VALUE"])):?>
       <span><?=$properties["ROUTE"]["DISPLAY_VALUE"]?></span>
    <?endif?>
    <div class="sidebar">
        <div class="tour-card">
            <h2><?=GetMessage('BLOCK_PRICE_FROM')?></h2>
            <?if(!empty($properties["PRICE"]["VALUE"]) && !empty($properties["CURRENCY"]["VALUE"])):?>
                <?=\travelsoft\currency\factory\Converter::getInstance()->convert($properties["PRICE"]["VALUE"], $properties["CURRENCY"]["VALUE"])->getResult()?><?=GetMessage('BLOCK_PRICE_NIGHT')?>
                <?/*=$properties["PRICE"]["VALUE"]*/?><!-- <?/*=$properties["CURRENCY"]["VALUE"]*/?>--><?/*=GetMessage('BLOCK_PRICE_NIGHT')*/?>
                <p class="price-note">
                    <?if (!empty($properties["DAYS"]["VALUE"])): ?>
                        <?=GetMessage('NIGHT_FROM')?> <?= num2word($properties["DAYS"]["VALUE"],array(GetMessage('NIGHT_1'),GetMessage('NIGHT_2'),GetMessage('NIGHT_ALL')))?><br />
                    <?php endif ?>
                </p>
            <?else:?>
                <div class="no-price"><?=GetMessage('ON_REQUEST')?></div>
            <?endif ?>
            <a href="#open" class="orange-button pp-open tour"><?=GetMessage('SEND_REQUEST')?></a>
            <br />
            <?if(!empty($properties["FILE_VISA"]["VALUE"])):?>
                <a href="<?=$properties["FILE_VISA"]["FILE_VALUE"]["SRC"]?>" class="doc-link"><?if(!empty($properties["FILE_VISA"]["DESCRIPTION"])):?><?=$properties["FILE_VISA"]["DESCRIPTION"]?><?else:?><?=$properties["FILE_VISA"]["FILE_VALUE"]["ORIGINAL_NAME"]?><?endif?></a>
            <?endif?>
            <?if(!empty($properties["FILE_TOUR"]["VALUE"])):?>
                <a href="<?=$properties["FILE_TOUR"]["FILE_VALUE"]["SRC"]?>" class="doc-link"><?if(!empty($properties["FILE_TOUR"]["DESCRIPTION"])):?><?=$properties["FILE_TOUR"]["DESCRIPTION"]?><?else:?><?=$properties["FILE_TOUR"]["FILE_VALUE"]["ORIGINAL_NAME"]?><?endif?></a>
            <?endif?>
        </div>
        <?=$arParams["MANAGERS_FOR_TOUR"]?>
        <div class="widgets">
            <?=$arParams["SIDEBAR_SUBSCRIPTION"]?>
            <?=$arParams["SIDEBAR_WIDGETS"]?>
        </div>
    </div>
    <?if($properties["PICTURES"]["VALUE"]):?>
        <?=$arParams["SLIDER"]?>
    <?endif?>
    <div class="content tour-info">
        <?/*dm($arResult);*/?>
        <?if($arResult["DIRECTIONS"]):?>
           <!-- <h4><?/*=GetMessage('SEARCH_FORM_TITLE')*/?></h4>-->
            <?=$arParams["SEARCH_FORM_STS"]?>
            <?=$arParams["PAGE_SEARCH_RESULT_STS"]?>
        <?endif?>
        <?if(!empty($properties["DESC"]["VALUE"]["TEXT"])):?>
            <h2 class="title"><?=$properties["DESC"]["NAME"]?></h2>
            <?=$properties["DESC"]["DISPLAY_VALUE"]?>
        <?endif?>
        <?if(!empty($properties["NDAYS"]["VALUE"]) || !empty($arResult['ROUTE_INFO'])):?>
            <div class="tabs">
                <?if(!empty($properties["NDAYS"]["VALUE"])):?>
                    <a href="" class="days-link flashlink active"><span><?=GetMessage('PROGRAM_TOUR')?></span></a>
                <?endif?>
                <?if(!empty($arResult['ROUTE_INFO'])):?>
                    <a href="" class="route-map-link flashlink<?if(empty($properties["NDAYS"]["VALUE"])):?> active<?endif?>"><span><?=GetMessage('ROUTE_MAP')?></span></a>
                <?endif?>
            </div>
            <script>
                $(function(){
                    $('div.tabs a').click(function(e){
                        $('div.tabs a').toggleClass('active');
                        $('div.days, div.route-map').toggleClass('none');
                        e.preventDefault();
                    });
                });
            </script>
            <?if(!empty($properties["NDAYS"]["VALUE"])):?>
                <div class="days">
                    <h2><?=GetMessage('PROGRAM_TOUR')?></h2>
                    <ul>
                        <li>
                            <div class="num title"><?=GetMessage('DAYS')?></div>
                            <div class="info title"><?=GetMessage('DESCRIPTION_DAY')?></div>
                        </li>
                        <?foreach ($properties["NDAYS"]["~VALUE"] as $day => $line): ?>
                            <li>
                                <div class="num">
                                    <?if(!empty($properties["NDAYS"]['DESCRIPTION'][$day])):?>
                                        <?=$properties["NDAYS"]['DESCRIPTION'][$day];?>
                                    <?else:?>
                                        <?= GetMessage('DAY') . ($day + 1);?>
                                    <?endif;?>
                                </div>
                                <div class="info"><p><?= $line['TEXT'] ?></p></div>
                            </li>
                        <?endforeach ?>
                    </ul>
                </div>
            <?endif?>
            <?if(!empty($arResult['ROUTE_INFO'])):?>
                <div class="route-map<?if(!empty($properties["NDAYS"]["VALUE"])):?> none<?endif?>">
                    <h2><?=GetMessage('ROUTE_MAP')?></h2>
                    <div id="<?= $htmlMapID ?>" style="width: 100%;height: 415px;"></div>
                    <script>
                        var loc = JSON.parse('<?=\Bitrix\Main\Web\Json::encode($arResult["ROUTE_INFO"])?>');

                        $(document).ready(function () {
                            var mapAdapter = new MapAdapter({
                                map_id: "map",
                                center: {
                                    lat: 53.53,
                                    lng: 27.34
                                },
                                object: "ymaps",
                                zoom: 15
                            });
                            if(typeof loc === "object" && loc.length > 1) {
                                mapAdapter.drawRoute(loc);
                            } else {
                                mapAdapter.addMarker(loc[0]);
                            }
                        });
                    </script>
                </div>
            <?endif?>
        <?endif?>
        <div class="tour-note">
            <?if(!empty($properties["PRICE_INCLUDE"]["VALUE"]["TEXT"])):?>
                <h2 class="title"><?=$properties["PRICE_INCLUDE"]["NAME"]?></h2>
                <?=$properties["PRICE_INCLUDE"]["DISPLAY_VALUE"]?>
            <?endif?>
            <?if(!empty($properties["ADDITIONAL_PAYMENTS"]["VALUE"]["TEXT"])):?>
                <h2 class="title"><?=$properties["ADDITIONAL_PAYMENTS"]["NAME"]?></h2>
                <?=$properties["ADDITIONAL_PAYMENTS"]["DISPLAY_VALUE"]?>
            <?endif?>
        </div>
        <div style="clear: both"></div>
    </div>

    <?=$arParams["TOURS_SLIDER_FOR_COUNTRY"]?>
</div>

<?=$arParams["COUNTRIES_LIST"]?>


