<?php

namespace travelsoft\booking\sts\operators;

use \travelsoft\booking\sts\_interfaces\Operator;
use \travelsoft\booking\sts\Utils;

\Bitrix\Main\Loader::includeModule('travelsoft.currency');

/**
 * Класс для работы с оператором Мастер-Тур
 *
 * @author juliya.sharlova
 * @copyright (c) 2017, travelsoft
 */
class MasterTour extends Operator
{
    protected $_api = null;
    protected $_request = null;
    protected $_result = null;
    protected $_method = null;
    protected $_operator_id = null;

    public function __construct()
    {
        //определить параметры. привести к нужному единому виду. записать в свойство $parameters
        $this->_api = new \travelsoft\booking\sts\api\MasterTour();

    }

    public function createUser (array $parameters) {

        //переработка $parameters
        $parameters["update"] = 0;
        if(!$parameters["is_agent"]) {
            $parameters["user_info"]["activated"] = 1;
        }
        $response = $this->_api->createUpdateMTUser($parameters);
        $response = json_decode($response,true);
        return $response;

    }

    public function updateUser (array $parameters) {

        //переработка $parameters
        $parameters["update"] = 1;
        $response = $this->_api->createUpdateMTUser($parameters);
        $response = json_decode($response,true);
        return $response;

    }

    public function authorizeUser (array $parameters) {

        //переработка $parameters
        $response = $this->_api->authorizeMtUser($parameters);
        $response = json_decode($response,true);

        unset($_SESSION['__TRAVELSOFT']["TOKEN"]["OP_".$parameters["operator_id"]]);
        if ($response["result"]["token"]) {
            $_SESSION['__TRAVELSOFT']["TOKEN"]["OP_".$parameters["operator_id"]] = $response["result"]["token"];
        }

        return $response;

    }

    public function logoutUser (array $parameters) {

        if($parameters["operator_id"]) {
            $parameters["token"] = $_SESSION['__TRAVELSOFT']["TOKEN"]["OP_".$parameters["operator_id"]];
        }
        $this->_api->logoutUser($parameters);
        unset($_SESSION['__TRAVELSOFT']["TOKEN"]["OP_".$parameters["operator_id"]]);
        unset($_SESSION['__TRAVELSOFT']["TOKEN"]);

    }

    public function makeBooking (array $parameters) {

        $request =  array();
        //переработка $parameters
        if($parameters["operator_id"]) {
            $parameters["booking_data"]["token"] = $_SESSION['__TRAVELSOFT']["TOKEN"]["OP_".$parameters["operator_id"]];
        }
        if(!empty($parameters["booking_data"]))
        {
            $request["booking_data"] = array(
                    "token" => $parameters["booking_data"]["token"],
                    "priceKey" => $parameters["booking_data"]["priceKey"],
                    "turists" => $parameters["booking_data"]["turists"],
                    "comment" => $parameters["booking_data"]["comment"],
                    "addParameter" => $parameters["booking_data"]["addParameter"]
            );
            $request["address"] = $parameters["address"];

        } else {
            throw new \Exception("Недостаточно данных для бронирования.");
        }

        $response = $this->_api->makeBooking($request);

        return $response;

    }

    public function makeArbitraryBooking (array $parameters) {

        $request =  array();
        //переработка $parameters
        if($parameters["operator_id"]) {
            $parameters["booking_data"]["token"] = $_SESSION['__TRAVELSOFT']["TOKEN"]["OP_".$parameters["operator_id"]];
        }
        if(!empty($parameters["booking_data"]))
        {
            $request["booking_data"] = array(
                "token" => $parameters["booking_data"]["token"],
                "services" => $parameters["booking_data"]["services"],
                "turists" => $parameters["booking_data"]["turists"],
                "comment" => $parameters["booking_data"]["comment"],
                "addParameter" => $parameters["booking_data"]["addParameter"]
            );
            $request["address"] = $parameters["address"];

        } else {
            throw new \Exception("Недостаточно данных для бронирования.");
        }

        $response = $this->_api->makeArbitraryBooking($request);

        return $response;

    }

    public function getOrderList (array $parameters) {

        //переработка $parameters
        $response = $this->_api->getOrderList($parameters);
        return $response;

    }

    public function getOrderDetail (array $parameters) {

        //переработка $parameters
        $response = $this->_api->getOrderDetail($parameters);
        return $response;

    }

    public function getServiceDetail (array $parameters) {

        $request = array();
        $result = array();
        //переработка $parameters
        if(!empty($parameters))
        {
            $request = array(
                "parameters" => array(
                    "priceKey" => $parameters["priceKey"]
                ),
                "address" => $parameters["address"],
            );
        } else {
            throw new \Exception("Недостаточно данных для бронирования.");
        }

        $response = $this->_api->getServiceDetail($request);

        $response = json_decode($response,true);

        if(isset($response["result"]) && !empty($response["result"])){

            $result["date"] = array(
                "dateFrom" => $response["result"]["dateFrom"],
                "dateTo" => $response["result"]["dateTo"]
            );
            $result["countTurist"] = $response["result"]["countTurist"];
            $result["prices"] = $response["result"]["prices"];
            $result["services"] = $response["result"]["services"];
            /*foreach ($response["result"]["hotels"] as $hotel){
                $result["hotels"][] = array(
                    "dateFrom" => $result["date"]["dateFrom"],
                    "dateTo" => $result["date"]["dateTo"],
                    "night" => $hotel["night"],
                    "night" => $hotel["night"],
                );
            }*/
            if(!empty($response["result"]["avia"])) {
                foreach ($response["result"]["avia"] as $avia) {
                    $result["avia"][] = array(
                        "flight" => $avia["flight"],
                        "airline" => $avia["airline"]["name"],
                        "places" => $avia["airService"]["name"],
                        "cityDep" => $avia["cityDep"]["name"],
                        "airportDep" => $avia["airportDep"]["name"] . " (" . $avia["airportDep"]["code"] . ")",
                        "dateFrom" => $avia["dateBegin"] . " " . $avia["timeFrom"],
                        "cityArr" => $avia["cityArr"]["name"],
                        "airportArr" => $avia["airportArr"]["name"] . " (" . $avia["airportArr"]["code"] . ")",
                        "dateTo" => $avia["dateEnd"] . " " . $avia["timeTo"],
                    );
                }
            }
            if(!empty($response["result"]["transfers"])) {
                foreach ($response["result"]["transfers"] as $transfer) {
                    $result["transfers"][] = array(
                        "dateFrom" => $transfer["dateBegin"],
                        "city" => $transfer["city"]["name"],
                        "route" => $transfer["route"],
                        "transport" => $transfer["transport"]["name"]
                    );
                }
            }
            if(!empty($response["result"]["excursions"])) {
                foreach ($response["result"]["excursions"] as $excursion) {
                    $result["excursions"][] = array(
                        "dateFrom" => $excursion["dateBegin"],
                        "city" => $excursion["city"]["name"],
                        "route" => $excursion["route"],
                        "transport" => $excursion["transport"]["name"]
                    );
                }
            }
            if(!empty($response["result"]["visas"])) {
                foreach ($response["result"]["visas"] as $visa) {
                    $result["visas"][] = array(
                        "dateFrom" => $visa["dateBegin"],
                        "country" => $visa["country"]["name"],
                        "name" => $visa["name"],
                        "days" => $visa["days"]
                    );
                }
            }
            if(!empty($response["result"]["insurances"])) {
                foreach ($response["result"]["insurances"] as $insurance) {
                    $result["insurances"][] = array(
                        "dateFrom" => $insurance["dateBegin"],
                        "name" => $insurance["name"],
                        "days" => $insurance["days"]
                    );
                }
            }

        } else {
            //ошибка
        }

        return $result;

    }

    public function orderToCancel (array $parameters) {

        //переработка $parameters
        $response = $this->_api->orderToCancel($parameters);
        return $response;

    }


}