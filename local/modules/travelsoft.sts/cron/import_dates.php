<?
$_SERVER["DOCUMENT_ROOT"] = realpath(dirname(__FILE__)."/../../../..");
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define("NO_AGENT_STATISTIC",true);
define('NO_AGENT_CHECK', true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
@set_time_limit(0);
@ignore_user_abort(true);

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

Bitrix\Main\Loader::includeModule('travelsoft.sts');

$operators = \travelsoft\sts\Utils::getOperators_(array("UF_MAIN_OPERATOR"=>1));

if(!empty($operators)){

    $arResponse["result"] = array();
    foreach($operators as $operator){

        $result = '';
        $result = \travelsoft\sts\Utils::jsonDecode((new \travelsoft\sts\ClientServerProxy(array("operator_id"=>$operator["ID"], "getDates"=>true)))->getDatesResult(),true);

        if(isset($result["result"]) && !empty($result["result"])){
            $arResponse["result"][$operator["ID"]] = $result["result"];
        }

    }

    if(!empty($arResponse["result"])){

        $countries = array();
        $cities = array();
        $totalResult = array();
        $tourTypes = array();
        $i = 0;

        foreach($arResponse["result"] as $key=>$items){

            foreach($items as $k=>$item){

                if($item["tourtype_id"] != 0) {

                    $flag = false;

                    if (!isset($cities[$key][$item["cityfrom_id"]])) {
                        $cities[$key][$item["cityfrom_id"]] = \travelsoft\sts\stores\RelationCities::getBxId(array("UF_OP_" . $key => $item["cityfrom_id"]));
                    }

                    if (!isset($countries[$key][$item["country_id"]])) {
                        $countries[$key][$item["country_id"]] = \travelsoft\sts\stores\RelationCountries::getBxId(array("UF_OP_" . $key => $item["country_id"]));
                    }

                    if($item["tourtype_id"] == -1){
                        $flag = true;
                    } elseif (!isset($tourTypes[$key][$item["tourtype_id"]])) {
                        $tourTypes[$key][$item["tourtype_id"]] = \travelsoft\sts\stores\RelationTourTypes::getBxId(array("UF_OP_" . $key => $item["tourtype_id"]));
                    }


                    if(!empty($cities[$key][$item["cityfrom_id"]]) && !empty($countries[$key][$item["country_id"]])) {

                        if($flag) {
                            $totalResult[$i] = $item;
                            $totalResult[$i]["cityfrom_id"] = $cities[$key][$item["cityfrom_id"]];
                            $totalResult[$i]["country_id"] = $countries[$key][$item["country_id"]];
                            $totalResult[$i]["bx_tourtype_id"] = 0;
                            $totalResult[$i]["operator_id"] = $key;
                        } elseif($tourTypes[$key][$item["tourtype_id"]] != 0) {
                            $totalResult[$i] = $item;
                            $totalResult[$i]["cityfrom_id"] = $cities[$key][$item["cityfrom_id"]];
                            $totalResult[$i]["country_id"] = $countries[$key][$item["country_id"]];
                            $totalResult[$i]["bx_tourtype_id"] = $tourTypes[$key][$item["tourtype_id"]];
                            $totalResult[$i]["operator_id"] = $key;
                        }


                        $i++;
                    }

                }

            }

        }

    }


    if(!empty($totalResult)){
        $rsData = \travelsoft\sts\stores\Dates::get();
        if(count($rsData) > 0){
            //update
            $arResultBlock = array();
            foreach($rsData as $item){
                $arResultBlock[$item["UF_OPERATOR_ID"]][$item["UF_CITYFROM_ID"]][$item["UF_COUNTRY_ID"]][$item["UF_BX_TOURTYPE_ID"]] = $item;
            }

            foreach ($totalResult as $item){

                if(isset($arResultBlock[$item["operator_id"]])
                    && isset($arResultBlock[$item["operator_id"]][$item["cityfrom_id"]])
                    && isset($arResultBlock[$item["operator_id"]][$item["cityfrom_id"]])
                    && isset($arResultBlock[$item["operator_id"]][$item["cityfrom_id"]][$item["country_id"]])
                    && isset($arResultBlock[$item["operator_id"]][$item["cityfrom_id"]][$item["country_id"]][$item["bx_tourtype_id"]])){
                    //update
                    $data = array(
                        "UF_DATES"=>serialize($item["dates"]),
                        "UF_NIGHTS"=>serialize($item["nights"])
                    );
                    $ID = $arResultBlock[$item["operator_id"]][$item["cityfrom_id"]][$item["country_id"]][$item["bx_tourtype_id"]]["ID"];
                    if(\travelsoft\sts\stores\Dates::update($ID, $data))
                    {
                        echo 'В справочнике изменена запись '.$ID.'<br />';
                    }
                    else {
                        echo 'Ошибка изменения записи';
                    }
                    unset($arResultBlock[$item["operator_id"]][$item["cityfrom_id"]][$item["country_id"]][$item["bx_tourtype_id"]]);
                }
                else {
                    //add
                    $data = array(
                        "UF_COUNTRY_ID"=>$item["country_id"],
                        "UF_CITYFROM_ID"=>$item["cityfrom_id"],
                        "UF_TOURTYPE_ID"=>$item["tourtype_id"],
                        "UF_BX_TOURTYPE_ID"=>$item["bx_tourtype_id"],
                        "UF_DATES"=>serialize($item["dates"]),
                        "UF_NIGHTS"=>serialize($item["nights"]),
                        "UF_OPERATOR_ID"=>$item["operator_id"],
                    );
                    $ID = \travelsoft\sts\stores\Dates::add($data);
                    if($ID)
                    {
                        echo 'В справочник добавлена запись '.$ID.'<br />';
                    }
                    else {
                        echo 'Ошибка добавления записи';
                    }
                }
            }
            if(!empty($arResultBlock)){

                foreach ($arResultBlock as $values){

                    foreach ($values as $value) {
                        foreach ($value as $val) {
                            foreach ($val as $k => $v) {
                                if(!empty($val[$k])) {
                                    //delete
                                    \travelsoft\sts\stores\Dates::delete($v["ID"]);
                                }
                            }
                        }
                    }

                }
            }

        } else {
            //add
            $data = array();
            foreach ($totalResult as $item){
                $data = array(
                    "UF_COUNTRY_ID"=>$item["country_id"],
                    "UF_CITYFROM_ID"=>$item["cityfrom_id"],
                    "UF_TOURTYPE_ID"=>$item["tourtype_id"],
                    "UF_BX_TOURTYPE_ID"=>$item["bx_tourtype_id"],
                    "UF_DATES"=>serialize($item["dates"]),
                    "UF_NIGHTS"=>serialize($item["nights"]),
                    "UF_OPERATOR_ID"=>$item["operator_id"],
                );
                $ID = \travelsoft\sts\stores\Dates::add($data);
                if($ID)
                {
                    echo 'В справочник добавлена запись '.$ID.'<br />';
                }
                else {
                    echo 'Ошибка добавления записи';
                }
            }
        }
    } else {
        echo "Возникла ошибка выгрузки дат";
    }

} else {
    echo "Нет активных операторов";
}