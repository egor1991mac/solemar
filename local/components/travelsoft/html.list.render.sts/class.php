<?php

class  TravelsoftHtmlListRenderComponent extends CBitrixComponent {
    
    public $dr;
    
    // include component_prolog.php
    public function includeComponentProlog()
    {
        $file = "component_prolog.php";

        $template_name = $this->GetTemplateName();

        if ($template_name == "")
            $template_name = ".default";

        $relative_path = $this->GetRelativePath();

        $this->dr = Bitrix\Main\Application::getDocumentRoot();

        $file_path = $this->dr . SITE_TEMPLATE_PATH . "/components" . $relative_path . "/" . $template_name . "/" . $file;

        $arParams = &$this->arParams;

        if(file_exists($file_path))
            require $file_path;
        else {

            $file_path = $this->dr . "/bitrix/templates/.default/components" . $relative_path . "/" . $template_name . "/" . $file;

            if(file_exists($file_path))
                require $file_path;
            else {
                $file_path = $this->dr . $this->__path . "/templates/" . $template_name . "/" . $file;
                if(file_exists($file_path))
                    require $file_path;
                else {

                    $file_path = $this->dr . "/local/components" . $relative_path . "/templates/" . $template_name . "/" . $file;

                    if(file_exists($file_path))
                        require $file_path;
                }

            }
        }
    }

    /**
     * Проверка параметров компонента
     * @throws Exception
     */
    protected function checkInputParameters () {

        $this->arParams['RESULT_LIST'] = (array)$this->arParams['RESULT_LIST'];

    }


    public function executeComponent () {
        
        $this->includeComponentProlog();

        Bitrix\Main\Loader::includeModule("travelsoft.sts");
        Bitrix\Main\Loader::includeModule("travelsoft.currency");
        \travelsoft\currency\Converter::getInstance()->initDefault();

        Bitrix\Main\Loader::includeModule("iblock");

        $arResult["SERVICES"] = services();

        $this->checkInputParameters();

        $this->arResult["ITEMS"] = $this->arParams['RESULT_LIST'];

        $this->IncludeComponentTemplate();
        
    }
    
}