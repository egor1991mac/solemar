<?php
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define("NO_AGENT_STATISTIC",true);
define('NO_AGENT_CHECK', true);
define("PUBLIC_AJAX_MODE", true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

Bitrix\Main\Loader::includeModule("travelsoft.sts");

$arResponse = array(
    "result" => null,
    "error" => false
);

try {

    $request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();

    if(!$request->isPost())
        throw new Exception();

    $cityFrom = \travelsoft\sts\Utils::jsonDecode($request->getPost('cityFrom'), true);
    $country = \travelsoft\sts\Utils::jsonDecode($request->getPost('country'), true);
    $tourTypes = \travelsoft\sts\Utils::jsonDecode($request->getPost('tourTypes'), true);

    $filter = [
        "filter" => [
            "UF_CITYFROM_ID" => $cityFrom,
            "UF_COUNTRY_ID" => $country,
            "UF_BX_TOURTYPE_ID" => $tourTypes
        ]
    ];

    $result = \travelsoft\sts\stores\Dates::get($filter);

    if (count($result) > 0) {
        $arResultBlock = array();
        $arResultNights = array();
        foreach ($result as $item) {

            //даты
            $date = unserialize($item["UF_DATES"]);

            $arResultBlock = array_merge($arResultBlock, $date);
            $gg = array_unique($arResultBlock);
            array_walk($gg, function (&$item_) {
                $item_ = MakeTimeStamp((string)$item_, "DD.MM.YYYY");
            });
            sort($gg);
            array_walk($gg, function (&$item_) {
                $item_ = date("d.m.Y", $item_);
            });
            $arResultBlock = $gg;

            //ночи
            $nights = unserialize($item["UF_NIGHTS"]);
            $arResultNights = array_merge($arResultNights, $nights);
            $ggn = array_unique($arResultNights);
            sort($ggn);
            $arResultNights = $ggn;

        }
        if (!empty($arResultBlock)) {
            $arResponse["result"]["dates"] = $arResultBlock;
        }
        if (!empty($arResultNights)) {
            $arResponse["result"]["nights"] = $arResultNights;
        }
    }

    echo \travelsoft\sts\Utils::jsonEncode($arResponse);

} catch(\Exception $e) {

    $arResponse["error"] = true;
    $arResponse["error_message"] = $e->getMessage();
    echo \travelsoft\sts\Utils::jsonEncode($arResponse);

}
