<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();


if(!CModule::IncludeModule("iblock")){
    ShowError(GetMessage("Модуль iblock не установлен."));
    return;
}

$arTypesEx = CIBlockParameters::GetIBlockTypes(array("-"=>" "));

$arIBlocks=array();
$db_iblock = CIBlock::GetList(array("SORT"=>"ASC"), array("SITE_ID"=>$_REQUEST["site"], "TYPE" => ($arCurrentValues["IBLOCK_TYPE"]!="-"?$arCurrentValues["IBLOCK_TYPE"]:"")));
while($arRes = $db_iblock->Fetch())
    $arIBlocks[$arRes["ID"]] = $arRes["NAME"];


$arComponentParameters['PARAMETERS'] = array(

    "GET_PARAMS_STS" => array(
        "PARENT" => "BASE",
        "NAME" => GetMessage("T_DESC_SEARCH"),
        "TYPE" => "STRING",
        "VALUES" => '={$_REQUEST["stsSearch"]}',
    ),
    "PARAMS_STS_FILTER" => array(
        "PARENT" => "BASE",
        "NAME" => GetMessage("T_PARAMS_FILTER"),
        "TYPE" => "LIST",
        "MULTIPLE" => "Y",
        "VALUES" => $arIBlocks
    ),
    "ACTION_URL" => array(
        "PARENT" => "BASE",
        "NAME" => GetMessage("T_DESC_ACTION_URL"),
        "TYPE" => "STRING",
    ),
    "SHOW_PRICE_FILTER" => array(
        "PARENT" => "BASE",
        "NAME" => GetMessage("T_SHOW_PRICE_FILTER"),
        "TYPE" => "CHECKBOX",
    )

);