<?php

namespace travelsoft\booking\sts;

use travelsoft\booking\sts\Utils as STSUtils;

/**
 * Класс методов обработки событий
 *
 * @author juliya.sharlova
 * @copyright (c) 2017, travelsoft
 */
class EventsHandlers {

    /**
     * @param array $arFields
     */
    static public function bxOnAfterUserSimpleRegister(&$arFields) {

        if ($arFields['USER_ID'] > 0) {

            $operator = STSUtils::getMainOperator();

            //авторизация пользователя
            (new $operator["UF_CLASS"])->authorizeUser(array(
                "address" => $operator["UF_GATEWAY"],
                "login" => $arFields["LOGIN"],
                "password" => $arFields["PASSWORD"],
                "operator_id" => $operator["ID"]
            ));

            // ОТПРАВКА СТАНДАРТНОГО ПИСЬМО О РЕГИСТРАЦИИ ПОЛЬЗОВАТЕЛЯ
            /*\Bitrix\Main\Mail\Event::send(array(
                "EVENT_NAME" => "TRAVELSOFT_BOOKING",
                "LID" => SITE_ID,
                "C_FIELDS" => array(
                    "EMAIL" => $arFields["EMAIL"],
                    "LOGIN" => $arFields["LOGIN"],
                    "PASSWORD" => $arFields["PASSWORD"],
                    "USER_ID" => $arFields['USER_ID']
                ),
                "DUPLICATE" => 'N',
                "MESSAGE_ID" => 2
            ));*/

        }

    }

    /**
     * @param array $arFields
     */
    static public function bxOnBeforeUserSimpleRegister(&$arFields) {

        $response = STSUtils::createUser(array("email" => $arFields["EMAIL"], "password" => $arFields["PASSWORD"]));

        if(!isset($response["result"]["result"]) || empty($response["result"]["user_id"])){
            return false;
        }

        $arFields["UF_USER_ID"] = $response["result"]["user_id"];

        STSUtils::setLoginEqlEmail($arFields);
        STSUtils::passWorker("save", $arFields["PASSWORD"]);
    }

    /**
     * @param array $arFields
     */
    static public function bxOnBeforeUserUpdate(&$arFields) {

        if ($arFields['ID']) {

            $user_group = \CUser::GetUserGroup($arFields["ID"]); // Группы ДО запроса на обновления
            $new_user_group = array();
            if(isset($arFields["GROUP_ID"])){
                foreach ($arFields["GROUP_ID"] as $group){
                    $new_user_group[] = $group["GROUP_ID"]; // Группы При изменениях
                }
            }

            if(!in_array(\Bitrix\Main\Config\Option::get("travelsoft.booking.sts", "AGENT_GROUP_ID"), $user_group) && in_array(\Bitrix\Main\Config\Option::get("travelsoft.booking.sts", "AGENT_GROUP_ID"),$new_user_group)){
                $GLOBALS["USER_AGENT"] = true;
                STSUtils::$group_agent = true;
                $arFields["UF_AGENT_NOT_ACTIVE"] = 0;
                $arFields["ACTIVE"] = "Y";
            } elseif(in_array(\Bitrix\Main\Config\Option::get("travelsoft.booking.sts", "AGENT_GROUP_ID"), $user_group)) {
                STSUtils::$group_agent = true;
                $arFields["UF_AGENT_NOT_ACTIVE"] = 0;
                if(in_array(\Bitrix\Main\Config\Option::get("travelsoft.booking.sts", "AGENT_GROUP_ID"), $new_user_group)){
                    $arFields["ACTIVE"] =  "Y";
                }
            }

            if($arFields["ACTIVE"] == "Y") {
                STSUtils::$user_active = true;
            }

            if ($arFields['PASSWORD'] != ""){
                STSUtils::passWorker("save", $arFields["PASSWORD"]);
            }

            $ar_fields["user_info"] = array();

            if(isset($arFields["UF_LEGAL_ADDRESS"]) && !empty($arFields["UF_LEGAL_ADDRESS"]))
                $ar_fields["user_info"]["address"] = $arFields["UF_LEGAL_ADDRESS"];
            if(isset($arFields["UF_INN"]) && !empty($arFields["UF_INN"]))
                $ar_fields["user_info"]["inn"] = $arFields["UF_INN"];

            $rsUser = \CUser::GetList($by = "", $order = "", array("ID" => $arFields["ID"]), array("SELECT" => array("UF_USER_ID","UF_LEGAL_NAME")));
            if ($arUser = $rsUser->Fetch()) {

                $ar_fields["user_info"]["user_id"] = !empty($arFields["UF_USER_ID"]) ? $arFields["UF_USER_ID"] : $arUser["UF_USER_ID"];
                $ar_fields["user_info"]["email"] = isset($arFields['EMAIL']) && !empty($arFields['EMAIL']) ? $arFields['EMAIL'] : $arUser["EMAIL"];
                //$ar_fields["user_info"]["login"] = $ar_fields["user_info"]["email"];
                $ar_fields["user_info"]["login"] = $arFields['LOGIN'];
                $ar_fields["user_info"]["company"] = isset($arFields["UF_LEGAL_NAME"]) && !empty($arFields["UF_LEGAL_NAME"]) ? $arFields["UF_LEGAL_NAME"] : $arUser["UF_LEGAL_NAME"];

                $ar_fields["user_info"]["username"] = isset($arFields["LAST_NAME"]) && !empty($arFields["LAST_NAME"]) ? $arFields["LAST_NAME"] : $arUser["LAST_NAME"];
                $ar_fields["user_info"]["username"] .= isset($arFields["NAME"]) && !empty($arFields["NAME"]) ? " ".$arFields["NAME"] : " ".$arUser["NAME"];

            }

            $ar_fields["user_info"]["password"] = isset($GLOBALS['PASSWORD']) && !empty($GLOBALS['PASSWORD']) ? $GLOBALS['PASSWORD'] : '';
            unset($GLOBALS['PASSWORD']);

            if(STSUtils::$group_agent) {
                $ar_fields["is_agent"] = 1;
                if(empty($ar_fields["user_info"]["username"]) && isset($ar_fields["user_info"]["username"])){
                    $ar_fields["user_info"]["username"] = $ar_fields["user_info"]["email"];
                }
            }

            $ar_fields["user_info"]["activated"] = isset($arFields["ACTIVE"]) ? ( STSUtils::$user_active ? 1 : 0 ) : null;

            $response = STSUtils::updateUser($ar_fields);
            if(!$response){
                return false;
            }

        }

        if (!defined("ADMIN_SECTION")) {
            STSUtils::setLoginEqlEmail($arFields);
        }

    }

    /**
     * @param array $arFields
     */
    static public function bxOnAfterUserUpdate($arFields)
    {

        if ($arFields['RESULT']) {

            if ($GLOBALS["USER"]->IsAdmin() && (defined("ADMIN_SECTION") && ADMIN_SECTION === true)) {

                $user_group = \CUser::GetUserGroup($arFields["ID"]);

                if (in_array(\Bitrix\Main\Config\Option::get("travelsoft.booking.sts", "AGENT_GROUP_ID"), $user_group) && $GLOBALS["USER_AGENT"]) {

                    // ОТПРАВКА ПИСЬМА АГЕНТУ О ЕГО АКТИВАЦИИ НА САЙТЕ КАК АГЕНТА
                    \Bitrix\Main\Mail\Event::send(array(
                        "EVENT_NAME" => "TRAVELSOFT_BOOKING",
                        "LID" => $arFields['LID'],
                        "C_FIELDS" => array(
                            "ID" => $arFields['ID'],
                            "EMAIL" => $arFields['EMAIL']
                        ),
                        "DUPLICATE" => 'N',
                        "MESSAGE_ID" => \Bitrix\Main\Config\Option::get("travelsoft.booking.sts", "AGENT_ACTIVE_MAIL_TEMPLATE")
                    ));

                }

            }

            $GLOBALS["USER_AGENT"] = false;

        }

    }

    /**
     * @param array $arFields
     */
    static public function bxOnAfterUserLogin(&$arFields) {

        $operator = STSUtils::getMainOperator();

        //авторизация пользователя
        (new $operator["UF_CLASS"])->authorizeUser(array(
            "address" => $operator["UF_GATEWAY"],
            "login" => $arFields["LOGIN"],
            "password" => $arFields["PASSWORD"],
            "operator_id" => $operator["ID"]
        ));

    }

    /**
     * @param array $arFields
     */
    public static function bxOnSendUserInfo(&$arFields) {
        // смена пароля
        $arFields["FIELDS"]["PASSWORD"] = STSUtils::passWorker("","");
        STSUtils::passWorker("delete","");
    }

    /**
     * @param array $arFields
     */
    public static function bxOnBeforeEventSend(&$arFields) {
        // регистрация нового пользователя в системе
        $arFields["PASSWORD"] = STSUtils::passWorker();
        STSUtils::passWorker("delete");
    }

    /**
     *
     * @param array $arParams
     */
    public static function bxOnAfterUserLogout($arParams) {

        if ($arParams["SUCCESS"] === true) {

            $operator = STSUtils::getMainOperator();

            // удаление connect
            (new $operator["UF_CLASS"])->logoutUser(array(
                "address" => $operator["UF_GATEWAY"],
                "operator_id" => $operator["ID"]
            ));

        }

    }

    /**
     * @param type $arFields
     */
    public static function bxOnBeforeUserChangePassword($arFields) {
        STSUtils::passWorker("save", $arFields["PASSWORD"]);
    }

    /**
     * Модификация полей пользователя перед регистрацией в системе bitrix
     * @param array $arFields
     */
    public static function bxOnBeforeUserRegister(&$arFields) {

        if (STSUtils::isAgentRequest()) {

            $arFields["ACTIVE"] = \Bitrix\Main\Config\Option::get("travelsoft.booking.sts", "AUTO_ACTIVATION_AGENTS") == "Y" ? 'Y' : 'N';

            // вносим дополнительные поля в профиль агента на сайте
            $add_fields = array("UF_LEGAL_NAME", "UF_LEGAL_ADDRESS", "UF_BANK_NAME", "UF_BANK_ADDRESS",
                "UF_BANK_CODE", "UF_CHECKING_ACCOUNT", "UF_OKPO", "UF_UNP");

            for ($i = 0, $cnt = count($add_fields); $i < $cnt; $i++) {
                if (STSUtils::getRequest()->getPost($add_fields[$i]) <> '') {
                    $arFields[$add_fields[$i]] = STSUtils::getRequest()->getPost($add_fields[$i]);
                }
            }

            $arFields["UF_AGENT_NOT_ACTIVE"] = \Bitrix\Main\Config\Option::get("travelsoft.booking.sts", "AUTO_ACTIVATION_AGENTS") == "Y" ? 0 : 1;

            //dm(array($arFields,STSUtils::isAgentRequest()),false,true,false);

            //агент
            $response = STSUtils::createUser(array(
                "email" => $arFields["EMAIL"],
                "password" => $arFields["PASSWORD"],
                "activated" => \Bitrix\Main\Config\Option::get("travelsoft.booking.sts", "AUTO_ACTIVATION_AGENTS") == "Y" ? 1 : 0,
                "is_agent" => true,
                "username" => $arFields["LAST_NAME"]." ".$arFields["NAME"],
                "company" => $arFields["UF_LEGAL_NAME"],
                "address" => $arFields["UF_LEGAL_ADDRESS"],
                "inn" => $arFields["UF_UNP"]
            ));

        } else {
            $response = STSUtils::createUser(array("email" => $arFields["EMAIL"], "password" => $arFields["PASSWORD"], "activated" => 1));
        }

        if(!isset($response["result"]["result"]) || empty($response["result"]["user_id"])){
            return false;
        }

        $arFields["UF_USER_ID"] = $response["result"]["user_id"];

        STSUtils::setLoginEqlEmail($arFields);
        STSUtils::passWorker("save", $arFields["PASSWORD"]);
    }

    /**
     * Модификация полей пользователя после регистрации в системе bitrix
     * @param array $arFields
     */
    public static function bxOnAfterUserRegister($arFields) {

        if ($arFields['USER_ID'] > 0) {

            if (STSUtils::isAgentRequest()) {

                // ОТПРАВКА ПИСЬМО О РЕГИСТРАЦИИ АГЕНТУ
                \Bitrix\Main\Mail\Event::send(array(
                    "EVENT_NAME" => "TRAVELSOFT_BOOKING",
                    "LID" => SITE_ID,
                    "C_FIELDS" => array(
                        "EMAIL" => $arFields["EMAIL"],
                        "LOGIN" => $arFields["LOGIN"],
                        "PASSWORD" => $arFields["PASSWORD"],
                        "USER_ID" => $arFields['USER_ID']
                    ),
                    "DUPLICATE" => 'N',
                    "MESSAGE_ID" => \Bitrix\Main\Config\Option::get("travelsoft.booking.sts", "AGENT_REGISTER_MAIL_TEMPLATE")
                ));

                // отправка письма менеджеру
                \Bitrix\Main\Mail\Event::send(array(
                    "EVENT_NAME" => "TRAVELSOFT_BOOKING",
                    "LID" => SITE_ID,
                    "C_FIELDS" => array(
                        "USER_ID" => $arFields['USER_ID']
                    ),
                    "DUPLICATE" => 'N',
                    "MESSAGE_ID" => \Bitrix\Main\Config\Option::get("travelsoft.booking.sts", "AGENT_REGISTER_MAIL_TEMPLATE_FOR_MANAGER")
                ));

            }
        }
    }

}
