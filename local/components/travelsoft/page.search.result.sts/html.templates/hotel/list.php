
<div class="ts-detail-search-result">
    <div class="ts-display">
        <div class="catalog-tour__list-elem-box border-right-none border-sm-left-none ts-width-100">
            <div class="catalog-tour__list-elem-box-key ">
                <div class="catalog-tour__list-elem-date ts-custom-style-text">#name#<a target="_blank" href="#hotelDetailUrl#">#hotelName# #hotelStars#</a></div>
            </div>
        </div>
    </div>
<div class="ts-content-detail-search-result">
    <div class="ts-items-detail-search-result ts-col-24 ts-col-sm-12 ts-col-xl-5 ts-width-first-block border-right-none border-left-none">
        <div class="ts-item-detail-search-result">
            <div class="ts-icon ts-padding-icon"><img src="<?=SITE_TEMPLATE_PATH?>/img/calendar_icon_grey.png" width="20px" height="20px" alt="date"></div>
            <div class="ts-text ts-width-text">#dateFrom# - #dateTo#</div>
        </div>
        <div class="ts-item-detail-search-result">
            <div class="ts-text ts-small">#night#</div>
        </div>
    </div>
    <div class="ts-items-detail-search-result ts-col-24 ts-col-sm-12 ts-col-xl-5 border-right-none">
        <div class="ts-item-detail-search-result">
            <div class="ts-icon ts-img-position">
                <img src="<?=SITE_TEMPLATE_PATH?>/img/food2_icon_grey.png" width="20px" height="20px" alt="food">
                <div class="ts-tittle">#foodFullName#</div>
            </div>
            <div class="ts-text">#food#</div>

        </div>
        <div class="ts-item-detail-search-result">
            <div class="ts-text ts-small">#tourType#</div>
        </div>
    </div>
    <div class="ts-items-detail-search-result ts-col-24 ts-col-sm-12 ts-col-xl-5 border-right-none">
        <div class="ts-item-detail-search-result">
            <div class="ts-text ts-bold">#placement#</div>
            <div class="ts-icon ts-mx-03 ts-img-positionY"><img src="<?=SITE_TEMPLATE_PATH?>/img/adult2_icon_grey.png" width="20px" height="20px" alt="<?=GetMessage('ADULTS')?>" title="<?=GetMessage('ADULTS')?>"></div>
            <div class="ts-text">#adults#,</div>
            <div class="ts-icon ts-mx-03 ts-img-positionY"><img src="<?=SITE_TEMPLATE_PATH?>/img/child2_icon_grey.png" width="20px" height="20px" alt="<?=GetMessage('CHILDREN')?>" title="<?=GetMessage('CHILDREN')?>"></div>
            <div class="ts-text">#children#</div>
            <div class="ts-tittle"></div>
        </div>
        <div class="ts-item-detail-search-result">
            <div class="ts-text ts-small">#roomType# #roomCatType#</div>
        </div>
    </div>
    <div class="ts-items-detail-search-result ts-col-24 ts-col-sm-12 ts-col-xl-5 ts-width-price border-right-none">
        <div class="ts-item-detail-search-result">
            <div class="ts-text ts-full-size">#price#</div>
        </div>
        <div class="ts-item-detail-search-result">
            <div class="ts-text ts-small ">#prices# #defCurrency#</div>
        </div>
    </div>
    <div class="ts-items-detail-search-result ts-col-24 ts-col-xl-4 border-right-none">
        <div class="button ts-py-1">
            <form action="#bookingUrl#" method="post">
                <?=bitrix_sessid_post()?>
                <input type="hidden" name="make_booking[operatorId]" value="#operatorId#">
                <input type="hidden" name="make_booking[priceKey]" value="#priceKey#">
                <input type="hidden" name="make_booking[hotelIdBx]" value="#hotelIdBx#">
                <input type="hidden" name="make_booking[hotelName]" value="#hotelName#">
                <input type="hidden" name="make_booking[hotelStars]" value="#hotelStars#">
                <input type="hidden" name="make_booking[hotelDetailUrl]" value="#hotelDetailUrl#">
                <input type="hidden" name="make_booking[country]" value="#country#">
                <input type="hidden" name="make_booking[city]" value="#city#">
                <input type="hidden" name="make_booking[dateBegin]" value="#dateFrom#">
                <input type="hidden" name="make_booking[dateEnd]" value="#dateTo#">
                <input type="hidden" name="make_booking[night]" value="#night#">
                <input type="hidden" name="make_booking[adults]" value="#adults#">
                <input type="hidden" name="make_booking[children]" value="#children#">
                <input type="hidden" name="make_booking[food]" value="#food#">
                <input type="hidden" name="make_booking[placement]" value="#placement#">
                <input type="hidden" name="make_booking[roomType]" value="#roomType#">
                <input type="hidden" name="make_booking[roomCatType]" value="#roomCatType#">
                <input type="hidden" name="make_booking[price]" value="#price#">
                <input type="hidden" name="make_booking[priceNotCurrency]" value="#priceNotCurrency#">
                <input type="hidden" name="make_booking[currency]" value="#currency#">
                <button type="submit" value="add2cart" class="button ts-button-blue" ><?=GetMessage('ORDER')?></button>
            </form>
        </div>
    </div>
</div>
</div>