/**
 * Created by julia on 18.10.17.
 */
function ProgressBar(options) {

    "use strict";

    var total = options.total;

    this.render = function (num) {

        num = num || 0;
        var progress = num / total * 100;

        var html = '<div class="search-progress-bar"> \
            <div style="width: '+(progress == 100 ? 99.6 : progress)+'%" class="search-progress" id="searchProgress"></div> \
            <div id="percentCount" class="percent-count">'+progress+'%</div> \
        </div>';

        //document.getElementById(options.mount).innerHTML = html;

    };

    this.getMountPoint = function () {
      return options.mount;
    };
}