<?php

namespace travelsoft\sts;

use travelsoft\sts\pages\Settings as STSPageSettings;
use travelsoft\sts\Utils as STSUtils;
use travelsoft\sts\Settings;
use travelsoft\sts\_interfaces\Operator;

/**
 * Класс методов обработки событий
 *
 * @author dimabresky
 * @copyright (c) 2017, travelsoft
 */
class EventsHandlers {


    /**
     * Добавляет пункт меню Поиск туров в global меню админ. части
     * @global type $USER
     * @param type $aGlobalMenu
     */
    public static function addGlobalAdminMenuItem(&$arGlobalMenu) {

        global $USER;

        //if (sts\Utils::access()) {
        //if ($USER->GetID() == 1) {

        $currentUserGroups = $USER->GetUserGroupArray();

        $arAllMenuItems = array(
            STSPageSettings::DIRECTIONS_URL => array(
                "text" => "Направления. Страны",
                "url" => STSPageSettings::DIRECTIONS_URL . "?lang=" . LANGUAGE_ID,
                "more_url" => array(STSPageSettings::DETAIL_DIRECTIONS_URL),
                "title" => "Направления. Страны",
            ),
            STSPageSettings::DIRECTIONS_RESORTS_URL => array(
                "text" => "Направления. Курорты",
                "url" => STSPageSettings::DIRECTIONS_RESORTS_URL . "?lang=" . LANGUAGE_ID,
                "more_url" => array(STSPageSettings::DETAIL_DIRECTIONS_RESORTS_URL),
                "title" => "Направления. Курорты",
            ),
            STSPageSettings::TABLES_LIST_URL => array(
                "text" => "Соответствия",
                "url" => STSPageSettings::TABLES_LIST_URL . "?lang=" . LANGUAGE_ID,
                "more_url" => array(STSPageSettings::TABLES_LIST_URL),
                "title" => "Соответствия",
            ),
        );

        $arGlobalMenu["global_menu_travelsoft_sts"] = array(
            "menu_id" => "travelsoft_sts",
            "text" => "Поиск туров",
            "title" => "Поиск туров",
            "sort" => 500,
            "items_id" => "global_menu_travelsoft_sts",
            "help_section" => "travelsoft_sts",
            "items" => array_values($arAllMenuItems)
        );
        //}
    }


    /**
     * @param Bitrix\Main\Entity\Event $event
     */
    public static function onAfterOperatorAdd($id) {

        global $APPLICATION;

        $arr_operator = \travelsoft\sts\stores\Operators::getById($id);

        if (isset($arr_operator["UF_NAME"]) && !empty($arr_operator["UF_NAME"])) {

            $oUserTypeEntity = new \CUserTypeEntity();

            $info = array();

            $arTableHL = array("relationcities", "relationcountries", "relationhotels", "relationtourtypes", "relationtours", "relationfood", "relationstars", "relationplacements", "relationdirections", "relationdirectionsresorts");
            foreach ($arTableHL as $hl) {

                $class = $hl . "StoreId";

                $highBlockID = \travelsoft\sts\Settings::$class();

                $aUserField = array(
                    "ENTITY_ID" => 'HLBLOCK_' . $highBlockID,
                    "FIELD_NAME" => Operator::$fieldCodeName . $arr_operator["ID"],
                    "USER_TYPE_ID" => 'string',
                    "XML_ID" => "",
                    "SORT" => 100,
                    "MULTIPLE" => in_array($hl, array("relationdirections", "relationdirectionsresorts")) ? 'Y' : 'N',
                    'MANDATORY' => 'N',
                    'SHOW_FILTER' => 'N',
                    'SHOW_IN_LIST' => 'Y',
                    'IS_SEARCHABLE' => 'N',
                    'SETTINGS' => array(
                        'DEFAULT_VALUE' => "",
                        'SIZE' => '20',
                        'ROWS' => 1,
                        'MIN_LENGTH' => 0,
                        'MAX_LENGTH' => 0,
                        'REGEXP' => ''
                    ),
                    'EDIT_FORM_LABEL' => array(
                        'ru' => $arr_operator["UF_NAME"],
                        'en' => $arr_operator["UF_NAME"],
                    ),
                    'LIST_COLUMN_LABEL' => array(
                        'ru' => $arr_operator["UF_NAME"],
                        'en' => $arr_operator["UF_NAME"],
                    ),
                    'LIST_FILTER_LABEL' => array(
                        'ru' => $arr_operator["UF_NAME"],
                        'en' => $arr_operator["UF_NAME"],
                    ),
                    'ERROR_MESSAGE' => array(
                        'ru' => 'Ошибка при заполнении поля '. $arr_operator["UF_NAME"],
                        'en' => 'An error in completing the field '. $arr_operator["UF_NAME"],
                    ),
                    'HELP_MESSAGE' => array(
                        'ru' => '',
                        'en' => '',
                    ),
                );

                if ($idUserTypeProp = $oUserTypeEntity->Add($aUserField)) {
                    $info[] = STSUtils::oGetMessage('USER_TYPE_ADDED', array(
                        '#FIELD_NAME#' => $aUserField['FIELD_NAME'],
                        '#ENTITY_ID#' => $aUserField['ENTITY_ID'],
                    ));
                } else {
                    if (($ex = $APPLICATION->GetException())) {
                        $info[] = STSUtils::oGetMessage('USER_TYPE_ADDED_ERROR', array(
                            '#FIELD_NAME#' => $aUserField['FIELD_NAME'],
                            '#ENTITY_ID#' => $aUserField['ENTITY_ID'],
                            '#ERROR#' => $ex->GetString(),
                        ));
                    }
                }


            }

            //TODO: дописать запись массива $info в log

            $uf_code_operator = Operator::$fieldCodeName;
            $arOperatorProp = \travelsoft\sts\stores\Operators::update($arr_operator["ID"], Array("UF_CODE" => $uf_code_operator . $arr_operator["ID"]));

        }
    }

    /**
     * @param Bitrix\Main\Entity\Event $event
     */
    public static function onAfterOperatorDelete($id) {

        $info = array();

        $oUserTypeEntity = new \CUserTypeEntity();

        $arTableHL = array("relationcities","relationcountries","relationhotels","relationtourtypes","relationtours","relationfood","relationstars","relationplacements","relationdirections","relationdirectionsresorts");
        foreach ($arTableHL as $hl) {

            $class = $hl . "StoreId";

            $highBlockID = \travelsoft\sts\Settings::$class();

            $aUserField = array(
                'ENTITY_ID' => 'HLBLOCK_' . $highBlockID,
                'FIELD_NAME' => Operator::$fieldCodeName . $id
            );

            $resProperty = \CUserTypeEntity::GetList(
                array(),
                array('ENTITY_ID' => $aUserField['ENTITY_ID'], 'FIELD_NAME' => $aUserField['FIELD_NAME'])
            );

            if ($aUserHasField = $resProperty->Fetch()) {
                $idUserTypeProp = $aUserHasField['ID'];

                $oUserTypeEntity->Delete( $idUserTypeProp );
                $info[] = STSUtils::oGetMessage('USER_TYPE_DELETED', array(
                    '#FIELD_NAME#' => $aUserHasField['FIELD_NAME'],
                    '#ENTITY_ID#' => $aUserHasField['ENTITY_ID'],
                ));
            }

        }

        //TODO: дописать запись массива $info в log

    }

    /**
     * @param array $arFields
     */
    public static function bxOnAfterIBlockElementAdd(&$arFields)
    {

        if($arFields["ID"]) {
            $operator = current(\travelsoft\sts\stores\Operators::get(array("filter" => array("UF_MAIN_OPERATOR" => 1))));

            if ($operator) {

                switch ($arFields["IBLOCK_ID"]) {
                    case \travelsoft\sts\Settings::citiesStoreId():

                        STSUtils::setValuesTables($arFields['ID'], $arFields['PROPERTY_VALUES'], '\travelsoft\sts\stores\RelationCities', 'MT_KEY', $operator["UF_CODE"]);
                        STSUtils::setCheckboxDeparture($arFields['ID'], $arFields['PROPERTY_VALUES'], '\travelsoft\sts\stores\RelationCities', 'CITY_DEPARTURE', $operator["UF_CODE"]);

                        break;
                    case \travelsoft\sts\Settings::countriesStoreId():

                        STSUtils::setValuesTables($arFields['ID'], $arFields['PROPERTY_VALUES'], '\travelsoft\sts\stores\RelationCountries', 'MT_KEY', $operator["UF_CODE"]);

                        break;
                    case \travelsoft\sts\Settings::foodStoreId():

                        STSUtils::setValuesTables($arFields['ID'], $arFields['PROPERTY_VALUES'], '\travelsoft\sts\stores\RelationFood', 'MT_KEY', $operator["UF_CODE"]);

                        break;
                    case \travelsoft\sts\Settings::hotelsStoreId():

                        STSUtils::setValuesTables($arFields['ID'], $arFields['PROPERTY_VALUES'], '\travelsoft\sts\stores\RelationHotels', 'HOTELKEY', $operator["UF_CODE"]);

                        break;
                    case \travelsoft\sts\Settings::toursStoreId():

                        STSUtils::setValuesTables($arFields['ID'], $arFields['PROPERTY_VALUES'], '\travelsoft\sts\stores\RelationTours', 'MT_KEY', $operator["UF_CODE"]);

                        break;
                    case \travelsoft\sts\Settings::tourtypesStoreId():

                        STSUtils::setValuesTablesProp($arFields['ID'], $arFields['PROPERTY_VALUES'], '\travelsoft\sts\stores\RelationTourTypes', 'MT_KEY', $operator["UF_CODE"], $arFields["SORT"], $arFields["IBLOCK_ID"], array("TYPE_SEARCH"));

                        break;
                    case \travelsoft\sts\Settings::starsStoreId():

                        if (!empty($arFields["NAME"]) && $arFields['ID'] > 0) {

                            $class = new \travelsoft\sts\stores\RelationStars();

                            $relCity = current($class::get(array("filter" => array("UF_BX_ID" => $arFields['ID']), "select" => array("ID", $operator["UF_CODE"]))));

                            $data = array(
                                "UF_BX_ID" => $arFields['ID'],
                                $operator["UF_CODE"] => $arFields["NAME"],
                                "UF_VALUE" => $arFields["NAME"]
                            );

                            if (isset($relCity["ID"]) && $relCity[$operator["UF_CODE"]] != $arFields["NAME"]) {

                                $class::update($relCity["ID"], $data);

                            } elseif (!isset($relCity["ID"])) {

                                $class::add($data);

                            }

                        }

                        break;
                }

            }

            $toursStoreId = \travelsoft\sts\Settings::toursStoreId();

            if ($arFields["IBLOCK_ID"] == $toursStoreId) {

                $prop_price = '';
                $prop_currency = '';
                $property = \CIBlockProperty::GetList(Array("sort" => "asc", "name" => "asc"), Array("ACTIVE" => "Y", "IBLOCK_ID" => $toursStoreId));
                while ($prop_fields = $property->GetNext()) {
                    if ($prop_fields["CODE"] == "PRICE") {
                        $prop_price = $prop_fields["ID"];
                    }
                    if ($prop_fields["CODE"] == "CURRENCY") {
                        $prop_currency = $prop_fields["ID"];
                    }
                }

                if (isset($arFields['PROPERTY_VALUES'][$prop_price]) && !empty($arFields['PROPERTY_VALUES'][$prop_price]) && !empty($arFields['PROPERTY_VALUES'][$prop_currency])) {

                    $price_val = current($arFields['PROPERTY_VALUES'][$prop_price]);
                    $currency_val = current($arFields['PROPERTY_VALUES'][$prop_currency]);

                    if (!empty($price_val["VALUE"]) && !empty($currency_val["VALUE"])) {

                        $arCurrencyIso = \CIBlockProperty::GetPropertyEnum("CURRENCY", Array(), Array("IBLOCK_ID" => $toursStoreId, "ID" => $currency_val["VALUE"]))->GetNext();

                        $price_byn = number_format(current(\travelsoft\currency\factory\Converter::getInstance()->convert($price_val["VALUE"], $arCurrencyIso["VALUE"])->getResultLikeArray()), 0, '.', '');
                        if (!empty($price_byn)) {
                            \CIBlockElement::SetPropertyValuesEx($arFields["ID"], false, array("PRICE_BYN" => $price_byn));
                        }

                    }

                }

            }
        }

    }

    /**
     * @param array $arFields
     */
    public static function bxOnAfterIBlockElementUpdate(&$arFields)
    {

        if($arFields["ID"]) {
            $operator = current(\travelsoft\sts\stores\Operators::get(array("filter" => array("UF_MAIN_OPERATOR" => 1))));

            if ($operator) {

                switch ($arFields["IBLOCK_ID"]) {
                    case \travelsoft\sts\Settings::citiesStoreId():

                        STSUtils::setValuesTables($arFields['ID'], $arFields['PROPERTY_VALUES'], '\travelsoft\sts\stores\RelationCities', 'MT_KEY', $operator["UF_CODE"]);
                        STSUtils::setCheckboxDeparture($arFields['ID'], $arFields['PROPERTY_VALUES'], '\travelsoft\sts\stores\RelationCities', 'CITY_DEPARTURE', $operator["UF_CODE"]);

                        break;
                    case \travelsoft\sts\Settings::countriesStoreId():

                        STSUtils::setValuesTables($arFields['ID'], $arFields['PROPERTY_VALUES'], '\travelsoft\sts\stores\RelationCountries', 'MT_KEY', $operator["UF_CODE"]);

                        break;
                    case \travelsoft\sts\Settings::foodStoreId():

                        STSUtils::setValuesTables($arFields['ID'], $arFields['PROPERTY_VALUES'], '\travelsoft\sts\stores\RelationFood', 'MT_KEY', $operator["UF_CODE"]);

                        break;
                    case \travelsoft\sts\Settings::hotelsStoreId():

                        STSUtils::setValuesTables($arFields['ID'], $arFields['PROPERTY_VALUES'], '\travelsoft\sts\stores\RelationHotels', 'HOTELKEY', $operator["UF_CODE"]);

                        break;
                    case \travelsoft\sts\Settings::toursStoreId():

                        STSUtils::setValuesTables($arFields['ID'], $arFields['PROPERTY_VALUES'], '\travelsoft\sts\stores\RelationTours', 'MT_KEY', $operator["UF_CODE"]);

                        break;
                    case \travelsoft\sts\Settings::tourtypesStoreId():

                        STSUtils::setValuesTablesProp($arFields['ID'], $arFields['PROPERTY_VALUES'], '\travelsoft\sts\stores\RelationTourTypes', 'MT_KEY', $operator["UF_CODE"], $arFields["SORT"], $arFields["IBLOCK_ID"], array("TYPE_SEARCH"));

                        break;
                    case \travelsoft\sts\Settings::starsStoreId():

                        if (!empty($arFields["NAME"]) && $arFields['ID'] > 0) {

                            $class = new \travelsoft\sts\stores\RelationStars();

                            $relCity = current($class::get(array("filter" => array("UF_BX_ID" => $arFields['ID']), "select" => array("ID", $operator["UF_CODE"]))));

                            $data = array(
                                "UF_BX_ID" => $arFields['ID'],
                                $operator["UF_CODE"] => $arFields["NAME"],
                                "UF_VALUE" => $arFields["NAME"]
                            );

                            if (isset($relCity["ID"]) && $relCity[$operator["UF_CODE"]] != $arFields["NAME"]) {

                                $class::update($relCity["ID"], $data);

                            } elseif (!isset($relCity["ID"])) {

                                $class::add($data);

                            }

                        }

                        break;
                }

            }

            $toursStoreId = \travelsoft\sts\Settings::toursStoreId();

            if ($arFields["IBLOCK_ID"] == $toursStoreId) {

                $prop_price = '';
                $prop_currency = '';
                $property = \CIBlockProperty::GetList(Array("sort" => "asc", "name" => "asc"), Array("ACTIVE" => "Y", "IBLOCK_ID" => $toursStoreId));
                while ($prop_fields = $property->GetNext()) {
                    if ($prop_fields["CODE"] == "PRICE") {
                        $prop_price = $prop_fields["ID"];
                    }
                    if ($prop_fields["CODE"] == "CURRENCY") {
                        $prop_currency = $prop_fields["ID"];
                    }
                }

                if (isset($arFields['PROPERTY_VALUES'][$prop_price]) && !empty($arFields['PROPERTY_VALUES'][$prop_price]) && !empty($arFields['PROPERTY_VALUES'][$prop_currency])) {

                    $price_val = current($arFields['PROPERTY_VALUES'][$prop_price]);
                    $currency_val = current($arFields['PROPERTY_VALUES'][$prop_currency]);

                    if (!empty($price_val["VALUE"]) && !empty($currency_val["VALUE"])) {

                        $arCurrencyIso = \CIBlockProperty::GetPropertyEnum("CURRENCY", Array(), Array("IBLOCK_ID" => $toursStoreId, "ID" => $currency_val["VALUE"]))->GetNext();

                        $price_byn = number_format(current(\travelsoft\currency\factory\Converter::getInstance()->convert($price_val["VALUE"], $arCurrencyIso["VALUE"])->getResultLikeArray()), 0, '.', '');
                        if (!empty($price_byn)) {
                            \CIBlockElement::SetPropertyValuesEx($arFields["ID"], false, array("PRICE_BYN" => $price_byn));
                        }

                    }

                }

            }
        }

    }

}
