<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?$upCountryName = ucwords($arResult["COUNTRY"]["CODE"]);
$arImgFlag = getSrc($arResult["COUNTRY"]["PROPERTY_FLAG_VALUE"], array("width"=>24,"height"=>24), SITE_TEMPLATE_PATH.'/flags/'.$upCountryName.'.png', 1, false);?>
<div class="country-menu">
    <div class="container">
        <p class="country-title" style="background-image:url(<?=$arImgFlag[0]?>)"><a href="<?=$arParams['FOLDER_URL']?>"><?=$arResult["COUNTRY"]["NAME"]?></a></p>
        <p class="country-title-mobile" style="background-image:url(<?=$arImgFlag[0]?>)">
            <a href="#toggle" href="<?=$arParams['FOLDER_URL']?>" id="country-menu-button" class="flashlink"><?=$arResult["COUNTRY"]["NAME"]?></a>
        </p>
        <ul class="tabs">
            <?if($arResult["CITIES"]):?>
                <li class="active"><a href="<?=$arParams['FOLDER_URL']?>resorts/"><span><?=GetMessage('RESORTS')?></span></a></li>
            <?endif?>
            <?if($arResult["HOTELS"]):?>
                <li><a href="<?=$arParams['FOLDER_URL']?>hotels/"><span><?=GetMessage('HOTELS')?></span></a></li>
            <?endif?>
            <?if($arResult["VISAS"]):?>
                <li><a href="<?=$arParams['FOLDER_URL']?>visa/"><span><?=GetMessage('VISAS')?></span></a></li>
            <?endif?>
            <?if(!empty($arResult["COUNTRY"]["PROPERTY_TOURIST_GUIDE_VALUE"])):?>
                <li><a href="<?=$arParams['FOLDER_URL']?>tourist-info/"><span><?=GetMessage('TOURIST_GUIDE')?></span></a></li>
            <?endif?>
        </ul>
    </div>
</div>
<div class="container col1">
    <h1 class=""><?=GetMessage('PAGE_H1')?><?if(!empty($arParams["TEXT_TITLE"])):?> <?=$arParams["TEXT_TITLE"]?><?endif?></h1>
    <div class="content resorts">
        <?if(!empty($arResult["ITEMS"])):?>

            <?foreach($arResult["ITEMS"] as $type):?>

                <h2><?= $type["NAME"] ?></h2>
                <?foreach(array_chunk($type["ITEMS"], ceil(count($type["ITEMS"]) / 4)) as $n => $chunk):?>
                    <ul class="list-<?= ($n + 1) ?>">
                        <?foreach ($chunk as $region): ?>
                            <li>
                                <h3><?= $region["NAME"] ?></h3>
                                <ul>
                                    <?foreach ($region["ITEMS"] as $arItem): ?>
                                        <?
                                        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                                        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                                        ?>
                                        <li><a href="<?=$arParams['FOLDER_URL']?>resorts/<?=$arItem["CODE"]?>/"><?= $arItem["NAME"] ?></a></li>
                                    <?endforeach ?>
                                </ul>
                            </li>
                        <?endforeach ?>
                    </ul>
                <?endforeach;?>
                <div class="clear"></div>
            <?endforeach;?>

        <?else:?>
            <p><?=GetMessage('NOT_RESORTS')?></p>
        <?endif?>
    </div>
</div>
