<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
<ul class="col1-3">
    <?
    $previousLevel = 0;
    foreach($arResult as $arItem):?>

        <?if(!isset($arItem["PARAMS"]["menu"]) || $arItem["PARAMS"]["menu"] != 'right'):?>

            <?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
                <?=str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
            <?endif?>

            <?if ($arItem["IS_PARENT"]):?>

                <?if ($arItem["DEPTH_LEVEL"] == 1):?>

                    <li class="<?if ($arItem["SELECTED"]):?>active<?endif?>">
                        <a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
                        <ul class="dropmenu">

                <?else:?>

                    <li class="<?if ($arItem["SELECTED"]):?>active<?endif?>">
                        <a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
                        <ul class="dropmenu">

                <?endif?>

            <?else:?>

                <?if ($arItem["PERMISSION"] > "D"):?>

                    <?if ($arItem["DEPTH_LEVEL"] == 1):?>
                        <li><a class="<?if ($arItem["SELECTED"]):?>active<?endif?>" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
                    <?else:?>
                        <li><a class="<?if ($arItem["SELECTED"]):?>active<?endif?>" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
                    <?endif?>

                <?else:?>

                    <?if ($arItem["DEPTH_LEVEL"] == 1):?>
                        <li><a class="<?if ($arItem["SELECTED"]):?>active<?endif?>" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
                    <?else:?>
                        <li><a class="<?if ($arItem["SELECTED"]):?>active<?endif?>" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
                    <?endif?>

                <?endif?>

            <?endif?>

            <?$previousLevel = $arItem["DEPTH_LEVEL"];?>

        <?endif?>
    <?endforeach?>
    <?$k = 0;?>
    <?foreach($arResult as $arItem):?>

        <?if(isset($arItem["PARAMS"]["menu"]) && $arItem["PARAMS"]["menu"] == 'right'):?>

            <?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
                <?=str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
            <?endif?>

            <?if ($arItem["IS_PARENT"]):?>

                <?if ($arItem["DEPTH_LEVEL"] == 1):?>

                    <li class="<?if ($arItem["SELECTED"]):?>active<?endif?>">
                        <a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
                        <ul class="dropmenu">

                <?else:?>

                    <li class="<?if ($arItem["SELECTED"]):?>active<?endif?>">
                        <a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
                        <ul class="dropmenu">

                <?endif?>

            <?else:?>

                <?if ($arItem["PERMISSION"] > "D"):?>

                    <?if ($arItem["DEPTH_LEVEL"] == 1):?>
                        <li><?if($k == 0):?><br><?endif?><a class="<?if ($arItem["SELECTED"]):?>active<?endif?>" href="<?=$arItem["LINK"]?>"><span><?=$arItem["TEXT"]?></span></a></li>
                    <?else:?>
                        <li><?if($k == 0):?><br><?endif?><a class="<?if ($arItem["SELECTED"]):?>active<?endif?>" href="<?=$arItem["LINK"]?>"><span><?=$arItem["TEXT"]?></span></a></li>
                    <?endif?>

                <?else:?>

                    <?if ($arItem["DEPTH_LEVEL"] == 1):?>
                        <li><?if($k == 0):?><br><?endif?><a class="<?if ($arItem["SELECTED"]):?>active<?endif?>" href="<?=$arItem["LINK"]?>"><span><?=$arItem["TEXT"]?></span></a></li>
                    <?else:?>
                        <li><?if($k == 0):?><br><?endif?><a class="<?if ($arItem["SELECTED"]):?>active<?endif?>" href="<?=$arItem["LINK"]?>"><span><?=$arItem["TEXT"]?></span></a></li>
                    <?endif?>

                <?endif?>

            <?endif?>

            <?$previousLevel = $arItem["DEPTH_LEVEL"];?>
            <?$k++?>

        <?endif?>
    <?endforeach?>

    <?if ($previousLevel > 1)://close last item tags?>
        <?=str_repeat("</ul></li>", ($previousLevel-1) );?>
    <?endif?>
</ul>
<?endif?>
