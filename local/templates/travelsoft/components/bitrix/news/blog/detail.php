<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
use Bitrix\Main\Context,
    Bitrix\Main\Type\DateTime,
    Bitrix\Main\Loader,
    Bitrix\Iblock;
?>
<?if($APPLICATION->GetDirProperty("SHOW_TOP_FORM") == "Y"):?>
    <?$APPLICATION->IncludeComponent(
        "travelsoft:search.form.sts",
        "main",
        Array(
            "ACTION_URL" => "/online/",
            "GET_PARAMS_STS" => $_REQUEST["stsSearch"],
            "SHOW_PANEL_HEADING" => "N",
            "PREDLOG" => "N"
        )
    );?>
<?endif?>
<?$res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID"=>$arParams["IBLOCK_ID"], "ACTIVE"=>"Y","CODE"=>$arResult["VARIABLES"]["ELEMENT_CODE"]), false, false, Array("ID", "IBLOCK_ID", "PROPERTY_COUNTRY"));
if($res->SelectedRowsCount() >= 1):?>
<? $template = (isset($arParams["TEMPLATE_DETAIL"]) && !empty($arParams["TEMPLATE_DETAIL"])) ? $arParams["TEMPLATE_DETAIL"] : ""; ?>
<? $ElementID = $APPLICATION->IncludeComponent(
    "travelsoft:travelsoft.news.detail",
    $template,
    Array(
        "DISPLAY_DATE" => $arParams["DISPLAY_DATE"],
        "DISPLAY_NAME" => $arParams["DISPLAY_NAME"],
        "DISPLAY_PICTURE" => $arParams["DISPLAY_PICTURE"],
        "DISPLAY_PREVIEW_TEXT" => $arParams["DISPLAY_PREVIEW_TEXT"],
        "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        "FIELD_CODE" => $arParams["DETAIL_FIELD_CODE"],
        "PROPERTY_CODE" => $arParams["DETAIL_PROPERTY_CODE"],
        "DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["detail"],
        "SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
        "META_KEYWORDS" => $arParams["META_KEYWORDS"],
        "META_DESCRIPTION" => $arParams["META_DESCRIPTION"],
        "BROWSER_TITLE" => $arParams["BROWSER_TITLE"],
        "SET_CANONICAL_URL" => $arParams["DETAIL_SET_CANONICAL_URL"],
        "DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
        "SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
        "SET_TITLE" => $arParams["SET_TITLE"],
        "MESSAGE_404" => $arParams["MESSAGE_404"],
        "SET_STATUS_404" => $arParams["SET_STATUS_404"],
        "SHOW_404" => $arParams["SHOW_404"],
        "FILE_404" => $arParams["FILE_404"],
        "INCLUDE_IBLOCK_INTO_CHAIN" => $arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
        "ADD_SECTIONS_CHAIN" => $arParams["ADD_SECTIONS_CHAIN"],
        "ACTIVE_DATE_FORMAT" => $arParams["DETAIL_ACTIVE_DATE_FORMAT"],
        "CACHE_TYPE" => $arParams["CACHE_TYPE"],
        "CACHE_TIME" => $arParams["CACHE_TIME"],
        "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
        "USE_PERMISSIONS" => $arParams["USE_PERMISSIONS"],
        "GROUP_PERMISSIONS" => $arParams["GROUP_PERMISSIONS"],
        "DISPLAY_TOP_PAGER" => $arParams["DETAIL_DISPLAY_TOP_PAGER"],
        "DISPLAY_BOTTOM_PAGER" => $arParams["DETAIL_DISPLAY_BOTTOM_PAGER"],
        "PAGER_TITLE" => $arParams["DETAIL_PAGER_TITLE"],
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => $arParams["DETAIL_PAGER_TEMPLATE"],
        "PAGER_SHOW_ALL" => $arParams["DETAIL_PAGER_SHOW_ALL"],
        "CHECK_DATES" => $arParams["CHECK_DATES"],
        "ELEMENT_ID" => $arResult["VARIABLES"]["ELEMENT_ID"],
        "ELEMENT_CODE" => $arResult["VARIABLES"]["ELEMENT_CODE"],
        "SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
        "SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
        "IBLOCK_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["news"],
        "USE_SHARE" => $arParams["USE_SHARE"],
        "SHARE_HIDE" => $arParams["SHARE_HIDE"],
        "SHARE_TEMPLATE" => $arParams["SHARE_TEMPLATE"],
        "SHARE_HANDLERS" => $arParams["SHARE_HANDLERS"],
        "SHARE_SHORTEN_URL_LOGIN" => $arParams["SHARE_SHORTEN_URL_LOGIN"],
        "SHARE_SHORTEN_URL_KEY" => $arParams["SHARE_SHORTEN_URL_KEY"],
        "ADD_ELEMENT_CHAIN" => (isset($arParams["ADD_ELEMENT_CHAIN"]) ? $arParams["ADD_ELEMENT_CHAIN"] : ''),
        'STRICT_SECTION_CHECK' => (isset($arParams['STRICT_SECTION_CHECK']) ? $arParams['STRICT_SECTION_CHECK'] : ''),
    ),
    $component
); ?>
<?else:?>
<div class="container<?if(!empty($APPLICATION->GetDirProperty("DIRECTORY_CLASS"))):?> <?=$APPLICATION->GetDirProperty("DIRECTORY_CLASS")?><?endif?><?if($APPLICATION->GetDirProperty("SHOW_RIGHT") == "Y"):?> col2<?else:?> col1<?endif?>">
    <h1<?if($APPLICATION->GetDirProperty("SHOW_TOP_FORM") != "Y"):?> class="nopadding"<?endif?>><?=$APPLICATION->ShowTitle(false)?></h1>
    <?if(is_numeric($arResult["VARIABLES"]["ELEMENT_CODE"])):?>
        <?if($APPLICATION->GetDirProperty("SHOW_RIGHT") == "Y"):?>
            <?$arCountries = array();
            $countries[0] = array(
                "ID" => 0,
                "NAME" => GetMessage('ALL_COUNTRIES'),
                "CODE" => '',
                "PAGE" => $arParams["SEF_FOLDER"]
            );
            $flag = false;
            $res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => COUNTRIES_IBLOCK_ID, "ACTIVE" => "Y"), false, false, Array("IBLOCK_ID", "ID", "NAME", "CODE", "DETAIL_PAGE_URL"));
            while ($ob = $res->GetNext()) {
                $flag = true;
                $countries[$ob["ID"]] = array(
                    "ID" => $ob["ID"],
                    "CODE" => $ob["CODE"],
                    "NAME" => $ob["NAME"],
                    "PAGE" => $arParams["SEF_FOLDER"].$ob["CODE"]."/"
                );
                $arCountries[] = $ob["CODE"];
            }
            $year_today = date("Y");
            $first_year = $year_today - 6;
            ?>
            <div class="sidebar">
                <?if(!empty($countries) && $flag):?>
                    <ul>
                        <?foreach ($countries as $county):?>
                            <?if($county["ID"] == 0):?>
                                <li class="active">
                                    <?= $county["NAME"] ?>
                                </li>
                            <?else:?>
                                <li>
                                    <a href="<?=$county["PAGE"] ?>"><?= $county["NAME"] ?></a>
                                </li>
                            <?endif?>
                        <?endforeach;?>
                    </ul>
                <?endif?>
                <ul>
                    <li><a href="<?=$arParams["SEF_FOLDER"]?>" ><?=GetMessage('ALL_YEARS')?></a></li>
                    <?for ($i = $year_today; $i >= $first_year; $i--):?>
                        <?if(is_numeric($arResult["VARIABLES"]["ELEMENT_CODE"]) && $i == $arResult["VARIABLES"]["ELEMENT_CODE"]):?>
                            <li class="active"><?=$i ?></li>
                        <?else:?>
                            <li><a href="<?=$arParams["SEF_FOLDER"].$i."/" ?>"><?=$i ?></a></li>
                        <?endif?>
                    <?endfor;?>
                </ul>
                <?
                $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "",
                        "PATH" => SITE_DIR . "include/sidebarSubscription.php"
                    )
                );
                ?>
                <?
                $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "",
                        "PATH" => SITE_DIR . "include/sidebarWidgets.php"
                    )
                );
                ?>
            </div>
        <?endif;?>
        <div class="content">
            <? $GLOBALS[$arParams["FILTER_NAME"]] = array(">=DATE_ACTIVE_FROM"=>date($DB->DateFormatToPHP(CLang::GetDateFormat("SHORT")), mktime(0,0,0,1,1,$arResult["VARIABLES"]["ELEMENT_CODE"])), "<=DATE_ACTIVE_FROM"=>date($DB->DateFormatToPHP(CLang::GetDateFormat("SHORT")), mktime(23,59,59,12,31,$arResult["VARIABLES"]["ELEMENT_CODE"])));?>
            <? $template = (isset($arParams["TEMPLATE_LIST"]) && !empty($arParams["TEMPLATE_LIST"])) ? $arParams["TEMPLATE_LIST"] : "";?>
            <? $APPLICATION->IncludeComponent(
                "travelsoft:travelsoft.news.list",
                $template,
                Array(
                    "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                    "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                    "NEWS_COUNT" => $arParams["NEWS_COUNT"],
                    "SORT_BY1" => !empty($_REQUEST["sort_by1"]) ? $_REQUEST["sort_by1"] : $arParams["SORT_BY1"],
                    "SORT_ORDER1" => !empty($_REQUEST["sort_order1"]) ? $_REQUEST["sort_order1"] : $arParams["SORT_ORDER1"],
                    "SORT_BY2" => $arParams["SORT_BY2"],
                    "SORT_ORDER2" => $arParams["SORT_ORDER2"],
                    "FIELD_CODE" => $arParams["LIST_FIELD_CODE"],
                    "PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
                    "DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["detail"],
                    "SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
                    "IBLOCK_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["news"],
                    "DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
                    "SET_TITLE" => $arParams["SET_TITLE"],
                    "SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
                    "MESSAGE_404" => $arParams["MESSAGE_404"],
                    "SET_STATUS_404" => $arParams["SET_STATUS_404"],
                    "SHOW_404" => $arParams["SHOW_404"],
                    "FILE_404" => $arParams["FILE_404"],
                    "INCLUDE_IBLOCK_INTO_CHAIN" => $arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
                    "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                    "CACHE_TIME" => $arParams["CACHE_TIME"],
                    "CACHE_FILTER" => $arParams["CACHE_FILTER"],
                    "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                    "DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
                    "DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
                    "PAGER_TITLE" => $arParams["PAGER_TITLE"],
                    "PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
                    "PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
                    "PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
                    "PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
                    "PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
                    "PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
                    "PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
                    "DISPLAY_DATE" => $arParams["DISPLAY_DATE"],
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => $arParams["DISPLAY_PICTURE"],
                    "DISPLAY_PREVIEW_TEXT" => $arParams["DISPLAY_PREVIEW_TEXT"],
                    "PREVIEW_TRUNCATE_LEN" => $arParams["PREVIEW_TRUNCATE_LEN"],
                    "ACTIVE_DATE_FORMAT" => $arParams["LIST_ACTIVE_DATE_FORMAT"],
                    "USE_PERMISSIONS" => $arParams["USE_PERMISSIONS"],
                    "GROUP_PERMISSIONS" => $arParams["GROUP_PERMISSIONS"],
                    "FILTER_NAME" => $arParams["FILTER_NAME"],
                    "HIDE_LINK_WHEN_NO_DETAIL" => $arParams["HIDE_LINK_WHEN_NO_DETAIL"],
                    "CHECK_DATES" => $arParams["CHECK_DATES"],
                ),
                $component
            ); ?>
        </div>
    <?else:?>
        <?$res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID"=>COUNTRIES_IBLOCK_ID, "ACTIVE"=>"Y" , "CODE"=>$arResult["VARIABLES"]["ELEMENT_CODE"]), false, false, Array("ID", "IBLOCK_ID", "NAME"));?>
        <?if($res->SelectedRowsCount() >= 1):?>
            <?
            while($ar_fields = $res->GetNext())
            {

                $country = $ar_fields["ID"];

            }
            if($country):?>
                <?if($APPLICATION->GetDirProperty("SHOW_RIGHT") == "Y"):?>
                    <?$arCountries = array();
                    $countries[0] = array(
                        "ID" => 0,
                        "NAME" => GetMessage('ALL_COUNTRIES'),
                        "CODE" => '',
                        "PAGE" => $arParams["SEF_FOLDER"]
                    );
                    $flag = false;
                    $res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => COUNTRIES_IBLOCK_ID, "ACTIVE" => "Y"), false, false, Array("IBLOCK_ID", "ID", "NAME", "CODE", "DETAIL_PAGE_URL"));
                    while ($ob = $res->GetNext()) {
                        $flag = true;
                        $countries[$ob["ID"]] = array(
                            "ID" => $ob["ID"],
                            "CODE" => $ob["CODE"],
                            "NAME" => $ob["NAME"],
                            "PAGE" => $arParams["SEF_FOLDER"].$ob["CODE"]."/"
                        );
                        $arCountries[] = $ob["CODE"];
                    }
                    $year_today = date("Y");
                    $first_year = $year_today - 6;
                    ?>
                    <div class="sidebar">
                        <?if(!empty($countries) && $flag):?>
                            <ul>
                                <?foreach ($countries as $county):?>
                                    <?if($county["ID"] == 0):?>
                                        <li>
                                            <a href="<?=$arParams["SEF_FOLDER"]?>"><?= $county["NAME"] ?></a>
                                        </li>
                                    <?else:?>
                                        <?if($county["CODE"] == $arResult["VARIABLES"]["ELEMENT_CODE"]):?>
                                            <li class="active">
                                                <?= $county["NAME"] ?>
                                            </li>
                                        <?else:?>
                                            <li>
                                                <a href="<?=$county["PAGE"] ?>"><?= $county["NAME"] ?></a>
                                            </li>
                                        <?endif?>

                                    <?endif?>
                                <?endforeach;?>
                            </ul>
                        <?endif?>
                        <ul>
                            <li <?if(!is_numeric($arResult["VARIABLES"]["ELEMENT_CODE"])):?>class="active"<?endif?>><?=GetMessage('ALL_YEARS')?></li>
                            <?for ($i = $year_today; $i >= $first_year; $i--):?>
                                <li<?if(is_numeric($arResult["VARIABLES"]["ELEMENT_CODE"]) && $i == $arResult["VARIABLES"]["ELEMENT_CODE"]):?> class="active" <?endif?>><a href="<?=$arParams["SEF_FOLDER"].$i."/" ?>"><?=$i ?></a></li>
                            <?endfor;?>
                        </ul>
                        <?
                        $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "AREA_FILE_SHOW" => "file",
                                "AREA_FILE_SUFFIX" => "inc",
                                "EDIT_TEMPLATE" => "",
                                "PATH" => SITE_DIR . "include/sidebarSubscription.php"
                            )
                        );
                        ?>
                        <?
                        $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "AREA_FILE_SHOW" => "file",
                                "AREA_FILE_SUFFIX" => "inc",
                                "EDIT_TEMPLATE" => "",
                                "PATH" => SITE_DIR . "include/sidebarWidgets.php"
                            )
                        );
                        ?>
                    </div>
                <?endif;?>
                <div class="content">
                    <? $GLOBALS[$arParams["FILTER_NAME"]] = array("PROPERTY_COUNTRY"=>$country);?>
                    <? $template = (isset($arParams["TEMPLATE_LIST"]) && !empty($arParams["TEMPLATE_LIST"])) ? $arParams["TEMPLATE_LIST"] : "";?>
                    <? $APPLICATION->IncludeComponent(
                        "travelsoft:travelsoft.news.list",
                        $template,
                        Array(
                            "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                            "NEWS_COUNT" => $arParams["NEWS_COUNT"],
                            "SORT_BY1" => !empty($_REQUEST["sort_by1"]) ? $_REQUEST["sort_by1"] : $arParams["SORT_BY1"],
                            "SORT_ORDER1" => !empty($_REQUEST["sort_order1"]) ? $_REQUEST["sort_order1"] : $arParams["SORT_ORDER1"],
                            "SORT_BY2" => $arParams["SORT_BY2"],
                            "SORT_ORDER2" => $arParams["SORT_ORDER2"],
                            "FIELD_CODE" => $arParams["LIST_FIELD_CODE"],
                            "PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
                            "DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["detail"],
                            "SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
                            "IBLOCK_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["news"],
                            "DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
                            "SET_TITLE" => $arParams["SET_TITLE"],
                            "SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
                            "MESSAGE_404" => $arParams["MESSAGE_404"],
                            "SET_STATUS_404" => $arParams["SET_STATUS_404"],
                            "SHOW_404" => $arParams["SHOW_404"],
                            "FILE_404" => $arParams["FILE_404"],
                            "INCLUDE_IBLOCK_INTO_CHAIN" => $arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
                            "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                            "CACHE_TIME" => $arParams["CACHE_TIME"],
                            "CACHE_FILTER" => $arParams["CACHE_FILTER"],
                            "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                            "DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
                            "DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
                            "PAGER_TITLE" => $arParams["PAGER_TITLE"],
                            "PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
                            "PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
                            "PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
                            "PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
                            "PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
                            "PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
                            "PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
                            "DISPLAY_DATE" => $arParams["DISPLAY_DATE"],
                            "DISPLAY_NAME" => "Y",
                            "DISPLAY_PICTURE" => $arParams["DISPLAY_PICTURE"],
                            "DISPLAY_PREVIEW_TEXT" => $arParams["DISPLAY_PREVIEW_TEXT"],
                            "PREVIEW_TRUNCATE_LEN" => $arParams["PREVIEW_TRUNCATE_LEN"],
                            "ACTIVE_DATE_FORMAT" => $arParams["LIST_ACTIVE_DATE_FORMAT"],
                            "USE_PERMISSIONS" => $arParams["USE_PERMISSIONS"],
                            "GROUP_PERMISSIONS" => $arParams["GROUP_PERMISSIONS"],
                            "FILTER_NAME" => $arParams["FILTER_NAME"],
                            "HIDE_LINK_WHEN_NO_DETAIL" => $arParams["HIDE_LINK_WHEN_NO_DETAIL"],
                            "CHECK_DATES" => $arParams["CHECK_DATES"],
                        ),
                        $component
                    ); ?>
                </div>
            <?else:?>
                <?if($APPLICATION->GetDirProperty("SHOW_RIGHT") == "Y"):?>
                    <?$arCountries = array();
                    $countries[0] = array(
                        "ID" => 0,
                        "NAME" => GetMessage('ALL_COUNTRIES'),
                        "CODE" => '',
                        "PAGE" => $arParams["SEF_FOLDER"]
                    );
                    $flag = false;
                    $res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => COUNTRIES_IBLOCK_ID, "ACTIVE" => "Y"), false, false, Array("IBLOCK_ID", "ID", "NAME", "CODE", "DETAIL_PAGE_URL"));
                    while ($ob = $res->GetNext()) {
                        $flag = true;
                        $countries[$ob["ID"]] = array(
                            "ID" => $ob["ID"],
                            "CODE" => $ob["CODE"],
                            "NAME" => $ob["NAME"],
                            "PAGE" => $arParams["SEF_FOLDER"].$ob["CODE"]."/"
                        );
                        $arCountries[] = $ob["CODE"];
                    }
                    $year_today = date("Y");
                    $first_year = $year_today - 6;
                    ?>
                    <div class="sidebar">
                        <?if(!empty($countries) && $flag):?>
                            <ul>
                                <?foreach ($countries as $county):?>
                                    <?if($county["ID"] == 0 && !in_array($arResult["VARIABLES"]["ELEMENT_CODE"],$arCountries)):?>
                                        <li class="active">
                                            <?= $county["NAME"] ?>
                                        </li>
                                    <?else:?>
                                        <li<?if($county["CODE"] == $arResult["VARIABLES"]["ELEMENT_CODE"]):?> class="active" <?endif?>>
                                            <a href="<?=$county["PAGE"] ?>"><?= $county["NAME"] ?></a>
                                        </li>
                                    <?endif?>
                                <?endforeach;?>
                            </ul>
                        <?endif?>
                        <ul>
                            <li <?if(!is_numeric($arResult["VARIABLES"]["ELEMENT_CODE"])):?>class="active"<?endif?>><?=GetMessage('ALL_YEARS')?></li>
                            <?for ($i = $year_today; $i >= $first_year; $i--):?>
                                <li<?if(is_numeric($arResult["VARIABLES"]["ELEMENT_CODE"]) && $i == $arResult["VARIABLES"]["ELEMENT_CODE"]):?> class="active" <?endif?>><a href="<?=$arParams["SEF_FOLDER"].$i."/" ?>"><?=$i ?></a></li>
                            <?endfor;?>
                        </ul>
                        <?
                        $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "AREA_FILE_SHOW" => "file",
                                "AREA_FILE_SUFFIX" => "inc",
                                "EDIT_TEMPLATE" => "",
                                "PATH" => SITE_DIR . "include/sidebarSubscription.php"
                            )
                        );
                        ?>
                        <?
                        $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "AREA_FILE_SHOW" => "file",
                                "AREA_FILE_SUFFIX" => "inc",
                                "EDIT_TEMPLATE" => "",
                                "PATH" => SITE_DIR . "include/sidebarWidgets.php"
                            )
                        );
                        ?>
                    </div>
                <?endif;?>
                <div class="content">
                    <p><?=GetMessage('NOT_ELEMENTS')?></p>
                </div>
            <?endif?>
        <?endif?>
    <?endif?>
</div>
<?endif;?>
