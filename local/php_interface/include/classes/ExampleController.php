<?php

namespace travelsoft;

\Bitrix\Main\Loader::includeModule('iblock');

/**
 * @author juliya.sharlova
 *
 * Контроллер
 */

class ExampleController
{

    /* Устанавливать вручную!!!
    * Инфоблоки стран и городов
    */
    static public $_iblock_countries = 7;
    static public $_iblock_cities = 8;

    /**
     * Country Action
     */
    public static function countryAction($url, $country)
    {

        $result = array();

        $country_db = \CIBlockElement::GetList(Array(), Array("IBLOCK_ID"=>self::$_iblock_countries, "CODE"=>$country, "ACTIVE"=>"Y"), false, false, Array("ID","NAME","IBLOCK_ID"));
        if($country_result = $country_db->Fetch())
        {

            $result = array(
                "ELEMENT_CODE" => $country,
                "ELEMENT_ID" => $country_result["ID"],
                "FILE" => 'country',
                "URL" => $url,
            );

        }

        return $result;

    }

    /**
     * Resorts list Action
     */
    public static function resortsAction($url, $country)
    {

        $result = array();

        $country_db = \CIBlockElement::GetList(Array(), Array("IBLOCK_ID"=>self::$_iblock_countries, "CODE"=>$country, "ACTIVE"=>"Y"), false, false, Array("ID","NAME","IBLOCK_ID","DETAIL_PAGE_URL", "PROPERTY_CN_NAME_CHEGO"));
        if($country_result = $country_db->Fetch())
        {

            $page_temp = $country_result["DETAIL_PAGE_URL"];
            $page_new = str_replace("#SITE_DIR#/",$country_result["LANG_DIR"],$page_temp);
            $page_new = str_replace('#ELEMENT_CODE#',$country,$page_new);

            $result = array(
                "ELEMENT_CODE" => $country,
                "ELEMENT_ID" => $country_result["ID"],
                "ELEMENT_NAME" => $country_result["NAME"],
                "ELEMENT_NAME_CHEGO" => $country_result["PROPERTY_CN_NAME_CHEGO_VALUE"],
                "ELEMENT_PAGE" => $page_new,
                "FILE" => 'resorts',
                "URL" => $url,
                "LANG_LIST" => array("ru"=>"Курорты", "en"=>"Resorts")
            );

        }

        return $result;

    }

    /**
     * Resort detail Action
     */
    public static function resortAction($url, $country, $city)
    {

        $result = array();

        $country_db = \CIBlockElement::GetList(Array(), Array("IBLOCK_ID"=>self::$_iblock_countries, "CODE"=>$country, "ACTIVE"=>"Y"), false, false, Array("ID","NAME","IBLOCK_ID","DETAIL_PAGE_URL"))->Fetch();

        $filter = Array("CODE"=>$city, "ACTIVE"=>"Y");
        if(!empty($country_db["ID"])){
            $filter = array_merge($filter, array("PROPERTY_COUNTRY"=>$country_db["ID"]));
        }

        $city_db = \CIBlockElement::GetList(Array(), $filter, false, false, Array("ID","NAME","IBLOCK_ID","DETAIL_PAGE_URL","PROPERTY_COUNTRY"));
        if($city_db->SelectedRowsCount() >= 1) {
            if ($city_result = $city_db->Fetch()) {

                if($city_result["IBLOCK_ID"] == CITIES_IBLOCK_ID) {

                    $page_temp = $city_result["DETAIL_PAGE_URL"];
                    $page_new = str_replace("#SITE_DIR#/", $city_result["LANG_DIR"], $page_temp);
                    $page_new = str_replace('#SECTION_CODE#', $country, $page_new);
                    $page_new = str_replace('#ELEMENT_CODE#', $city, $page_new);

                    $result = array(
                        "ELEMENT_CODE" => $city,
                        "ELEMENT_ID" => $city_result["ID"],
                        "ELEMENT_NAME" => $city_result["NAME"],
                        "ELEMENT_PAGE" => $page_new,
                        "SECTION_CODE" => $country,
                        "SECTION_ID" => $country_db["ID"],
                        "FILE" => 'resort',
                        "URL" => $url,
                        "LANG_LIST" => array("ru" => "Курорты", "en" => "Resorts")
                    );

                } elseif($city_result["IBLOCK_ID"] == DETAIL_PAGES_ID_IBLOCK) {

                    $result = array(
                        "ELEMENT_CODE" => $city,
                        "ELEMENT_ID" => $city_result["ID"],
                        "ELEMENT_NAME" => $city_result["NAME"],
                        //"ELEMENT_PAGE" => $page_new,
                        "SECTION_CODE" => $country,
                        "FILE" => 'page',
                        "URL" => $url,
                        "LANG_LIST" => array("ru" => "Тип тура", "en" => "Tours Type")
                    );

                }

                if (!empty($city_result["PROPERTY_COUNTRY_VALUE"])) {
                    $country_db = \CIBlockElement::GetByID($city_result["PROPERTY_COUNTRY_VALUE"]);
                    if ($ar_res = $country_db->GetNext()) {
                        $result["SECTION_ID"] = $ar_res['ID'];
                        $result["SECTION_NAME"] = $ar_res['NAME'];
                        $result["SECTION_PAGE"] = $ar_res['DETAIL_PAGE_URL'];
                    }
                }

            }
        }

        return $result;

    }

    /**
     * Visas list Action
     */
    public static function visaAction($url, $country)
    {

        $result = array();
        $ar_country = array();

        $country_db = \CIBlockElement::GetList(Array(), Array("CODE"=>$country, "ACTIVE"=>"Y"), false, false, Array("ID","IBLOCK_ID","NAME","DETAIL_PAGE_URL"));
        if($country_result = $country_db->Fetch())
        {

            $page_temp = $country_result["DETAIL_PAGE_URL"];
            $page_new = str_replace("#SITE_DIR#/",$country_result["LANG_DIR"],$page_temp);
            $page_new = str_replace('#ELEMENT_CODE#',$country,$page_new);

            $ar_country = array(
                "ID" => $country_result["ID"],
                "NAME" => $country_result["NAME"],
                "PAGE_URL" => $page_new
            );
        }

        if($ar_country["ID"] > 0) {

            $visa_db = \CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => VISAS_IBLOCK_ID, "PROPERTY_COUNTRY" => $ar_country["ID"], "ACTIVE" => "Y"), false, false, Array("ID", "NAME", "IBLOCK_ID"));
            if ($visas_result = $visa_db->Fetch()) {

                $result = array(
                    "ELEMENT_ID" => $visas_result["ID"],
                    "SECTION_ID" => $ar_country["ID"],
                    "SECTION_NAME" => $ar_country["NAME"],
                    "SECTION_CODE" => $country,
                    "SECTION_PAGE" => $ar_country["PAGE_URL"],
                    "FILE" => 'visa',
                    "URL" => $url,
                    "LANG_LIST" => array("ru" => "Виза", "en" => "Visa")
                );

            }
        }

        return $result;

    }

    /**
     * Toutist info Action
     */
    public static function infoAction($url, $country)
    {

        $result = array();

        $country_db = \CIBlockElement::GetList(Array(), Array("IBLOCK_ID"=>self::$_iblock_countries, "CODE"=>$country, "ACTIVE"=>"Y"), false, false, Array("ID","NAME","IBLOCK_ID","PROPERTY_CN_NAME_KUDA"));
        if($country_result = $country_db->Fetch())
        {

            $result = array(
                "ELEMENT_CODE" => $country,
                "ELEMENT_ID" => $country_result["ID"],
                "ELEMENT_NAME_KUDA" => $country_result["PROPERTY_CN_NAME_KUDA_VALUE"],
                "FILE" => 'tourist-guide',
                "URL" => $url,
            );

        }

        return $result;

    }

    /**
     * Hotels list Action
     */
    public static function hotelsAction($url, $country)
    {
        
        $result = array();

        $country_db = \CIBlockElement::GetList(Array(), Array("IBLOCK_ID"=>self::$_iblock_countries, "CODE"=>$country, "ACTIVE"=>"Y"), false, false, Array("ID","NAME","IBLOCK_ID","DETAIL_PAGE_URL"));
        if($country_result = $country_db->Fetch())
        {

            $page_temp = $country_result["DETAIL_PAGE_URL"];
            $page_new = str_replace("#SITE_DIR#/",$country_result["LANG_DIR"],$page_temp);
            $page_new = str_replace('#ELEMENT_CODE#',$country,$page_new);

            $result = array(
                "ELEMENT_CODE" => $country,
                "ELEMENT_ID" => $country_result["ID"],
                "ELEMENT_NAME" => $country_result["NAME"],
                "ELEMENT_PAGE" => $page_new,
                "FILE" => 'hotels',
                "URL" => $url,
                "LANG_LIST" => array("ru"=>"Отели", "en"=>"Hotels")
            );

        }

        return $result;

    }

    /**
     * Tours list Action
     */
    public static function toursAction($url, $country)
    {

        $result = array();

        $country_db = \CIBlockElement::GetList(Array(), Array("IBLOCK_ID"=>self::$_iblock_countries, "CODE"=>$country, "ACTIVE"=>"Y"), false, false, Array("ID","NAME","IBLOCK_ID","DETAIL_PAGE_URL"));
        if($country_result = $country_db->Fetch())
        {

            $page_temp = $country_result["DETAIL_PAGE_URL"];
            $page_new = str_replace("#SITE_DIR#/",$country_result["LANG_DIR"],$page_temp);
            $page_new = str_replace('#ELEMENT_CODE#',$country,$page_new);

            $result = array(
                "ELEMENT_CODE" => $country,
                "ELEMENT_ID" => $country_result["ID"],
                "ELEMENT_NAME" => $country_result["NAME"],
                "ELEMENT_PAGE" => $page_new,
                "FILE" => 'hotels',
                "URL" => $url,
                "LANG_LIST" => array("ru"=>"Туры", "en"=>"Tours")
            );

        }

        return $result;

    }

}