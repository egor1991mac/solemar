<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$this->addExternalJS(SITE_TEMPLATE_PATH . "/js/MapAdapter/MapAdapter.js");

/*\Bitrix\Main\Loader::includeModule('travelsoft.currency');
$converter = \travelsoft\currency\factory\Converter::getInstance();*/
$htmlMapID = 'map';
?>
<? $properties = $arResult["DISPLAY_PROPERTIES"];?>
<?$upItaly = ucwords($arResult["CODE"]);
$arImgFlag = getSrc($arResult["DISPLAY_PROPERTIES"]["FLAG"]["VALUE"], array("width"=>24,"height"=>24), SITE_TEMPLATE_PATH.'/flags/'.$upItaly.'.png', 1, false);
$h1 = !empty($properties["CN_NAME_CHEGO"]["VALUE"]) ? vowelLetter($properties["CN_NAME_CHEGO"]["VALUE"],false) : $arResult["NAME"];?>

<div class="country-menu">
    <div class="container">
        <p class="country-title" style="background-image:url(<?=$arImgFlag[0]?>)"><?=$arResult["NAME"]?></p>
        <p class="country-title-mobile" style="background-image:url(<?=$arImgFlag[0]?>)">
            <a href="#toggle" id="country-menu-button" class="flashlink"><?=$arResult["NAME"]?></a>
        </p>
        <ul class="tabs">
            <?if($arResult["CITIES"]):?>
                <li><a href="<?=$arParams["DETAIL_PAGE"]?>resorts/"><span><?=GetMessage('RESORTS')?></span></a></li>
            <?endif?>
            <?if($arResult["HOTELS"]):?>
                <li><a href="<?=$arParams["DETAIL_PAGE"]?>hotels/"><span><?=GetMessage('HOTELS')?></span></a></li>
            <?endif?>
            <!--<li><a href="#about"><span><?/*=GetMessage('DESC')*/?></span></a></li>-->
            <?if($arResult["VISAS"]):?>
                <li><a href="<?=$arParams["DETAIL_PAGE"]?>visa/"><span><?=GetMessage('VISAS')?></span></a></li>
            <?endif?>
            <?if(!empty($properties["TOURIST_GUIDE"]["VALUE"]["TEXT"])):?>
                <li><a href="<?=$arParams["DETAIL_PAGE"]?>tourist-info/"><span><?=GetMessage('TOURIST_GUIDE')?></span></a></li>
            <?endif?>

            <!--<li><a href="https://online.solemare.by/online/Extra/QuotedDynamic.aspx" class="important" onclick="return !window.open(this.href)"><span>Подбор тура online</span></a></li>-->

        </ul>
    </div>
</div>
<!--<div class="fast-bar">
    <div class="container">
        <div id="curr">
            <ul>
                <li class="text">Цены в:</li>
                <li class="byn"><a href="https://solemare.by/tours/italy/about#byn" class="flashlink"><span>BYN</span></a></li>
                <li class="usd"><a href="https://solemare.by/tours/italy/about#usd" class="flashlink"><span>USD</span></a></li>
                <li class="eur"><a href="https://solemare.by/tours/italy/about#eur" class="flashlink"><span>EUR</span></a></li>
                <li class="gbp"><a href="https://solemare.by/tours/italy/about#gbp" class="flashlink"><span>GBP</span></a></li>
            </ul>
        </div>
        <ul>
        </ul>
    </div>
</div>-->
<div class="container about-country col2 ts-mt-4">
    <h1 class="nopadding"><?=$h1?></h1>
    <div class="sidebar">
        <?if(isset($arResult["PAGES_INFO"]) && !empty($arResult["PAGES_INFO"])):?>
            <ul>
                <?foreach ($arResult["PAGES_INFO"] as $page):?>
                    <?$page_name = !empty($page["BUTTON_NAME"]) ? $page["BUTTON_NAME"] : $page["NAME"];?>
                    <li class="cat-item cat-item-1 nav-tab-item">
                        <a href="<?=$arParams["DETAIL_PAGE"]?><?=$page["CODE"].'/'?>"><?=$page_name?></a>
                    </li>
                <?endforeach;?>
            </ul>
        <?endif?>
    </div>
    <?if($properties["PICTURES"]["VALUE"]):?>
        <?=$arParams["SLIDER"]?>
    <?endif?>
    <div class="content">


        <?if(!empty($properties["DESC"]["VALUE"]["TEXT"])):?>
            <h2 class="title"><?=$properties["DESC"]["NAME"]?></h2>
            <?=$properties["DESC"]["DISPLAY_VALUE"]?>
        <?endif?>
        <?if(!empty($properties["VIDEO"]["VALUE"])):?>
            <iframe width="1040" height="350" src="https://www.youtube.com/embed/<?=$properties["VIDEO"]["VALUE"]?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
        <?endif?>
        <?if(!empty($properties["LANGUAGE_TEXT"]["VALUE"]["TEXT"])):?>
            <h2 class="title"><?=$properties["LANGUAGE_TEXT"]["NAME"]?></h2>
            <?=$properties["LANGUAGE_TEXT"]["DISPLAY_VALUE"]?>
        <?endif?>
        <?if(!empty($properties["CLIMATE_TEXT"]["VALUE"]["TEXT"])):?>
            <h2 class="title"><?=$properties["CLIMATE_TEXT"]["NAME"]?></h2>
            <?=$properties["CLIMATE_TEXT"]["DISPLAY_VALUE"]?>
        <?endif?>
        <?if(!empty($properties["GEOGRAFY"]["VALUE"]["TEXT"])):?>
            <h2 class="title"><?=$properties["GEOGRAFY"]["NAME"]?></h2>
            <?=$properties["GEOGRAFY"]["DISPLAY_VALUE"]?>
        <?endif?>
        <?if(!empty($properties["POPULATION"]["VALUE"]["TEXT"])):?>
            <h2 class="title"><?=$properties["POPULATION"]["NAME"]?></h2>
            <?=$properties["POPULATION"]["DISPLAY_VALUE"]?>
        <?endif?>
        <?if(!empty($properties["TOURIST_SEASON"]["VALUE"]["TEXT"])):?>
            <h2 class="title"><?=$properties["TOURIST_SEASON"]["NAME"]?></h2>
            <?=$properties["TOURIST_SEASON"]["DISPLAY_VALUE"]?>
        <?endif?>
        <?if(!empty($properties["HOLIDAYS"]["VALUE"]["TEXT"])):?>
            <h2 class="title"><?=$properties["HOLIDAYS"]["NAME"]?></h2>
            <?=$properties["HOLIDAYS"]["DISPLAY_VALUE"]?>
        <?endif?>
        <?if(!empty($properties["CONNECTIVITY"]["VALUE"]["TEXT"])):?>
            <h2 class="title"><?=$properties["CONNECTIVITY"]["NAME"]?></h2>
            <?=$properties["CONNECTIVITY"]["DISPLAY_VALUE"]?>
        <?endif?>
        <?if(!empty($properties["IMPORTANT"]["VALUE"]["TEXT"])):?>
            <h2 class="title"><?=$properties["IMPORTANT"]["NAME"]?></h2>
            <?=$properties["IMPORTANT"]["DISPLAY_VALUE"]?>
        <?endif?>
        <?if($arResult["DIRECTIONS"]):?>
            <!--<h4><?/*=GetMessage('SEARCH_FORM_TITLE')*/?></h4>-->
            <?=$arParams["SEARCH_FORM_STS"]?>
            <?=$arParams["PAGE_SEARCH_RESULT_STS"]?>
        <?endif?>
    </div>
    <?=$arParams["TOURS_SLIDER_FOR_COUNTRY"]?>
    <?=$arParams["REVIEWS_FOR_COUNTRY"]?>
</div>

<?=$arParams["COUNTRIES_LIST"]?>