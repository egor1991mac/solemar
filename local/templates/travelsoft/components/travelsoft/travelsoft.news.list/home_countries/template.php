<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if(!empty($arResult["ITEMS"])):?>

    <div class="countries mainpage ts-pb-0">
        <div class="container">
            <div class="ts-wrap">
                <div class="ts-row">

                <?foreach($arResult["ITEMS"] as $arItem):?>
                    <?
                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                    ?>

                    <?$upCountryName = ucwords($arItem["CODE"]);
                    $arImg = getSrc($arItem["DISPLAY_PROPERTIES"]["FLAG"]["VALUE"], array("width"=>24,"height"=>24), SITE_TEMPLATE_PATH.'/flags/'.$upCountryName.'.png', 1, false);?>
                    <div class="ts-col-24 ts-col-sm-12 ts-col-md-6  ts-col-lg-4 ts-py-1" title="<?=$arItem["NAME"]?>" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                        <?if(!empty($arItem["DISPLAY_PROPERTIES"]["RECOMMEND"]["VALUE"]) && $arItem["DISPLAY_PROPERTIES"]["RECOMMEND"]["VALUE"] == "Y"):?>

                                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="ts-d-flex">
                                    <div style="background-image:url(<?=$arImg[0]?>);  width:24px; height: 24px" class="ts-mr-1">></div><?=$arItem["NAME"]?>
                                </a>
                        <?else:?>
                            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="ts-d-flex">
                                <div style="background-image:url(<?=$arImg[0]?>); width:24px; height: 24px" class="ts-mr-1"></div><?=$arItem["NAME"]?>
                            </a>
                        <?endif;?>
                    </div>
                <?endforeach;?>
                </div>
            </div>
            <div class="ts-row ts-justify-content__center ts-mt-1">
            <a href="/countries/" data="see_more">

                <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 24 24"><path d="M7.41 8.59L12 13.17l4.59-4.58L18 10l-6 6-6-6 1.41-1.41z"/><path fill="none" d="M0 0h24v24H0V0z"/></svg>

            </div>
        </div>
    </div>


<?endif?>

<script>
    var height = document.querySelector('.countries.mainpage .container .ts-wrap .ts-row').offsetHeight;
    document.querySelector('a[data="see_more"]').onclick = function(e) {
        e.preventDefault();
        this.classList.toggle('active');
        console.log(this);
        if (!this.closest('.container').children[0].classList.contains('active')) {
            this.closest('.container').children[0].classList.add('active');
            this.closest('.container').children[0].style.height = height + 'px';
        } else {
            this.closest('.container').children[0].classList.remove('active');
            this.closest('.container').children[0].style.height = '80px';
        }
    }


</script>

<style>
    .countries.mainpage .container .ts-wrap {
        height: 80px;
        transition: height 0.5s;
        overflow: hidden;
    }
    a[data="see_more"]{
        transform: rotate(0deg);
        transition: transform 0.5s;
    }
    a[data="see_more"].active{
        transform: rotate(180deg);
        transition: transform 0.5s;
    }


</style>