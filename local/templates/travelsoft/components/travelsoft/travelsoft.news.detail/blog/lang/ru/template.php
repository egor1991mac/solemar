<?
$MESS["RESORTS"] = "Курорты";
$MESS["HOTELS"] = "Отели";
$MESS["VISAS"] = "Виза";
$MESS["VIDEO"] = "Видео";
$MESS["DESC"] = "О стране";
$MESS["CODE_TOWN"] = "Города и курорты";
$MESS["TOURS"] = "Лучшие туры";
$MESS["SEARCH_FORM_TITLE"] = "Подбор туров";
$MESS["TOURIST_GUIDE"] = "Памятка туристу";
$MESS["MAP"] = "Отель на карте";
$MESS["TOURS_FOR_RESORT"] = "Туры в #RESORT_NAME#";
$MESS["HOTELS_FOR_RESORT"] = "Отели в #RESORT_NAME#";
$MESS["BLOCK_PRICE"] = "Проживание в отеле ";
$MESS["BLOCK_PRICE_FROM"] = "от ";
$MESS["BLOCK_PRICE_NIGHT"] = " / ночь ";
$MESS["BOOKING"] = "Забронировать!";
$MESS["RATING_FROM_BOOKING"] = "Рейтинг <span class='booking'>Booking.com</span> ";
?>