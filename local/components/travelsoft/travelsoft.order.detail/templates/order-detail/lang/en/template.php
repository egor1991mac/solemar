<?php
$MESS["UNKNOWN_ERROR"] = "An error occurred while trying to get information upon request. Refer to the administration of the site";
$MESS["UNKNOWN_ORDER_ID"] = "Order number unknown";
$MESS["ORDER"] = "Order";
$MESS["FIO"] = "Full Name";
$MESS["PERSONS"] = "Number of persons";
$MESS["STATUS"] = "Status";
$MESS["DURATIONS"] = "Duration | days";
$MESS["CREATION"] = "Date of creation";
$MESS["ARRIVAL"] = "Arrival date";
$MESS["PRICE"] = "Price";
$MESS["DISCOUNT"] = "A discount";
$MESS["DOCUMENTATION"] = "Documentation";
$MESS["PAID"] = "Paid";
$MESS["TOPAY"] = "To pay";
$MESS["SERVICES"] = "Services";
$MESS["NAME"] = "Name";
$MESS["SERVICEDURATIONS"] = "Duration | days";
$MESS["SERVICESTATUS"] = "Status";
$MESS["SERVICEPRICE"] = "Cost of";
$MESS["PAYMENTOPTIONS"] = "Payment Methods";
$MESS["BANKCARD"] = "Bank card";
$MESS["ERIP"] = "Calculation (ERIP)";
$MESS["CASH"] = "Cash";
$MESS["BANKPAYMENT"] = "Bank payment";
$MESS["PAYMENTOPTIONSTEXT"] = "#PAY_FORM#<p>Оплата банковской картой происходит на странице банка через авторизационный сервер ASSIST</p>
<p class='pay'><img src='/local/templates/travelsoft/images/pay.png'></p>
			   <h5>БЕЗОПАСНОСТЬ</h5>
				<ul>
					<li>Соответствие стандарту PCI DSS</li>
					<li>Использование протокола SSL для передачи данных через интернет</li>
					<li>Сертификат COMODO</li> 
					<li>3-D Secure для VISA/Master Card</li>
					<li>Лицензионные продукты от Arcot (UK)</li>
					<li>Уникальная система фрод-мониторинга ASSIST ANTIFRAUD на основе нейронных сетей</li>
			   </ul>";
$MESS["ERIPTEXT"] = "#PAY_FORM#<p>СSystem «System» (or «Raschet») - Automated Information System Uniform settlement and information space (AIS SSIS) established by the National Bank of the Republic of Belarus for simplification of organization of reception of payments from physical and legal persons (system website - www.raschet.by, telephone contact center - 141). 
    <b>Available for citizens who are on the territory of the Republic of Belarus</b>.</p>
<p>To make a payment, you need:</p>
<ol>
<li>Click the button «Pay», after which you will go to the page of the payment system \"ASSIST\", where your order number will be generated for payment in the ERIP system</li>
<li>You can make payments using cash, electronic money and bank payment cards at the points of bank service (cash departments of banks, ATMs, information kiosks) that provide services for receiving payments, as well as through remote
banking tools (Internet banking, mobile banking)</li>
<li>Select the item <b>\"CALCULATION\" (ERIP) - Tourism and Recreation - Travel agencies, tour operators - \"CENTRKURORT - Tourist services at VETLIVA\"</b></li>
<li>To pay for the \"Goods / Services\" enter the Order Number, which consists of 8 digits.<br>
<b>IMPORTANT:</b> The order number in the notification letter does not match the order number in the \"Calculation\" system (ERIP). The 8-digit number required for 
    payment is generated on the page of the payment system \"ASSIST\" (see point 1)</li>
<li>Upon completion of payment, a notification of payment (about transfer of money to the payee's account) comes to the e-mail address specified by you. In case of successful payment, the order is considered to be issued.</li>
</ol>
";
$MESS["CASHTEXT"] = "<p>Оплата тура в офисах продаж компании ЦЕНТРКУРОРТ</p>
				<a href='http://www.otpusk.by/kontakty/' class='btn'>Список офисов</a>";
$MESS["BANKPAYMENTTEXT"] = "<p>Оплата тура по безналичному расчету по выставленному счету</p>";
$MESS["CANCEL_FAIL"] = "Fail. Call administrator";
$MESS["PAY_BTN_TITLE"] = "TO PAY";
$MESS["NOT_ALLOW_TO_PAY"] = "Payments will be available after confirmation of the order manager";
$MESS["DATE_FROM"] = "Date from";
$MESS["DATE_TO"] = "Date to";

$MESS['CANCEL'] = "Cancellation";

$MESS['FORSPOTPAYMENTTEXT'] = '<p><b>To cofirm «payment for upon arrival», you need to click the button «Pay».</b></p>'
        . '<p>After that, a notification letter confirming «payment for upon arrival» will be sent to your e-mail indicated at the time of booking, indicating the amount to be paid. The letter contains all the essential
details of your order, as well as a booking sheet, which must be printed and provided at the time of the service. The booking sheet is also available for download in your personal account on the VETLIVA portat.</p>';
$MESS['FORSPOTPAYMENT'] = 'Payment on the spot';

$MESS['FORSPOTPAYMENTCHOOSE'] = 'You have chosen to pay on the spot';

$MESS['IN_CANCELLATION_PROCESS'] = "Your cancellation request has been accepted. After confirming the cancellation, you will be notified by e-mail.";


$MESS['PAY_BY_INVOICE'] = "Payment against invoice in bank";
$MESS['PAY_BY_INVOICE_BUTTON'] = "Download Invoice";
$MESS['PAY_BY_INVOICE_TEXT'] = "You can download the Invoice, print it out and submit it to the bank for payment.";