<?php

namespace travelsoft\booking\sts\api;


class MasterTour
{

    public static function sendRequest(array $request): string {

        /*dm(json_encode(
            array(
                "jsonrpc" => "2.0",
                "method"  => $request["method"],
                "params"  =>  $request["params"],
                "id" => rand()
            )
        ),false,true,false);*/

        return file_get_contents(
            $request["url"],
            false,
            stream_context_create(
                array(
                    'ssl' => array("verify_peer" => false),
                    'http' => array(
                        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                        'method'  => 'POST',
                        'content' => json_encode(
                            array(
                                "jsonrpc" => "2.0",
                                "method"  => $request["method"],
                                "params"  =>  $request["params"],
                                "id" => rand()
                            )
                        ),
                    ),
                )
            )
        );

    }

    /**
     * Регистрация пользователя в ПК-МастерТур
     * @param array $parameters
     * @return ** Json String ** {"token": "значение"} или ошибка
     */
    public static function createUpdateMTUser (array $parameters) {

        $response = self::sendRequest(array(
            "url" => $parameters["address"],
            "method" => "CreateNewUser",
            "params" => array(
                "user_info" => $parameters["user_info"],
                "isAgent" => $parameters["is_agent"] ? 1 : 0,
                "update" => $parameters["update"] ? 1 : 0,
            )
        ));

        return $response;

    }

    /**
     * Метод производит авторизацию пользователя с последующей выдачи ключа
     * (токена) для его дальнейшего использования в методах сервиса ПК-МастерТур.
     * @param array $parameters
     * @return json
     */
    public static function authorizeMtUser (array $parameters) {

        $response = self::sendRequest(array(
            "url" => $parameters["address"],
            "method" => "Connect",
            "params" => array(
                "login" => $parameters["login"],
                "password" => $parameters["password"]
            )
        ));

        return $response;

    }

    public static function logoutUser (array $parameters) {

        self::sendRequest(array(
            "url" => $parameters["address"],
            "method" => "Logout",
            "params" => array(
                "token" => $parameters["token"]
            )
        ));

    }

    /**
     * Создание путёвки в ПК-МастерТур (бронирование)
     * @param array $parameters
     * @return json
     */
    public static function makeBooking (array $parameters) {

        $response = self::sendRequest(array(
            "url" => $parameters["address"],
            "method" => "CreateNewDogovor",
            "params" => $parameters["booking_data"]
        ));

        return $response;

    }

    /**
     * Создание произвольной путёвки в ПК-МастерТур (бронирование)
     * @param array $parameters
     * @return json
     */
    public static function makeArbitraryBooking (array $parameters) {

        $response = self::sendRequest(array(
            "url" => $parameters["address"],
            "method" => "CreateNewArbitraryDogovor",
            "params" => $parameters["booking_data"]
        ));

        return $response;

    }

    /**
     * получение списка заказов
     * @param array $parameters
     * @return json
     */
    public static function getOrderList (array $parameters) {

        $response = self::sendRequest(array(
            "url" => $parameters["address"],
            "method" => "GetDogovorsByUser",
            "params" => $parameters["parameters"]
        ));

        return $response;

    }

    /**
     * получение информации по заказу
     * @param array $parameters
     * @return json
     */
    public static function getOrderDetail (array $parameters) {

        $response = self::sendRequest(array(
            "url" => $parameters["address"],
            "method" => "GetDogovorInfo",
            "params" => $parameters["parameters"]
        ));

        return $response;

    }

    /**
     * получение информации по услуге
     * @param array $parameters
     * @return json
     */
    public static function getServiceDetail (array $parameters) {

        $response = self::sendRequest(array(
            "url" => $parameters["address"],
            "method" => "GetInfoServices",
            "params" => $parameters["parameters"]
        ));

        return $response;

    }


    public static function orderToCancel (array $parameters) {

        $response = self::sendRequest(array(
            "url" => $parameters["address"],
            "method" => "AnnulateDogovor",
            "params" => $parameters["parameters"]
        ));

        return $response;

    }

    /**
     * Получение списка сообщений пользователя по бронированию из ПК-МастерТур
     * @param array $parameters
     * @return json
     */
    /*public static function getBookingMessages (array $parameters) {
        return self::sendRequest(array(
            "url" => $parameters["address"],
            "method" => "get_dogovor_messages",
            "params" => $parameters["params"],
        ));
    }*/

    /**
     * Отправка сообщения пользователя по бронированию в ПК-МастерТур
     * @param array $parameters
     * @return json
     */
    /*public static function sendBookingMessage (array $parameters) {
        return self::sendRequest(array(
            "url" => $parameters["address"],
            "method" => "send_message",
            "params" => $parameters["params"],
        ));
    }*/

}