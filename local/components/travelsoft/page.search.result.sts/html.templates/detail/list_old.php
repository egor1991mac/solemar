<div class="catalog-tour__list-elem">
    <div class="catalog-tour__list-elem-wrap">
        <div class="catalog-tour__list-elem-box item_fly ts-col-5">
            <div class="catalog-tour__list-elem-box-key from-icon">
                <div class="catalog-tour__list-elem-icon"><img src="<?=SITE_TEMPLATE_PATH?>/img/calendar_icon_grey.png" alt="date"></div>
                <div class="catalog-tour__list-elem-date"> #dateFrom# - #dateTo#</div>
            </div>
            <div class="catalog-tour__list-elem-box-value">#night#</div>
        </div>
        <div class="catalog-tour__list-elem-box item_meals ts-col-5">
            <div class="catalog-tour__list-elem-box-key from-icon">
                <div class="catalog-tour__list-elem-icon"><img src="<?=SITE_TEMPLATE_PATH?>/img/food2_icon_grey.png"></div>
                <div class="catalog-tour__list-elem-date"> #food#</div>
            </div>
            <div class="catalog-tour__list-elem-box-value">#foodFullName#</div>
        </div>
        <div class="catalog-tour__list-elem-box item_room">
            <div class="catalog-tour__list-elem-box-key from-icon" title="#placement#">
                <div class="catalog-tour__list-elem-date">#placement#</div>
                <div class="catalog-tour__list-elem-icon">
                    <span class="icon_mans from-icon"> -
                        <div class="catalog-tour__list-elem-icon">
                            <img src="<?=SITE_TEMPLATE_PATH?>/img/adult2_icon_grey.png" title="<?=GetMessage('ADULTS')?>">
                        </div>
                        <div class="catalog-tour__list-elem-date">#adults#,</div>
                        <div class="catalog-tour__list-elem-icon">
                            <img src="<?=SITE_TEMPLATE_PATH?>/img/child2_icon_grey.png" title="<?=GetMessage('CHILDREN')?>">
                        </div>
                        <div class="catalog-tour__list-elem-date">#children#</div>
                    </span>
                </div>
            </div>
            <div class="catalog-tour__list-elem-box-value" title="#roomType# #roomCatType#">#roomType# #roomCatType#</div>
        </div>
        <div class="catalog-tour__list-elem-box item_price">
            <div class="catalog-tour__list-elem-box-key color-blue">#price#</div>
            <div class="catalog-tour__list-elem-box-value">#prices# #defCurrency#</div>
            <!--<div class="catalog-tour__list-elem-box-other"><?/*=GetMessage('FOR_PRICE')*/?></div>-->
        </div>
        <form action="#bookingUrl#" method="post">
            <?=bitrix_sessid_post()?>
            <input type="hidden" name="make_booking[operatorId]" value="#operatorId#">
            <input type="hidden" name="make_booking[priceKey]" value="#priceKey#">
            <input type="hidden" name="make_booking[hotelIdBx]" value="#hotelIdBx#">
            <input type="hidden" name="make_booking[hotelName]" value="#hotelName#">
            <input type="hidden" name="make_booking[hotelStars]" value="#hotelStars#">
            <input type="hidden" name="make_booking[hotelDetailUrl]" value="#hotelDetailUrl#">
            <input type="hidden" name="make_booking[country]" value="#country#">
            <input type="hidden" name="make_booking[city]" value="#city#">
            <input type="hidden" name="make_booking[dateBegin]" value="#dateFrom#">
            <input type="hidden" name="make_booking[dateEnd]" value="#dateTo#">
            <input type="hidden" name="make_booking[night]" value="#night#">
            <input type="hidden" name="make_booking[adults]" value="#adults#">
            <input type="hidden" name="make_booking[children]" value="#children#">
            <input type="hidden" name="make_booking[food]" value="#food#">
            <input type="hidden" name="make_booking[placement]" value="#placement#">
            <input type="hidden" name="make_booking[roomType]" value="#roomType#">
            <input type="hidden" name="make_booking[roomCatType]" value="#roomCatType#">
            <input type="hidden" name="make_booking[price]" value="#price#">
            <input type="hidden" name="make_booking[priceNotCurrency]" value="#priceNotCurrency#">
            <input type="hidden" name="make_booking[currency]" value="#currency#">
            <button type="submit" value="add2cart" class="catalog-tour__list-elem-btn c-button bg-dr-blue-2 hv-dr-blue-2-o" ><?=GetMessage('ORDER')?></button>
        </form>
    </div>
</div>