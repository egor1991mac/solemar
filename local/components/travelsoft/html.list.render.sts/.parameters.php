<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentParameters['PARAMETERS'] = array(

    "RESULT_LIST" => array(
        "PARENT" => "BASE",
        "NAME" => GetMessage("T_DESC_RESULT_LIST"),
        "TYPE" => "STRING",
        "VALUES" => '',
    )

);