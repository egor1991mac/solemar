<?php
$templatePath = "/local/templates/travelsoft";

define("VISAS_IBLOCK_ID", 18);
define("HOTELS_IBLOCK_ID", 15);
define("CITIES_IBLOCK_ID", 8);
define("COUNTRIES_IBLOCK_ID", 7);
define("DETAIL_PAGES_ID_IBLOCK", 26);
define("TOURS_ID_IBLOCK", 20);
define("REVIEWS_ID_IBLOCK", 2);
define("REGIONS_ID_IBLOCK", 23);
define("RESORTS_TYPE_ID_IBLOCK", 24);

define("NO_PHOTO_447_170",$templatePath."/img/no_photo_447_170.jpg");
define("NO_PHOTO_280_170",$templatePath."/img/no_photo_280_170.jpg");
define("NO_PHOTO_258_170",$templatePath."/img/no_photo_258_170.jpg");
define("NO_PHOTO_245_170",$templatePath."/img/no_photo_245_170.jpg");
define("NO_PHOTO_267_187",$templatePath."/img/no_photo_267_187.jpg");
