<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?>
<p class="title search none"><?= !empty($arParams["TITLE"]) ? $arParams["TITLE"] : GetMessage("CONTACT_US"); ?></p>
<div class="add-feedback">
    <h2><?= !empty($arParams["TITLE"]) ? $arParams["TITLE"] : GetMessage("CONTACT_US"); ?></h2>
    <form action="<?= POST_FORM_ACTION_URI ?>" method="POST" id="feedback-form">
        <?= bitrix_sessid_post() ?>
        <input type="hidden" value="" name="item" id="review_item_id">
        <fieldset>
            <dl>
                <dd><input type="text" name="user_name" id="Feedback-name" placeholder="<?= GetMessage("MFT_NAME") ?>"
                           value="<?= $arResult["AUTHOR_NAME"] ?>" <?if(!empty($arResult["ERROR_MESSAGE"]["user_name"])):?>class="error"<?endif?>>
                </dd>
                <dd>
                    <input type="text" name="user_email" id="Feedback-email" placeholder="<?= GetMessage("MFT_EMAIL") ?>"
                           value="<?= $arResult["AUTHOR_EMAIL"] ?>">
                    <input type="text" name="user_phone" id="Feedback-phone" placeholder="<?= GetMessage("MFT_PHONE") ?>"
                           value="<?= $arResult["AUTHOR_PHONE"] ?>">
                    <p class="note"><?=GetMessage('CONTACT_NOTE')?></p>
                </dd>
                <dd><textarea name="MESSAGE" rows="5" id="Feedback-question" placeholder="<?= GetMessage("MFT_MESSAGE") ?>"
                              cols="40" <?if(!empty($arResult["ERROR_MESSAGE"]["MESSAGE"])):?>class="error" <?endif?>><?= $arResult["MESSAGE"] ?></textarea></dd>
            </dl>
            <?
            if (strlen($arResult["OK_MESSAGE"]) > 0) {
                ?>
                <p class="mf-ok-text message"><?= $arResult["OK_MESSAGE"] ?></p><?
            }
            ?>
            <input type="hidden" name="PARAMS_HASH" value="<?= $arResult["PARAMS_HASH"] ?>">
            <button type="submit" name="submit" value="<?= GetMessage("MFT_SUBMIT") ?>"><span><?= GetMessage("MFT_SUBMIT") ?></span></button>
        </fieldset>
    </form>
</div>