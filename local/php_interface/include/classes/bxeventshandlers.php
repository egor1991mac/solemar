<?php

namespace travelsoft;

/**
 * Класс с обработчиками событий bitrix
 * @author juliya.sharlova by travelsoft
 */
class BxEventsHandlers {

    /**
     * @param array $arFields
     */
    static public function bxOnBeforeUserUpdate(&$arFields) {
        if (!defined("ADMIN_SECTION")) {
            self::setLoginEqlEmail($arFields);
        }
    }

    /**
     * Делаем LOGIN = EMAIL
     * @param array $arFields
     */
    public static function setLoginEqlEmail(&$arFields) {
        $arFields['LOGIN'] = $arFields['EMAIL'];
    }

}