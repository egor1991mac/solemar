<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$rsET = CEventType::GetList(array('LID' => LANGUAGE_ID));
while ($arET = $rsET->Fetch()) {
    $mailEv[$arET['EVENT_NAME']] = "[" . $arET['EVENT_NAME'] . "]" . $arET['NAME'];
}

$arComponentParameters = array(
    "PARAMETERS" => array(
        "_POST" => array (
            "PARENT" => "BASE",
            "NAME" => GetMessage('REQUEST_POST_DATA'),
            "DEFAULT" => '={$_POST["make_booking"]}'
        ),
        "PAYMENT_PAGE" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage('PAGE_PAYMENT'),
            "TYPE" => "STRING",
            "DEFAULT" => "/lichnyy-kabinet/oplata/"
        ),
        "INC_JQUERY_MASKEDINPUT" => array(
            "PARENT" => "BASE",
            "TYPE" => "CHECKBOX",
            "NAME" => GetMessage('ADD_MASKEDINPUT_J'),
            "DEFAULT" => "Y"
        )
    )
);