<?php

/* 
 * Компонент детальной информации по заказу
 */

class TravelsoftOrderDetail extends CBitrixComponent {

    protected $_operator = null;

    /** детальная информация по заказу */
    protected function __setOrderDetail () {
        
        if ($this->arParams["ORDER_ID"] <> '') {

            if (!empty($this->arResult["OPERATOR"])) {

                if (class_exists($this->arResult["OPERATOR"]['UF_CLASS'])) {
                    $class = $this->arResult["OPERATOR"]['UF_CLASS'];
                    $this->_operator = new $class();
                    $strResponse = $this->_operator->getOrderDetail(array(
                        "address" => $this->arResult["OPERATOR"]['UF_GATEWAY'],
                        "parameters" => array("token" => $_SESSION['__TRAVELSOFT']["TOKEN"]["OP_".$this->arResult["OPERATOR"]['ID']], "dogovor_code" => $this->arParams["ORDER_ID"])
                    ));
                    $response = \Bitrix\Main\Web\Json::decode($strResponse);

                    if ($response["result"]) {
                        $this->arResult["ORDER"] = $response["result"];
                    } else {
                        $this->arResult["ERRORS"][] = "UNKNOWN_ERROR";
                    }
                }
                else {
                    $this->arResult["ERRORS"][] = "UNKNOWN_ORDER_ID";
                }
            }

        } else {
            $this->arResult["ERRORS"][] = "UNKNOWN_ORDER_ID";
        }
        
    }
    
    /** подключение модулей */
    protected function __include_modules () {
        if (!\Bitrix\Main\Loader::includeModule("travelsoft.booking.sts")) {
            throw new Exception("Не найден модуль \"инструменты бронирования\" ");
        }
    }
    
    /** проверка, что пользователь пришёл со страницы */
    protected function __check_just_booked () {
        if ($_SESSION["__TRAVELSOFT"]["JUST_BOOKED"] == "Y") {
            $this->arResult["JUST_BOOKED"] = true;
            unset($_SESSION["__TRAVELSOFT"]["JUST_BOOKED"]);
        }
    }
    
    protected function _procActionRequest () {

        if (check_bitrix_sessid() && $this->arParams["ORDER_ID"]) {
            
            if ($_REQUEST['action'] == "cancel") {

                if(!is_null($this->_operator)){

                    $strResponse = $this->_operator->orderToCancel(array(
                        "address" => $this->arResult["OPERATOR"]['UF_GATEWAY'],
                        "parameters" => array("token" => $_SESSION['__TRAVELSOFT']["TOKEN"]["OP_".$this->arResult["OPERATOR"]['ID']], "dogovor_code" => $this->arParams["ORDER_ID"], "service_keys" => array())
                    ));
                    $response = \Bitrix\Main\Web\Json::decode($strResponse);

                    if ($response["result"]["result"]) {
                        $_SESSION['__TRAVELSOFT']['JUST_NOW_CANCELLATION'] = true;
                        LocalRedirect($GLOBALS["APPLICATION"]->GetCurPageParam("order_id=" . $this->arParams["ORDER_ID"], array("order_id", "action", "sessid")));
                    } else {
                        $this->arResult["ERRORS"][] = "CANCEL_FAIL";
                    }

                }
                
            }
            
        }
        
    }

    /**
     * Устанавливает массив оператора
     */
    protected function _getOperator(array $query) {

        $operator = \travelsoft\booking\sts\stores\Operators::get($query);

        return $operator;

    }
    
    public function executeComponent() {
        
        $this->__include_modules();
        
        $this->__check_just_booked();

        $this->arResult["OPERATOR"] = current($this->_getOperator(array("filter"=>array("UF_MAIN_OPERATOR"=>1))));
        
        $this->__setOrderDetail();
        
        $this->_procActionRequest();
        
        $this->IncludeComponentTemplate();
        
    }
    
}

