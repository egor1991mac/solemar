<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?$properties = $arResult["DISPLAY_PROPERTIES"];?>
<?$upItaly = ucwords($arResult["CODE"]);
$arImgFlag = getSrc($properties["FLAG"]["VALUE"], array("width"=>24,"height"=>24), SITE_TEMPLATE_PATH.'/flags/'.$upItaly.'.png', 1, false);
?>

    <div class="country-menu">
        <div class="container">
            <p class="country-title" style="background-image:url(<?=$arImgFlag[0]?>)"><?=$arResult["NAME"]?></p>
            <p class="country-title-mobile" style="background-image:url(<?=$arImgFlag[0]?>)">
                <a href="#toggle" id="country-menu-button" class="flashlink"><?=$arResult["NAME"]?></a>
            </p>
            <ul class="tabs">
                <?if($arResult["CITIES"]):?>
                    <li><a href="<?=$arParams["DETAIL_PAGE"]?>resorts/"><span><?=GetMessage('RESORTS')?></span></a></li>
                <?endif?>
                <?if($arResult["HOTELS"]):?>
                    <li><a href="<?=$arParams["DETAIL_PAGE"]?>hotels/"><span><?=GetMessage('HOTELS')?></span></a></li>
                <?endif?>
                <?if($arResult["VISAS"]):?>
                    <li><a href="<?=$arParams["DETAIL_PAGE"]?>visa/"><span><?=GetMessage('VISAS')?></span></a></li>
                <?endif?>
                <?if(!empty($properties["TOURIST_GUIDE"]["VALUE"]["TEXT"])):?>
                    <li class="active"><a href="<?=$arParams["DETAIL_PAGE"]?>tourist-info/"><span><?=GetMessage('TOURIST_GUIDE')?></span></a></li>
                <?endif?>

                <!--<li><a href="https://online.solemare.by/online/Extra/QuotedDynamic.aspx" class="important" onclick="return !window.open(this.href)"><span>Подбор тура online</span></a></li>-->

            </ul>
        </div>
    </div>
<!--<div class="fast-bar">
    <div class="container">
        <div id="curr">
            <ul>
                <li class="text">Цены в:</li>
                <li class="byn"><a href="https://solemare.by/tours/italy/about#byn" class="flashlink"><span>BYN</span></a></li>
                <li class="usd"><a href="https://solemare.by/tours/italy/about#usd" class="flashlink"><span>USD</span></a></li>
                <li class="eur"><a href="https://solemare.by/tours/italy/about#eur" class="flashlink"><span>EUR</span></a></li>
                <li class="gbp"><a href="https://solemare.by/tours/italy/about#gbp" class="flashlink"><span>GBP</span></a></li>
            </ul>
        </div>
        <ul>
        </ul>
    </div>
</div>-->

<div class="container country col2">
    <h1 class="nopadding"><?=!empty($arParams["TITLE_DESC_BLOCK"]) ? $arParams["TITLE_DESC_BLOCK"] : (!empty($properties["CN_NAME_KUDA"]["VALUE"]) ? GetMessage('TOURIST_GUIDE').' '.$properties["CN_NAME_KUDA"]["VALUE"] : GetMessage('TOURIST_GUIDE'))?></h1>
    <div class="sidebar">
        <?=$arParams["TOURS_RIGHT_FOR_COUNTRY"]?>
        <div class="widgets">
            <?=$arParams["SIDEBAR_SUBSCRIPTION"]?>
            <?=$arParams["SIDEBAR_WIDGETS"]?>
        </div>
    </div>
    <div class="content">
        <?=$properties["TOURIST_GUIDE"]["DISPLAY_VALUE"]?>
        <div style="clear: both"></div>
    </div>
    <?=$arParams["TOURS_SLIDER_FOR_COUNTRY"]?>
</div>

<?=$arParams["COUNTRIES_LIST"]?>