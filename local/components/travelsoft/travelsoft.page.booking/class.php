<?php

use travelsoft\booking\sts\Settings as S;
use travelsoft\booking\sts\stores\Hotels;
use travelsoft\booking\sts\stores\Operators;

/*  Компонент бронирования  */

class TravelsoftBookingComponent extends CBitrixComponent {

    protected $_basket = null;

    protected $_operator = null;

    /** обработка входящего запроса * */
    private function __proccessing_request() {

        if (
            $GLOBALS["USER"]->IsAuthorized() &&
            check_bitrix_sessid() &&
            strlen($this->arParams["_POST"]["submit"]) > 0 &&
            $this->__check_post()
        ) {

            if ($_arr_booking_data = $this->__get_prepared_booking_data()) {

                if (!empty($this->arResult["OPERATOR"])) {

                    if (class_exists($this->arResult["OPERATOR"]['UF_CLASS'])) {

                        $class = $this->arResult["OPERATOR"]['UF_CLASS'];
                        $_address = $this->arResult["OPERATOR"]['UF_GATEWAY'];
                        $_operator = $this->arResult["OPERATOR"]['ID'];

                        $this->_operator = new $class($_arr_booking_data);

                        if($_arr_booking_data["operatorId"] == $this->arResult["OPERATOR"]['UF_OP_ID']){

                            $strResponse = $this->_operator->makeBooking(array(
                                "address" => $_address,
                                "booking_data" => $_arr_booking_data,
                                "operator_id" => $_operator
                            ));

                        } else {

                            $_arr_booking_data["PROGRAM_TOUR"] = isset($_SESSION['__TRAVELSOFT']["ORDER"][$this->arParams["_POST"]["operatorId"]]["priceKey"]["PROGRAM_TOUR"]) && !empty($_SESSION['__TRAVELSOFT']["ORDER"][$this->arParams["_POST"]["operatorId"]]["priceKey"]["PROGRAM_TOUR"]) ? $_SESSION['__TRAVELSOFT']["ORDER"][$this->arParams["_POST"]["operatorId"]]["priceKey"]["PROGRAM_TOUR"] : '';
                            $_arr_booking_data["services"] = $this->_getArbitraryBookingDataCommon($_arr_booking_data);

                            $strResponse = $this->_operator->makeArbitraryBooking(array(
                                "address" => $_address,
                                "booking_data" => $_arr_booking_data,
                                "operator_id" => $_operator
                            ));

                        }

                        $response = \Bitrix\Main\Web\Json::decode($strResponse,true);

                        if ($response["result"]["dogovor_code"]) {

                            $_SESSION['__TRAVELSOFT']["MAIN_OPERATOR"] = $_operator;
                            unset($_SESSION['__TRAVELSOFT']["ORDER"][$this->arParams["_POST"]["operatorId"]]["priceKey"]);

                            LocalRedirect($this->arParams["PAYMENT_PAGE"] . "/detail.php?order_id=" . $response["result"]["dogovor_code"]);
                        }
                    }

                }
            }

            $this->arResult["ERRORS"]["BOOKING"] = true;
        }
    }

    /** проверка формы * */
    private function __check_post() {

        $_post = $this->arParams["_POST"];

        $errors = null;

        $email = filter_var($_post["email"], FILTER_VALIDATE_EMAIL);

        if (!$email) {
            $errors[] = "WRONG_EMAIL";
        }

        if (!strlen($_post["phone"])) {
            $errors[] = "WRONG_PHONE";
        }

        if (empty($_post["tourist"])) {
            $errors[] = "WRONG_TOURIST";
        }

        foreach ($_post["tourist"] as $k => $val) {

            if (!strlen($val["name"])) {
                $errors["TOURIST"][$k][] = "WRONG_NAME";
            }
            if (!strlen($val["last_name"])) {
                $errors["TOURIST"][$k][] = "WRONG_LAST_NAME";
            }
            if (!isset($this->arResult["MALE"][$val["male"]])) {
                $errors["TOURIST"][$k][] = "WRONG_MALE";
            }
            if (preg_match("#" . $this->arResult["PATTERNS"]["birthdate"] . "#", $val['birthdate']) !== 1) {
                $errors["TOURIST"][$k][] = "WRONG_BIRTHDATE";
            }
            if (!strlen($val["passport"])) {
                $errors["TOURIST"][$k][] = "WRONG_PASSPORT";
            }
            if (preg_match("#" . $this->arResult["PATTERNS"]["passport_date"] . "#", $val['passport_date']) !== 1) {
                $errors["TOURIST"][$k][] = "WRONG_PASSPORT_DATE";
            }
            if (!strlen($val["citizenship"])) {
                $errors["TOURIST"][$k][] = "WRONG_CITIZENSHIP";
            }
            if ($val["citizenship"] == "Беларусь" && !strlen($val["passport_id"])) {
                $errors["TOURIST"][$k][] = "WRONG_PASSPORT_ID";
            }

        }

        if ($errors) {
            $this->arResult["ERRORS"]["FORM"] = $errors;
            return false;
        }

        return true;
    }

    /**
     * Возвращает общие данные для бронирования
     * @param array $arItem
     * @return array
     */
    protected function _getBookingDataCommon(array $arItem) {

        if(isset($arItem) && (!empty($arItem["operatorId"]) || !empty($arItem["priceKey"]))) {

            $arResult["operatorId"] = $arItem["operatorId"];
            $arResult["priceKey"] = $arItem["priceKey"];
            $arResult["hotelName"] = $arItem["hotelName"];
            $arResult["hotelStars"] = $arItem["hotelStars"];
            $arResult["hotelDetailUrl"] = $arItem["hotelDetailUrl"];
            $arResult["country"] = $arItem["country"];
            $arResult["city"] = $arItem["city"];
            $arResult["dateBegin"] = $arItem["dateBegin"];
            $arResult["dateEnd"] = $arItem["dateEnd"] ? $arItem["dateEnd"] : $arItem["dateBegin"];
            $arResult["adults"] = $arItem["adults"];
            $arResult["children"] = $arItem["children"];
            $arResult["night"] = $arItem["night"];

            $arResult["nmen"] = $arResult["adults"] + $arResult["children"];

            $arResult["accm"]["placement"] = isset($arItem["accm"]["placement"]) ? $arItem["accm"]["placement"] : $arItem["placement"];
            $arResult["accm"]["roomType"] = isset($arItem["accm"]["roomType"]) ? $arItem["accm"]["roomType"] : $arItem["roomType"];
            $arResult["accm"]["roomCatType"] = isset($arItem["accm"]["roomCatType"]) ? $arItem["accm"]["roomCatType"] : $arItem["roomCatType"];
            $arResult["food"] = $arItem["food"];

            if(!empty($arItem["hotelIdBx"]) && $arItem["hotelIdBx"] > 0){
                $arResult["services"] = Hotels::getElementServices($arItem["hotelIdBx"]);
            }

            $arResult["price"] = $arItem["price"];
            $arResult["priceNotCurrency"] = $arItem["priceNotCurrency"];
            $arResult["currency"] = $arItem["currency"];

            return $arResult;

        }
        else {

            throw new Exception("Ошибка бронирования. Обратитесь к администратору.");

        }


    }

    /**
     * Возвращает "услуги" для произвольного бронирования
     * @param array $arItem
     * @return array
     */
    protected function _getArbitraryBookingDataCommon(array $arItem) {

        if(isset($arItem) && (!empty($arItem["operatorId"]) || !empty($arItem["priceKey"]))) {

            $services = array();

            $services[] = array(
                "dateBegin" => $arItem["dateBegin"],
                "dateEnd" => $arItem["dateEnd"],
                "name" => "hotel::".$arItem["hotelName"]." ".$arItem["hotelStars"]."/".$arItem["accm"]["placement"]." ".$arItem["accm"]["roomType"]." ".$arItem["accm"]["roomCatType"].", ".$arItem["food"],
                "country" => $arItem["country"],
                "partner" => \travelsoft\sts\stores\Operators::getElementName(array("ID"=>$arItem["operatorId"])),
                "brutto" => $arItem["priceNotCurrency"],
                "currency" => $arItem["currency"]
            );

            if(isset($arItem["PROGRAM_TOUR"]) && !empty($arItem["PROGRAM_TOUR"])) {

                $arServices = array("avia","transfers","excursions","visas","insurances");

                foreach ($arItem["PROGRAM_TOUR"] as $k=>$item) {
                    if(in_array($k, $arServices)) {
                        foreach ($item as $val) {
                            $name = '';
                            switch ($k) {
                                case "avia":
                                    $name = "avia::" . $val["flight"] . " " . $val["cityDep"] . "/" . $val["airportDep"] . " — " . $val["cityArr"] . "/" . $val["airportArr"] . ", " . $val["places"];
                                    break;
                                case "transfers":
                                    $name = "transfers::" . $val["city"] . ", " . $val["transport"] . ", " . $val["route"];
                                    break;
                                case "excursions":
                                    $name = "excursions::" . $val["city"] . ", " . $val["transport"] . ", " . $val["route"];
                                    break;
                                case "visas":
                                    $name = "visas::" . $val["name"] . ", " . num2word($val["days"], array("день", "дня", "дней"));
                                    break;
                                case "insurances":
                                    $name = "insurances::" . $val["name"] . ", " . num2word($val["days"], array("день", "дня", "дней"));
                                    break;
                            }

                            $services[] = array(
                                "dateBegin" => $val["dateFrom"],
                                "dateEnd" => isset($val["dateTo"]) ? $val["dateTo"] : $val["dateFrom"],
                                "name" => $name,
                                "country" => isset($val["country"]) ? $val["country"] : '',
                                "partner" => isset($val["airline"]) && !empty($val["airline"]) ? $val["airline"] : \travelsoft\sts\stores\Operators::getElementName(array("ID" => $arItem["operatorId"])),
                                "brutto" => isset($val["price"]) ? $val["price"] : 0,
                                "currency" => $arItem["currency"]
                            );
                        }
                    }
                }

            }

            return $services;

        }

    }

    /**
     * подготовка параметров для бронирования
     * @return null | array
     */
    private function __get_prepared_booking_data() {

        $data = null;

        $data = $this->_getBookingDataCommon($this->arResult["ORDER"]);

        if (!$data) {
            return null;
        }

        /*$data["buyer_info"]["email"] = $this->arParams["_POST"]["email"];
        $data["buyer_info"]["phone"] = $this->arParams["_POST"]["phone"];*/
        $data["addParameter"]["phone"] = $this->arParams["_POST"]["phone"];
        $data["comment"] = !$this->arParams["_POST"]["comment"] ? "" : $this->arParams["_POST"]["comment"];

        $countOfPeople = $data["nmen"];
        foreach ($this->arParams["_POST"]["tourist"] as $key => $tourist) {
            if (($key + 1) <= $countOfPeople) {
                $data["turists"][] = array(
                    "first_name" => $tourist["name"],
                    "last_name" => $tourist["last_name"],
                    "sex" => $tourist["male"],
                    "citizenship" => $tourist["citizenship"],
                    "passport_num" => $tourist["passport"],
                    "passport_date" => $tourist["passport_date"],
                    "passport_id" => $tourist["passport_id"],
                    "birth_date" => $tourist["birthdate"]
                );
            }
        }

        return $data;

    }

    /** установка полей пользователя * */
    private function __set_user_info() {

        $this->arResult["USER"] = array(
            "is_authorized" => $GLOBALS["USER"]->IsAuthorized(),
            "email" => $GLOBALS["USER"]->GetEmail()
        );

    }

    /** подключение модулей * */
    private function __include_modules() {
        if (!Bitrix\Main\Loader::includeModule("travelsoft.booking.sts")) {
            throw new Exception("It is impossible to make a booking");
        }
        if (!Bitrix\Main\Loader::includeModule("travelsoft.sts")) {
            throw new Exception("It is impossible to make a booking");
        }
        if (!Bitrix\Main\Loader::includeModule("highloadblock")) {
            throw new Exception("Highloadblock not found");
        }
        if (!Bitrix\Main\Loader::includeModule("travelsoft.currency")) {
            throw new Exception("currency module not found");
        }
        if (!Bitrix\Main\Loader::includeModule("iblock")) {
            throw new Exception("iblock module not found");
        }
    }

    /**
     * подключение js
     */
    private function __include_js() {

        $asset = Bitrix\Main\Page\Asset::getInstance();

        if ((string) $this->arParams["INC_JQUERY_MASKEDINPUT"] === "Y") {
            $asset->addJs("https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js", true);
        }

        if ($this->__template && $this->__template->__folder) {
            $asset->addJs($this->__template->__folder . "/_script.js", true);
        }
    }

    /**
     * регулярные выражение для html аттрибута pattern
     */
    private function __set_input_check_patterns() {

        $this->arResult["PATTERNS"] = array(
            "birthdate" => "^\d{2}\.\d{2}\.\d{4}$",
            "passport_date" => "^\d{2}\.\d{2}\.\d{4}$",
            "phone" => "^\+?[0-9,\s]{0,}$"
        );
    }

    /**
     * Данные по полу
     */
    private function __set_male_values() {
        $this->arResult["MALE"] = array("0" => "MAN", "1" => "WOMAN");
    }

    /**
     * возвращает ресайз-массив изображения для услуги
     * @param mixed $imgId
     * @return array|null
     */
    protected function _getResizeImage($imgId) {
        $imgId = (array) $imgId;
        return CFile::ResizeImageGet(
            $imgId[0], array('width' => 200, 'height' => 150), BX_RESIZE_IMAGE_PROPORTIONAL, true
        );
    }

    /**
     * Устанавливает массив оператора
     */
    protected function _getOperator(array $query) {

        $operator = \travelsoft\booking\sts\stores\Operators::get($query);

        return $operator;

    }

    /**
     * Устанавливает массив $arResult
     */
    protected function _setArResult() {

        if(isset($this->arParams["_POST"])) {

            if(isset($_SESSION['__TRAVELSOFT']["ORDER"][$this->arParams["_POST"]["operatorId"]][$this->arParams["_POST"]["priceKey"]])){
                if(!isset($_SESSION['__TRAVELSOFT']["ORDER"][$this->arParams["_POST"]["operatorId"]][$this->arParams["_POST"]["priceKey"]]["price"]) || empty($_SESSION['__TRAVELSOFT']["ORDER"][$this->arParams["_POST"]["operatorId"]][$this->arParams["_POST"]["priceKey"]]["price"])) {
                    unset($_SESSION['__TRAVELSOFT']["ORDER"][$this->arParams["_POST"]["operatorId"]][$this->arParams["_POST"]["priceKey"]]);
                    $_SESSION['__TRAVELSOFT']["ORDER"][$this->arParams["_POST"]["operatorId"]][$this->arParams["_POST"]["priceKey"]] = $this->arParams["_POST"];
                }
            }
            elseif(!isset($_SESSION['__TRAVELSOFT']["ORDER"][$this->arParams["_POST"]["operatorId"]][$this->arParams["_POST"]["priceKey"]])){
                $_SESSION['__TRAVELSOFT']["ORDER"][$this->arParams["_POST"]["operatorId"]][$this->arParams["_POST"]["priceKey"]] = $this->arParams["_POST"];
            }

            //$this->arResult["ORDER"] = $this->_getBookingDataCommon($this->arParams["_POST"]);
            $this->arResult["ORDER"] = $this->_getBookingDataCommon($_SESSION['__TRAVELSOFT']["ORDER"][$this->arParams["_POST"]["operatorId"]][$this->arParams["_POST"]["priceKey"]]);

            if (empty($this->arResult["ORDER"])) {
                throw new Exception("Ошибка бронирования. Обратитесь к администратору.");
            }

            $arOperator = current($this->_getOperator(array("filter" => array("UF_OP_ID" => $this->arResult["ORDER"]["operatorId"]))));

            if (!empty($arOperator)) {

                if (class_exists($arOperator['UF_CLASS'])) {
                    $class = $arOperator['UF_CLASS'];
                    $this->arResult["ORDER"]["address"] = $arOperator['UF_GATEWAY'];
                    $this->_operator = new $class($this->arResult["ORDER"]);
                    $program = $this->_operator->getServiceDetail($this->arResult["ORDER"]);
                    $this->arResult["PROGRAM_TOUR"] = $program;
                    if(!empty($this->arResult["PROGRAM_TOUR"]) && !empty($this->arResult["ORDER"])) {
                        $this->arResult["ORDER"]["PROGRAM_TOUR"] = $_SESSION['__TRAVELSOFT']["ORDER"][$this->arParams["_POST"]["operatorId"]][$this->arParams["_POST"]["priceKey"]]["PROGRAM_TOUR"] = $this->arResult["PROGRAM_TOUR"];
                    }
                }

            }

            return $this->arResult["ORDER"];

        }

    }

    public function executeComponent() {

        try {
            $this->__include_modules();

            $this->arResult["ORDER"] = array();

            $this->arResult["OPERATOR"] = current($this->_getOperator(array("filter"=>array("UF_MAIN_OPERATOR"=>1))));

            //$this->arResult["SERVICES"] = services();

            $this->_setArResult();

            $this->__set_input_check_patterns();

            $this->__set_male_values();

            $this->__proccessing_request();

            $this->__set_user_info();

            $this->IncludeComponentTemplate();

            $this->__include_js();
        } catch (\Exception $e) {

            ShowError($e->getMessage());
        }
    }

}
