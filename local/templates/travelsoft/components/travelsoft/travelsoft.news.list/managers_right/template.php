<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if(!empty($arResult["ITEMS"])):?>

    <?if(!empty($arParams["TEXT_TITLE"])):?>
        <div class="manager-menu">
            <div class="container">
                <p><a href="#toggle" id="manager-menu-button" class="flashlink"><?=$arParams["TEXT_TITLE"]?></a></p>
            </div>
        </div>
    <?endif?>
    <div class="tour-manager<?php if (count($arResult["ITEMS"]) > 1): ?> group<?endif ?>" id="tour-manager">
        <h2><?if (count($arResult["ITEMS"]) > 1): ?><?=GetMessage('TITLE_MANAGERS_BLOCK')?><?else:?><?=GetMessage('TITLE_MANAGER_BLOCK')?><?endif?> <?=GetMessage('MANAGERS_BLOCK_TOUR')?></h2>

        <?foreach($arResult["ITEMS"] as $arItem):?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <?$arImg = getSrc($arItem["DISPLAY_PROPERTIES"]["PICTURES"]["VALUE"], array("width"=>120,"height"=>120), NO_PHOTO_AVATAR_120_120, 1, false);?>

            <div id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                <p class="name"><?= $arItem["NAME"] ?></p>
                <img src="<?= $arImg[0] ?>" alt="" />
                <?if(!empty($arItem["DISPLAY_PROPERTIES"]["PHONE"]["VALUE"])):?>
                    <p><?= implode("<br>", $arItem["DISPLAY_PROPERTIES"]["PHONE"]["VALUE"])?></p>
                    <?/*foreach ($arItem["DISPLAY_PROPERTIES"]["PHONE"]["VALUE"] as $phone):*/?><!--
                        <?/*=$phon */?>
                    --><?/*endforeach;*/?>

                <?endif?>
                <p>
                    <?if(!empty($arItem["DISPLAY_PROPERTIES"]["SKYPE"]["VALUE"])):?>
                        <span class="skype"><?= $arItem["DISPLAY_PROPERTIES"]["SKYPE"]["VALUE"] ?></span>
                    <?endif ?>
                    <?/*if(!empty($arItem["DISPLAY_PROPERTIES"]["EMAIL"]["VALUE"])):*/?><!--
                        <br /><a href="mailto:<?/*=$arItem["DISPLAY_PROPERTIES"]["EMAIL"]["VALUE"]*/?>" class="mail"><?/*= $arItem["DISPLAY_PROPERTIES"]["EMAIL"]["VALUE"] */?></a>
                    --><?/*endif */?>
                </p>
                <?if(!empty($arItem["DISPLAY_PROPERTIES"]["LINK_FORM"]["VALUE"])):?>
                    <p><a href="<?= $arItem["DISPLAY_PROPERTIES"]["LINK_FORM"]["VALUE"] ?>" class="blue-button" onclick="return !window.open(this.href)"><?=GetMessage('MANAGERS_BLOCK_TOUR_BTN')?></a></p>
                <?endif?>
            </div>

        <?endforeach;?>

    </div>
<?endif?>