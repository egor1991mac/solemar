<?php

namespace travelsoft\booking\sts;

use travelsoft\booking\sts\Utils as U;

/**
 * Валидация переменных бронирования
 *
 * @author juliya.sharlova
 */
class Validation {

    /**
     * @param mixed $val
     * @return boolean
     */
    protected static function aboveZero($val) {
        if ($val > 0) {
            return true;
        }
        return false;
    }

    /**
     * @param mixed $val
     * @return boolean
     */
    protected static function geZero($val) {
        if ($val >= 0) {
            return true;
        }
        return false;
    }
    
    public function checkPrice ($price) {
        if (!self::geZero($price)) {
            throw new Exception(__CLASS__ . ": Price must be >= 0");
        }
    }
    
    public static function checkDuration (int $duration) {
        if (!self::geZero($duration)) {
            throw new \Exception(__CLASS__ . ": Duration must be >= 0");
        }
    }

    /**
     * Проверка существования валюты
     * @param mixed $currency
     * @throws \Exception
     */
    public static function checkCurrency($currency) {

        $arCurrency = U::getAllCurrency();
        if (!isset($arCurrency[$currency]) && array_search($currency, array_column($arCurrency, "iso")) === false) {
            throw new \Exception(__CLASS__ . ": Unknown currency (\"" . $currency . "\")");
        }
    }

    /**
     * Проверка даты начала
     * @param int $dateFrom
     * @throws \Exception
     */
    public static function checkDateFrom(int $dateFrom) {
        if (!self::aboveZero($dateFrom)) {
            throw new \Exception(__CLASS__ . ": Date_from must be > 0");
        }
    }
    
    /**
     * Проверка масиива целых чисел на "положительность"
     * @param array $array
     */
    public static function checkArray (array $array) {
        
        if (empty($array)) {
            throw new \Exception(__CLASS__ . ": Array with numbers > 0 is empty");
        }
        
        foreach ($array as $val) {
            if (!self::aboveZero($val)) {
                throw new \Exception(__CLASS__ . ": Some number is < or = 0");
            }
        }
        
    }
    
    /**
     * Проверка даты окончания
     * @param int $dateTo
     * @param int $dateFrom
     * @throws \Exception
     */
    public static function checkDateTo(int $dateTo, int $dateFrom) {
        if (($dateTo - $dateFrom) < 0) {
            throw new \Exception(__CLASS__ . ": Date_to (\"" . $dateTo . "\") must be > date_from (\"" . $dateFrom . "\") ");
        }
    }

    /**
     * Проверка количества взрослых
     * @param int $adults
     * @throws \Exception
     */
    public static function checkAdults(int $adults) {
        if (!self::aboveZero($adults)) {
            throw new \Exception(__CLASS__ . ": Unknown adults count");
        }
    }

    /**
     * Проверка количества детей
     * @param int $children
     * @throws \Exception
     */
    public static function checkChildren(int $children) {
        if (!self::geZero($children)) {
            throw new \Exception(__CLASS__ . ": Unknown children count");
        }
    }

}
