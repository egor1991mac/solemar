<?php


return array(
    "table" => "tsoperators",
    "table_data" => array(
        "NAME" => "TSOPERATORS",
        "ERR" => "Ошибка при создании highloadblock'a операторов",
        "LANGS" => array(
            "ru" => 'Операторы',
            "en" => "Operators"
        ),
        "OPTION_PARAMETER" => "OPERATORS_HL"
    ),
    "fields" => array(
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_CODE",
            "USER_TYPE_ID" => 'string',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Код оператора',
                'en' => 'Code operator',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Код оператора',
                'en' => 'Code operator',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Код оператора',
                'en' => 'Code operator',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Код оператора" ',
                'en' => 'An error in completing the field "Code operator"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_NAME",
            "USER_TYPE_ID" => 'string',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Наименование оператора',
                'en' => 'Name operator',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Наименование оператора',
                'en' => 'Name operator',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Наименование оператора',
                'en' => 'Name operator',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Наименование оператора" ',
                'en' => 'An error in completing the field "Name operator"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_ACTIVE",
            "USER_TYPE_ID" => 'boolean',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "1",
                'LABEL' => array(0 => "нет", 1 => "да"),
                "DISPLAY" => "CHECKBOX",
                'LABEL_CHECKBOX' => "да"
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Активен',
                'en' => 'Active',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Активен',
                'en' => 'Active',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Активен',
                'en' => 'Active',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Активен" ',
                'en' => 'An error in completing the field "Active"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_GATEWAY",
            "USER_TYPE_ID" => 'string',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Шлюз',
                'en' => 'Gateway',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Шлюз',
                'en' => 'Gateway',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Шлюз',
                'en' => 'Gateway',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Шлюз" ',
                'en' => 'An error in completing the field "Gateway"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_CLASS",
            "USER_TYPE_ID" => 'string',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Класс обрабочик оператора',
                'en' => 'Operator handling class',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Класс обрабочик оператора',
                'en' => 'Operator handling class',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Класс обрабочик оператора',
                'en' => 'Operator handling class',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Класс обрабочик оператора" ',
                'en' => 'An error in completing the field "Operator handling class"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_LOGO",
            "USER_TYPE_ID" => 'file',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Логотип',
                'en' => 'Logo',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Логотип',
                'en' => 'Logo',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Логотип',
                'en' => 'Logo',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Логотип" ',
                'en' => 'An error in completing the field "Logo"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_MAIN_OPERATOR",
            "USER_TYPE_ID" => 'boolean',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'N',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                "DEFAULT_VALUE" => "0",
                'LABEL' => array(0 => "нет", 1 => "да"),
                "DISPLAY" => "CHECKBOX",
                'LABEL_CHECKBOX' => "да",
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Основной оператор',
                'en' => 'Main operator',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Основной оператор',
                'en' => 'Main operator',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Основной оператор',
                'en' => 'Main operator',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Основной оператор" ',
                'en' => 'An error in completing the field "Main operator"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
    )
);
