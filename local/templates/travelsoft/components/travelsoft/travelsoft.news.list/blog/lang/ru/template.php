<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "Будет удалена вся информация, связанная с этой записью. Продолжить?";
$MESS["TITLE_COUNTRIES_BLOCK"] = "СТРАНЫ";
$MESS["FROM_PRICE"] = "от ";
$MESS["ON_REQUEST"] = "Под запрос";
$MESS["ON"] = "на";
$MESS["NIGHT_1"] = "ночь";
$MESS["NIGHT_2"] = "ночи";
$MESS["NIGHT_ALL"] = "ночей";
$MESS["NEAREST_DEPARTURES"] = "Ближайшие выезды:";
$MESS["READ_MORE"] = "Читать далее →";
?>