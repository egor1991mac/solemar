<?php


return array(
    "table" => "tsdirections",
    "table_data" => array(
        "NAME" => "TSDIRECTIONS",
        "ERR" => "Ошибка при создании highloadblock'a направлений",
        "LANGS" => array(
            "ru" => 'Направления',
            "en" => "Directions"
        ),
        "OPTION_PARAMETER" => "DIRECTIONS_HL"
    ),
    "fields" => array(
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_CITYFROM_ID",
            "USER_TYPE_ID" => 'integer',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'Y',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'ID города отправления',
                'en' => 'Departure City ID',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'ID города отправления',
                'en' => 'Departure City ID',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'ID города отправления',
                'en' => 'Departure City ID',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "ID города отправления" ',
                'en' => 'An error in completing the field "Departure City ID"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_CITYFROM_BX_ID",
            "USER_TYPE_ID" => 'integer',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'ID города отправления (Битрикс)',
                'en' => 'Departure City ID (Bitrix)',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'ID города отправления (Битрикс)',
                'en' => 'Departure City ID (Bitrix)',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'ID города отправления (Битрикс)',
                'en' => 'Departure City ID (Bitrix)',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "ID города отправления (Битрикс)" ',
                'en' => 'An error in completing the field "Departure City ID (Bitrix)"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_COUNTRY_ID",
            "USER_TYPE_ID" => 'integer',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'Y',
            'MANDATORY' => 'Y',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'ID стран прибытия',
                'en' => 'ID of arrival countries',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'ID стран прибытия',
                'en' => 'ID of arrival countries',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'ID стран прибытия',
                'en' => 'ID of arrival countries',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "ID стран прибытия" ',
                'en' => 'An error in completing the field "ID of arrival countries"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_COUNTRY_BX_ID",
            "USER_TYPE_ID" => 'integer',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'Y',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'ID стран прибытия (Битрикс)',
                'en' => 'ID of arrival countries (Bitrix)',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'ID стран прибытия (Битрикс)',
                'en' => 'ID of arrival countries (Bitrix)',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'ID стран прибытия (Битрикс)',
                'en' => 'ID of arrival countries (Bitrix)',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "ID стран прибытия (Битрикс)" ',
                'en' => 'An error in completing the field "ID of arrival countries (Bitrix)"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
    )
);
