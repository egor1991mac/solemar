 <?php

 use Bitrix\Main\Localization\Loc,
     Bitrix\Main\ModuleManager,
     Bitrix\Main\Loader,
     Bitrix\Main\Config\Option;

 Loc::loadMessages(__FILE__);

class travelsoft_booking_sts extends CModule {

    public $MODULE_ID = "travelsoft.booking.sts";
    public $MODULE_VERSION;
    public $MODULE_VERSION_DATE;
    public $MODULE_NAME;
    public $MODULE_DESCRIPTION;
    public $MODULE_GROUP_RIGHTS = "N";
    protected $namespaceFolder = "travelsoft";
    public $componentsList = array(
        "travelsoft.page.booking",
        "travelsoft.order.list",
        "travelsoft.order.detail",
    );
    public $highloadblocksFiles = array();
    public $emailsFiles = array();
    public $eventType = "TRAVELSOFT_BOOKING";

    function __construct() {
        $arModuleVersion = array();
        $path = str_replace("\\", "/", __FILE__);
        $path = substr($path, 0, strlen($path) - strlen("/index.php"));
        include($path . "/version.php");
        if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        }
        $this->MODULE_NAME = "Модуль бронирования туров";
        $this->MODULE_DESCRIPTION = "Модуль бронирования туров представленных операторов";
        $this->PARTNER_NAME = "juliya-alexandrovna";
        $this->PARTNER_URI = "";

        Loader::includeModule('iblock');
        Loader::includeModule("highloadblock");
        $this->__loadHigloadblockFiles();
        $this->__loadMailFiles();
        set_time_limit(0);

    }

    public function copyFiles() {
        foreach ($this->componentsList as $componentName) {
            CopyDirFiles(
                $_SERVER["DOCUMENT_ROOT"] . "/local/modules/" . $this->MODULE_ID . "/install/components/" . $componentName, $_SERVER["DOCUMENT_ROOT"] . "/local/components/" . $this->namespaceFolder . "/" . $componentName, true, true
            );
        }
    }

    public function deleteFiles() {
        foreach ($this->componentsList as $componentName) {
            DeleteDirFilesEx("/local/components/" . $this->namespaceFolder . "/" . $componentName);
        }
        if (!glob($_SERVER["DOCUMENT_ROOT"] . "/local/components/" . $this->namespaceFolder . "/*")) {
            DeleteDirFilesEx("/local/components/" . $this->namespaceFolder);
        }
        return true;
    }

    public function DoInstall() {
        try {

            if (!ModuleManager::isModuleInstalled("iblock")) {
                throw new Exception("Для работы модуля необходим модуль инфоблока");
            }

            if (!ModuleManager::isModuleInstalled("highloadblock")) {
                throw new Exception("Для работы модуля необходим модуль highloadblock");
            }

            # проверка зависимостей модуля
            if (!ModuleManager::isModuleInstalled("travelsoft.currency")) {
                $errors[] = "Для работы модуля необходимо наличие установленного модуля <a target='_blank' href='https://github.com/dimabresky/travelsoft.currency'>travelsoft.currency</a>";
            }

            if (!ModuleManager::isModuleInstalled("travelsoft.sts")) {
                $errors[] = "Для работы модуля необходимо наличие установленного модуля travelsoft.sts";
            }

            if (isset($errors) && !empty($errors)) {
                throw new Exception(implode("<br>", $errors));
            }

            // register module
            ModuleManager::registerModule($this->MODULE_ID);

            # создание higloadblock модуля
            $this->createHighloadblockTables();

            # создание почтовых сообщений
            $this->createMailMessages();

            # добавление полей для пользователей
            $this->addUserFields();

            # добавление группы "Агенты"
            $this->addAgentGroup();

            # добавление группы "Клиенты"
            $this->addClientGroup();

            # копирование файлов
            $this->copyFiles();

            # добавление зависимостей модуля
            $this->addModuleDependencies();

            # добавление параметров выбора для модуля
            $this->addOptions();

        } catch (Exception $ex) {
            $GLOBALS["APPLICATION"]->ThrowException($ex->getMessage());
            $this->DoUninstall();
            return false;
        }

        return true;
    }

    public function DoUninstall() {

        # удаляем зависимости модуля
        $this->deleteModuleDependencies();

        # удаление почтовых сообщений
        $this->deleteMailMessages();

        # удаление таблиц higloadblock
        $this->deleteHighloadblockTables();

        # удаление полей для пользователей
        $this->deleteUserFields();

        # удаление группы "Агенты"
        $this->deleteAgentGroup();

        # удаление группы "Клиенты"
        $this->deleteClientGroup();

        # удаление файлов
        $this->deleteFiles();

        # удаление параметров модуля
        $this->deleteOptions();

        ModuleManager::unRegisterModule($this->MODULE_ID);

        return true;
    }

    public function addModuleDependencies () {

        RegisterModuleDependences("main", "OnBeforeUserSimpleRegister", $this->MODULE_ID, "\\travelsoft\\booking\\sts\\EventsHandlers", "bxOnBeforeUserSimpleRegister");
        RegisterModuleDependences("main", "OnAfterUserSimpleRegister", $this->MODULE_ID, "\\travelsoft\\booking\\sts\\EventsHandlers", "bxOnAfterUserSimpleRegister");
        RegisterModuleDependences("main", "OnBeforeUserRegister", $this->MODULE_ID, "\\travelsoft\\booking\\sts\\EventsHandlers", "bxOnBeforeUserRegister");
        RegisterModuleDependences("main", "OnAfterUserRegister", $this->MODULE_ID, "\\travelsoft\\booking\\sts\\EventsHandlers", "bxOnAfterUserRegister");
        RegisterModuleDependences("main", "OnBeforeUserUpdate", $this->MODULE_ID, "\\travelsoft\\booking\\sts\\EventsHandlers", "bxOnBeforeUserUpdate");
        RegisterModuleDependences("main", "OnAfterUserUpdate", $this->MODULE_ID, "\\travelsoft\\booking\\sts\\EventsHandlers", "bxOnAfterUserUpdate");
        RegisterModuleDependences("main", "OnAfterUserLogin", $this->MODULE_ID, "\\travelsoft\\booking\\sts\\EventsHandlers", "bxOnAfterUserLogin");
        RegisterModuleDependences("main", "OnAfterUserLogout", $this->MODULE_ID, "\\travelsoft\\booking\\sts\\EventsHandlers", "bxOnAfterUserLogout");
        RegisterModuleDependences("main", "OnBeforeUserChangePassword", $this->MODULE_ID, "\\travelsoft\\booking\\sts\\EventsHandlers", "bxOnBeforeUserChangePassword");
        //RegisterModuleDependences("main", "OnBeforeEventSend", $this->MODULE_ID, "\\travelsoft\\booking\\sts\\EventsHandlers", "bxOnBeforeEventSend");
        RegisterModuleDependences("main", "OnSendUserInfo", $this->MODULE_ID, "\\travelsoft\\booking\\sts\\EventsHandlers", "bxOnSendUserInfo");

    }

    public function deleteModuleDependencies () {

        UnRegisterModuleDependences("main", "OnBeforeUserSimpleRegister", $this->MODULE_ID, "\\travelsoft\\booking\\sts\\EventsHandlers", "bxOnBeforeUserSimpleRegister");
        UnRegisterModuleDependences("main", "OnAfterUserSimpleRegister", $this->MODULE_ID, "\\travelsoft\\booking\\sts\\EventsHandlers", "bxOnAfterUserSimpleRegister");
        UnRegisterModuleDependences("main", "OnBeforeUserRegister", $this->MODULE_ID, "\\travelsoft\\booking\\sts\\EventsHandlers", "bxOnBeforeUserRegister");
        UnRegisterModuleDependences("main", "OnAfterUserRegister", $this->MODULE_ID, "\\travelsoft\\booking\\sts\\EventsHandlers", "bxOnAfterUserRegister");
        UnRegisterModuleDependences("main", "OnBeforeUserUpdate", $this->MODULE_ID, "\\travelsoft\\booking\\sts\\EventsHandlers", "bxOnBeforeUserUpdate");
        UnRegisterModuleDependences("main", "OnAfterUserUpdate", $this->MODULE_ID, "\\travelsoft\\booking\\sts\\EventsHandlers", "bxOnAfterUserUpdate");
        UnRegisterModuleDependences("main", "OnAfterUserLogin", $this->MODULE_ID, "\\travelsoft\\booking\\sts\\EventsHandlers", "bxOnAfterUserLogin");
        UnRegisterModuleDependences("main", "OnAfterUserLogout", $this->MODULE_ID, "\\travelsoft\\booking\\sts\\EventsHandlers", "bxOnAfterUserLogout");
        UnRegisterModuleDependences("main", "OnBeforeUserChangePassword", $this->MODULE_ID, "\\travelsoft\\booking\\sts\\EventsHandlers", "bxOnBeforeUserChangePassword");
        //UnRegisterModuleDependences("main", "OnBeforeEventSend", $this->MODULE_ID, "\\travelsoft\\booking\\sts\\EventsHandlers", "bxOnBeforeEventSend");
        UnRegisterModuleDependences("main", "OnSendUserInfo", $this->MODULE_ID, "\\travelsoft\\booking\\sts\\EventsHandlers", "bxOnSendUserInfo");

    }

    public function addOptions() {
        Option::set($this->MODULE_ID, "HOTELS_HL");
        Option::set($this->MODULE_ID, "URL_OPERATOR");
        Option::set($this->MODULE_ID, "AUTO_ACTIVATION_AGENTS");
    }

    public function deleteOptions() {
        Option::delete($this->MODULE_ID, array("name" => "HOTELS_HL"));
        Option::delete($this->MODULE_ID, array("name" => "URL_OPERATOR"));
        Option::delete($this->MODULE_ID, array("name" => "AUTO_ACTIVATION_AGENTS"));
    }

    public function createHighloadblockTables() {

        foreach ($this->highloadblocksFiles as $file) {

            $arr = include "highloadblocks/" . $file;

            $result = Bitrix\Highloadblock\HighloadBlockTable::add(array(
                'NAME' => $arr["table_data"]["NAME"],
                'TABLE_NAME' => $arr["table"],
//                        'LANGS' => $arr["table_data"]["LANGS"]
            ));

            if (!$result->isSuccess()) {
                throw new Exception($arr["table_data"]['ERR'] . "<br>" . implode("<br>", (array) $result->getErrorMessages()));
            }

            $table_id = $result->getId();

            Option::set($this->MODULE_ID, $arr["table_data"]["OPTION_PARAMETER"], $table_id);

            $arr_fields = $arr["fields"];

            $oUserTypeEntity = new CUserTypeEntity();

            foreach ($arr_fields as $arr_field) {

                $arr_field["ENTITY_ID"] = str_replace("{{table_id}}", $table_id, $arr_field["ENTITY_ID"]);

                if (!$oUserTypeEntity->Add($arr_field)) {
                    throw new Exception("Возникла ошибка при добавлении свойства " . $arr_field["ENTITY_ID"] . "[" . $arr_field["FIELD_NAME"] . "]" . $oUserTypeEntity->LAST_ERROR);
                }
            }

            if (isset($arr["items"]) && !empty($arr["items"])) {

                $entity   = Bitrix\Highloadblock\HighloadBlockTable::compileEntity(
                    Bitrix\Highloadblock\HighloadBlockTable::getById($table_id)->fetch());
                $class = $entity->getDataClass();
                foreach ($arr["items"] as $item) {
                    $class::add($item);
                }
            }
        }
    }

    public function deleteHighloadblockTables() {

        foreach ($this->highloadblocksFiles as $file) {

            $arr = include "highloadblocks/" . $file;

            $table_id = Option::get($this->MODULE_ID, $arr["table_data"]["OPTION_PARAMETER"]);
            if ($table_id > 0) {
                Bitrix\Highloadblock\HighloadBlockTable::delete($table_id);
            }
            Option::delete($this->MODULE_ID, array("name" => $arr["table_data"]["OPTION_PARAMETER"]));
        }
    }

    public function createMailMessages() {

        $et = new CEventType;
        if (!$et->Add(array(
            "LID" => "ru",
            "EVENT_NAME" => $this->eventType,
            "NAME" => "Сообщения модуля online бронирования туров",
            "DESCRIPTION" => ""
        ))
        ) {
            throw new Exception($et->LAST_ERROR);
        }

        $arFields = array(
            "ACTIVE" => "Y",
            "EVENT_NAME" => $this->eventType,
            "LID" => $this->getSiteId(),
            "EMAIL_FROM" => "#DEFAULT_EMAIL_FROM#",
            "EMAIL_TO" => "#EMAIL_TO#",
            "BODY_TYPE" => "html",
            "BCC" => '',
            "CC" => '',
            "REPLY_TO" => '',
            "IN_REPLY_TO" => '',
            "PRIORITY" => '',
            "FIELD1_NAME" => '',
            "FIELD1_VALUE" => '',
            "FIELD2_NAME" => '',
            "FIELD2_VALUE" => '',
            "SITE_TEMPLATE_ID" => '',
            "ADDITIONAL_FIELD" => array(),
            "LANGUAGE_ID" => ''
        );

        $emess = new CEventMessage;
        foreach ($this->emailsFiles as $file) {

            $arr = include "emails/" . $file;

            $arFields["MESSAGE"] = $arr["MESSAGE"];
            $arFields["SUBJECT"] = $arr["SUBJECT"];

            if (!($id = $emess->Add($arFields))) {
                throw new Exception($emess->LAST_ERROR);
            }

            Option::set($this->MODULE_ID, $arr["OPTION_PARAMETER"], $id);
        }
    }

    public function deleteMailMessages() {

        $emess = new CEventMessage;
        $dbMess = $emess->GetList($by = "site_id", $order = "desc", array("TYPE_ID" => $this->eventType));
        while ($mess = $dbMess->Fetch()) {
            $emess->Delete($mess['ID']);
        }
        $et = new CEventType;
        $et->Delete($this->eventType);


        foreach ($this->emailsFiles as $file) {

            $arr = include "emails/" . $file;

            Option::delete($this->MODULE_ID, array('name' => $arr["OPTION_PARAMETER"]));
        }
    }

    public function addUserFields() {

        $arr_fields = include "user_fields.php";

        $oUserTypeEntity = new CUserTypeEntity();

        foreach ($arr_fields as $arr_field) {

            if (!$oUserTypeEntity->Add($arr_field)) {
                throw new Exception("Возникла ошибка при добавлении свойства " . $arr_field["ENTITY_ID"] . "[" . $arr_field["FIELD_NAME"] . "]" . $oUserTypeEntity->LAST_ERROR);
            }
        }
    }

    public function deleteUserFields() {

        $arr_fields = include "user_fields.php";

        $oUserTypeEntity = new CUserTypeEntity();

        $arFilter = array("ENTITY_ID" => "USER");

        foreach ($arr_fields as $arr_field) {

            $arFilter["FIELD_NAME"][] = $arr_field["FIELD_NAME"];
        }

        $dbList = $oUserTypeEntity->GetList(array($by => $order), $arFilter);
        while ($arField = $dbList->Fetch()) {
            $oUserTypeEntity->Delete($arField["ID"]);
        }
    }

    public function addAgentGroup() {

        Option::set($this->MODULE_ID, "AGENT_GROUP_ID", $this->__addGroup(array(
            "NAME" => "Агент",
            "STRING_ID" => "agent"
        )));

    }

    public function deleteAgentGroup() {
        $this->__deleteGroup(Option::get($this->MODULE_ID, "AGENT_GROUP_ID"));
        Option::delete($this->MODULE_ID, array("name" => "AGENT_GROUP_ID"));
    }

    public function addClientGroup() {

        Option::set($this->MODULE_ID, "CLIENT_GROUP_ID", $this->__addGroup(array(
            "NAME" => "Клиент",
            "STRING_ID" => "client"
        )));

    }

    public function deleteClientGroup() {
        $this->__deleteGroup(Option::get($this->MODULE_ID, "CLIENT_GROUP_ID"));
        Option::delete($this->MODULE_ID, array("name" => "CLIENT_GROUP_ID"));
    }

    private function __loadHigloadblockFiles() {

        $this->highloadblocksFiles = $this->__loadFiles("highloadblocks");
    }

    private function __loadMailFiles() {

        $this->emailsFiles = $this->__loadFiles("emails");
    }

    private function __loadFiles($dirName) {

        $directory = __DIR__ . "/" . $dirName;
        return array_diff(scandir($directory), array('..', '.'));
    }

    public function getSiteId() {

        static $arSites = array();

        if (!empty($arSites)) {
            return $arSites;
        }

        $dbSites = CSite::GetList($by = "sort", $order = "asc");

        while ($arSite = $dbSites->Fetch()) {
            $arSites[] = $arSite['ID'];
        }

        return $arSites;
    }

    private function __addGroup ($arFields = array()) {

        $group = new CGroup;
        $arFields__ = Array(
            "ACTIVE" => "Y",
            "C_SORT" => 100
        );

        $NEW_GROUP_ID = $group->Add(array_merge($arFields__, $arFields));
        if (strlen($group->LAST_ERROR) > 0) {
            throw new Exception($group->LAST_ERROR);
        }

        return $NEW_GROUP_ID;
    }

    private function __deleteGroup ($id) {

        $group = new CGroup;
        $group->Delete($id);
    }

}
