<?
$interface_include_path = '/local/php_interface/include';

/* Вспомогательные классы */
\Bitrix\Main\Loader::registerAutoLoadClasses(
        null, 
        array(
            "travelsoft\Ajax" => $interface_include_path ."/classes/ajax.php",
            "travelsoft\BxEventsHandlers" => $interface_include_path ."/classes/bxeventshandlers.php",
            "travelsoft\Router" => $interface_include_path ."/classes/router.php",
            "travelsoft\ExampleController" => $interface_include_path ."/classes/ExampleController.php",
        )
    );
