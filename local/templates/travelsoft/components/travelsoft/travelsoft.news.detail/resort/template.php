<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$this->addExternalJS(SITE_TEMPLATE_PATH . "/js/MapAdapter/MapAdapter.js");

/*\Bitrix\Main\Loader::includeModule('travelsoft.currency');
$converter = \travelsoft\currency\factory\Converter::getInstance();*/
$htmlMapID = 'map';
?>
<?$properties = $arResult["DISPLAY_PROPERTIES"]; ?>
<?$upCountryName = ucwords($arResult["COUNTRY"]["CODE"]);
$arImgFlag = getSrc($arResult["COUNTRY"]["PROPERTY_FLAG_VALUE"], array("width"=>24,"height"=>24), SITE_TEMPLATE_PATH.'/flags/'.$upCountryName.'.png', 1, false);?>

<div class="country-menu">
    <div class="container">
        <p class="country-title" style="background-image:url(<?=$arImgFlag[0]?>)"><a href="<?=$arParams["PARAMS"]["SECTION_PAGE"]?>"><?=$arResult["COUNTRY"]["NAME"]?></a></p>
        <p class="country-title-mobile" style="background-image:url(<?=$arImgFlag[0]?>)">
            <a href="#toggle" id="country-menu-button" class="flashlink"><?=$arResult["COUNTRY"]["NAME"]?></a>
        </p>
        <ul class="tabs">
            <?if($arResult["CITIES"]):?>
                <li class="active"><a href="<?=$arParams["PARAMS"]["SECTION_PAGE"]?>resorts/"><span><?=GetMessage('RESORTS')?></span></a></li>
            <?endif?>
            <?if($arResult["HOTELS"]):?>
                <li><a href="<?=$arParams["PARAMS"]["SECTION_PAGE"]?>hotels/"><span><?=GetMessage('HOTELS')?></span></a></li>
            <?endif?>
            <?if($arResult["VISAS"]):?>
                <li><a href="<?=$arParams["PARAMS"]["SECTION_PAGE"]?>visa/"><span><?=GetMessage('VISAS')?></span></a></li>
            <?endif?>
            <?if(!empty($arResult["COUNTRY"]["PROPERTY_TOURIST_GUIDE_VALUE"])):?>
                <li><a href="<?=$arParams["PARAMS"]["SECTION_PAGE"]?>tourist-info/"><span><?=GetMessage('TOURIST_GUIDE')?></span></a></li>
            <?endif?>

            <!--<li><a href="https://online.solemare.by/online/Extra/QuotedDynamic.aspx" class="important" onclick="return !window.open(this.href)"><span>Подбор тура online</span></a></li>-->

        </ul>
    </div>
</div>
<!--<div class="fast-bar">
    <div class="container">
        <div id="curr">
            <ul>
                <li class="text">Цены в:</li>
                <li class="byn"><a href="https://solemare.by/tours/italy/about#byn" class="flashlink"><span>BYN</span></a></li>
                <li class="usd"><a href="https://solemare.by/tours/italy/about#usd" class="flashlink"><span>USD</span></a></li>
                <li class="eur"><a href="https://solemare.by/tours/italy/about#eur" class="flashlink"><span>EUR</span></a></li>
                <li class="gbp"><a href="https://solemare.by/tours/italy/about#gbp" class="flashlink"><span>GBP</span></a></li>
            </ul>
        </div>
        <ul>
        </ul>
    </div>
</div>-->
<div class="container resort col2">
    <h1 class=""><?=$arResult["NAME"]?></h1>
    <div class="sidebar">
        <?if(!empty($properties["WEATHER"]["VALUE"]["TEXT"])):?>
            <div class="weather">
                <?=$properties["WEATHER"]["DISPLAY_VALUE"]?>
            </div>
        <?endif?>
        <?=$arParams["TOURS_RIGHT_FOR_COUNTRY"]?>
        <div class="widgets">
            <?=$arParams["SIDEBAR_SUBSCRIPTION"]?>
            <?=$arParams["SIDEBAR_WIDGETS"]?>
        </div>
    </div>
    <?if($properties["PICTURES"]["VALUE"]):?>
        <?=$arParams["SLIDER"]?>
    <?endif?>
    <div class="content">
        <?/*if($arResult["DIRECTIONS"]):*/?><!--
            <h4><?/*=GetMessage('SEARCH_FORM_TITLE')*/?></h4>
            <?/*=$arParams["SEARCH_FORM_STS"]*/?>
            <?/*=$arParams["PAGE_SEARCH_RESULT_STS"]*/?>
        --><?/*endif*/?>
        <?if(!empty($properties["DESC"]["VALUE"]["TEXT"])):?>
            <h2 class="title"><?=$properties["DESC"]["NAME"]?></h2>
            <?=$properties["DESC"]["DISPLAY_VALUE"]?>
        <?endif?>
        <?if(!empty($properties["VIDEO"]["VALUE"])):?>
            <iframe width="1040" height="350" src="https://www.youtube.com/embed/<?=$properties["VIDEO"]["VALUE"]?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
        <?endif?>
        <?if(!empty($properties["LANGUAGE"]["VALUE"]["TEXT"])):?>
            <h2 class="title"><?=$properties["LANGUAGE"]["NAME"]?></h2>
            <?=$properties["LANGUAGE"]["DISPLAY_VALUE"]?>
        <?endif?>
        <?if(!empty($properties["CLIMATE"]["VALUE"]["TEXT"])):?>
            <h2 class="title"><?=$properties["CLIMATE"]["NAME"]?></h2>
            <?=$properties["CLIMATE"]["DISPLAY_VALUE"]?>
        <?endif?>
        <?if(!empty($properties["GEOGRAFY"]["VALUE"]["TEXT"])):?>
            <h2 class="title"><?=$properties["GEOGRAFY"]["NAME"]?></h2>
            <?=$properties["GEOGRAFY"]["DISPLAY_VALUE"]?>
        <?endif?>
        <?if(!empty($properties["POPULATION"]["VALUE"]["TEXT"])):?>
            <h2 class="title"><?=$properties["POPULATION"]["NAME"]?></h2>
            <?=$properties["POPULATION"]["DISPLAY_VALUE"]?>
        <?endif?>
        <div class="block_buttom">
            <div class="btn"><a href="" ><span><?=str_replace("#RESORT_NAME#", $arResult["NAME"],GetMessage('TOURS_FOR_RESORT'))?></span></a></div>
            <div class="btn"><a href="/hotels/?arFilterHotels_113_<?=abs(crc32($arResult["ID"]))?>=Y&set_filter=Показать" ><span><?=str_replace("#RESORT_NAME#", $arResult["NAME"],GetMessage('HOTELS_FOR_RESORT'))?></span></a></div>
        </div>
        <div style="clear: both"></div>
    </div>
    <div class="content mb-20">
        <? if (!empty($arResult["ROUTE_INFO"])): ?>

            <h2 class="title"><?=GetMessage("MAP")?></h2>
            <div id="<?= $htmlMapID ?>" style="width: 100%;height: 415px;"></div>

            <script>
                var loc = JSON.parse('<?=\Bitrix\Main\Web\Json::encode($arResult["ROUTE_INFO"])?>');

                $(document).ready(function () {
                    var mapAdapter = new MapAdapter({
                        map_id: "map",
                        center: {
                            lat: loc["lat"],
                            lng: loc["lng"]
                        },
                        object: "ymaps",
                        zoom: 16
                    });
                    mapAdapter.addMarker(loc);
                });
            </script>
        <?endif?>
    </div>
    <?=$arParams["TOURS_SLIDER_FOR_COUNTRY"]?>
</div>

<?=$arParams["COUNTRIES_LIST"]?>