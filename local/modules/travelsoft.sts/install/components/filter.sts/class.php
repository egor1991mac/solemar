<?php

Bitrix\Main\Loader::includeModule('travelsoft.sts');
Bitrix\Main\Loader::includeModule('highloadblock');

use Bitrix\Highloadblock\HighloadBlockTable as HL;
use Bitrix\Main\Entity;


class  TravelsoftFilterSTSComponent extends CBitrixComponent {

    public $dr;

    // include component_prolog.php
    public function includeComponentProlog()
    {
        $file = "component_prolog.php";

        $template_name = $this->GetTemplateName();

        if ($template_name == "")
            $template_name = ".default";

        $relative_path = $this->GetRelativePath();

        $this->dr = Bitrix\Main\Application::getDocumentRoot();

        $file_path = $this->dr . SITE_TEMPLATE_PATH . "/components" . $relative_path . "/" . $template_name . "/" . $file;

        $arParams = &$this->arParams;

        if(file_exists($file_path))
            require $file_path;
        else {

            $file_path = $this->dr . "/bitrix/templates/.default/components" . $relative_path . "/" . $template_name . "/" . $file;

            if(file_exists($file_path))
                require $file_path;
            else {
                $file_path = $this->dr . $this->__path . "/templates/" . $template_name . "/" . $file;
                if(file_exists($file_path))
                    require $file_path;
                else {

                    $file_path = $this->dr . "/local/components" . $relative_path . "/templates/" . $template_name . "/" . $file;

                    if(file_exists($file_path))
                        require $file_path;
                }

            }
        }
    }

    /**
     * Проверка параметров компонента
     * @throws Exception
     */
    protected function checkInputParameters () {

        $this->arParams['GET_PARAMS_STS'] = (array)$this->arParams['GET_PARAMS_STS'];
        $this->arParams['PARAMS_STS_FILTER'] = (array)$this->arParams['PARAMS_STS_FILTER'];

        if ($this->arParams['ACTION_URL'] == "") {
            throw new Exception("Укажите путь к странице поиска");
        }

    }

    public function getElementsHL ($hlblock_id) {

        $paramsHL = array();

        $hlblock = HL::getById($hlblock_id)->fetch();
        $paramsHL["NAME"] = $hlblock["NAME"];
        $entity = HL::compileEntity($hlblock);
        $entity_data_class = $entity->getDataClass();
        $rsData = $entity_data_class::getList(array(
            "select" => array('*'),
            "order" => array("ID" => "ASC")
        ));
        $i = 0;
        while($arData = $rsData->Fetch())
        {
            $paramsHL["ITEMS"][$i] = $arData;
            if (isset($arData["UF_BX_ID"])) {
                $res = \CIBlockElement::GetByID($arData["UF_BX_ID"]);
                if ($ar_res = $res->GetNext()) {
                    $paramsHL["ITEMS"][$i]["UF_VALUE"] = $ar_res["NAME"];
                }
            }
            $i++;
        }

        $paramsHL["ITEMS"] = \travelsoft\sts\Utils::customMultiSort($paramsHL["ITEMS"], "UF_VALUE");

        return $paramsHL;

    }

    public function getElementsIB ($iblock_id) {

        $paramsIB = array();

        $iblock = \CIBlockElement::GetList(Array("SORT"=>"ASC"), Array("IBLOCK_ID"=>(int)$iblock_id,"ACTIVE"=>"Y","PROPERTY_SHOW_FILTER_ONLINE_SEARCH_VALUE"=>"Y","!PROPERTY_MT_KEY"=>false), false, false, Array("ID","IBLOCK_ID","IBLOCK_CODE","NAME","PROPERTY_SHOW_FILTER_ONLINE_SEARCH"));
        $i = 0;
        while($ar_iblock = $iblock->GetNext())
        {
            $paramsIB["NAME"] = $ar_iblock["IBLOCK_CODE"];
            $paramsIB["ITEMS"][$i]["ID"] = $ar_iblock["ID"];
            $paramsIB["ITEMS"][$i]["NAME"] = $ar_iblock["NAME"];
            $i++;
        }

        $paramsIB["ITEMS"] = \travelsoft\sts\Utils::customMultiSort($paramsIB["ITEMS"], "NAME");

        return $paramsIB;

    }

    /**
     * Получение параметров фильтра подбора тура
     */
    protected function getFilterParameters () {

        $params = array();

        if(!empty($this->arParams['PARAMS_STS_FILTER'])) {

            foreach ($this->arParams['PARAMS_STS_FILTER'] as $keyIB) {

                $params[$keyIB] = $this->getElementsIB($keyIB);

            }

        }

        return $params;

    }


    public function executeComponent () {

        $this->includeComponentProlog();

        $this->checkInputParameters();

        $this->arResult["ACTION_URL"] = $this->arParams["ACTION_URL"];

        $this->arResult["PROPERTIES"] = $this->getFilterParameters();

        $this->arResult["REQUEST"] = array();
        $this->arResult["REQUEST_PROP"] = array();

        if(!empty($this->arParams['GET_PARAMS_STS'])){

            $this->arResult["REQUEST"] = array(

                "cityFrom" => (int)$this->arParams['GET_PARAMS_STS']['cityFrom'],
                "country" => (int)$this->arParams['GET_PARAMS_STS']['country'],
                "cities" => isset($this->arParams['GET_PARAMS_STS']['cities']) && !empty($this->arParams['GET_PARAMS_STS']['cities']) ? (array)$this->arParams['GET_PARAMS_STS']['cities'] : array(),
                "tourTypes" => isset($this->arParams['GET_PARAMS_STS']['tourTypes']) && !empty($this->arParams['GET_PARAMS_STS']['tourTypes']) ? (array)$this->arParams['GET_PARAMS_STS']['tourTypes'] : array(),
                "dateFrom" => $this->arParams['GET_PARAMS_STS']['dateFrom'],
                "dateTo" => $this->arParams['GET_PARAMS_STS']['dateTo'],
                "nightFrom" => (int)$this->arParams['GET_PARAMS_STS']['nightFrom'],
                "nightTo" => (int)$this->arParams['GET_PARAMS_STS']['nightTo'],
                "adults" => (int)$this->arParams['GET_PARAMS_STS']['adults'],
                "children" => (int)$this->arParams['GET_PARAMS_STS']['children'],

            );
            if(!isset($_GET["del_filter"])) {
                $this->arResult["REQUEST_PROP"] = array(
                    "tours" => (array)$this->arParams['GET_PARAMS_STS']['tours'],
                    "hotels" => (array)$this->arParams['GET_PARAMS_STS']['hotels'],
                    "stars" => (array)$this->arParams['GET_PARAMS_STS']['stars'],
                    "meals" => (array)$this->arParams['GET_PARAMS_STS']['meals'],
                    "currency" => isset($this->arParams['GET_PARAMS_STS']['currency']) && !empty($this->arParams['GET_PARAMS_STS']['currency']) ? $this->arParams['GET_PARAMS_STS']['currency'] : "BYN",
                );

                if(isset($this->arParams['GET_PARAMS_STS']["priceFrom"]) && !empty($this->arParams['GET_PARAMS_STS']["priceFrom"]) && isset($this->arResult["REQUEST_PROP"]["currency"]) && !empty($this->arResult["REQUEST_PROP"]["currency"])){

                    $this->arResult["REQUEST_PROP"]["priceFrom"] = $this->arParams['GET_PARAMS_STS']["priceFrom"];
                    $this->arResult['REQUEST_PROP']["priceTo"] = $this->arParams['GET_PARAMS_STS']['priceTo'] && !empty($this->arParams['GET_PARAMS_STS']['priceTo']) ? $this->arParams['GET_PARAMS_STS']['priceTo'] : $this->arParams['GET_PARAMS_STS']["priceFrom"];

                } elseif(isset($this->arParams['GET_PARAMS_STS']["priceTo"]) && !empty($this->arParams['GET_PARAMS_STS']["priceTo"]) && isset($this->arResult["REQUEST_PROP"]["currency"]) && !empty($this->arResult["REQUEST_PROP"]["currency"])) {

                    $this->arResult["REQUEST_PROP"]["priceTo"] = $this->arParams['GET_PARAMS_STS']["priceTo"];
                    $this->arResult["REQUEST_PROP"]["priceFrom"] = $this->arParams['GET_PARAMS_STS']['priceFrom'] && !empty($this->arParams['GET_PARAMS_STS']['priceFrom']) ? $this->arParams['GET_PARAMS_STS']['priceFrom'] : $this->arParams['GET_PARAMS_STS']["priceTo"];

                }

            }

        }
        else {

            $date = \travelsoft\sts\Settings::getDefaultSearchDate();
            $night = \travelsoft\sts\Settings::getDefaultSearchNight();

            $this->arResult["REQUEST"] = array(

                "cityFrom" => \travelsoft\sts\Settings::getDefaultCityFrom(),
                "country" => \travelsoft\sts\Settings::getDefaultCountry(),
                "cities" => array(),
                "tourTypes" => array(),
                "dateFrom" => $date['dateFrom'],
                "dateTo" => $date['dateTo'],
                "nightFrom" => $night['nightFrom'],
                "nightTo" => $night['nightTo'],
                "adults" => \travelsoft\sts\Settings::getDefaultAdults(),
                "children" => \travelsoft\sts\Settings::getDefaultChildren(),

            );

        }

        $this->arResult["OPERATORS"] = array_keys(\travelsoft\sts\stores\Operators::get());

        $this->IncludeComponentTemplate();

    }

}