<?php


return array(
    "table" => "tsfood",
    "table_data" => array(
        "NAME" => "TSFOOD",
        "ERR" => "Ошибка при создании highloadblock'a типов питания",
        "LANGS" => array(
            "ru" => 'Питание',
            "en" => "Food"
        ),
        "OPTION_PARAMETER" => "FOOD_HL"
    ),
    "fields" => array(
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_BX_ID",
            "USER_TYPE_ID" => 'integer',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Битрикс ID',
                'en' => 'Bitrix ID',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Битрикс ID',
                'en' => 'Bitrix ID',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Битрикс ID',
                'en' => 'Bitrix ID',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Битрикс ID" ',
                'en' => 'An error in completing the field "Bitrix ID"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
    )
);
