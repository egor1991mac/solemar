<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?$properties = $arResult["DISPLAY_PROPERTIES"];?>
<?$upCountryName = ucwords($arResult["COUNTRY"]["CODE"]);
$arImgFlag = getSrc($arResult["COUNTRY"]["PROPERTY_FLAG_VALUE"], array("width"=>24,"height"=>24), SITE_TEMPLATE_PATH.'/flags/'.$upCountryName.'.png', 1, false);?>

<div class="country-menu">
    <div class="container">
        <p class="country-title" style="background-image:url(<?=$arImgFlag[0]?>)"><a href="<?=$arParams["PARAMS"]["SECTION_PAGE"]?>"><?=$arResult["COUNTRY"]["NAME"]?></a></p>
        <p class="country-title-mobile" style="background-image:url(<?=$arImgFlag[0]?>)">
            <a href="#toggle" id="country-menu-button" class="flashlink"><?=$arResult["COUNTRY"]["NAME"]?></a>
        </p>
        <ul class="tabs">
            <?if($arResult["CITIES"]):?>
                <li><a href="<?=$arParams["PARAMS"]["SECTION_PAGE"]?>resorts/"><span><?=GetMessage('RESORTS')?></span></a></li>
            <?endif?>
            <?if($arResult["HOTELS"]):?>
                <li><a href="<?=$arParams["PARAMS"]["SECTION_PAGE"]?>hotels/"><span><?=GetMessage('HOTELS')?></span></a></li>
            <?endif?>
            <?if($arResult["VISAS"]):?>
                <li><a href="<?=$arParams["PARAMS"]["SECTION_PAGE"]?>visa/"><span><?=GetMessage('VISAS')?></span></a></li>
            <?endif?>
            <?if(!empty($arResult["COUNTRY"]["PROPERTY_TOURIST_GUIDE_VALUE"])):?>
                <li><a href="<?=$arParams["PARAMS"]["SECTION_PAGE"]?>tourist-info/"><span><?=GetMessage('TOURIST_GUIDE')?></span></a></li>
            <?endif?>

            <!--<li><a href="https://online.solemare.by/online/Extra/QuotedDynamic.aspx" class="important" onclick="return !window.open(this.href)"><span>Подбор тура online</span></a></li>-->

        </ul>
    </div>
</div>
<!--<div class="fast-bar">
    <div class="container">
        <div id="curr">
            <ul>
                <li class="text">Цены в:</li>
                <li class="byn"><a href="https://solemare.by/tours/italy/about#byn" class="flashlink"><span>BYN</span></a></li>
                <li class="usd"><a href="https://solemare.by/tours/italy/about#usd" class="flashlink"><span>USD</span></a></li>
                <li class="eur"><a href="https://solemare.by/tours/italy/about#eur" class="flashlink"><span>EUR</span></a></li>
                <li class="gbp"><a href="https://solemare.by/tours/italy/about#gbp" class="flashlink"><span>GBP</span></a></li>
            </ul>
        </div>
        <ul>
        </ul>
    </div>
</div>-->
<div class="container resort col2">
    <h1 class="nopadding"><?=$arResult["NAME"]?></h1>
    <div class="sidebar">
        <?if(isset($arResult["PAGES_INFO"]) && !empty($arResult["PAGES_INFO"])):?>
            <ul>
                <?foreach ($arResult["PAGES_INFO"] as $page):?>
                    <?$page_name = !empty($page["BUTTON_NAME"]) ? $page["BUTTON_NAME"] : $page["NAME"];?>
                    <?if($page["ID"] == $arResult["ID"]):?>
                        <li class="cat-item cat-item-1 nav-tab-item active">
                            <span><?=$page_name?></span>
                        </li>
                    <?else:?>
                        <li class="cat-item cat-item-1 nav-tab-item">
                            <a href="<?=$arParams["DETAIL_PAGE"]?><?=$page["CODE"].'/'?>"><?=$page_name?></a>
                        </li>
                    <?endif?>
                <?endforeach;?>
            </ul>
        <?endif?>
        <div class="widgets">
            <?=$arParams["SIDEBAR_SUBSCRIPTION"]?>
            <?=$arParams["SIDEBAR_WIDGETS"]?>
        </div>
    </div>
    <?if($properties["PICTURES"]["VALUE"]):?>
        <?=$arParams["SLIDER"]?>
    <?endif?>
    <div class="content">
        <?if(!empty($properties["DESC_TOP"]["VALUE"]["TEXT"])):?>
            <?=$properties["DESC_TOP"]["DISPLAY_VALUE"]?>
        <?endif?>
        <?/*if($arResult["DIRECTIONS"]):*/?><!--
            <h4><?/*=GetMessage('SEARCH_FORM_TITLE')*/?></h4>
            <?/*=$arParams["SEARCH_FORM_STS"]*/?>
            <?/*=$arParams["PAGE_SEARCH_RESULT_STS"]*/?>
        --><?/*endif*/?>
        <?if(!empty($properties["DESC_BOTTOM"]["VALUE"]["TEXT"])):?>
            <?=$properties["DESC_BOTTOM"]["DISPLAY_VALUE"]?>
        <?endif?>
        <div style="clear: both"></div>
    </div>
    <?=$arParams["TOURS_SLIDER_FOR_COUNTRY"]?>
</div>

<?=$arParams["COUNTRIES_LIST"]?>