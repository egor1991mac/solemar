<?php


return array(
    "table" => "tscities",
    "table_data" => array(
        "NAME" => "TSCITIES",
        "ERR" => "Ошибка при создании highloadblock'a городов",
        "LANGS" => array(
            "ru" => 'Города',
            "en" => "Cities"
        ),
        "OPTION_PARAMETER" => "CITIES_HL"
    ),
    "fields" => array(
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_BX_ID",
            "USER_TYPE_ID" => 'integer',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Битрикс ID',
                'en' => 'Bitrix ID',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Битрикс ID',
                'en' => 'Bitrix ID',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Битрикс ID',
                'en' => 'Bitrix ID',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Битрикс ID" ',
                'en' => 'An error in completing the field "Bitrix ID"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_CITYFROM",
            "USER_TYPE_ID" => 'boolean',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "0",
                'LABEL' => array(0 => "нет", 1 => "да"),
                "DISPLAY" => "CHECKBOX",
                'LABEL_CHECKBOX' => "да"
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Город вылета',
                'en' => 'City of departure',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Город вылета',
                'en' => 'City of departure',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Город вылета',
                'en' => 'City of departure',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Город вылета" ',
                'en' => 'An error in completing the field "City of departure"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
    )
);
