<?php
$MESS["EMPTY_CART"] = "Нет данных для оформления заказа";
$MESS["BOOKING_ERROR"] = "При попытке бронирования произошла ошибка. Обратитесь к администратору сайта";
$MESS["WRONG_EMAIL"] = "Введите корректный E-mail";
$MESS["WRONG_PHONE"] = "Введите телефон в указанном формате";
$MESS["WRONG_TOURIST"] = "Введите информацию о туристах";
$MESS["WRONG_SOME_TOURIST"] = "Информация о туристах введена неверно";
$MESS["WRONG_NAME"] = "Введите имя";
$MESS["WRONG_LAST_NAME"] = "Введите фамилию";
$MESS["WRONG_MALE"] = "Выберите пол";
$MESS["WRONG_BIRTHDATE"] = "Поле Дата рождения должно быть формата dd.mm.yyyy";
$MESS["WRONG_PASSPORT"] = "Введите номер и серию паспорта";
$MESS["WRONG_PASSPORT_DATE"] = "Введите дату окончания действия паспорта";
$MESS["WRONG_PASSPORT_ID"] = "Введите личный (идентификационный) номер паспорта";
$MESS["WRONG_CITIZENSHIP"] = "Выберите гражданство";
$MESS["WRONG_ACCEPT"] = "Необходимо согласиться с договором оферты";

$MESS["MUST_BE_AUTHORIZE_NOTIFY"] = "Бронирование доступно только для зарегистрированных пользователей."
        . " Авторизоваться или зарегистрироваться Вы можете в процессе заполнения полей или в шапке сайта при клике на ссылку вход";
$MESS["NO_ACTIVE_SERVICES_NOTIFY"] = "Нет активных услуг для бронирования";

$MESS["SERVICE_CHOOSE"] = "Выбор услуги";
$MESS["YOUR_ORDER_AND_PAY"] = "Ваш заказ и оплата";
$MESS["BOOKING_IS_DONE"] = "Бронирование закончено";
$MESS["CAN'T_BOOKING"] = "Данная услуга недоступна для бронирования";
$MESS["DATE_FROM"] = "Заезд";
$MESS["DATE_TO"] = "Выезд";
$MESS["ADULTS"] = "Взрослых";
$MESS["CHILDREN"] = "Детей";
$MESS["NAME"] = "Название";
$MESS["RATE"] = "Тариф";
$MESS["FOOD"] = "Питание";
$MESS["PRICE"] = "Цена";
$MESS["TOURIST"] = "Турист";
$MESS["TOURISTS"] = "Туристы";
$MESS["COMMENT"] = "Ваши пожелания";
$MESS["COMMENT_HINT"] = "Здесь вы можете указать ваши особые пожелания. Мы не можем гарантировать"
        . " выполнение всех особых пожеланий, но сделаем для этого все возможное.";
$MESS["TRANSFER_COMMENT"] = "Укажите данные о Вашем авиарейсе, поезде, автобусе или точный адрес отправления";
$MESS["TRANSFER_COMMENT_HINT"] = "Здесь вам необходимо указать точное время прибытия и тип транспортного средства.Ваши данные о времени, номере авиарейса, поезда, автобуса или точный адрес отправления очень важны для вашей встречи.";
$MESS["ACCEPT"] = "я согласен с условиями договора оферты";
$MESS["BOOKING_BTN"] = "Забронировать";
$MESS["NOTIFY"] = "Нажимая на кнопку \"Забронировать\" я подтверждаю, что прочитал и принимаю условия договора оферты.";

$MESS["DIALOG_EMPTY_EMAIL"] = "Пустое поле E-mail";
$MESS["DIALOG_CONFIRM_EMAIL_ERROR"] = "Некорректный E-mail подтверждения";
$MESS["DIALOG_CONFIRM_EMAIL_NOT_EQUAL_EMAIL"] = "Основной E-mail и E-mail для подтверждения не совпадают";
$MESS["DIALOG_PLACEHOLDER_CONFIRM_EMAIL"] = "Введите E-mail для подтверждения";
$MESS["DIALOG_DO_REGISTRATION_BTN"] = "Регистрация";
$MESS["DIALOG_DO_AUTHORIZE_BTN"] = "Авторизация";
$MESS["DIALOG_FORGOT_PASSWORD"] = "Забыли пароль ?";
$MESS["DIALOG_404"] = "System error. Пожалуйста свяжитесь с администратором (_404)";
$MESS["DIALOG_505"] = "System error. Пожалуйста свяжитесь с администратором (_505)";
$MESS["DIALOG_DEFAULT_ERROR_TEXT"] = "Error. Пожалуйста свяжитесь с администратором (_default)";
$MESS["DIALOG_STATUS_0"] = "Не удалось удалить позицию в корзине";
$MESS["DIALOG_STATUS_1"] = "Позиция в корзине успешно удалена";
$MESS["DIALOG_STATUS_2"] = "Введён некорректный E-mail";
$MESS["DIALOG_STATUS_3"] = "Вы авторизованы в системе";
$MESS["DIALOG_STATUS_4"] = "Произошла ошибка при попытке авторизации";
$MESS["DIALOG_STATUS_5"] = "Основной E-mail и E-mail для подтверждения не совпадают";
$MESS["DIALOG_STATUS_6"] = "Благодарим вас за регистрацию! На вашу электронную почту было выслано информационное письмо с вашими личными данными для входа";
$MESS["DIALOG_STATUS_7"] = "Произошла ошибка при попытке регистрации";
$MESS["DIALOG_STATUS_8"] = "Введите E-mail для подтверждения регистрации";
$MESS["DIALOG_STATUS_9"] = "Введите пароль для авторизации в системе";

# ФРАЗЫ ДЛЯ ПРОМОКОДОВ
$MESS["PROMO_TITLE"] = "ЕСТЬ ПРОМОКОД ?";
$MESS["PROMO_HINT"] = "Вы можете получить промокод в рамках акций, проводимых на нашем сайте или в социальных сетях. Полученный промокод необходимо внести в специальное поле в момент бронирования услуги.";
$MESS["APPLY_PROMO"] = "Применить";
$MESS["COST"] = "Стоимость";
$MESS["DISCOUNT"] = "Скидка";
$MESS["TOTAL_PEOPLE"] = "Количество туристов";
$MESS["TOTAL_COST"] = "Итого к оплате";
$MESS["DIALOG_STATUS_10"] = "Необходимо ввести промокод";
$MESS["DIALOG_STATUS_11"] = "Промокод не найден или срок его действия истек";
$MESS["DIALOG_STATUS_12"] = "Данный промокод уже применен";
$MESS["DIALOG_STATUS_13"] = "Промокод неактивен";
$MESS["DIALOG_STATUS_14"] = "Данный промокод закончился";
$MESS["DIALOG_STATUS_15"] = "Общая стоимость корзины недостаточна, чтобы применить данный промокод";
$MESS["DIALOG_STATUS_16"] = "Промокод не действует на данные виды услуг";
$MESS["DIALOG_STATUS_17"] = "Промокод успешно применен";
$MESS["DIALOG_STATUS_18"] = "Промокод недоступен для данной группы пользователей";
#

$MESS["PLACEHOLDER_YOUR_EMAIL"] = "E-mail";
$MESS["PLACEHOLDER_YOUR_PHONE"] = "Телефон";
$MESS["EMAIL_DESCRIPTION"] = "Пароль с доступом в личный кабинет будет выслан на Вашу почту";
$MESS["PHONE_DESCRIPTION"] = "В международном формате";
$MESS["PLACEHOLDER_NAME"] = "Имя";
$MESS["PLACEHOLDER_LAST_NAME"] = "Фамилия";
$MESS["PLACEHOLDER_BIRTHDATE"] = "Дата рождения";
$MESS["PLACEHOLDER_PASSPORT"] = "Серия и номер паспорта";
$MESS["PLACEHOLDER_PASSPORT_DATE"] = "Дата окончания срока действия паспорта";
$MESS["PLACEHOLDER_PASSPORT_ID"] = "Личный (идентификационный) номер паспорта";
$MESS["PLACEHOLDER_PASSPORT_ID_ADDTEXT"] = "(для граждан РБ)";
$MESS["PLACEHOLDER_CITIZEN"] = "Гражданство";
$MESS["PLACEHOLDER_COMMENT"] = "Введите текст";
$MESS["DAYS"] = "Дней";
$MESS["MAN"] = "Мужской";
$MESS["WOMAN"] = "Женский";
$MESS["SEX"] = "Пол";
$MESS["DDMMYYYY"] = "ДД.ММ.ГГГГ";
/*$MESS["YA_GOAL"] = "yaCounter42451344.reachGoal('book_cnc'); return true;";
$MESS["YA_GOAL_OPN_PAGE"] = "yaCounter42451344.reachGoal('book_open_cnc'); return true;";*/
$MESS['PROGRAM_BASKET_TITLE'] = 'Программа тура';
$MESS['BASKET_TITLE'] = 'Информация по туру';
$MESS['ADD_TOURIST_BTN_TITLE'] = '+ Добавить туриста';

$MESS['PROGRAM_ACCM_TITLE'] = 'Проживание';
$MESS['PROGRAM_ACCM_DATE'] = 'Даты';
$MESS['PROGRAM_ACCM_HOTEL'] = 'Отель';
$MESS['PROGRAM_ACCM_PLACEMENT'] = 'Размещение';
$MESS['PROGRAM_ACCM_FOOD'] = 'Питание';

$MESS['PROGRAM_AVIA_TITLE'] = 'Перелет';
$MESS['PROGRAM_AVIA_FLIGHT'] = 'Рейс';
$MESS['PROGRAM_AVIA_AIRLINE'] = 'Авиакомпания';
$MESS['PROGRAM_AVIA_PLACES'] = 'Места';
$MESS['PROGRAM_AVIA_AIRPORTDEP'] = 'Аэропорт вылета';
$MESS['PROGRAM_AVIA_DATETIMEDEP'] = 'Время вылета';
$MESS['PROGRAM_AVIA_AIRPORTARR'] = 'Аэропорт прибытия';
$MESS['PROGRAM_AVIA_DATETIMEARR'] = 'Время прибытия';

$MESS['PROGRAM_TRANSFER_TITLE'] = 'Трансфер';
$MESS['PROGRAM_TRANSFER_DATE'] = 'Дата';
$MESS['PROGRAM_TRANSFER_CITY'] = 'Город';
$MESS['PROGRAM_TRANSFER_ROUTE'] = 'Маршрут';
$MESS['PROGRAM_TRANSFER_TRANSPORT'] = 'Транспорт';

$MESS['PROGRAM_EXCURSION_TITLE'] = 'Экскурсии';
$MESS['PROGRAM_EXCURSION_DATE'] = 'Дата';
$MESS['PROGRAM_EXCURSION_CITY'] = 'Город';
$MESS['PROGRAM_EXCURSION_ROUTE'] = 'Маршрут';
$MESS['PROGRAM_EXCURSION_TRANSPORT'] = 'Транспорт';

$MESS['PROGRAM_INSURANCE_TITLE'] = 'Страховка';
$MESS['PROGRAM_INSURANCE_TEXT'] = 'В пакет тура входит #INSURANCE_NAME# от #INSURANCE_DATE# на #INSURANCE_DAYS#';

$MESS['PROGRAM_VISA_TITLE'] = 'Виза';
$MESS['PROGRAM_VISA_DATE'] = 'Дата начала';
$MESS['PROGRAM_VISA_COUNTRY'] = 'Страна';
$MESS['PROGRAM_VISA_NAME'] = 'Наименование';
$MESS['PROGRAM_VISA_DAYS'] = 'Продолжительность';

$MESS['PROGRAM_PRICE_TITLE'] = 'Стоимость';