<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if(!empty($arResult["ITEMS"])):?>

    <div class="other-posts">
        <div class="container">
            <?if(!empty($arParams["TEXT_TITLE"])):?>
                <h2><?=$arParams["TEXT_TITLE"]?></h2>
            <?endif?>
            <ul>
                <?foreach($arResult["ITEMS"] as $arItem):?>
                    <?
                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                    ?>
                    <?$arImg = getSrc($arItem["DISPLAY_PROPERTIES"]["PICTURES"]["VALUE"], array("width"=>286,"height"=>187), NO_PHOTO_286_187, 1);?>

                    <li id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                        <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="img" style="background-image:url('<?= $arImg[0] ?>')"></a>
                        <div class="info">
                            <span class="date"><?= $arItem["DISPLAY_ACTIVE_FROM"] ?></span>
                            <h3><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?= $arItem["NAME"] ?></a></h3>
                        </div>
                    </li>

                <?endforeach;?>
            </ul>

        </div>
    </div>

<?endif?>