$(function() {

    "use strict";

    $(function() {
        $('select').selectBoxIt({ autoWidth: false });

        $('#menu-button').click(function(e){
            $('#menu ul').toggleClass('hide');
            e.preventDefault();
        });

        $('#country-menu-button').click(function(e){
            $('.country-menu ul').toggleClass('hide');
            e.preventDefault();
        });

        $('#manager-menu-button').click(function(e){
            $('#tour-manager').toggleClass('hide');
            e.preventDefault();
        });

        $('#curr li').click(function(e){
            var i = $(this).index();

            if (i == 2 ) {
                setCurrency('usd');
            } else if (i == 3) {
                setCurrency('eur');
            } else if (i == 4) {
                setCurrency('gbp');
            } else {
                setCurrency('byn');
            }

            e.preventDefault();
        });

        var c = getCookie('currency');;
        if (c.length == 3 && $.inArray(c, ['usd', 'eur', 'gbp', 'byn'])) {
            setCurrency(c);
        }

        $('#orderByRate, #orderByPrice').click(function(e){
            e.preventDefault();
            setTourOrder($(this).is('#orderByPrice') + 0);
        });
    });

    function setCurrency (c) {
        $('body').removeClass('default byn usd eur gbp').addClass(c);
        setCookie('currency', c, 111);
    }

    function setTourOrder(order) {
        setCookie('tourOrder', order, 111);
        location.reload();
    }

    function setCookie(name, value, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 3600 * 1000));
        document.cookie = name + '=' + value + '; expires=' + d.toUTCString() + '; path=/';
    }

    function getCookie(name) {
        var ca = document.cookie.split(';');
        name = name + '=';
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return '';
    }

    $('.slides').slick({
        dots: true,
        infinite: false,
        arrows: false,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });
    $('.gallery').slick({
        dots: true,
        infinite: true,
        arrows: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
    });

    $('a.pp-open').fancybox({
        href: '#tour_request',
        padding: 0,
        topRatio: 0.2,
        helpers: {
            overlay: {
                locked: false
            }
        },
        beforeShow: function(x) {
            var isTour = $(this.element).hasClass('tour');
            $('div.pp .search').toggle(!isTour);
            $('div.pp .tour').toggle(isTour);
        }
    });

    $('a.pp-feedback').fancybox({
        href: '#feedback_form',
        padding: 0,
        topRatio: 0.2,
        helpers: {
            overlay: {
                locked: false
            }
        }
    });

    $('a.fancy').fancybox({
        padding: 0,
        topRatio: 0.2,
        helpers: {
            overlay: {
                locked: false
            }
        }
    });

    $('#tour_request form button').click(function(e){
        e.preventDefault();
        var f = $(this).closest('form');
        var p = $('#phone');
        var m = $('#email');

        p.toggleClass('error', !p.val());
        m.toggleClass('error', !m.val());

        if (!p.val() || !m.val()) {
            return;
        }

        $.post(f.attr('action'), f.serialize(), function(r){
            f.hide();
            $('#tour_request p.message').show();
        });
    });

    $('a.img').on('mouseenter mouseleave', function(e){
        $(this).parent().toggleClass('hover', e.type == 'mouseenter');
    });

    /*$("div.add-feedback form").submit(function(e){
        e.preventDefault();
        var f = $(this);
        var n = f.find("#Feedback-name");
        var q = f.find("#Feedback-question");

        n.toggleClass("error", !n.val());
        q.toggleClass("error", !q.val());

        if (n.val() && q.val()) {
            $.post(f.attr("action"), f.serialize(), function(data){
                if (data == 'ok') {
                    f.find("dl, button").hide();
                    f.closest(".add-feedback").find("p.message").show();
                    f[0].reset();
                }
            });
        }
    });*/

    var $allVideos = $("iframe[src^='//player.vimeo.com'], iframe[src^='//www.youtube.com'], object, embed"),
        $fluidEl = $("figure");

    $allVideos.each(function() {

        $(this)
        // jQuery .data does not work on object/embed elements
            .attr('data-aspectRatio', this.height / this.width)
            .removeAttr('height')
            .removeAttr('width');

    });

    $(window).resize(function() {

        var newWidth = $fluidEl.width();
        $allVideos.each(function() {

            var $el = $(this);
            $el
                .width(newWidth)
                .height(newWidth * $el.attr('data-aspectRatio'));

        });

    }).resize();

});