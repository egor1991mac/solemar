<?php

namespace travelsoft\booking\sts\stores;

use travelsoft\sts\adapters\Highloadblock;

/**
 * Класс для работы с таблицей заказов
 *
 * @author juliya.sharlova
 * @copyright (c) 2017, travelsoft
 */
class Orders extends Highloadblock
{
    protected static $storeName = 'orders';

}