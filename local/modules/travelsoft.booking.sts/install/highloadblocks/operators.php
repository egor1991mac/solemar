<?php

return array(
    "table" => "tsoperatorsbooking",
    "table_data" => array(
        "NAME" => "TSOPERATORSBOOKING",
        "ERR" => "Ошибка при создании highloadblock'a операторов бронирования",
        "LANGS" => array(
            "ru" => 'Операторы. Бронирование',
            "en" => "Operators. Booking"
        ),
        "OPTION_PARAMETER" => "OPERATORS_HL"
    ),
    "fields" => array(
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_OP_ID",
            "USER_TYPE_ID" => 'integer',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'ID оператора (модуль поиска)',
                'en' => 'Operator ID (search module)',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'ID оператора (модуль поиска)',
                'en' => 'Operator ID (search module)',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'ID оператора (модуль поиска)',
                'en' => 'Operator ID (search module)',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "ID оператора (модуль поиска)" ',
                'en' => 'An error in completing the field "Operator ID (search module)"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_GATEWAY",
            "USER_TYPE_ID" => 'string',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Шлюз',
                'en' => 'Gateway',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Шлюз',
                'en' => 'Gateway',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Шлюз',
                'en' => 'Gateway',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Шлюз" ',
                'en' => 'An error in completing the field "Gateway"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_CLASS",
            "USER_TYPE_ID" => 'string',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Класс обработчик бронирования оператора',
                'en' => 'Operator booking agent class',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Класс обрабочик бронирования оператора',
                'en' => 'Operator booking agent class',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Класс обрабочик бронирования оператора',
                'en' => 'Operator booking agent class',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Класс обрабочик бронирования оператора" ',
                'en' => 'An error in completing the field "Operator booking agent class"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_MAIN_OPERATOR",
            "USER_TYPE_ID" => 'boolean',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                "DEFAULT_VALUE" => "0",
                'LABEL' => array(0 => "нет", 1 => "да"),
                "DISPLAY" => "CHECKBOX",
                'LABEL_CHECKBOX' => "да",
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Основной оператор',
                'en' => 'Main operator',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Основной оператор',
                'en' => 'Main operator',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Основной оператор',
                'en' => 'Main operator',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Основной оператор" ',
                'en' => 'An error in completing the field "Main operator"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
    )
);
