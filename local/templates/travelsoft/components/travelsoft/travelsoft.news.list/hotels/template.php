<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
if(isset($arResult["COUNTRY"]) && isset($arParams["COUNTRY_ID"])):
    $upCountryName = ucwords($arResult["COUNTRY"]["CODE"]);
    $arImgFlag = getSrc($arResult["COUNTRY"]["PROPERTY_FLAG_VALUE"], array("width"=>24,"height"=>24), SITE_TEMPLATE_PATH.'/flags/'.$upCountryName.'.png', 1, false);?>
    <div class="country-menu">
        <div class="container">
            <p class="country-title" style="background-image:url(<?=$arImgFlag[0]?>)"><a href="<?=$arParams['FOLDER_URL']?>"><?=$arResult["COUNTRY"]["NAME"]?></a></p>
            <p class="country-title-mobile" style="background-image:url(<?=$arImgFlag[0]?>)">
                <a href="#toggle" href="<?=$arParams['FOLDER_URL']?>" id="country-menu-button" class="flashlink"><?=$arResult["COUNTRY"]["NAME"]?></a>
            </p>
            <ul class="tabs">
                <?if($arResult["CITIES"]):?>
                    <li><a href="<?=$arParams['FOLDER_URL']?>resorts/"><span><?=GetMessage('RESORTS')?></span></a></li>
                <?endif?>
                <?if($arResult["HOTELS"]):?>
                    <li class="active"><a href="<?=$arParams['FOLDER_URL']?>hotels/"><span><?=GetMessage('HOTELS')?></span></a></li>
                <?endif?>
                <?if($arResult["VISAS"]):?>
                    <li><a href="<?=$arParams['FOLDER_URL']?>visa/"><span><?=GetMessage('VISAS')?></span></a></li>
                <?endif?>
                <?if(!empty($arResult["COUNTRY"]["PROPERTY_TOURIST_GUIDE_VALUE"])):?>
                    <li><a href="<?=$arParams['FOLDER_URL']?>tourist-info/"><span><?=GetMessage('TOURIST_GUIDE')?></span></a></li>
                <?endif?>
            </ul>
        </div>
    </div>

    <div class="container col2">
        <h1 class=""><?=GetMessage('HOTELS')?></h1>
        <div class="content ts-wrap">
<?endif?>
        <div class="dates">
            <?if(!empty($arResult["ITEMS"])):?>

                <div class="hotels-list">
                    <ul>

                        <?foreach($arResult["ITEMS"] as $arItem):?>
                            <?
                            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                            ?>
                            <?$arImg = getSrc($arItem["DISPLAY_PROPERTIES"]["PICTURES"]["VALUE"], array("width"=>245,"height"=>170), NO_PHOTO_245_170, 1);?>

                            <li id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                                    <span class="img" style="background-image:url('<?= $arImg[0] ?>')"></span>
                                    <?if(!empty($arItem["DISPLAY_PROPERTIES"]["CATEGORY"]["VALUE"])):
                                        $star = (int)$arItem["DISPLAY_PROPERTIES"]["CATEGORY"]["VALUE"];?>
                                        <?if($star):?>
                                            <span class="stars"><?= $star ?></span>
                                        <?endif?>
                                    <?endif?>
                                    <b class="title"><span><?/*if(!empty($arItem["DISPLAY_PROPERTIES"]["TYPE"]["VALUE"])):*/?><!--<?/*=strip_tags($arItem["DISPLAY_PROPERTIES"]["TYPE"]["DISPLAY_VALUE"])*/?> --><?/*endif*/?><?= $arItem["NAME"] ?></span></b>
                                </a>
                            </li>

                        <?endforeach;?>

                    </ul>
                </div>
            <?endif?>
        </div>
<?if(isset($arResult["COUNTRY"]) && isset($arParams["COUNTRY_ID"])):?>
        </div>
    </div>
<?else:?>
</div>
    <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
        <div style="clear: both"></div>
        <?=$arResult["NAV_STRING"]?>
    <?endif;?>
<div>
<?endif?>
