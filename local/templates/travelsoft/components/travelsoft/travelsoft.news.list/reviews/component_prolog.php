<?
global $APPLICATION;

ob_start();
$APPLICATION->IncludeComponent(
    "travelsoft:travelsoft.feedback",
    "feedback",
    Array(
        /*"AJAX_MODE" => "Y",  // режим AJAX
        "AJAX_OPTION_SHADOW" => "N", // затемнять область
        "AJAX_OPTION_JUMP" => "N", // скроллить страницу до компонента.
        "AJAX_OPTION_STYLE" => "Y", // подключать стили
        "AJAX_OPTION_HISTORY" => "N",*/
        "EMAIL_TO" => "j.sharlova@travelsoft.by",
        "EVENT_MESSAGE_ID" => array("7"),
        "IBLOCK_ID" => "2",
        "IBLOCK_TYPE" => "site",
        "OK_TEXT" => "Ваш отзыв успешно отправлен и будет размещён на сайте после проверки модератором.",
        "PROPERTY_CODES" => array("5", "6"),
        "PROPERTY_CODES_REQUIRED" => array("5", "6"),
        "REQUIRED_FIELDS" => array("NAME", "MESSAGE"),
        "TITLE" => "Добавление отзыва",
        "USE_CAPTCHA" => "N",
        "ELEMENT_ID" => $arParams["ELEMENT_ID"]
    )
            );
$this->arParams["REVIEWS_FORM"] = ob_get_clean();
?>
