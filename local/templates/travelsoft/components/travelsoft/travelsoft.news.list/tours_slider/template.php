<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if(!empty($arResult["ITEMS"])):?>

    <div class="other-tours-hor tours-simple-list">
        <div class="container" style="width: 100%;">
            <?if(!empty($arParams["TEXT_TITLE"])):?>
                <h2 class="title"><?=$arParams["TEXT_TITLE"]?></h2>
            <?endif?>

                <div class="slides">

                    <?foreach($arResult["ITEMS"] as $arItem):?>
                        <?
                        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                        ?>
                        <?$arImg = getSrc($arItem["DISPLAY_PROPERTIES"]["PICTURES"]["VALUE"], array("width"=>258,"height"=>170), NO_PHOTO_258_170, 1);?>

                        <div class="tour item" style="background: white !important;" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                                <?if($arItem["DISPLAY_PROPERTIES"]["DATE"]["DATES_FORMAT"]):?>
                                    <span class="dates">
                                        <p><b><?=GetMessage('NEAREST_DEPARTURES')?></b><br />
                                            <?foreach ($arItem["DISPLAY_PROPERTIES"]["DATE"]["DATES_FORMAT"] as $date): ?>
                                                <?= $date ?><br />
                                            <?endforeach ?>
                                        </p>
                                    </span>
                                <?endif?>
                                <span class="img" style="background-image:url('<?= $arImg[0] ?>')"></span>
                                <b class="title ts-d-block ts-mx-2 ts-py-1"><span><?=$arItem["NAME"]?></span></b>
                            </a>
                            <div class="text-info ts-px-2 ts-pb-2 ts-d-flex ts-flex-direction__column">
                                <?if(!empty($arItem["DISPLAY_PROPERTIES"]["PREVIEW_TEXT"]["VALUE"]["TEXT"])):?>
                                    <p class="desc ts-mx-0 ts-my-0 ts-pb-1"><?= substr2($arItem["DISPLAY_PROPERTIES"]["PREVIEW_TEXT"]["DISPLAY_VALUE"],200) ?></p>
                                <?endif?>

                                <?if(!empty($arItem["DISPLAY_PROPERTIES"]["PRICE"]["VALUE"]) && !empty($arItem["DISPLAY_PROPERTIES"]["CURRENCY"]["VALUE"])):?>
                                    <?=\travelsoft\currency\factory\Converter::getInstance()->convert($arItem["DISPLAY_PROPERTIES"]["PRICE"]["VALUE"], $arItem["DISPLAY_PROPERTIES"]["CURRENCY"]["VALUE"])->getResult();?>
                                <?else:?>
                                    <div class="no-price ts-mx-0 ts-my-0"><?=GetMessage('ON_REQUEST')?></div>
                                <?endif?>

                                <?if (!empty($arItem["DISPLAY_PROPERTIES"]["DAYS"]["VALUE"])): ?>
                                    <span class="per"><?=GetMessage('ON')?> <?= num2word($arItem["DISPLAY_PROPERTIES"]["DAYS"]["VALUE"],array(GetMessage('NIGHT_1'),GetMessage('NIGHT_2'),GetMessage('NIGHT_ALL')))?></span>
                                <?php endif ?>
                            </div>
                        </div>

                    <?endforeach;?>

                </div>

        </div>
    </div>
<?endif?>