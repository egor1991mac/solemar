<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "Будет удалена вся информация, связанная с этой записью. Продолжить?";
$MESS["PAGE_H1"] = "Курорты";
$MESS["NOT_RESORTS"] = "Курорты не найдены";
$MESS["RESORTS"] = "Курорты";
$MESS["HOTELS"] = "Отели";
$MESS["VISAS"] = "Виза";
$MESS["TOURIST_GUIDE"] = "Памятка";
$MESS["MAP"] = "Карта";
?>