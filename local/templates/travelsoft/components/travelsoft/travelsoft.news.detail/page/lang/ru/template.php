<?
$MESS["RESORTS"] = "Курорты";
$MESS["HOTELS"] = "Отели";
$MESS["VISAS"] = "Виза";
$MESS["VIDEO"] = "Видео";
$MESS["DESC"] = "О стране";
$MESS["CODE_TOWN"] = "Города и курорты";
$MESS["TOURS"] = "Лучшие туры";
$MESS["SEARCH_FORM_TITLE"] = "Подбор туров";
$MESS["TOURIST_GUIDE"] = "Памятка";
$MESS["MAP"] = "Курорт на карте";
$MESS["TOURS_FOR_RESORT"] = "Туры в #RESORT_NAME#";
$MESS["HOTELS_FOR_RESORT"] = "Отели в #RESORT_NAME#";
?>