<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
<div id="menu">
    <div class="container">
        <a href="#toggle" id="menu-button" class="flashlink"><span><?=GetMessage('MENU_ITEMS_TITLE')?></span></a>

        <ul>

            <?
            $previousLevel = 0;
            foreach($arResult as $arItem):?>

                <?if(!isset($arItem["PARAMS"]["menu"]) || $arItem["PARAMS"]["menu"] != 'right'):?>

                    <?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
                        <?=str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
                    <?endif?>

                    <?if ($arItem["IS_PARENT"]):?>

                        <?if ($arItem["DEPTH_LEVEL"] == 1):?>

                            <li class="<?if ($arItem["SELECTED"]):?>active<?endif?>">
                                <a href="<?=$arItem["LINK"]?>">
                                    <span><?=$arItem["TEXT"]?></span></a>
                                <ul class="dropmenu">

                        <?else:?>

                            <li class="<?if ($arItem["SELECTED"]):?>active<?endif?>">
                                <a href="<?=$arItem["LINK"]?>"><span><?=$arItem["TEXT"]?></span></a>
                                <ul class="dropmenu">

                        <?endif?>

                    <?else:?>

                        <?if ($arItem["PERMISSION"] > "D"):?>

                            <?if ($arItem["DEPTH_LEVEL"] == 1):?>
                                <li><a class="<?if ($arItem["SELECTED"]):?>active<?endif?>" href="<?=$arItem["LINK"]?>"><span><?=$arItem["TEXT"]?></span></a></li>
                            <?else:?>
                                <li><a class="<?if ($arItem["SELECTED"]):?>active<?endif?>" href="<?=$arItem["LINK"]?>"><span><?=$arItem["TEXT"]?></span></a></li>
                            <?endif?>

                        <?else:?>

                            <?if ($arItem["DEPTH_LEVEL"] == 1):?>
                                <li><a class="<?if ($arItem["SELECTED"]):?>active<?endif?>" href="<?=$arItem["LINK"]?>"><span><?=$arItem["TEXT"]?></span></a></li>
                            <?else:?>
                                <li><a class="<?if ($arItem["SELECTED"]):?>active<?endif?>" href="<?=$arItem["LINK"]?>"><span><?=$arItem["TEXT"]?></span></a></li>
                            <?endif?>

                        <?endif?>

                    <?endif?>

                    <?$previousLevel = $arItem["DEPTH_LEVEL"];?>

                <?endif?>

            <?endforeach?>

            <?if ($previousLevel > 1)://close last item tags?>
                <?=str_repeat("</ul></li>", ($previousLevel-1) );?>
            <?endif?>
        </ul>
        <ul class="right">
            <?
            $previousLevel = 0;
            foreach($arResult as $arItem):?>

                <?if(isset($arItem["PARAMS"]["menu"]) && $arItem["PARAMS"]["menu"] == 'right'):?>

                    <?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
                        <?=str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
                    <?endif?>

                    <?if ($arItem["IS_PARENT"]):?>

                        <?if ($arItem["DEPTH_LEVEL"] == 1):?>

                            <li class="<?if ($arItem["SELECTED"]):?>active<?endif?> ts-dropdown">
                                <div class="nav-link">
                                <a href="<?=$arItem["LINK"]?>"><span><?=$arItem["TEXT"]?></span> </a>
                                <span class="arrow"></span></div>
                                <ul class="ts-dropdown__links">

                        <?else:?>

                            <li class="<?if ($arItem["SELECTED"]):?>active<?endif?> ts-dropdown">
                                <a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
                                <ul class="ts-dropdown__links">

                        <?endif?>

                    <?else:?>

                        <?if ($arItem["PERMISSION"] > "D"):?>

                            <?if ($arItem["DEPTH_LEVEL"] == 1):?>
                                <li><a class="<?if ($arItem["SELECTED"]):?>active<?endif?>" href="<?=$arItem["LINK"]?>"><span><?=$arItem["TEXT"]?></span></a></li>
                            <?else:?>
                                <li><a class="<?if ($arItem["SELECTED"]):?>active<?endif?>" href="<?=$arItem["LINK"]?>"><span><?=$arItem["TEXT"]?></span></a></li>
                            <?endif?>

                        <?else:?>

                            <?if ($arItem["DEPTH_LEVEL"] == 1):?>
                                <li><a class="<?if ($arItem["SELECTED"]):?>active<?endif?>" href="<?=$arItem["LINK"]?>"><span><?=$arItem["TEXT"]?></span></a></li>
                            <?else:?>
                                <li><a class="<?if ($arItem["SELECTED"]):?>active<?endif?>" href="<?=$arItem["LINK"]?>"><span><?=$arItem["TEXT"]?></span></a></li>
                            <?endif?>

                        <?endif?>

                    <?endif?>

                <?$previousLevel = $arItem["DEPTH_LEVEL"];?>

                <?endif?>

            <?endforeach?>

            <?if ($previousLevel > 1)://close last item tags?>
                <?=str_repeat("</ul></li>", ($previousLevel-1) );?>
            <?endif?>
            <li> <a href="/personal/my-profile/" > <span><?=GetMessage('MENU_ITEM_PERSONAL')?></span></a></li>
        </ul>
    </div>
</div>
<?endif?>
