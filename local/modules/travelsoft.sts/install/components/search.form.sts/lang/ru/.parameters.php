<?
$MESS["T_DESC_SEARCH"] = 'Параметры поиска туров';
$MESS["T_DESC_ACTION_URL"] = 'Путь к странице поиска';
$MESS["T_DESC_SHOW_PANEL_HEADING"] = 'Выводить шапку формы';
$MESS["T_DESC_COUNTRY_ID"] = 'ID страны прилета';
$MESS["T_DESC_CITY_ID"] = 'ID курорта';
$MESS["T_DESC_TOUR_TYPE_ID"] = 'ID типа тура';
$MESS["T_DESC_HOTEL_ID"] = 'ID отеля';
?>