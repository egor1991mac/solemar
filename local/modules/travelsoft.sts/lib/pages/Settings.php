<?php

namespace travelsoft\sts\pages;

use Bitrix\Main\Config\Option;

/**
 * Класс статических настроек модуля поиска туров
 *
 * @author juliya.sharlova
 * @copyright (c) 2018, travelsoftc
 */
class Settings {

    /**
     * ID таблицы направлений
     */
    const DIRECTIONS_TABLE_ID = 'DIRECTIONS_HL';

    /**
     * Url страницы направлений
     */
    const DIRECTIONS_URL = 'travelsoft_sts_directions_list.php';

    /**
     * Url страницы редактирования формы направления
     */
    const DETAIL_DIRECTIONS_URL = 'travelsoft_sts_directions_edit.php';

    /**
     * ID таблицы направлений Курорты
     */
    const DIRECTIONS_RESORTS_TABLE_ID = 'DIRECTIONS_RESORTS_HL';

    /**
     * Url страницы направлений
     */
    const DIRECTIONS_RESORTS_URL = 'travelsoft_sts_directions_resorts_list.php';

    /**
     * Url страницы редактирования формы направления
     */
    const DETAIL_DIRECTIONS_RESORTS_URL = 'travelsoft_sts_directions_resorts_edit.php';

    /**
     * Url страницы соотвествий
     */
    const TABLES_LIST_URL = 'travelsoft_sts_tables_list.php';

    /**
     * Url страницы соотвествий
     */
    const DETAIL_OBJECT_TABLES_URL = 'travelsoft_sts_tables_edit.php';

    /**
     * ID таблицы отелей
     */
    const HOTEL_TABLE_ID = 'HOTELS_HL';

    /**
     * Url страницы актуализации таблицы отелей
     */
    const UPDATE_HOTEL_TABLE_URL = 'travelsoft_sts_update_hotel_table.php';

    /**
     * Относительный путь к папке модуля
     */
    const REL_PATH_TO_MODULE = "/local/modules/travelsoft.sts/pages/";


    /**
     * Возвращает id таблицы направлений
     * @return int
     */
    public static function directionsStoreId(): int {

        return (int) self::get("DIRECTIONS_HL");
    }

    /**
     * Возвращает id таблицы направлений по курортам
     * @return int
     */
    public static function directionsresortsStoreId(): int {

        return (int) self::get("DIRECTIONS_RESORTS_HL");
    }

    /**
     * @param string $name
     * @return string
     */
    protected static function get(string $name): string {

        return (string) Option::get("travelsoft.sts", $name);
    }

}
