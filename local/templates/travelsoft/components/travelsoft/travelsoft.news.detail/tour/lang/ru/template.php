<?
$MESS["ROUTE_MAP"] = "Маршрут на карте";
$MESS["PROGRAM_TOUR"] = "Программа тура";
$MESS["OTHER_TOURS"] = "Другие туры в #COUNTRY_NAME#";
$MESS["BLOCK_PRICE"] = "Проживание в отеле ";
$MESS["BLOCK_PRICE_NIGHT"] = " / ночь ";
$MESS["BOOKING"] = "Забронировать!";
$MESS["DAY"] = "День ";
$MESS["DAYS"] = "Дни";
$MESS["DESCRIPTION_DAY"] = "Описание";
$MESS["BLOCK_PRICE_FROM"] = "Базовая стоимость от";
$MESS["ON_REQUEST"] = "Под запрос";
$MESS["NIGHT_FROM"] = "за";
$MESS["SEND_REQUEST"] = "Отправить заявку";
?>