<?php

$classes = array(
    "travelsoft\\booking\\sts\\adapters\\Highloadblock" => "lib/adapters/Highloadblock.php",
    "travelsoft\\booking\\sts\\adapters\\Iblock" => "lib/adapters/Iblock.php",
    "travelsoft\\booking\\sts\\Settings" => "lib/Settings.php",
    "travelsoft\\booking\\sts\\stores\\Operators" => "lib/stores/Operators.php",
    "travelsoft\\booking\\sts\\stores\\Hotels" => "lib/stores/Hotels.php",
    "travelsoft\\booking\\sts\\_interfaces\\Operator" => "lib/_interfaces/Operator.php",
    "travelsoft\\booking\\sts\\operators\\MasterTour" => "lib/operators/MasterTour.php",
    "travelsoft\\booking\\sts\\api\\MasterTour" => "lib/api/MasterTour.php",
    "travelsoft\\booking\\sts\\Utils" => "lib/Utils.php",
    "travelsoft\\booking\\sts\\EventsHandlers" => "lib/EventsHandlers.php",
    "travelsoft\\booking\\sts\\Validation" => "lib/Validation.php",
);

CModule::AddAutoloadClasses("travelsoft.booking.sts", $classes);

