<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$this->addExternalJs($componentPath."/js/StsApp.js",true);
$this->addExternalJs($componentPath."/js/ProgressBar.js",true);
$this->addExternalJs($componentPath."/js/PageNavigator.js",true);
$this->addExternalJs($componentPath."/js/Templater.js",true);
$this->addExternalJs($componentPath."/js/Cache.min.js",true);

use \travelsoft\sts\Utils;
$age1 = 0; $age2 = 0; $price = array();
if($arResult["REQUEST"]["children"] == 1){
    $age1 = isset($arResult["REQUEST"]["age1"]) && !empty($arResult["REQUEST"]["age1"]) ? $arResult["REQUEST"]["age1"] : 0;
} elseif($arResult["REQUEST"]["children"] >= 1){
    $age1 = isset($arResult["REQUEST"]["age1"]) && !empty($arResult["REQUEST"]["age1"]) ? $arResult["REQUEST"]["age1"] : 0;
    $age2 = isset($arResult["REQUEST"]["age2"]) && !empty($arResult["REQUEST"]["age2"]) ? $arResult["REQUEST"]["age2"] : 0;
}
if(isset($arResult["REQUEST"]["price"])){
    $price = $arResult["REQUEST"]["price"];
}
?>
<?$country_id = md5($arResult["REQUEST"]["country"]);?>
<div class="hotel-item shadow-none" id="tur-form-result">
    <div class="catalog-tour catalog-tour_details ts-wrap" id="country_<?=$country_id?>"></div>
    <div class="more-btn"></div>
</div>
<script>

    var operators = (function () {
        var operators = [];
        <?foreach($arResult["OPERATORS"] as $id):?>
            operators.push(<?= $id?>);
        <?endforeach?>
        return operators;
    })();
    var $request = {
         cityFrom: <?=$arResult["REQUEST"]["cityFrom"]?>,
         country: <?=$arResult["REQUEST"]["country"]?>,
         dateFrom: '<?=$arResult["REQUEST"]["dateFrom"]?>',
         dateTo: '<?=$arResult["REQUEST"]["dateTo"]?>',
         nightFrom: <?=$arResult["REQUEST"]["nightFrom"]?>,
         nightTo: <?=$arResult["REQUEST"]["nightTo"]?>,
         adults: <?=$arResult["REQUEST"]["adults"]?>,
         children: <?=$arResult["REQUEST"]["children"]?>,
         age1: <?=$age1?>,
         age2: <?=$age2?>,
         cities: <?=Utils::jsonEncode($arResult["REQUEST"]["cities"])?>,
         hotels: <?=Utils::jsonEncode($arResult["REQUEST"]["hotels"])?>,
         tourTypes: <?=Utils::jsonEncode($arResult["REQUEST"]["tourTypes"])?>,
         tours: <?=Utils::jsonEncode($arResult["REQUEST"]["tours"])?>,
         stars: <?=Utils::jsonEncode($arResult["REQUEST"]["stars"])?>,
         meals: <?=Utils::jsonEncode($arResult["REQUEST"]["meals"])?>
    };
    var $mount = 'country_<?=$country_id?>';
    window.app = new StsApp({
            request: $request,
            operators: operators,
            url: '<?=$componentPath?>' + '/ajax/sts.php',
            renderUrl: '<?=$componentPath?>' + '/ajax/stsRender.php',
            viewTempl: 'hotel',
            mount: $mount,
            paginatorMount: 'pageNavigator',
            htmlTemplates: <?=Utils::jsonEncode($arResult["TEMPLATES"])?>,
            services: <?=Utils::jsonEncode($arResult["SERVICES"])?>,
            renderCallback: function () {
                //$('[data-toggle="tooltip"]').tooltip();
            },
            bookingPage: '<?=$arParams["BOOKING_PAGE"]?>'
        },
        jQuery,
        new ProgressBar({total:operators.length, mount: 'progressBox'}),
        PageNavigator,
        new Templater(<?=Utils::jsonEncode($arResult["TEMPLATES"])?>)
    );

    window.app.getItemsDetailHotel($mount);

</script>