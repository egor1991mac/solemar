<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2014 Bitrix
 */

/**
 * Bitrix vars
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

//one css for all system.auth.* forms
$APPLICATION->SetAdditionalCSS("/bitrix/css/main/system.auth/flat/reg.css");
?>
<div class="bx-authform">

<?
if(!empty($arParams["~AUTH_RESULT"])):

	$text = str_replace(array("<br>", "<br />"), "\n", $arParams["~AUTH_RESULT"]["MESSAGE"]);

    if($arParams["~AUTH_RESULT"]["TYPE"] == "OK"){
        $text = str_replace(array("<br>", "<br />"), "\n", $arParams["~AUTH_RESULT"]["MESSAGE"].GetMessage('AUTH_SEND_TEXT_NEW'));
    }
?>
	<div class="alert <?=($arParams["~AUTH_RESULT"]["TYPE"] == "OK"? "alert-success":"alert-danger")?>"><?=nl2br(htmlspecialcharsbx($text))?></div>
<?endif?>

<?if($arResult["USE_EMAIL_CONFIRMATION"] === "Y" && is_array($arParams["AUTH_RESULT"]) &&  $arParams["AUTH_RESULT"]["TYPE"] === "OK"):?>
	<div class="alert alert-success"><?echo GetMessage("AUTH_EMAIL_SENT")?></div>
<?else:?>

<?if($arResult["USE_EMAIL_CONFIRMATION"] === "Y"):?>
	<div class="alert alert-warning"><?echo GetMessage("AUTH_EMAIL_WILL_BE_SENT")?></div>
<?endif?>

<noindex>
<!-- Контейнер -->
<div class="ts-theme">
    <!-- Форма -->
	<form method="post" action="<?=$arResult["AUTH_URL"]?>" name="bform" class="">
        <?if($arResult["BACKURL"] <> ''):?>
                <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
        <?endif?>
		<input type="hidden" name="AUTH_FORM" value="Y" />
		<input type="hidden" name="TYPE" value="REGISTRATION" />
        <div class="ts-title ts-px-1 ts-py-1">
            <h2 class="ts-h2 ts-reset ts-color-white"><?=GetMessage("AUTH_REGISTER_TITLE")?></h2>
            <button class="ts-d-none">x</button>
        </div>
        <!-- Контент  привет-->
        <div class="ts-content ">

            <!-- Имя -->
            <div class="ts-col-8 ts-mb-2">
                <div class="ts-input">
                    <label for="" class="ts-d-none"></label>
                    <div class="placeholder"><?=GetMessage("AUTH_NAME")?>*</div>
                    <div class="validation"></div>
                    <div class="input">
                        <input required type="text" name="USER_NAME" maxlength="255" value="<?=$arResult["USER_NAME"]?>">
                    </div>
                </div>
            </div>
            <!-- Фамилия -->
            <div class="ts-col-8 ts-mb-2">
                <div class="ts-input">
                    <label for="" class="ts-d-none"></label>
                    <div class="placeholder"><?=GetMessage("AUTH_LAST_NAME")?>*</div>
                    <div class="validation"></div>
                    <div class="input">
                        <input required type="text" name="USER_LAST_NAME" maxlength="255" value="<?=$arResult["USER_LAST_NAME"]?>">
                    </div>
                </div>
            </div>
            <!-- Пароль -->
            <div class="ts-col-8 ts-mb-2">
                <div class="ts-input">
                    <label for="" class="ts-d-none"></label>
                    <div class="placeholder"><?=GetMessage("AUTH_PASSWORD_REQ")?>*</div>
                    <div class="validation"></div>
                    <?if($arResult["SECURE_AUTH"]):?>
                        <div class="bx-authform-psw-protected" id="bx_auth_secure" style="display:none"><div class="bx-authform-psw-protected-desc"><span></span><?echo GetMessage("AUTH_SECURE_NOTE")?></div></div>
                        <script type="text/javascript">
                            document.getElementById('bx_auth_secure').style.display = '';
                        </script>
                    <?endif?>
                    <div class="input">
                        <input required type="password" name="USER_PASSWORD" maxlength="255" value="<?=$arResult["USER_PASSWORD"]?>" autocomplete="off">
                    </div>
                </div>
            </div>
            <!-- Пароль -->
            <div class="ts-col-8 ts-mb-2">
                <div class="ts-input">
                    <label for="" class="ts-d-none"></label>
                    <div class="placeholder"><?=GetMessage("AUTH_CONFIRM")?>*</div>
                    <div class="validation"></div>
                    <?if($arResult["SECURE_AUTH"]):?>
                        <div class="bx-authform-psw-protected" id="bx_auth_secure_conf" style="display:none"><div class="bx-authform-psw-protected-desc"><span></span><?echo GetMessage("AUTH_SECURE_NOTE")?></div></div>
                        <script type="text/javascript">
                            document.getElementById('bx_auth_secure_conf').style.display = '';
                        </script>
                    <?endif?>
                    <div class="input">
                        <input required type="password" name="USER_CONFIRM_PASSWORD" maxlength="255" value="<?=$arResult["USER_CONFIRM_PASSWORD"]?>" autocomplete="off">
                    </div>
                </div>
            </div>
            <!-- Email -->
            <div class="ts-col-8 ts-mb-2">
                <div class="ts-input">
                    <label for="" class="ts-d-none"></label>
                    <div class="placeholder"><?=GetMessage("AUTH_EMAIL")?>*</div>
                    <div class="validation"></div>
                    <div class="input">
                        <input required type="text" name="USER_EMAIL" maxlength="255" value="<?=$arResult["USER_EMAIL"]?>">
                    </div>
                </div>
            </div>

            <?/*if($arResult["USER_PROPERTIES"]["SHOW"] == "Y"):*/?><!--
                <?/*foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField):*/?>

                    <div class="col-xs-12 col-sm-12">
                        <div class="form-block type-2 clearfix">
                            <div class="form-label color-dark-2"><?/*=$arUserField["EDIT_FORM_LABEL"]*/?><?/*if($arUserField["MANDATORY"]=="Y"):*/?><span class="form__required">*</span><?/*endif*/?></div>
                            <div class="input-style-1 b-50 brd-0 type-2 color-3">
                                <?/*
                                $APPLICATION->IncludeComponent(
                                    "bitrix:system.field.edit",
                                    $arUserField["USER_TYPE"]["USER_TYPE_ID"],
                                    array(
                                        "bVarsFromForm" => $arResult["bVarsFromForm"],
                                        "arUserField" => $arUserField,
                                        "form_name" => "bform"
                                    ),
                                    null,
                                    array("HIDE_ICONS"=>"Y")
                                );
                                */?>
                            </div>
                        </div>
                    </div>

                <?/*endforeach;*/?>
            --><?/*endif;*/?>

            <?if ($arResult["USE_CAPTCHA"] == "Y"):?>
                <input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />

                <div class="ts-col-8 ts-mb-2">
                    <div class="ts-input">
                        <label for="" class="ts-d-none"></label>
                        <div class="placeholder"><?=GetMessage("CAPTCHA_REGF_PROMT")?></div>
                        <div class="bx-captcha"><img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" /></div>
                        <div class="input">
                            <input required type="text" name="captcha_word" maxlength="50" value="" autocomplete="off">
                        </div>
                    </div>
                </div>

            <?endif?>

            <div id="agent-container" style="display: none">

                <div class="confirm-terms">
                    <div class="input-entry color-3">
                        <input type="checkbox" id="IS_AGENT" name="IS_AGENT" value="Y" class="checkbox-form">
                        <label class="clearfix" for="IS_AGENT">
                            <span class="sp-check"><i class="fa fa-check"></i></span>
                            <span class="checkbox-text"><?=GetMessage("AUTH_IS_AGENT")?></span>
                        </label>
                    </div>
                </div>

            </div>
            <!-- Text -->
            <div class="ts-col-8 ts-mb-2">
                <div class="ts-text">
                    <p class="ts-reset">*<?=GetMessage("AUTH_REQ")?></p>
                </div>
            </div>
            <!-- Submit -->
            <div class="ts-col-8">
                <div class="ts-submit ts-max-width-370">
                    <input type="submit" name="Register" class="c-button w-100" value="<?=GetMessage("AUTH_REGISTER")?>">
                </div>
            </div>

        </div>

	</form>

        <div class="ts-col-8 ts-mx-auto ts-mb-3">
            <div class="ts-link ts-d-flex ts-flex-wrap ts-justify-center">
                <a href="<?=$arResult["AUTH_FORGOT_PASSWORD_URL"]?>" rel="nofollow"><?=GetMessage("AUTH_FORGOT_PASSWORD_2")?></a>
                <a href="<?=$arResult["AUTH_AUTH_URL"]?>" rel="nofollow"><?=GetMessage("AUTH_AUTH")?></b></a>
                <a href="/agencies/registration/" rel="nofollow"><?=GetMessage("AUTH_REGISTER_AGENT")?></a>
            </div>
        </div>

</div>
</noindex>

<script>
    let input = document.querySelectorAll('.ts-input input');
    let checkboxSaveMe = $('.ts-checkbox i');

    if($(input).val().length != 0){
        $(input).closest('.ts-input').addClass('active');
    }

    checkboxSaveMe.on('click',function(e){
        $(this).toggleClass('active');
    });
    $(input).on('focus',function(e){
        $(this).closest('.ts-input').addClass('active focus');

    });
    $(input).on('focusout',function(e){
        if($(this).val().length == 0){
            $(this).closest('.ts-input').removeClass('active focus');
        }
        else $(this).closest('.ts-input').removeClass('focus');

    });
    $(input).on('keyup',function(e){
        if($(this).val().length != 0){
            $(this).closest('.ts-input').addClass('active');
        }
        else{
            $(this).closest('.ts-input').removeClass('active');
        }
    });

    document.bform.USER_NAME.focus();
</script>

<?
$arUserFields = $GLOBALS['USER_FIELD_MANAGER']->GetUserFields('USER', 0, LANGUAGE_ID);
if (!empty($arUserFields)):
?>

<script type="text/javascript">

/**
 * @param {jQuery} $
 * @returns {undefined}
 */
(function ($) {
    
    function render () {
            
        var html, component_container = "<div class='col-xs-12 col-sm-12'><div class='form-block type-2 clearfix'>#COMPONENT#</div></div>",

        input_container = "#INPUT#",
        
        text_input = "<div class='input-style-1 b-50 brd-0 type-2 color-3'><input required type='text' name='#NAME#'></div>",

        label_container = "<div class='form-label color-dark-2'>#LABEL#<span class='form__required'>*</span></div>";

        html = "<div class='row'><div id='additional_fields__container'>";

             html += component_container.replace("#COMPONENT#", label_container.replace("#LABEL#", "<?= $arUserFields["UF_LEGAL_NAME"]["EDIT_FORM_LABEL"]?>") 
                                     + input_container.replace("#INPUT#", text_input.replace("#NAME#", "UF_LEGAL_NAME")));

             html += component_container.replace("#COMPONENT#", label_container.replace("#LABEL#", "<?= $arUserFields["UF_LEGAL_ADDRESS"]["EDIT_FORM_LABEL"]?>") 
                                     + input_container.replace("#INPUT#", text_input.replace("#NAME#", "UF_LEGAL_ADDRESS")));

             html += component_container.replace("#COMPONENT#", label_container.replace("#LABEL#", "<?= $arUserFields["UF_BANK_NAME"]["EDIT_FORM_LABEL"]?>") 
                                     + input_container.replace("#INPUT#", text_input.replace("#NAME#", "UF_BANK_NAME")));

             html += component_container.replace("#COMPONENT#", label_container.replace("#LABEL#", "<?= $arUserFields["UF_BANK_ADDRESS"]["EDIT_FORM_LABEL"]?>") 
                                     + input_container.replace("#INPUT#", text_input.replace("#NAME#", "UF_BANK_ADDRESS")));

             html += component_container.replace("#COMPONENT#", label_container.replace("#LABEL#", "<?= $arUserFields["UF_BANK_CODE"]["EDIT_FORM_LABEL"]?>") 
                                     + input_container.replace("#INPUT#", text_input.replace("#NAME#", "UF_BANK_CODE")));

             html += component_container.replace("#COMPONENT#", label_container.replace("#LABEL#", "<?= $arUserFields["UF_CHECKING_ACCOUNT"]["EDIT_FORM_LABEL"]?>") 
                                     + input_container.replace("#INPUT#", text_input.replace("#NAME#", "UF_CHECKING_ACCOUNT")));

             html += component_container.replace("#COMPONENT#", label_container.replace("#LABEL#", "<?= $arUserFields["UF_UNP"]["EDIT_FORM_LABEL"]?>") 
                                     + input_container.replace("#INPUT#", text_input.replace("#NAME#", "UF_UNP")));

             html += component_container.replace("#COMPONENT#", label_container.replace("#LABEL#", "<?= $arUserFields["UF_OKPO"]["EDIT_FORM_LABEL"]?>") 
                                     + input_container.replace("#INPUT#", text_input.replace("#NAME#", "UF_OKPO")));

        html += "</div></div>";


        $("#agent-container").before(html);

    }

    function destroy () {

        $("#additional_fields__container").remove();

    }
    
    $("#IS_AGENT").on("change", function () {
        
        var $this = $(this);

        if ($this.is(":checked")) {
            render();
            
        } else {
            
            destroy();
            
        }
        
    });
    
})(jQuery);

</script>
<?endif?>
<?endif?>
</div>