<?php

namespace travelsoft\booking\sts\stores;

use travelsoft\booking\sts\adapters\Highloadblock;

/**
 * Класс для работы с таблицей операторов
 *
 * @author juliya.sharlova
 * @copyright (c) 2017, travelsoft
 */
class Operators extends Highloadblock
{
    protected static $storeName = 'operators';

}