<?php

namespace travelsoft\sts\stores;

use travelsoft\sts\adapters\Highloadblock;

/**
 * Класс для работы с таблицей дат для календаря
 *
 * @author juliya.sharlova
 * @copyright (c) 2017, travelsoft
 */
class Dates extends Highloadblock
{
    protected static $storeName = 'dates';

}