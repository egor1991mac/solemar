<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$this->addExternalJS(SITE_TEMPLATE_PATH . "/js/MapAdapter/MapAdapter.js");
$htmlMapID = 'map';
?>
<?$properties = $arResult["DISPLAY_PROPERTIES"];?>
<?$upCountryName = ucwords($arResult["COUNTRY"]["CODE"]);
$arImgFlag = getSrc($arResult["COUNTRY"]["PROPERTY_FLAG_VALUE"], array("width"=>24,"height"=>24), SITE_TEMPLATE_PATH.'/flags/'.$upCountryName.'.png', 1, false);?>

<div class="country-menu">
    <div class="container">
        <p class="country-title" style="background-image:url(<?=$arImgFlag[0]?>)"><a href="<?=$arResult["COUNTRY"]["DETAIL_PAGE_URL"]?>"><?=$arResult["COUNTRY"]["NAME"]?></a></p>
        <p class="country-title-mobile" style="background-image:url(<?=$arImgFlag[0]?>)">
            <a href="#toggle" id="country-menu-button" class="flashlink"><?=$arResult["COUNTRY"]["NAME"]?></a>
        </p>
        <ul class="tabs">
            <?if($arResult["CITIES"]):?>
                <li><a href="<?=$arResult["COUNTRY"]["DETAIL_PAGE_URL"]?>resorts/"><span><?=GetMessage('RESORTS')?></span></a></li>
            <?endif?>
            <?if($arResult["HOTELS"]):?>
                <li class="active"><a href="<?=$arResult["COUNTRY"]["DETAIL_PAGE_URL"]?>hotels/"><span><?=GetMessage('HOTELS')?></span></a></li>
            <?endif?>
            <?if($arResult["VISAS"]):?>
                <li><a href="<?=$arResult["COUNTRY"]["DETAIL_PAGE_URL"]?>visa/"><span><?=GetMessage('VISAS')?></span></a></li>
            <?endif?>
            <?if(!empty($arResult["COUNTRY"]["PROPERTY_TOURIST_GUIDE_VALUE"])):?>
                <li><a href="<?=$arResult["COUNTRY"]["DETAIL_PAGE_URL"]?>tourist-info/"><span><?=GetMessage('TOURIST_GUIDE')?></span></a></li>
            <?endif?>

            <!--<li><a href="https://online.solemare.by/online/Extra/QuotedDynamic.aspx" class="important" onclick="return !window.open(this.href)"><span>Подбор тура online</span></a></li>-->

        </ul>
    </div>
</div>
<!--<div class="fast-bar">
    <div class="container">
        <div id="curr">
            <ul>
                <li class="text">Цены в:</li>
                <li class="byn"><a href="https://solemare.by/tours/italy/about#byn" class="flashlink"><span>BYN</span></a></li>
                <li class="usd"><a href="https://solemare.by/tours/italy/about#usd" class="flashlink"><span>USD</span></a></li>
                <li class="eur"><a href="https://solemare.by/tours/italy/about#eur" class="flashlink"><span>EUR</span></a></li>
                <li class="gbp"><a href="https://solemare.by/tours/italy/about#gbp" class="flashlink"><span>GBP</span></a></li>
            </ul>
        </div>
        <ul>
        </ul>
    </div>
</div>-->
<div class="container hotel col2">
    <div class="ts-wrap ts-px-0">

    <h1 class=""><?=$arResult["NAME"]?></h1>
    <div class="sidebar">
        <?if(!empty($properties["PRICE"]["VALUE"]) && !empty($properties["CURRENCY"]["VALUE"])):?>
            <div class="tour-card">
                <?=GetMessage('BLOCK_PRICE')?>
                <h2><?=$arResult["NAME"]?></h2>
                <?if(!empty($properties["PRICE"]["VALUE"]) && !empty($properties["CURRENCY"]["VALUE"])):?>
                    <?=GetMessage('BLOCK_PRICE_FROM')?> <b><?=\travelsoft\currency\factory\Converter::getInstance()->convert($properties["PRICE"]["VALUE"], $properties["CURRENCY"]["VALUE"])->getResult()?></b><?=GetMessage('BLOCK_PRICE_NIGHT')?>
                <?else:?>
                    <div class="no-price"><?=GetMessage('ON_REQUEST')?></div>
                <?endif ?>
                <a href="#open" class="orange-button pp-open tour"><?=GetMessage('BOOKING')?></a>
                <br />
            </div>

            <!--<div class="block_price">
                <span><?/*=GetMessage('BLOCK_PRICE')*/?>
                    <div class="hotel_name">
                        <?/*=$arResult["NAME"]*/?>
                    </div>
                </span>
                <div style="clear: both"></div>
                <span><?/*=GetMessage('BLOCK_PRICE_FROM')*/?><b></b><?/*=\travelsoft\currency\factory\Converter::getInstance()->convert($properties["PRICE"]["VALUE"], $properties["CURRENCY"]["VALUE"])->getResult();*/?></b><?/*=GetMessage('BLOCK_PRICE_NIGHT')*/?></span>
                <div style="clear: both"></div>
                <a class="button"><?/*=GetMessage('BOOKING')*/?></a>
            </div>-->
        <?endif?>
        <div class="widgets">
            <?=$arParams["SIDEBAR_SUBSCRIPTION"]?>
            <?=$arParams["SIDEBAR_WIDGETS"]?>
        </div>
    </div>
    <?if($properties["PICTURES"]["VALUE"]):?>
        <?=$arParams["SLIDER"]?>
    <?endif?>
    <div class="content">
        <div class="block_buttom">
            <?if(isset($arResult["CITY"]) && !empty($arResult["CITY"]["PROPERTY_WEATHER_VALUE"])):?>
                <div class="btn">
                    <?=$arResult["CITY"]["~PROPERTY_WEATHER_VALUE"]["TEXT"]?>
                </div>
            <?endif?>
            <?if(!empty($properties["RATING"]["VALUE"])):?>
                <div class="btn">
                    <div class="rating">
                        <?=GetMessage('RATING_FROM_BOOKING');?> - <b><?=$properties["RATING"]["VALUE"]?></b>
                    </div>
                </div>
            <?endif?>
        </div>
        <?if(!empty($properties["DESC"]["VALUE"]["TEXT"])):?>
            <h2 class="title"><?=$properties["DESC"]["NAME"]?></h2>
            <?=$properties["DESC"]["DISPLAY_VALUE"]?>
        <?endif?>
        <?/*if(!empty($properties["VIDEO"]["VALUE"])):*/?><!--
            <iframe width="1040" height="350" src="https://www.youtube.com/embed/<?/*=$properties["VIDEO"]["VALUE"]*/?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
        --><?/*endif*/?>
        <?if(!empty($properties["DESCBEACH"]["VALUE"]["TEXT"])):?>
            <h2 class="title"><?=$properties["DESCBEACH"]["NAME"]?></h2>
            <?=$properties["DESCBEACH"]["DISPLAY_VALUE"]?>
        <?endif?>
        <?if(!empty($properties["DESCROOM"]["VALUE"]["TEXT"])):?>
            <h2 class="title"><?=$properties["DESCROOM"]["NAME"]?></h2>
            <?=$properties["DESCROOM"]["DISPLAY_VALUE"]?>
        <?endif?>
        <?if(!empty($properties["DESCSPORT"]["VALUE"]["TEXT"])):?>
            <h2 class="title"><?=$properties["DESCSPORT"]["NAME"]?></h2>
            <?=$properties["DESCSPORT"]["DISPLAY_VALUE"]?>
        <?endif?>
        <?if(!empty($properties["DESCCHILDREN"]["VALUE"]["TEXT"])):?>
            <h2 class="title"><?=$properties["DESCCHILDREN"]["NAME"]?></h2>
            <?=$properties["DESCCHILDREN"]["DISPLAY_VALUE"]?>
        <?endif?>
        <?if(!empty($properties["MEDICAL_TREATMENT"]["VALUE"]["TEXT"])):?>
            <h2 class="title"><?=$properties["MEDICAL_TREATMENT"]["NAME"]?></h2>
            <?=$properties["MEDICAL_TREATMENT"]["DISPLAY_VALUE"]?>
        <?endif?>
        <?if(!empty($properties["DESCMEAL"]["VALUE"]["TEXT"])):?>
            <h2 class="title"><?=$properties["DESCMEAL"]["NAME"]?></h2>
            <?=$properties["DESCMEAL"]["DISPLAY_VALUE"]?>
        <?endif?>



        <? if (!empty($arResult["ROUTE_INFO"])): ?>

            <h2 class="title"><?=GetMessage("MAP")?></h2>
            <div id="<?= $htmlMapID ?>" style="width: 100%;height: 415px;"></div>

            <script>
                var loc = JSON.parse('<?=\Bitrix\Main\Web\Json::encode($arResult["ROUTE_INFO"])?>');

                $(document).ready(function () {
                    var mapAdapter = new MapAdapter({
                        map_id: "map",
                        center: {
                            lat: loc["lat"],
                            lng: loc["lng"]
                        },
                        object: "ymaps",
                        zoom: 16
                    });
                    mapAdapter.addMarker(loc);
                });
            </script>
        <?endif?>
        <div class="ts-mt-3">
            <?if($arResult["DIRECTIONS"]):?>
                <?=$arParams["SEARCH_FORM_STS"]?>
                <?=$arParams["PAGE_SEARCH_RESULT_STS"]?>
            <?endif?>

        </div>

    </div>
    </div>

</div>
<div class="container about-country col2">
<?=$arParams["REVIEWS_FOR_HOTEL"]?>
<?=$arParams["TOURS_SLIDER_FOR_COUNTRY"]?>
</div>
<?=$arParams["COUNTRIES_LIST"]?>