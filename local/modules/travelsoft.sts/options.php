<?php
if (!$USER->isAdmin())
    return;

\Bitrix\Main\Loader::includeModule("highloadblock");
\Bitrix\Main\Loader::includeModule("iblock");
\Bitrix\Main\Loader::includeModule("travelsoft.sts");

global $APPLICATION;

$mid = "travelsoft.sts";

function renderOptions($arOptions, $mid) {

    foreach ($arOptions as $name => $arValues) {
        
        $cur_opt_val = htmlspecialcharsbx(Bitrix\Main\Config\Option::get($mid, $name));
        $name = htmlspecialcharsbx($name);

        if ($name == "DATARANGE"):
            foreach ($arValues["DEF"] as $inputName => $Values):
                $arCurVals = array_map(function ($val) {
                    return intVal($val);
                }, unserialize(Bitrix\Main\Config\Option::get($mid, $inputName)));
                $from = $arCurVals[0] > 0 ? $arCurVals[0] : $Values["DEFVALUES"][0];
                $to = $arCurVals[1] > 0 ? $arCurVals[1] : $Values["DEFVALUES"][1];

                $options .= '<tr>';
                $options .= '<td width="40%">';
                $options .= '<label for="'.$name.'">'.$Values["TITLE"].':</label>';
                $options .= '</td>';
                $options .= '<td width="60%">';
                if($inputName == "DATE_RANGE") {
                    $options .= '(с сегодня(' . date("d.m.Y", time()) . ') + <input class="dinp" type="text" value="' . $from . '" name="' . $name . '[' . $inputName . '][]"> '.num2word($from,array("день","дня","дней"),false).')';
                    if ($to) {
                        $options .= ' + <input class="dinp" type = "text" value = "'.$to.'" name = "'. $name . '[' . $inputName . '][]" > ';
                    }
                    $options .= num2word($to,array("день","дня","дней"),false);
                } /*elseif($inputName == "NIGHT_RANGE") {
                    $options .= 'с <input class="dinp" type="text" value="' . $from . '" name="' . $name . '[' . $inputName . '][]"> '.num2word($from,array("ночь","ночи","ночей"),false);
                    if ($to) {
                        $options .= ' по <input class="dinp" type = "text" value = "'.$to.'" name = "'. $name . '[' . $inputName . '][]" > ';
                    }
                    $options .= num2word($to,array("ночь","ночи","ночей"),false);
                }*/
                else {
                    $options .= 'с <input class="dinp" type="text" value="' . $from . '" name="' . $name . '[' . $inputName . '][]"> ';
                    if ($to) {
                        $options .= ' по <input class="dinp" type = "text" value = "'.$to.'" name = "'. $name . '[' . $inputName . '][]" > ';
                    }
                }
                $options .= '</td>';
                $options .= '</tr>';
            endforeach;
        else:

            $options .= '<tr>';
            $options .= '<td width="40%">';
            $options .= '<label for="' . $name . '">' . $arValues['DESC'] . ':</label>';
            $options .= '</td>';
            $options .= '<td width="60%">';
            if ($arValues['TYPE'] == 'select') {

                $options .= '<select id="' . $name . '" name="' . $name . '">';
               foreach ($arValues['VALUES'] as $key => $value) {
                    $options .= '<option '.($cur_opt_val == $key ? 'selected' : '').' value="'.$key.'">'.$value.'</option>';
                }
                $options .= '</select>';

            } elseif ($arValues['TYPE'] == 'text') {

                $options .= '<input type="text" name="'.$name.'" value="'.$cur_opt_val.'">';
            }
            $options .= '</td>';
            $options .= '</tr>';

        endif;
    }
    echo $options;
}

function loadFiles($dirName) {

    $directory = __DIR__ . "/" . $dirName;
    return array_diff(scandir($directory), array('..', '.'));
}

$dbHLList = Bitrix\Highloadblock\HighloadBlockTable::getList(array(
            "order" => array("ID" => "ASC")
        ))->fetchAll();

foreach ($dbHLList as $arHL) {
    $arHLS[$arHL["ID"]] = $arHL["NAME"];
}

$dbIBList = CIBlock::GetList(
                array(), array("ACTIVE" => "Y")
);
while ($arIB = $dbIBList->Fetch()) {
    $arIBS[$arIB["ID"]] = $arIB["NAME"];
}

$dbMails = CEventMessage::GetList($by = "site_id", $order = "desc", array('TYPE_ID' => "TRAVELSOFT_BOOKING"));
while ($arMail = $dbMails->Fetch()) {
    $arMails[$arMail['ID']] = $arMail['SUBJECT'] . "(" . $arMail['ID'] . ")";
}

$dbGroupsList = Bitrix\Main\GroupTable::getList(array("select" => array("ID", "NAME")))->fetchAll();

for ($i = 0, $cnt = count($dbGroupsList); $i < $cnt; $i++) {
    $arGroups[$dbGroupsList[$i]["ID"]] = $dbGroupsList[$i]["NAME"];
}

$main_options = array(
    'STORES' => array(
        "CITIES_IB" => array("DESC" => "Инфоблок городов", "VALUES" => $arIBS, 'TYPE' => 'select'),
        "COUNTRIES_IB" => array("DESC" => "Инфоблок стран", "VALUES" => $arIBS, 'TYPE' => 'select'),
        "REGIONS_IB" => array("DESC" => "Инфоблок регионов", "VALUES" => $arIBS, 'TYPE' => 'select'),
        "HOTELS_IB" => array("DESC" => "Инфоблок отелей", "VALUES" => $arIBS, 'TYPE' => 'select'),
        "FOOD_IB" => array("DESC" => "Инфоблок питания", "VALUES" => $arIBS, 'TYPE' => 'select'),
        "STARS_IB" => array("DESC" => "Инфоблок звездности", "VALUES" => $arIBS, 'TYPE' => 'select'),
        "PLACEMENTS_IB" => array("DESC" => "Инфоблок размещений", "VALUES" => $arIBS, 'TYPE' => 'select'),
        "TOURTYPES_IB" => array("DESC" => "Инфоблок типов туров", "VALUES" => $arIBS, 'TYPE' => 'select'),
        "TOURS_IB" => array("DESC" => "Инфоблок предложений", "VALUES" => $arIBS, 'TYPE' => 'select'),
    ),
    "PARAMS_FORM" => array(
        'CITYFROM_ID' => array("DESC" => 'ID города вылета поиска туров (по-умолчанию)', "VALUES" => '', 'TYPE' => 'text'),
        'COUNTRY_ID' => array("DESC" => 'ID страны прилета поиска туров (по-умолчанию)', "VALUES" => '', 'TYPE' => 'text'),
        'DATARANGE' => array("DESC" => "", "TYPE" => "", "DEF" => array(
            "DATE_RANGE" => array("TITLE" => "Период поиска туров по датам (по-умолчанию)", "DEFVALUES" => array(0, 7)),
            "NIGHT_RANGE" => array("TITLE" => "Период поиска туров по количеству ночей (по-умолчанию)", "DEFVALUES" => array(7, 14)),
        )
        ),
        "ADULT" => array("DESC" => 'Количество взрослых (по-умолчанию)', "VALUES" => '', 'TYPE' => 'text'),
        "CHILD" => array("DESC" => 'Количество детей (по-умолчанию)', "VALUES" => '', 'TYPE' => 'text'),
    ),
    "PARAMS" => array(
        "PATH_NOPHOTO" => array("DESC" => "Путь к картинке-заглушке", "VALUES" => '', 'TYPE' => 'text'),
    )
);

$highloadblocksFiles = loadFiles("install/highloadblocks");
foreach ($highloadblocksFiles as $file) {

    $arr = include "install/highloadblocks/" . $file;

    $main_options["STORES"][$arr["table_data"]["OPTION_PARAMETER"]] = array("DESC" => $arr["table_data"]["LANGS"]["ru"], "VALUES" => $arHLS, 'TYPE' => 'select');

}

$tabs = array(
    array(
        "DIV" => "edit1",
        "TAB" => "Хранение данных",
        "ICON" => "",
        "TITLE" => "Укажите необходимые инфоблоки, highloadblock'и"
    ),
    array(
        "DIV" => "edit2",
        "TAB" => "Настройки формы поиска по-умолчанию",
        "ICON" => "",
        "TITLE" => "Укажите необходимые настройки"
    ),
    array(
        "DIV" => "edit3",
        "TAB" => "Другие настройки",
        "ICON" => "",
        "TITLE" => "Укажите необходимые настройки"
    ),
);

$o_tab = new CAdminTabControl("TravelsoftTabControl", $tabs);
if ($REQUEST_METHOD == "POST" && strlen($save . $reset) > 0 && check_bitrix_sessid()) {

    if (strlen($reset) > 0) {
        foreach ($main_options as $name => $desc) {
            \Bitrix\Main\Config\Option::delete($mid, array('name' => $name));
        }
    } else {
        foreach ($main_options as $arBlockOption) {

            foreach ($arBlockOption as $name => $arValues) {
                if (isset($_REQUEST[$name])) {
                    if ($name == "DATARANGE") {
                        foreach ($_REQUEST["DATARANGE"] as $inputName => $arValues) {
                            \Bitrix\Main\Config\Option::set($mid, $inputName, serialize(array(abs(intVal($arValues[0])), abs(intVal($arValues[1])))));
                        }
                    } else {
                        \Bitrix\Main\Config\Option::set($mid, $name, $_REQUEST[$name]);
                    }
                }
            }
        }
    }

    LocalRedirect($APPLICATION->GetCurPage() . "?mid=" . urlencode($mid) . "&lang=" . urlencode(LANGUAGE_ID) . "&" . $o_tab->ActiveTabParam());
}
$o_tab->Begin();
?>

<form method="post" action="<? echo $APPLICATION->GetCurPage() ?>?mid=<?= urlencode($mid) ?>&amp;lang=<? echo LANGUAGE_ID ?>">
    <?

    foreach ($main_options as $arOption) {
        $o_tab->BeginNextTab();
        renderOptions($arOption, $mid);
    }
    $o_tab->Buttons(); ?>
    <input type="submit" name="save" value="Сохранить" title="Сохранить" class="adm-btn-save">
    <input type="submit" name="reset" title="Сбросить" OnClick="return confirm('Сбросить')" value="Сбросить">
    <?= bitrix_sessid_post(); ?>
    <? $o_tab->End(); ?>
</form>