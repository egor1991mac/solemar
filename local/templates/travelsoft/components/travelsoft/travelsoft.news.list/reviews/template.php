<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if(!empty($arResult["ITEMS"])):?>

    <?if(!empty($arParams["TEXT_TITLE"])):?>
        <div class="feedbacks">

            <h2 class="title"><?=$arParams["TEXT_TITLE"]?></h2>

            <div class="content">
                <p class="feedback-message center">
                    <a href="<?=$templateFolder.'/popup_form.php'?>" class="blue-button flashlink pp-feedback popup" <?if(isset($arParams["ELEMENT_ID"]) && !empty($arParams["ELEMENT_ID"])):?>data-element-id="<?=$arParams["ELEMENT_ID"]?>"<?endif?>><span><?=GetMessage('ADD_REVIEW')?> <?= $arParams["VOWEL_LETTER"] ?></span></a>
                </p>
                <!--<div id="feedbacks-list">
                    <?/*= $this->renderPartial('feedbacks.inc', compact('feedbacks', 'pages'), true) */?>
                </div>-->
    <?endif?>
            <?foreach($arResult["ITEMS"] as $arItem):?>
                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>

                <div class="message" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                    <!--<p class="date"><?/*= $arItem["DISPLAY_ACTIVE_FROM"]*/?><?/*if(!empty($arItem["DISPLAY_PROPERTIES"]["NAME"]["VALUE"])):*/?>, <?/*= $arItem["NAME"] */?><?/*endif*/?></p>-->
                    <p class="date"><?= $arItem["NAME"] ?></p>
                    <p><?= $arItem["DISPLAY_PROPERTIES"]["MESSAGE"]["DISPLAY_VALUE"] ?></p>
                    <?if (!empty($arItem["DISPLAY_PROPERTIES"]["ANSWER"]["VALUE"]["TEXT"])): ?>
                        <p class="answer"><?= $arItem["DISPLAY_PROPERTIES"]["ANSWER"]["DISPLAY_VALUE"] ?></p>
                    <?endif ?>
                </div>

            <?endforeach;?>
    <?if(!empty($arParams["TEXT_TITLE"])):?>
            </div>
        </div>
    <?endif?>
    <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
        <div style="clear: both"></div>
        <?=$arResult["NAV_STRING"]?>
    <?endif;?>
<?else:?>
    <p class="feedback-message center no-feedbacks ">
        <?=str_replace("#VOWEL_LETTER#",$arParams["VOWEL_LETTER"],GetMessage('EMPTY_REVIEWS'))?><br /><br />
        <a href="<?=$templateFolder.'/popup_form.php'?>" class="blue-button flashlink pp-feedback popup ts-mx-2" <?if(isset($arParams["ELEMENT_ID"]) && !empty($arParams["ELEMENT_ID"])):?>data-element-id="<?=$arParams["ELEMENT_ID"]?>"<?endif?>><span><?=GetMessage('ADD_REVIEW')?> <?= $arParams["VOWEL_LETTER"] ?></span></a>
    </p>
<?endif?>

<?if(isset($arParams["ELEMENT_ID"]) && !empty($arParams["ELEMENT_ID"])):?>
    <script>
        $(function() {
            $('a.popup').fancybox({
                'overlayShow': false,
                'padding': 0,
                'margin' : 0,
                'scrolling' : 'no',
                'titleShow': false,
                'type': 'ajax',
                'href': '<?=$templateFolder.'/popup_form.php'?>',
                'afterLoad': function() {

                    element_id = <?=$arParams["ELEMENT_ID"]?>;

                    setTimeout(function () {
                        $('#review_item_id').val(element_id);
                    },100);

                },
            });
        });
    </script>
<?endif?>