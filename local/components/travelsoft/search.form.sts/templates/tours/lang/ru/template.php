<?
$MESS["TITLE_FORM"] = "Поиск";
$MESS["NOMATCHES"] = "Совпадений не найдено";
$MESS["CITYFROM"] = "Откуда";
$MESS["COUNTRYTO"] = "Куда";
$MESS["TITLE_COUNTRYTO"] = "Выберите курорт прибывания";
$MESS["RESORT"] = "Курорт";
$MESS["TYPETOUR"] = "Тип тура";
$MESS["DATE"] = "Период вылета";
$MESS["DATE_FROM"] = "Дата вылета с";
$MESS["DATE_TO"] = "Дата вылета по";
$MESS["NIGHTS"] = "Ночей";
$MESS["NIGHTSFROM"] = "с";
$MESS["NIGHTSTO"] = "по";
$MESS["ADULTS"] = "Взрослых";
$MESS["CHILDRENS"] = "Детей";
$MESS["TITLE_HEADING"] = "Туры";
$MESS["TEXT_SUBMIT"] = "Подобрать";
$MESS["TOURISTS"] = "Туристы";
$MESS["TITLE_TOURISTS"] = "Выберите количество туристов";

$MESS["CHILDRENS_AGE"] = "Возраст детей";
$MESS["CHILDRENS_AGE_1"] = "1-й ребенок";
$MESS["CHILDRENS_AGE_2"] = "2-й ребенок";
$MESS["CHILDRENS_AGE_3"] = "3-й ребенок";
?>