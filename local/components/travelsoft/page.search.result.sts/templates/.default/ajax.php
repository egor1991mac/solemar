<?
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define("NO_AGENT_STATISTIC",true);
define('NO_AGENT_CHECK', true);
define("PUBLIC_AJAX_MODE", true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

?>

<?$template = isset($_POST["view"]) && !empty($_POST["view"]) ? $_POST["view"] : ".default";?>

<?$APPLICATION->IncludeComponent(
    "travelsoft:html.ajax.render",
    $template,
    Array(
        "RESULT_LIST" => $_POST["data"]
    )
);?>