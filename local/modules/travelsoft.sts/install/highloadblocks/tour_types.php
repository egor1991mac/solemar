<?php


return array(
    "table" => "tstourtypes",
    "table_data" => array(
        "NAME" => "TSTOURTYPES",
        "ERR" => "Ошибка при создании highloadblock'a типов туров",
        "LANGS" => array(
            "ru" => 'Типы туров',
            "en" => "Tour types"
        ),
        "OPTION_PARAMETER" => "TOURTYPES_HL"
    ),
    "fields" => array(
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_BX_ID",
            "USER_TYPE_ID" => 'integer',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Битрикс ID',
                'en' => 'Bitrix ID',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Битрикс ID',
                'en' => 'Bitrix ID',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Битрикс ID',
                'en' => 'Bitrix ID',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Битрикс ID" ',
                'en' => 'An error in completing the field "Bitrix ID"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_TYPE_SEARCH",
            "USER_TYPE_ID" => 'string',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Тип поиска',
                'en' => 'Search type',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Тип поиска',
                'en' => 'Search type',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Тип поиска',
                'en' => 'Search type',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Тип поиска" ',
                'en' => 'An error in completing the field "Search type"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
        array(
            "ENTITY_ID" => 'HLBLOCK_{{table_id}}',
            "FIELD_NAME" => "UF_SORT",
            "USER_TYPE_ID" => 'integer',
            "XML_ID" => "",
            "SORT" => 100,
            "MULTIPLE" => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'N',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => "",
                'SIZE' => '20',
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => 'Сортировка',
                'en' => 'Sorting',
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => 'Сортировка',
                'en' => 'Sorting',
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => 'Сортировка',
                'en' => 'Sorting',
            ),
            'ERROR_MESSAGE' => array(
                'ru' => 'Ошибка при заполнении поля "Сортировка" ',
                'en' => 'An error in completing the field "Sorting"',
            ),
            'HELP_MESSAGE' => array(
                'ru' => '',
                'en' => '',
            ),
        ),
    )
);
