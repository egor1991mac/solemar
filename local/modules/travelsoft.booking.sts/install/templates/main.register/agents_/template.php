<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2014 Bitrix
 */
/**
 * Bitrix vars
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @param array $arParams
 * @param array $arResult
 * @param CBitrixComponentTemplate $this
 * @var string $templateName
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

CJSCore::Init(array('ajax'));

$arResult['HASH'] = md5(serialize($arParams) . $templateName);


function _er(string $code, bool $div = false, string $message = '') {

    $message = strlen($message) > 0 ? $message : GetMessage($code);

    if ($div) {
        echo '<div class="error">' . $message . '</div>';
    } else {
        echo '<span class="error">' . $message . '</span>';
    }
}

?>

<div class="bx-auth-reg">

    <?if ($USER->IsAuthorized()): ?>

        <div class="alert alert-success"><? echo GetMessage("MAIN_REGISTER_AUTH") ?></div>

    <?elseif (!empty($arResult["VALUES"]["USER_ID"]) && isset($_POST['IS_AGENT']) && $_POST['IS_AGENT'] == 'Y' && !$USER->IsAuthorized()): ?>

        <div class="alert alert-success"><? echo GetMessage("MAIN_REGISTER_AUTH_AGENT") ?></div>

    <?elseif (empty($arResult["ERRORS"]) && empty($arResult["VALUES"]["USER_ID"]) && isset($_POST['IS_AGENT']) && $_POST['IS_AGENT'] == 'Y' && !$USER->IsAuthorized()): ?>

        <div class="alert alert-warning"><? echo GetMessage("MAIN_NO_REGISTER_AUTH_AGENT") ?></div>

    <? else: ?>
        <?
        if (count($arResult["ERRORS"]) > 0) {

            ?><div class="alert alert-warning"><?
            foreach ($arResult["ERRORS"] as $key => $error) {
                if(strpos("логин",$error) === false) {
                    ShowError("Пользователь с таким e-mail уже существует.");
                }
                else{
                    ShowError($error);
                }
                /**/?><!--<p><?/* echo $error */?></p>--><?
            }
            ?></div><?
        }
        elseif ($arResult["USE_EMAIL_CONFIRMATION"] === "Y"){
            ?>
            <p><? echo GetMessage("REGISTER_EMAIL_WILL_BE_SENT") ?></p>
        <?}?>

        <form method="post" action="<?= POST_FORM_ACTION_URI ?>" name="regform" id="reg-form-<?=$arResult['HASH']?>" enctype="multipart/form-data" class="form simple-from">
            <input name="HASH" type="hidden" value="<?= $arResult['HASH'] ?>">
            <input type="hidden" name="REGISTER[LOGIN]" value="temp_login">
            <?
            if ($arResult["BACKURL"] <> ''):
                ?>
                <input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>" />
                <?
            endif;
            ?>

            <div class="simple-group">

                <div class="row">

                    <? foreach ($arResult["SHOW_FIELDS"] as $FIELD): ?>
                        <?
                        /* if ($FIELD == "AUTO_TIME_ZONE" && $arResult["TIME_ZONE_ENABLED"] == true): ?>
                          <tr>
                          <td><? echo GetMessage("main_profile_time_zones_auto") ?><? if ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y"): ?><span class="starrequired">*</span><? endif ?></td>
                          <td>
                          <select name="REGISTER[AUTO_TIME_ZONE]" onchange="this.form.elements['REGISTER[TIME_ZONE]'].disabled = (this.value != 'N')">
                          <option value=""><? echo GetMessage("main_profile_time_zones_auto_def") ?></option>
                          <option value="Y"<?= $arResult["VALUES"][$FIELD] == "Y" ? " selected=\"selected\"" : "" ?>><? echo GetMessage("main_profile_time_zones_auto_yes") ?></option>
                          <option value="N"<?= $arResult["VALUES"][$FIELD] == "N" ? " selected=\"selected\"" : "" ?>><? echo GetMessage("main_profile_time_zones_auto_no") ?></option>
                          </select>
                          </td>
                          </tr>
                          <tr>
                          <td><? echo GetMessage("main_profile_time_zones_zones") ?></td>
                          <td>
                          <select name="REGISTER[TIME_ZONE]"<? if (!isset($_REQUEST["REGISTER"]["TIME_ZONE"])) echo 'disabled="disabled"' ?>>
                          <? foreach ($arResult["TIME_ZONE_LIST"] as $tz => $tz_name): ?>
                          <option value="<?= htmlspecialcharsbx($tz) ?>"<?= $arResult["VALUES"]["TIME_ZONE"] == $tz ? " selected=\"selected\"" : "" ?>><?= htmlspecialcharsbx($tz_name) ?></option>
                          <? endforeach ?>
                          </select>
                          </td>
                          </tr>
                          <? else: */
                        if ($FIELD == 'LOGIN') {
                            continue;
                        }
                        ?>

                        <div class="col-xs-12 col-sm-12">
                            <div class="form-block type-2 clearfix <? if (in_array('WRONG_'.$FIELD, $arResult['CODE_ERRORS']) || in_array('USER_NOT_FOUND', $arResult['CODE_ERRORS']) || in_array('WRONG_ENTERED_'.$FIELD, $arResult['CODE_ERRORS'])): ?>has-error<? endif ?>">
                                <div class="form-label color-dark-2" for="REGISTER[<?= $FIELD ?>]"><?= GetMessage("REGISTER_FIELD_" . $FIELD) ?></div>
                                <div class="input-style-1 b-50 brd-0 type-2 color-3">
                            <?
                            switch ($FIELD) {

                                case "EMAIL":
                                    ?>
                                    <?
                                    if (in_array('WRONG_'.$FIELD, $arResult['CODE_ERRORS'])) {
                                        _er('REG_FORM_WRONG_'.$FIELD);
                                    }
                                    if (in_array('USER_NOT_FOUND', $arResult['CODE_ERRORS'])) {
                                        _er('REG_FORM_USER_NOT_FOUND');
                                    }
                                    if (in_array('REGISTER_FAIL', $arResult['CODE_ERRORS'])) {
                                        _er('REG_FORM_REGISTER_FAIL');
                                    }
                                    ?>
                                    <input <?
                                    /*if ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y") {
                                        echo 'required=""';
                                    }*/
                                    ?> type="text" name="REGISTER[<?= $FIELD ?>]" value="<?= $arResult["VALUES"][$FIELD] ?>">
                                        <?
                                        break;

                                    case "PERSONAL_PHONE":
                                        ?>
                                        <?
                                        if (in_array('WRONG_'.$FIELD, $arResult['CODE_ERRORS'])) {
                                            _er('REG_FORM_WRONG_'.$FIELD);
                                        }
                                        ?>
                                    <input <?
                                    /*if ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y") {
                                        echo 'required=""';
                                    }*/
                                    ?> size="30" type="tel" name="REGISTER[<?= $FIELD ?>]" value="<?= $arResult["VALUES"][$FIELD] ?>" placeholder="+375292221133">
                                        <?
                                        break;

                                    case "PASSWORD":
                                        ?>
                                        <?
                                        if (in_array('WORNG_'.$FIELD, $arResult['CODE_ERRORS'])) {
                                            _er('WORNG_'.$FIELD, false, $arResult['SYSTEM_ERRORS_MESSAGES']['WORNG_PASSWORD']);
                                        }
                                        if (in_array('WRONG_ENTERED_'.$FIELD, $arResult['CODE_ERRORS'])) {
                                            _er('REG_FORM_WRONG_ENTERED_'.$FIELD);
                                        }
                                        ?>
                                        <input <?
                                    /*if ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y") {
                                        echo 'required=""';
                                    }*/
                                    ?> size="30" type="password" name="REGISTER[<?= $FIELD ?>]" value="<?= $arResult["VALUES"][$FIELD] ?>" autocomplete="off">
                                        <? /* if ($arResult["SECURE_AUTH"]): ?>
                                          <span class="bx-auth-secure" id="bx_auth_secure" title="<? echo GetMessage("AUTH_SECURE_NOTE") ?>" style="display:none">
                                          <div class="bx-auth-secure-icon"></div>
                                          </span>
                                          <noscript>
                                          <span class="bx-auth-secure" title="<? echo GetMessage("AUTH_NONSECURE_NOTE") ?>">
                                          <div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
                                          </span>
                                          </noscript>
                                          <script type="text/javascript">
                                          document.getElementById('bx_auth_secure').style.display = 'inline-block';
                                          </script>
                                          <? endif */ ?>
                                        <?
                                        break;
                                    case "CONFIRM_PASSWORD":
                                        ?>
                                        <?
                                        if (in_array('WRONG_'.$FIELD, $arResult['CODE_ERRORS'])) {
                                            _er('REG_FORM_WRONG_'.$FIELD);
                                        }
                                        ?>
                                        <input <?
                                    /*if ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y") {
                                        echo 'required=""';
                                    }*/
                                    ?> size="30" type="password" name="REGISTER[<?= $FIELD ?>]" value="<?= $arResult["VALUES"][$FIELD] ?>" autocomplete="off" ><?
                                        break;

                                    case "NAME":
                                        ?>
                                        <?
                                        if (in_array('WRONG_'.$FIELD, $arResult['CODE_ERRORS'])) {
                                            _er('REG_FORM_WRONG_'.$FIELD);
                                        }
                                        ?>
                                        <input <?
                                        /*if ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y") {
                                            echo 'required=""';
                                        }*/
                                        ?> size="30" type="text" name="REGISTER[<?= $FIELD ?>]" value="<?= $arResult["VALUES"][$FIELD] ?>" autocomplete="off" ><?
                                        break;

                                    case "LAST_NAME":
                                        ?>
                                        <?
                                        if (in_array('WRONG_'.$FIELD, $arResult['CODE_ERRORS'])) {
                                            _er('REG_FORM_WRONG_'.$FIELD);
                                        }
                                        ?>
                                        <input <?
                                        /*if ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y") {
                                            echo 'required=""';
                                        }*/
                                        ?> size="30" type="text" name="REGISTER[<?= $FIELD ?>]" value="<?= $arResult["VALUES"][$FIELD] ?>" autocomplete="off" ><?
                                        break;

                                    /* case "PERSONAL_GENDER":
                                      ?><select name="REGISTER[<?= $FIELD ?>]">
                                      <option value=""><?= GetMessage("USER_DONT_KNOW") ?></option>
                                      <option value="M"<?= $arResult["VALUES"][$FIELD] == "M" ? " selected=\"selected\"" : "" ?>><?= GetMessage("USER_MALE") ?></option>
                                      <option value="F"<?= $arResult["VALUES"][$FIELD] == "F" ? " selected=\"selected\"" : "" ?>><?= GetMessage("USER_FEMALE") ?></option>
                                      </select><?
                                      break; */

                                    case "PERSONAL_COUNTRY":
                                    case "WORK_COUNTRY":
                                        ?><select <?
                                    if ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y") {
                                        echo 'required=""';
                                    }
                                    ?> class="form-control" name="REGISTER[<?= $FIELD ?>]"><?
                                        foreach ($arResult["COUNTRIES"]["reference_id"] as $key => $value) {
                                            ?><option value="<?= $value ?>"<? if ($value == $arResult["VALUES"][$FIELD]): ?> selected="selected"<? endif ?>><?= $arResult["COUNTRIES"]["reference"][$key] ?></option>
                                            <?
                                        }
                                        ?></select><?
                                    break;

                                case "PERSONAL_PHOTO":
                                case "WORK_LOGO":
                                    ?><input <?
                                        if ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y") {
                                            echo 'required=""';
                                        }
                                        ?> size="30" type="file" name="REGISTER_FILES_<?= $FIELD ?>"><?
                                        break;

                                    case "PERSONAL_NOTES":
                                    case "WORK_NOTES":
                                        ?><textarea <?
                                    if ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y") {
                                        echo 'required=""';
                                    }
                                    ?> cols="30" rows="5" name="REGISTER[<?= $FIELD ?>]"><?= $arResult["VALUES"][$FIELD] ?></textarea><?
                                        break;
                                    default:
                                        if ($FIELD == "PERSONAL_BIRTHDAY"):
                                            ?><small><?= $arResult["DATE_FORMAT"] ?></small><br /><? endif;
                                        ?><input <?
                                    if ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y") {
                                        echo 'required=""';
                                    }
                                    ?> size="30" type="text" name="REGISTER[<?= $FIELD ?>]" value="<?= $arResult["VALUES"][$FIELD] ?>"><?
                                        if ($FIELD == "PERSONAL_BIRTHDAY")
                                            $APPLICATION->IncludeComponent(
                                                    'bitrix:main.calendar', '', array(
                                                'SHOW_INPUT' => 'N',
                                                'FORM_NAME' => 'regform',
                                                'INPUT_NAME' => 'REGISTER[PERSONAL_BIRTHDAY]',
                                                'SHOW_TIME' => 'N'
                                                    ), null, array("HIDE_ICONS" => "Y")
                                            );
                                        ?><? }
                                ?>
                                <? // endif  ?>

                                </div>
                            </div>
                        </div>
                    <? endforeach ?>

                    </div>
                    <? // ********************* User properties ***************************************************?>
                    <?
                    if ($arResult["USER_PROPERTIES"]["SHOW"] == "Y"):
                        $arAgentFields = array(
                            0 => "UF_LEGAL_NAME",
                            1 => "UF_LEGAL_ADDRESS",
                            2 => "UF_BANK_NAME",
                            3 => "UF_BANK_ADDRESS",
                            4 => "UF_BANK_CODE",
                            5 => "UF_CHECKING_ACCOUNT",
                            6 => "UF_OKPO",
                            7 => "UF_UNP",
                        );
                        $agentFieldsHtml = $simpleClientFieldsHtml = $html = '';
                        ?>

                        <?
                        foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField):

                            $html = '<div class="col-xs-12 col-sm-12"><div class="form-block type-2 clearfix">
                                <div class="form-label color-dark-2" for="'.$FIELD_NAME.'">' . $arUserField["EDIT_FORM_LABEL"] . ':';
                            /*if ($arUserField["MANDATORY"] == "Y") {
                                $html .= '<span class="form__required">*</span>';
                            }*/

                            $html .= '</div><div class="input-style-1 b-50 brd-0 type-2 color-3">';

                            ob_start();
                            $APPLICATION->IncludeComponent(
                                    "bitrix:system.field.edit", $arUserField["USER_TYPE"]["USER_TYPE_ID"], array("bVarsFromForm" => $arResult["bVarsFromForm"], "arUserField" => $arUserField, "form_name" => "regform"), null, array("HIDE_ICONS" => "Y"));

                            $html .= ob_get_contents();
                            ob_end_clean();
                            $html .= '</div></div></div>';

                            if (in_array($FIELD_NAME, $arAgentFields)) {

                                $agentFieldsHtml .= $html;
                            } else {

                                $simpleClientFieldsHtml .= $html;
                            }
                        endforeach;
                        ?>

                        <? if (strlen($agentFieldsHtml) > 0): ?>

                            <div class="confirm-terms">
                                <div class="input-entry color-3 save_param">
                                    <input class="checkbox-form" <?
                                    if ($arParams["USER_TYPE"] == "agent" || isset($_POST['IS_AGENT']) && $_POST['IS_AGENT'] == 'Y') {
                                        echo 'checked=""';
                                    }
                                    ?> id="is-agent-btn" type="radio" name="IS_AGENT" value="Y">
                                    <label class="clearfix" for="IS_AGENT">
                                        <span class="sp-check"><i class="fa fa-check"></i></span>
                                        <span class="checkbox-text"><?=GetMessage("IS_AGENT")?></span>
                                    </label>
                                </div>
                            </div>
                                <? //= strlen(trim($arParams["USER_PROPERTY_NAME"])) > 0 ? $arParams["USER_PROPERTY_NAME"] : GetMessage("USER_TYPE_EDIT_TAB")  ?>
                            <div id="agent-fields-area" <? if ($arParams["USER_TYPE"] == "agent" || $_POST['IS_AGENT'] == 'Y'): ?>class="show"<? else: ?>class="hide"<? endif ?>>
                                <div class="row"> <?= $agentFieldsHtml; ?> </div>
                            </div>
                            <div id="client-fields-area" <? if ($arParams["USER_TYPE"] == "client" || $_POST['IS_AGENT'] != 'Y'): ?>class="show"<? else: ?>class="hide"<? endif ?>>
                                <div class="row"> <?= $simpleClientFieldsHtml; ?> </div>
                            </div>
                        <? endif ?>
                    <? endif; ?>
                    <? // ******************** /User properties ***************************************************   ?>

                    <div class="row">

                        <?
                        /* CAPTCHA */
                        if ($arResult["USE_CAPTCHA"] == "Y") {
                            ?>

                            <div class="col-xs-12 col-sm-12">
                                <div class="form-block type-2 clearfix">
                                    <div class="form-label color-dark-2"><?=GetMessage("REGISTER_CAPTCHA_TITLE")?></div>
                                    <div class="input-style-1 b-50 brd-0 type-2 color-3">
                                        <input type="hidden" name="captcha_sid" value="<?= $arResult["CAPTCHA_CODE"] ?>" />
                                        <div class="col-xs-12 col-sm-6 h-50" >
                                            <img src="/bitrix/tools/captcha.php?captcha_sid=<?= $arResult["CAPTCHA_CODE"] ?>" width="180" height="40" alt="CAPTCHA" />
                                        </div>
                                        <div class="col-xs-12 col-sm-6">
                                            <input class="form__input" type="text" name="captcha_word" maxlength="50" value="">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <?
                        }
                        /* !CAPTCHA */
                        ?>

                    </div>
            </div>

            <input class="form__send btn c-button bg-dr-blue-2 hv-dr-blue-2-o" id="reg-btn-<?= $arResult['HASH'] ?>" type="submit" name="register_submit_button" value="<?=GetMessage("AUTH_REGISTER")?>">

                <!--<input class="button" id="reg-btn-<?/*= $arResult['HASH'] */?>" type="submit" name="register_submit_button" value="<?/*= GetMessage("AUTH_REGISTER") */?>" />-->

                <!--<p><?/* echo $arResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"]; */?></p>
                <p><span class="starrequired">*</span><?/*= GetMessage("AUTH_REQ") */?></p>-->


        </form>

        <hr class="bxe-light">

        <div class="bx-authform-link-container form-label">
            <p class="form-label form-label-grey"><a href="<?= $arParams["AUTH_URL"] ?>"><b><?= GetMessage("AUTH_AUTH") ?></b></a></p>
        </div>
        <div class="bx-authform-link-container form-label">
            <p class="form-label form-label-grey"><a href="/lichnyy-kabinet/?forgot_password=yes" rel="nofollow"><b><?=GetMessage('AUTH_FORGOTPASSWORD')?></b></a></p>
        </div>

<? endif ?>
</div>

<script>
    BX.ready(function () {
        
        var areaTypes = ['client', 'agent'];
        
        function initSwitcher (currentType) {
            BX.bind(BX('is-' + currentType + '-btn'), 'click', function (e) {

                for (var j = 0; j < areaTypes.length; j++) {
                    BX.addClass(BX(areaTypes[j] + '-fields-area'), 'hide');
                    BX.removeClass(BX(areaTypes[j] + '-fields-area'), 'show');
                }

                BX.addClass(BX(currentType + '-fields-area'), 'show');

            });
        }


        
        for (var i = 0; i < areaTypes.length; i++) {
            initSwitcher(areaTypes[i]);
        }



        var form = BX('reg-form-<?=$arResult['HASH']?>');

        var errors = [];

        function _cleanErrors() {

            BX.findChildren(form, {className: 'form-block'}, true).forEach(function (el) {

                BX.removeClass(el, 'has-error');
            });

            BX.findChildren(form, {className: 'error'}, true).forEach(function (el) {

                BX.remove(el);
            });

        }

        function _scrollToError() {
            BX.scrollToNode(BX.findChild(form, {className: 'error'}, true));
        }

        function _er(code, div, message) {

            var messages = {

                REG_FORM_REGISTER_FAIL: '<?= GetMessage('REG_FORM_REGISTER_FAIL') ?>',
                REG_FORM_WRONG_EMAIL: '<?= GetMessage('REG_FORM_WRONG_EMAIL') ?>',
                REG_FORM_WRONG_ENTERED_EMAIL: '<?= GetMessage('REG_FORM_WRONG_ENTERED_EMAIL') ?>',
                REG_FORM_USER_NOT_FOUND: '<?= GetMessage('REG_FORM_USER_NOT_FOUND') ?>',
                REG_FORM_WRONG_PASSWORD: '<?= GetMessage('REG_FORM_WORNG_PASSWORD') ?>',
                REG_FORM_WRONG_ENTERED_PASSWORD: '<?= GetMessage('REG_FORM_WRONG_ENTERED_PASSWORD') ?>',
                REG_FORM_WRONG_CONFIRM_PASSWORD: '<?= GetMessage('REG_FORM_WRONG_CONFIRM_PASSWORD') ?>',
                REG_FORM_WRONG_LAST_NAME: '<?= GetMessage('REG_FORM_WRONG_LAST_NAME') ?>',
                REG_FORM_WRONG_NAME: '<?= GetMessage('REG_FORM_WRONG_NAME') ?>',
                REG_FORM_WRONG_PERSONAL_PHONE: '<?= GetMessage('REG_FORM_WRONG_PERSONAL_PHONE') ?>',
                REG_FORM_WRONG_UF_BANK_NAME: '<?= GetMessage('REG_FORM_WRONG_UF_BANK_NAME') ?>',
                REG_FORM_WRONG_UF_BANK_ADDRESS: '<?= GetMessage('REG_FORM_WRONG_UF_BANK_ADDRESS') ?>',
                REG_FORM_WRONG_UF_BANK_CODE: '<?= GetMessage('REG_FORM_WRONG_UF_BANK_CODE') ?>',
                REG_FORM_WRONG_UF_LEGAL_ADDRESS: '<?= GetMessage('REG_FORM_WRONG_UF_LEGAL_ADDRESS') ?>',
                REG_FORM_WRONG_UF_CHECKING_ACCOUNT: '<?= GetMessage('REG_FORM_WRONG_UF_CHECKING_ACCOUNT') ?>',
                REG_FORM_WRONG_UF_OKPO: '<?= GetMessage('REG_FORM_WRONG_UF_OKPO') ?>',
                REG_FORM_WRONG_UF_LEGAL_NAME: '<?= GetMessage('REG_FORM_WRONG_UF_LEGAL_NAME') ?>',
                REG_FORM_WRONG_UF_UNP: '<?= GetMessage('REG_FORM_WRONG_UF_UNP') ?>',
                REG_FORM_WRONG_CAPTCHA: '<?= GetMessage('REG_FORM_WRONG_CAPTCHA') ?>',

            }

            var tag = div === true ? 'div' : 'span';

            code = code || '';

            message = message || (typeof messages[code] !== 'undefined' ? messages[code] : '');

            return BX.create(tag, {attrs: {className: 'error'}, text: ' ' + message});

        }

        function isValidEmailAddress(emailAddress) {
            var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
            return pattern.test(emailAddress);
        }

        function isValidPhone(phone) {
            str = phone;
            str.replace('/\s/', '');
            var pattern = new RegExp(/^\+?[0-9]{7,}$/i);
            return pattern.test(phone);
        }

        BX.bind(form, 'submit', function (e) {

            var email = BX.findChild(this, {tagName: 'input', attribute: {name: 'REGISTER[EMAIL]'}}, true);
            var password = BX.findChild(this, {tagName: 'input', attribute: {name: 'REGISTER[PASSWORD]'}}, true);
            var name = BX.findChild(this, {tagName: 'input', attribute: {name: 'REGISTER[NAME]'}}, true);
            var lastName = BX.findChild(this, {tagName: 'input', attribute: {name: 'REGISTER[LAST_NAME]'}}, true);
            var phone = BX.findChild(this, {tagName: 'input', attribute: {name: 'REGISTER[PERSONAL_PHONE]'}}, true);
            var confirmPassword = BX.findChild(this, {tagName: 'input', attribute: {name: 'REGISTER[CONFIRM_PASSWORD]'}}, true);
            var agent = BX.findChild(this, {tagName: 'input', attribute: {name: 'IS_AGENT', id: 'is-agent-btn'}}, true);
            var captcha = BX.findChild(this, {tagName: 'input', attribute: {name: 'captcha_word'}}, true);

            var agent_info = [
                UF_LEGAL_NAME = BX.findChild(this, {tagName: 'input', attribute: {name: 'UF_LEGAL_NAME'}}, true),
                UF_LEGAL_ADDRESS = BX.findChild(this, {tagName: 'input', attribute: {name: 'UF_LEGAL_ADDRESS'}}, true),
                UF_BANK_NAME = BX.findChild(this, {tagName: 'input', attribute: {name: 'UF_BANK_NAME'}}, true),
                UF_BANK_ADDRESS = BX.findChild(this, {tagName: 'input', attribute: {name: 'UF_BANK_ADDRESS'}}, true),
                UF_BANK_CODE = BX.findChild(this, {tagName: 'input', attribute: {name: 'UF_BANK_CODE'}}, true),
                UF_CHECKING_ACCOUNT = BX.findChild(this, {tagName: 'input', attribute: {name: 'UF_CHECKING_ACCOUNT'}}, true),
                //UF_OKPO = BX.findChild(this, {tagName: 'input', attribute: {name: 'UF_OKPO'}}, true),
                UF_UNP = BX.findChild(this, {tagName: 'input', attribute: {name: 'UF_UNP'}}, true)
            ];

            /*var unp = BX.findChild(this, {tagName: 'input', attribute: {name: 'UF_INN'}}, true);
            var legal_name = BX.findChild(this, {tagName: 'input', attribute: {name: 'UF_LEGAL_NAME'}}, true);
            var contact_name = BX.findChild(this, {tagName: 'input', attribute: {name: 'UF_CONTACT_PERSON'}}, true);
            var country = BX.findChild(this, {tagName: 'input', attribute: {name: 'UF_COUNTRY'}}, true);
            var city = BX.findChild(this, {tagName: 'input', attribute: {name: 'UF_CITY'}}, true);
            var legal_address = BX.findChild(this, {tagName: 'input', attribute: {name: 'UF_LEGAL_ADDRESS'}}, true);
            var index = BX.findChild(this, {tagName: 'input', attribute: {name: 'UF_INDEX'}}, true);
            var fax = BX.findChild(this, {tagName: 'input', attribute: {name: 'UF_FAX'}}, true);*/

            _cleanErrors();

            if(email.value.length <= 0){

                BX.addClass(BX.findParent(email, {className: 'form-block'}), 'has-error');
                BX.insertAfter(_er('REG_FORM_WRONG_EMAIL'), BX.findPreviousSibling(BX.findParent(email,{className: 'input-style-1'}), {tag: 'DIV', attribute: {'for': 'REGISTER[EMAIL]'}}));

            } else {

                if(!isValidEmailAddress(email.value)){

                    BX.addClass(BX.findParent(email, {className: 'form-block'}), 'has-error');
                    BX.insertAfter(_er('REG_FORM_WRONG_ENTERED_EMAIL'), BX.findPreviousSibling(BX.findParent(email,{className: 'input-style-1'}), {tag: 'DIV', attribute: {'for': 'REGISTER[EMAIL]'}}));

                }

            }

            if(password.value.length < 6){

                BX.addClass(BX.findParent(password, {className: 'form-block'}), 'has-error');
                BX.insertAfter(_er('REG_FORM_WRONG_PASSWORD'), BX.findPreviousSibling(BX.findParent(password,{className: 'input-style-1'}), {tag: 'DIV', attribute: {'for': 'REGISTER[PASSWORD]'}}));

                e.preventDefault();

            }
            if(confirmPassword.value != password.value){

                BX.addClass(BX.findParent(password, {className: 'form-block'}), 'has-error');
                BX.insertAfter(_er('REG_FORM_WRONG_CONFIRM_PASSWORD'), BX.findPreviousSibling(BX.findParent(password,{className: 'input-style-1'}), {tag: 'DIV', attribute: {'for': 'REGISTER[CONFIRM_PASSWORD]'}}));

            }
            if(phone.value.length <= 0 || !isValidPhone(phone.value)){

                BX.addClass(BX.findParent(phone, {className: 'form-block'}), 'has-error');
                BX.insertAfter(_er('REG_FORM_WRONG_PERSONAL_PHONE'), BX.findPreviousSibling(BX.findParent(phone,{className: 'input-style-1'}), {tag: 'DIV', attribute: {'for': 'REGISTER[PERSONAL_PHONE]'}}));

            }

            if(name.value.length < 2){

                BX.addClass(BX.findParent(name, {className: 'form-block'}), 'has-error');
                BX.insertAfter(_er('REG_FORM_WRONG_NAME'), BX.findPreviousSibling(BX.findParent(name,{className: 'input-style-1'}), {tag: 'DIV', attribute: {'for': 'REGISTER[NAME]'}}));

            }
            if(lastName.value.length < 2){

                BX.addClass(BX.findParent(lastName, {className: 'form-block'}), 'has-error');
                BX.insertAfter(_er('REG_FORM_WRONG_LAST_NAME'), BX.findPreviousSibling(BX.findParent(lastName,{className: 'input-style-1'}), {tag: 'DIV', attribute: {'for': 'REGISTER[LAST_NAME]'}}));

            }

            if(agent.value == "Y" && agent.checked) {

                for (key in agent_info){

                    console.log(agent_info[key]);
                    if(agent_info[key].value.length <= 0) {

                        BX.addClass(BX.findParent(agent_info[key], {className: 'form-block'}), 'has-error');
                        BX.insertAfter(_er('REG_FORM_WRONG_'+agent_info[key].name), BX.findPreviousSibling(BX.findParent(agent_info[key],{className: 'input-style-1'}), {tag: 'DIV', attribute: {'for': agent_info[key].name}}));
                        /*BX.insertAfter(_er('REG_FORM_WRONG_'+agent_info[key].name), BX.findPreviousSibling(BX.findParent(agent_info[key], {className: 'input'}), {tag: 'DIV', attribute: {'for': agent_info[key].name}}));*/

                    }

                }

            }
            if(typeof captcha !== "undefined" && captcha !== null && captcha.value.length <= 4){

                BX.addClass(BX.findParent(captcha, {className: 'form-block'}), 'has-error');
                BX.insertAfter(_er('REG_FORM_WRONG_CAPTCHA'), BX.findPreviousSibling(BX.findParent(captcha,{className: 'input-style-1'}), {tag: 'DIV', attribute: {'for': 'captcha_word'}}));

            }

            _scrollToError();

            e.preventDefault() ;

        });



    });
</script>