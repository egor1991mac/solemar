<?
namespace travelsoft;

/**
 * @author juliya.sharlova
 *
 * Маршрутизатор
 */

/*
 * Пример подстановочных знаков:
 * `/page/:seg` - любые символы в одном сегменте, такие как `/page/qwerty` or `/page/123`;
 * `/page/:num` - цифры только как `/page/123`;
 * `/page/:any` - любые символы, такие как `/page/qwerty` или `/page/qwerty/123`;
 */

class Router
{

    protected $routes = [];

    protected $requestUri;

    protected $requestMethod;

    protected $requestHandler;

    protected $params = [];

    protected $placeholders = [
        ':seg' => '([^\/]+)',
        ':num'  => '([0-9]+)',
        /*':any'  => '(.+)'*/
        ':any'  => '([0-9a-zA-Z_-]+)(.*)'
    ];

    public function __construct($uri, $method = 'GET')
    {
        $this->requestUri = $uri;
        $this->requestMethod = $method;
    }

    /**
     * Метод Router работы с глобальными переменнымиы.
     * @return Router
     */
    public static function fromGlobals()
    {
        $uri = $_SERVER['REQUEST_URI'];
        if (false !== $pos = strpos($uri, '?')) {
            $uri = substr($uri, 0, $pos);
        }
        $uri = rawurldecode($uri);
        return new static($uri, $_SERVER['REQUEST_METHOD']);
    }
    /**
     * Текущий обработанный URI.
     * @return string
     */
    public function getRequestUri()
    {
        return $this->requestUri; // ?: '/';
    }

    /**
     * Метод запроса.
     * @return string
     */
    public function getRequestMethod()
    {
        return $this->requestMethod;
    }

    /**
     * Результат обработчика запроса.
     * @return string|callable
     */
    public function getRequestHandler()
    {
        return $this->requestHandler;
    }

    /**
     * Установка обработчика запроса.
     * @param $handler string|callable
     */
    public function setRequestHandler($handler)
    {
        $this->requestHandler = $handler;
    }

    /**
     * Параметры подстановки.
     * @return array
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * Добавление правила маршрута.
     *
     * @param string|array $route Строка или массив маршрута URI
     * @param mixed $handler Любая строка или действие с именем контроллера, например, "ControllerClass@actionMethod"
     */
    public function add($route, $handler = null)
    {
        if ($handler !== null && !is_array($route)) {
            $route = array($route => $handler);
        }
        $this->routes = array_merge($this->routes, $route);
        return $this;
    }
    /**
     * Запрошенный запрос URI.
     * @return bool
     */
    public function isFound()
    {
        $uri = $this->getRequestUri();

        // если URI равен маршруту
        if (isset($this->routes[$uri])) {
            $this->requestHandler = $this->routes[$uri];
            return true;
        }

        $find    = array_keys($this->placeholders);
        $replace = array_values($this->placeholders);

        foreach ($this->routes as $route => $handler) {

            // Заменить подстановочные знаки с помощью регулярных выражений
            if (strpos($route, ':') !== false) {
                $route = str_replace($find, $replace, $route);
            }

            $matches = array();
            // Правило маршрута сопоставлено
            if (preg_match('#^' . $route . '$#', $uri, $matches)) {

                $this->requestHandler = $handler;

                //$this->params = array_diff(array_slice($matches, 1), array(''));
                $this->params = array_diff($matches, array(''));
                return true;

            }
        }

        return false;
    }

    /**
     * Выполнение обработчика запросов.
     *
     * @param string|callable $handler
     * @param array $params
     * @return mixed
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     */
    public function executeHandler($handler = null, $params = array())
    {
        if ($handler === null) {
            throw new \InvalidArgumentException(
                'Request handler not setted out. Please check '.__CLASS__.'::isFound() first'
            );
        }

        // выполнить действие в вызываемом
        if (is_callable($handler)) {
            return call_user_func_array($handler, $params);
        }
        // выполнить действие в контроллерах
        if (strpos($handler, '@')) {
            $ca = explode('@', $handler);
            $controllerName = $ca[0];
            $action = $ca[1];
            if (class_exists($controllerName)) {
                $controller = new $controllerName();
            } else {
                throw new \RuntimeException("Controller class '{$controllerName}' not found");
            }
            if (!method_exists($controller, $action)) {
                throw new \RuntimeException("Method '{$controllerName}::{$action}' not found");
            }

            return call_user_func_array(array($controller, $action), $params);
        }
    }


}