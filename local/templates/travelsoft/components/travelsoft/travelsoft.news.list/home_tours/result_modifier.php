<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
foreach ($arResult["ITEMS"] as $k=>$arItem){

    if(!empty($arItem["DISPLAY_PROPERTIES"]["DATE"]["VALUE"])){

        $date = array();

        if(is_array($arItem["DISPLAY_PROPERTIES"]["DATE"]["VALUE"])){
            $date_ = date("d.m.Y");
            foreach($arItem["DISPLAY_PROPERTIES"]["DATE"]["VALUE"] as $t=>$item){

                if(strtotime($date_) < strtotime($item)){
                    $date[] = CIBlockFormatProperties::DateFormat("d.m.Y", MakeTimeStamp($item, CSite::GetDateFormat()));
                }

            }
        }

        $arResult["ITEMS"][$k]["DISPLAY_PROPERTIES"]["DATE"]["DATES_FORMAT"] = $date;

    }

}

$arResult["BIG_TOUR"] = array();
$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), array("IBLOCK_ID"=>$arParams["IBLOCK_ID"],"ACTIVE"=>"Y","PROPERTY_SHOW_MAIN_VALUE"=>"Y","PROPERTY_BIG_TOUR_VALUE"=>"Y"), false, Array("nTopCount"=>1), Array("IBLOCK_ID", "ID", "NAME", "ID", "CODE", "DETAIL_PAGE_URL"));
while($ob = $res->GetNextElement()){
    $arFields = $ob->GetFields();
    $arProps = $ob->GetProperties();
    $arResult["BIG_TOUR"] = $arFields;
    $arResult["BIG_TOUR"]["PROPERTIES"] = $arProps;
}