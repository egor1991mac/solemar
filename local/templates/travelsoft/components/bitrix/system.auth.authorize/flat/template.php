<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponent $component
 */

//one css for all system.auth.* forms
$APPLICATION->SetAdditionalCSS("/bitrix/css/main/system.auth/flat/reg.css");
?>

<div class="bx-authform">

<?
if(!empty($arParams["~AUTH_RESULT"])):
	$text = str_replace(array("<br>", "<br />"), "\n", $arParams["~AUTH_RESULT"]["MESSAGE"]);
?>
	<div class="alert alert-danger"><?=nl2br(htmlspecialcharsbx($text))?></div>
<?endif?>

<?
if($arResult['ERROR_MESSAGE'] <> ''):
	$text = str_replace(array("<br>", "<br />"), "\n", $arResult['ERROR_MESSAGE']);
?>
	<div class="alert alert-danger"><?=nl2br(htmlspecialcharsbx($text))?></div>
<?endif?>
    <!-- Контейнер -->
    <div class="bx-auth-reg">
    <div class="ts-theme ">
        <!-- Форма -->
        <form name="form_auth" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>" class="ts-wrap ts-px-0 ts-px-md-2">

            <input type="hidden" name="AUTH_FORM" value="Y" />
            <input type="hidden" name="TYPE" value="AUTH" />
            <?if (strlen($arResult["BACKURL"]) > 0):?>
                <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
            <?endif?>
            <?foreach ($arResult["POST"] as $key => $value):?>
                <input type="hidden" name="<?=$key?>" value="<?=$value?>" />
            <?endforeach?>

            <!-- Заголовок -->
            <div class="ts-title ts-px-0 ts-px-md-2 ts-py-0 ts-mx-0 ts-md-mx-2">
                <h2 ><?=GetMessage("AUTH_PLEASE_AUTH")?></h2>
                <button class="ts-d-none">x</button>
            </div>

            <!-- Контент форма авторизации -->
            <div class="ts-content ">

                <?if($arResult["NEW_USER_REGISTRATION"] == "Y" && $arParams["AUTHORIZE_REGISTRATION"] != "Y"):?>
                    <div class="ts-col-24 ts-col-lg-12 ts-mb-2 ts-px-0 ts-px-md-2">
                        <div class="ts-text">
                            <p class="ts-reset"><?=GetMessage("AUTH_FIRST_ONE")?></p>
                        </div>
                    </div>
                <?endif?>

                <!-- Логин -->
                    <div class="ts-col-24 ts-col-lg-12 ts-mb-2 ts-px-0 ts-px-md-2">
                        <div class="ts-input">
                            <label for="" class="ts-d-none"></label>
                            <div class="placeholder"><?=GetMessage("AUTH_LOGIN")?></div>
                            <div class="validation"></div>
                            <div class="input">
                                <input type="text" required name="USER_LOGIN" maxlength="255" value="<?=$arResult["LAST_LOGIN"]?>" >
                            </div>
                        </div>
                    </div>

                <!-- Пароль -->
                    <div class="ts-col-24 ts-col-lg-12 ts-mb-2 ts-px-0 ts-px-md-2">
                        <div class="ts-input">
                            <label for="" class="ts-d-none"></label>
                            <div class="placeholder"><?=GetMessage("AUTH_PASSWORD")?></div>
                            <div class="validation"></div>
                            <div class="input">
                                <input type="password" name="USER_PASSWORD" maxlength="255" autocomplete="off" required >
                                <!-- Фигня -->
                                <?if($arResult["SECURE_AUTH"]):?>
                                    <div class="bx-authform-psw-protected" id="bx_auth_secure" style="display:none">
                                        <div class="bx-authform-psw-protected-desc">
                                            <span></span><?echo GetMessage("AUTH_SECURE_NOTE")?>
                                        </div>
                                    </div>

                                    <script type="text/javascript">
                                        document.getElementById('bx_auth_secure').style.display = '';
                                    </script>
                                <?endif?>
                            </div>
                        </div>
                    </div>

                <!-- Капча -->
                 <?if($arResult["CAPTCHA_CODE"]):?>
                     <input type="hidden" name="captcha_sid" value="<?echo $arResult["CAPTCHA_CODE"]?>" />
                        <div class="ts-col-24 ts-col-lg-12 ts-mb-2">
                            <div class="ts-input">
                                <label for="" class="ts-d-none"></label>
                                <div class="placeholder"><?=GetMessage("AUTH_CAPTCHA_PROMT")?></div>
                                <div class="validation"></div>
                                <div class="bx-captcha"><img src="/bitrix/tools/captcha.php?captcha_sid=<?echo $arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" /></div>
                                <div class="input">
                                    <input type="text" required name="captcha_word" maxlength="50" value="" autocomplete="off" >
                                </div>
                            </div>
                        </div>
                 <?endif;?>
                <!-- Запомнить меня -->
                <div class="ts-col-24 ts-col-lg-12 ts-mb-2 ts-px-0 ts-px-md-2">
                    <div class="ts-checkbox ts-mx-0"><i class="icon fa ts-d-none"></i>
                        <input type="checkbox" id="USER_REMEMBER" name="USER_REMEMBER" value="Y" class="ts-d-block">
                        <label for="" class="ts-pl-1  ts-py-0 ts-mx-0 ts-my-0"><?=GetMessage("AUTH_REMEMBER_ME")?></label>

                    </div>
                </div>
                <!-- Submit -->
                    <div class="ts-col-24 ts-col-lg-12 ts-px-0 ts-px-md-2">
                        <div class="ts-submit ts-max-width-370 ts-mx-0">
                            <input type="submit" name="Login" class="c-button w-100" value="<?=GetMessage("AUTH_AUTHORIZE")?>">
                        </div>
                    </div>

            </div>
        </form>
        <?if ($arParams["NOT_SHOW_LINKS"] != "Y"):?>

            <noindex>
                <div class="ts-col-24 ts-my-3 ts-px-0 ts-px-md-2">
                    <ul class="dop-pol ts-px-2 ">
                        <li class="ts-mb-1 ts-mt-0">
                            <a href="<?=$arResult["AUTH_FORGOT_PASSWORD_URL"]?>" rel="nofollow"><?=GetMessage("AUTH_FORGOT_PASSWORD_2")?></a>
                        </li>

                        <?if($arResult["NEW_USER_REGISTRATION"] == "Y" && $arParams["AUTHORIZE_REGISTRATION"] != "Y"):?>
                            <li class="ts-mb-1 ts-mt-0">
                            <a href="/personal/?register=yes" rel="nofollow"><?=GetMessage("AUTH_REGISTER")?></a>
                        </li>
                            <li class="ts-mb-1 ts-mt-0">
                            <a href="/agencies/registration/" rel="nofollow"><?=GetMessage("AUTH_REGISTER_AGENT")?></a>
                        </li>
                        <?endif?>
                    </ul>
                </div>
            </noindex>
        <?endif?>
    </div>
    </div>
</div>
</div>

<script type="text/javascript">

    let input = document.querySelectorAll('.ts-input input');
    let checkboxSaveMe = $('.ts-checkbox i');

    if($(input).val().length != 0){
        $(input).closest('.ts-input').addClass('active');
    }

    checkboxSaveMe.on('click',function(e){
        $(this).toggleClass('active');
    });
    $(input).on('focus',function(e){
        $(this).closest('.ts-input').addClass('active focus');

    });
    $(input).on('focusout',function(e){
        if($(this).val().length == 0){
            $(this).closest('.ts-input').removeClass('active focus');
        }
        else $(this).closest('.ts-input').removeClass('focus');

    });
    $(input).on('keyup',function(e){
        if($(this).val().length != 0){
            $(this).closest('.ts-input').addClass('active');
        }
        else{
            $(this).closest('.ts-input').removeClass('active');
        }
    });

<?if (strlen($arResult["LAST_LOGIN"])>0):?>
try{document.form_auth.USER_PASSWORD.focus();}catch(e){}
<?else:?>
try{document.form_auth.USER_LOGIN.focus();}catch(e){}


<?endif?>
</script>

