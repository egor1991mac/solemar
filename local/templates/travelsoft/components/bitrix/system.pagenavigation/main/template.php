<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(!$arResult["NavShowAlways"])
{
	if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
		return;
}
?>

<div class="c_pagination clearfix pages">
    <?
    $strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
    $strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");
    ?>
        <?$StartPage = true;?>
        <?$EndPage = true;?>

        <?/*if($arResult["NavPageNomer"] == "1"):*/?><!--
            <a class="c-button b-40 bg-dr-blue-2 hv-dr-blue-2-o fl"><?/*=GetMessage('nav_prev')*/?></a>
        <?/*else:*/?>
            <?/*if($arResult["NavPageNomer"]-1 == 1):*/?>
                <a href="<?/*=str_replace(array($strNavQueryStringFull,"&"), "", $arResult["sUrlPathParams"])*/?>" class="c-button b-40 bg-dr-blue-2 hv-dr-blue-2-o fl"><?/*=GetMessage('nav_prev')*/?></a>
            <?/*else:*/?>
                <a href="<?/*=$arResult["sUrlPathParams"]*/?>PAGEN_<?/*=$arResult["NavNum"]*/?>=<?/*=($arResult["NavPageNomer"]-1)*/?>" class="c-button b-40 bg-dr-blue-2 hv-dr-blue-2-o fl"><?/*=GetMessage('nav_prev')*/?></a>
            <?/*endif*/?>
        --><?/*endif;*/?>


        <ul class="ts-d-flex ts-justify-content__center">
            <?for($page = 1; $page <= $arResult["NavPageCount"]; $page++):?>

                <?if($arResult["NavPageNomer"] == $page):?>
                    <li class="active"><?=$page?></li>
                <?else:?>
                    <?if($page == "1"):?>
                        <li><a href="<?=str_replace(array($strNavQueryStringFull,"&"), "", $arResult["sUrlPathParams"])?>"><?=$page?></a></li>
                        <!--<li><a href="<?/*=$arResult["sUrlPathParams"]*/?>"><?/*=$page*/?></a></li>-->
                    <?elseif($page == $arResult["NavPageCount"]):?>
                        <li><a href="<?=$arResult["sUrlPathParams"]?>PAGEN_<?=$arResult["NavNum"]?>=<?=$page;?>"><?=$page?></a></li>
                    <?elseif(($arResult["nStartPage"] > $page) && (($arResult["nStartPage"] - 2) != "1")):?>
                        <?if($StartPage):?>
                            <li><a>...</a></li>
                            <?$StartPage = false;?>
                        <?endif;?>
                    <?elseif(($arResult["nEndPage"] < $page) && (($arResult["nEndPage"] + 2) != $arResult["NavPageCount"])):?>
                        <?if($EndPage):?>
                            <li><a>...</a></li>
                            <?$EndPage = false;?>
                        <?endif;?>
                    <?else:?>
                        <li><a href="<?=$arResult["sUrlPathParams"]?>PAGEN_<?=$arResult["NavNum"]?>=<?=$page;?>"><?=$page?></a></li>
                    <?endif;?>
                <?endif;?>

            <?endfor;?>
        </ul>

        <?/*if($arResult["NavPageNomer"] == $arResult["NavPageCount"]):*/?><!--
            <a href="#" class="c-button b-40 bg-dr-blue-2 hv-dr-blue-2-o fr"><?/*=GetMessage('nav_next')*/?></a>
        <?/*else:*/?>
            <a href="<?/*=$arResult["sUrlPathParams"]*/?>PAGEN_<?/*=$arResult["NavNum"]*/?>=<?/*=($arResult["NavPageNomer"]+1)*/?>" class="c-button b-40 bg-dr-blue-2 hv-dr-blue-2-o fr"><?/*=GetMessage('nav_next')*/?></a>
        --><?/*endif;*/?>

</div>