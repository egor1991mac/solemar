<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?>
<?
if (!empty($arResult["ERROR_MESSAGE"])) {
    foreach ($arResult["ERROR_MESSAGE"] as $v)
        ShowError($v);
}
if (strlen($arResult["OK_MESSAGE"]) > 0) {
    ?>
    <div class="mf-ok-text" style="padding-top: 2em;"><?= $arResult["OK_MESSAGE"] ?></div><?
}
?>
<div class="contact-colls-lbl"><?= !empty($arParams["TITLE"]) ? $arParams["TITLE"] : GetMessage("CONTACT_US"); ?></div>
<div class="booking-form">
    <form action="<?= POST_FORM_ACTION_URI ?>" method="POST" id="feedback-form">
        <div class="row">
            <?= bitrix_sessid_post() ?>
            <div class="booking-form-i">
                <label><?= GetMessage("MFT_NAME") ?></label>
                <div class="input">
                    <input type="text" name="user_name" placeholder="<?= GetMessage("MFT_NAME") ?>"
                           value="<?= $arResult["AUTHOR_NAME"] ?>">
                </div>
            </div>
            <div class="clear"></div>
            <div class="booking-form-i">
                <label><?= GetMessage("MFT_EMAIL") ?></label>
                <div class="input">
                    <input type="text" name="user_email" placeholder="<?= GetMessage("MFT_EMAIL") ?>"
                           value="<?= $arResult["AUTHOR_EMAIL"] ?>">
                </div>
            </div>
            <div class="clear"></div>
            <div class="booking-form-i">
                <label><?= GetMessage("MFT_PHONE") ?></label>
                <div class="input">
                    <input type="text" name="user_phone" placeholder="<?= GetMessage("MFT_PHONE") ?>"
                           value="<?= $arResult["AUTHOR_PHONE"] ?>">
                </div>
            </div>
            <div class="clear"></div>
            <div class="booking-form-i textarea">
                <label><?= GetMessage("MFT_MESSAGE") ?></label>
                <div class="textarea-wrapper">
                   <textarea name="MESSAGE" rows="5" placeholder="<?= GetMessage("MFT_MESSAGE") ?>"
                             cols="40"><?= $arResult["MESSAGE"] ?></textarea>
                </div>
            </div>
            <div class="clear"></div>
            <? if ($arParams["USE_CAPTCHA"] == "Y"): ?>
                <div class="mf-captcha">
                    <div class="mf-text"><?= GetMessage("MFT_CAPTCHA") ?></div>
                    <input type="hidden" name="captcha_sid" value="<?= $arResult["capCode"] ?>">
                    <img src="/bitrix/tools/captcha.php?captcha_sid=<?= $arResult["capCode"] ?>" width="180"
                         height="40" alt="CAPTCHA">
                    <div class="mf-text"><?= GetMessage("MFT_CAPTCHA_CODE") ?><span class="mf-req">*</span>
                    </div>
                    <input type="text" name="captcha_word" size="30" maxlength="50" value="">
                </div>
            <? endif; ?>
            <input type="hidden" name="PARAMS_HASH" value="<?= $arResult["PARAMS_HASH"] ?>">
            <div class="clear"></div>
            <input class="contacts-send" type="submit" name="submit"
                   value="<?= GetMessage("MFT_SUBMIT") ?>"/>
        </div>
    </form>
</div>
