<?
global $APPLICATION;
$picturesID = Array();
$picturesDescription = Array();

$res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "CODE" => $arParams["ELEMENT_CODE"], "ACTIVE" => "Y"), false, false, Array("IBLOCK_ID", "ID", "NAME", "DETAIL_PAGE_URL"));
if ($ob = $res->GetNextElement()) {
    $arFields = $ob->GetFields();
    $arProps = $ob->GetProperties();
    $picturesID = (array)$arProps["PICTURES"]["VALUE"];
    $picturesDescription = $arProps["PICTURES"]["DESCRIPTION"];
}

if (!empty($picturesID)) {
    ob_start(); ?>
    <div class="gallery">
        <?foreach ($picturesID as $pict):?>
            <?$picture = current(getSrc($pict, array("width"=>1100,"height"=>450), '', 1));?>
            <span style="background-image:url('<?=$picture?>');"></span>
        <?endforeach;?>
    </div>

    <?/*$APPLICATION->IncludeComponent(
        "travelsoft:travelsoft.slider",
        "",
        Array(
            "AUTO_PLAY_BIG" => "N",
            "AUTO_PLAY_HOVER_PAUSE_BIG" => "Y",
            "AUTO_PLAY_TIMEOUT_BIG" => "3000",
            "DATA_DESCRIPTION" => $picturesDescription,
            "DATA_SOURCE" => $picturesID,
            "DOT_BIG" => "N",
            "DOT_SMALL" => "N",
            "DO_NOT_INC_MAGNIFIC_POPUP" => "N",
            "DO_NOT_INC_OWL_CAROUSEL" => "N",
            "HEIGHT" => "450",
            "INC_JQUERY" => "N",
            "ITEM_COUNT_BIG" => "1",
            "ITEM_COUNT_SMALL" => "6",
            "LAZY_LOAD_BIG" => "Y",
            "LAZY_LOAD_SMALL" => "Y",
            "MARGIN_PICTURES_BIG" => "10",
            "MARGIN_PICTURES_SMALL" => "10",
            "NAV_BIG" => "Y",
            "NAV_SMALL" => "Y",
            "NO_PHOTO_PATH" => NO_PHOTO_1100_450,
            "WIDTH" => "1100"
        )
    );*/
    $this->arParams["SLIDER"] = ob_get_clean();
}

ob_start();
$GLOBALS["arFilterPromotionsList"] = array("!ID"=>$arFields["ID"]);
$APPLICATION->IncludeComponent(
    "travelsoft:travelsoft.news.list",
    "promotions",
    array(
        "ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
        "ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
        "AFP_ID" => "",	// ID элемента(ов)(если несколько значений, то указывать через запятую)
        "AJAX_MODE" => "N",	// Включить режим AJAX
        "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
        "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
        "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
        "AJAX_OPTION_STYLE" => "N",	// Включить подгрузку стилей
        "CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
        "CACHE_GROUPS" => "N",	// Учитывать права доступа
        "CACHE_TIME" => "36000000",	// Время кеширования (сек.)
        "CACHE_TYPE" => "A",	// Тип кеширования
        "CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
        "COMPONENT_TEMPLATE" => "promotions",
        "DESCRIPTION_LINK" => "",
        "DESCRIPTION_NAMELINK" => "",
        "DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
        "DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
        "DISPLAY_DATE" => "Y",	// Выводить дату элемента
        "DISPLAY_NAME" => "Y",	// Выводить название элемента
        "DISPLAY_PICTURE" => "N",	// Выводить изображение для анонса
        "DISPLAY_PREVIEW_TEXT" => "N",	// Выводить текст анонса
        "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
        "FIELD_CODE" => array(	// Поля
            0 => "",
            1 => "",
        ),
        "FILTER_NAME" => "arFilterPromotionsList",	// Фильтр
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
        "IBLOCK_ID" => $arFields["IBLOCK_ID"],	// Код информационного блока
        "IBLOCK_TYPE" => "tourist",	// Тип информационного блока (используется только для проверки)
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
        "INCLUDE_SUBSECTIONS" => "N",	// Показывать элементы подразделов раздела
        "MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
        "NEWS_COUNT" => "15",	// Количество новостей на странице
        "PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
        "PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
        "PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
        "PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
        "PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
        "PAGER_TITLE" => "Новости",	// Название категорий
        "PARENT_SECTION" => "",	// ID раздела
        "PARENT_SECTION_CODE" => "",	// Код раздела
        "PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
        "PROPERTY_CODE" => array(	// Свойства
            0 => "PICTURES",
            1 => "PREVIEW_TEXT",
            2 => "DESC",
        ),
        "SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
        "SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
        "SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
        "SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
        "SET_STATUS_404" => "N",	// Устанавливать статус 404
        "SET_TITLE" => "N",	// Устанавливать заголовок страницы
        "SHOW_404" => "N",	// Показ специальной страницы
        "SORT_BY1" => "ACTIVE_FROM",	// Поле для первой сортировки новостей
        "SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
        "SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
        "SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
        "TEXT_DESCRIPTION" => "",
        "TEXT_TITLE" => "Другие акции",
        "LINK_TITLE" => "/actions/"
    )
);
$this->arParams["PROMOTIONS_LIST"] = ob_get_clean();

ob_start();
$APPLICATION->IncludeComponent(
    "travelsoft:travelsoft.news.list",
    "countries",
    array(
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "ADD_SECTIONS_CHAIN" => "N",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "N",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "N",
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "A",
        "CHECK_DATES" => "Y",
        "COMPONENT_TEMPLATE" => "countries",
        "DESCRIPTION_LINK" => "",
        "DESCRIPTION_NAMELINK" => "",
        "DETAIL_URL" => "",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "DISPLAY_DATE" => "Y",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "DISPLAY_TOP_PAGER" => "N",
        "FIELD_CODE" => array(
            0 => "",
            1 => "",
            2 => "",
        ),
        "FILTER_NAME" => "arFilterReviewsCountry",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "IBLOCK_ID" => COUNTRIES_IBLOCK_ID,
        "IBLOCK_TYPE" => "directories",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "INCLUDE_SUBSECTIONS" => "N",
        "MESSAGE_404" => "",
        "NEWS_COUNT" => "500",
        "PAGER_BASE_LINK_ENABLE" => "N",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => ".default",
        "PAGER_TITLE" => "",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "PREVIEW_TRUNCATE_LEN" => "",
        "PROPERTY_CODE" => array(
            0 => "FLAG",
            1 => "RECOMMEND",
            2 => "",
        ),
        "SET_BROWSER_TITLE" => "N",
        "SET_LAST_MODIFIED" => "N",
        "SET_META_DESCRIPTION" => "N",
        "SET_META_KEYWORDS" => "N",
        "SET_STATUS_404" => "N",
        "SET_TITLE" => "N",
        "SHOW_404" => "N",
        "SORT_BY1" => "SORT",
        "SORT_BY2" => "NAME",
        "SORT_ORDER1" => "ASC",
        "SORT_ORDER2" => "ASC",
        "TEXT_DESCRIPTION" => "",
        "TEXT_TITLE" => "",
    ),
    false
);
$this->arParams["COUNTRIES_LIST"] = ob_get_clean();

ob_start();
$APPLICATION->IncludeComponent(
    "travelsoft:travelsoft.news.list",
    "tours_right",
    array(
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "ADD_SECTIONS_CHAIN" => "N",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "N",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "N",
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "A",
        "CHECK_DATES" => "Y",
        "COMPONENT_TEMPLATE" => "tours_right",
        "DESCRIPTION_LINK" => "",
        "DESCRIPTION_NAMELINK" => "",
        "DETAIL_URL" => "",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "DISPLAY_DATE" => "Y",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "DISPLAY_TOP_PAGER" => "N",
        "FIELD_CODE" => array(
            0 => "",
            1 => "",
            2 => "",
        ),
        "FILTER_NAME" => "arFilterRightToursCountry",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "IBLOCK_ID" => TOURS_ID_IBLOCK,
        "IBLOCK_TYPE" => "tourpoduct",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "INCLUDE_SUBSECTIONS" => "N",
        "MESSAGE_404" => "",
        "NEWS_COUNT" => "3",
        "PAGER_BASE_LINK_ENABLE" => "N",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => ".default",
        "PAGER_TITLE" => "",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "PREVIEW_TRUNCATE_LEN" => "",
        "PROPERTY_CODE" => array(
            0 => "PICTURES",
            1 => "PREVIEW_TEXT",
            2 => "PRICE",
            3 => "CURRENCY",
            4 => "DATE",
            5 => "DAYS",
        ),
        "SET_BROWSER_TITLE" => "N",
        "SET_LAST_MODIFIED" => "N",
        "SET_META_DESCRIPTION" => "N",
        "SET_META_KEYWORDS" => "N",
        "SET_STATUS_404" => "N",
        "SET_TITLE" => "N",
        "SHOW_404" => "N",
        "SORT_BY1" => "NAME",
        "SORT_BY2" => "ID",
        "SORT_ORDER1" => "RAND",
        "SORT_ORDER2" => "ASC",
        "TEXT_DESCRIPTION" => "",
        "TEXT_TITLE" => "",
        "FOLDER_URL" => $arParams["DETAIL_PAGE"]
    ),
    false
);
$this->arParams["TOURS_RIGHT_FOR_COUNTRY"] = ob_get_clean();

ob_start();
$APPLICATION->IncludeComponent(
    "bitrix:main.include",
    "",
    Array(
        "AREA_FILE_SHOW" => "file",
        "AREA_FILE_SUFFIX" => "inc",
        "EDIT_TEMPLATE" => "",
        "PATH" => SITE_DIR . "include/sidebarSubscription.php"
    )
);
$this->arParams["SIDEBAR_SUBSCRIPTION"] = ob_get_clean();

ob_start();
$APPLICATION->IncludeComponent(
    "bitrix:main.include",
    "",
    Array(
        "AREA_FILE_SHOW" => "file",
        "AREA_FILE_SUFFIX" => "inc",
        "EDIT_TEMPLATE" => "",
        "PATH" => SITE_DIR . "include/sidebarWidgets.php"
    )
);
$this->arParams["SIDEBAR_WIDGETS"] = ob_get_clean();

?>
