<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
if(isset($arParams["COUNTRY_ID"])) {

    $country_db = \CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => COUNTRIES_IBLOCK_ID, "ACTIVE" => "Y", "ID" => $arParams["COUNTRY_ID"]), false, false, Array("ID", "NAME", "CODE", "IBLOCK_ID", "PROPERTY_FLAG", "PROPERTY_TOURIST_GUIDE"));
    while ($ar_country = $country_db->GetNext()) {
        $arResult["COUNTRY"] = $ar_country;
    }
    $cities_db = \CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => CITIES_IBLOCK_ID, "ACTIVE" => "Y", "PROPERTY_COUNTRY" => $arParams["COUNTRY_ID"]), false, false, Array("ID", "NAME", "IBLOCK_ID"));
    if ($cities_db->SelectedRowsCount() >= 1) {
        $arResult["CITIES"] = true;
    }
    $hotels_db = \CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => HOTELS_IBLOCK_ID, "ACTIVE" => "Y", "PROPERTY_COUNTRY" => $arParams["COUNTRY_ID"]), false, false, Array("ID", "NAME", "IBLOCK_ID"));
    if ($hotels_db->SelectedRowsCount() >= 1) {
        $arResult["HOTELS"] = true;
    }

    $visas_db = \CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => VISAS_IBLOCK_ID, "ACTIVE" => "Y", "PROPERTY_COUNTRY" => $arParams["COUNTRY_ID"]), false, false, Array("ID", "NAME", "IBLOCK_ID"));
    if ($visas_db->SelectedRowsCount() >= 1) {
        $arResult["VISAS"] = true;
    }

}