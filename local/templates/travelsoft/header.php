<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
IncludeTemplateLangFile(__FILE__);

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
$oAsset = Bitrix\Main\Page\Asset::getInstance();
CJSCore::Init();
?>
<!DOCTYPE html>
<html lang="<?= LANGUAGE_ID ?>">
<head>
    <title><?= $APPLICATION->ShowTitle() ?></title>
    <? $APPLICATION->ShowHead() ?>
    <link rel="icon" href="<?= SITE_TEMPLATE_PATH ?>/favicon.ico"/>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta name="author" content="TravelSoft">

    <?// $oAsset->addCss(SITE_TEMPLATE_PATH . "/css/print.css?v=10") ?>
    <? $oAsset->addCss(SITE_TEMPLATE_PATH . "/css/style.css?v=357") ?>
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto:400,300,400italic,700&amp;subset=cyrillic,latin" />
    <? $oAsset->addCss(SITE_TEMPLATE_PATH . "/css/jquery.selectBoxIt.css?v=2") ?>
    <? $oAsset->addCss(SITE_TEMPLATE_PATH . "/slick/slick.css") ?>
    <? $oAsset->addCss(SITE_TEMPLATE_PATH . "/slick/slick-theme.css") ?>
    <? $oAsset->addCss(SITE_TEMPLATE_PATH . "/fancy/jquery.fancybox.css") ?>
    <? $oAsset->addCss(SITE_TEMPLATE_PATH . "/css/ts-theme_v2.css") ?>

    <!--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>-->
    <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/js/jquery-1.8.3.min.js") ?>
    <?if($APPLICATION->GetCurDir() == "/booking/"):?>
        <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/js/jquery-2.1.3.min.js") ?>
    <?endif?>
    <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/js/jquery-ui-1.9.2.min.js") ?>
    <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/js/jquery.selectBoxIt.min.js") ?>
    <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/slick/slick.min.js") ?>
    <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/fancy/jquery.fancybox.pack.js") ?>
    <? $oAsset->addJs(SITE_TEMPLATE_PATH . "/js/main.js") ?>

</head>
<body <?if(HOME_PAGE === true):?>class="default"<?endif?>>
    <?\Bitrix\Main\Loader::includeModule("travelsoft.currency");?>
    <div id="header">
        <div class="container">
            <div id="header-top">
                <?if(HOME_PAGE !== true):?>
                    <a href="/" id="logo"></a>
                <?else:?>
                    <div id="logo"></div>
                <?endif?>

                <p><? $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "",
                        "PATH" => SITE_DIR . "include/slogan.php"
                    )
                ); ?></p>
                <div class="button-border-wrap">
                    <script id="bx24_form_button" data-skip-moving="true">
                        (function(w,d,u,b){w['Bitrix24FormObject']=b;w[b] = w[b] || function(){arguments[0].ref=u;
                            (w[b].forms=w[b].forms||[]).push(arguments[0])};
                            if(w[b]['forms']) return;
                            var s=d.createElement('script');s.async=1;s.src=u+'?'+(1*new Date());
                            var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
                        })(window,document,'https://solemare.bitrix24.by/bitrix/js/crm/form_loader.js','b24form');
                        b24form({"id":"7","lang":"ru","sec":"53gcm3","type":"button","click":""});
                    </script><button class="b24-web-form-popup-btn-7 button-border flashlink"><span>Отправить заявку на подбор тура</span></button>
                    <!--<a href="#open" class="button-border flashlink pp-open"><span>Отправить заявку на&nbsp;подбор тура</span></a>-->
                </div>
            </div>
            <ul class="contacts">
                <? $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "",
                        "PATH" => SITE_DIR . "include/headerPhonesList.php"
                    )
                ); ?>
                <? $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "",
                        "PATH" => SITE_DIR . "include/headerAddress.php"
                    )
                ); ?>
            </ul>
            <p class="show-mobile">
                <? $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "",
                        "PATH" => SITE_DIR . "include/headerMode.php"
                    )
                ); ?>
            </p>
        </div>
    </div>

    <? $APPLICATION->IncludeComponent(
        "bitrix:menu",
        "top.menu",
        array(
            "ALLOW_MULTI_SELECT" => "N",
            "CHILD_MENU_TYPE" => "left",
            "DELAY" => "N",
            "MAX_LEVEL" => "2",
            "MENU_CACHE_GET_VARS" => array(),
            "MENU_CACHE_TIME" => "3600",
            "MENU_CACHE_TYPE" => "N",
            "MENU_CACHE_USE_GROUPS" => "Y",
            "ROOT_MENU_TYPE" => "top",
            "USE_EXT" => "N",
            "COMPONENT_TEMPLATE" => "top.menu"
        ),
        false
    ); ?>


    <?if($APPLICATION->GetDirProperty("SHOW_TOP_PANEL") != "N" && HOME_PAGE !== true):?>

        <? if ($APPLICATION->GetDirProperty("SHOW_MENU") == "Y"): ?>
            <? $APPLICATION->IncludeComponent(
                "bitrix:menu",
                "left.menu",
                array(
                    "ALLOW_MULTI_SELECT" => "N",
                    "CHILD_MENU_TYPE" => "left",
                    "DELAY" => "N",
                    "MAX_LEVEL" => "1",
                    "MENU_CACHE_GET_VARS" => array(),
                    "MENU_CACHE_TIME" => "3600000",
                    "MENU_CACHE_TYPE" => "A",
                    "MENU_CACHE_USE_GROUPS" => "N",
                    "ROOT_MENU_TYPE" => "left",
                    "USE_EXT" => "Y",
                    "COMPONENT_TEMPLATE" => "left.menu"
                ),
                false
            ); ?>
        <?endif?>

        <?if($APPLICATION->GetDirProperty("SHOW_TOP_FORM") != "N"):?>
            <?$APPLICATION->IncludeComponent(
                "travelsoft:search.form.sts",
                "main",
                Array(
                    "ACTION_URL" => "/online/",
                    "GET_PARAMS_STS" => $_REQUEST["stsSearch"],
                    "SHOW_PANEL_HEADING" => "N",
                    "PREDLOG" => "N"
                )
            );?>
        <?endif?>
        <div class="container<?if(!empty($APPLICATION->GetDirProperty("DIRECTORY_CLASS"))):?> <?=$APPLICATION->GetDirProperty("DIRECTORY_CLASS")?><?endif?><?if($APPLICATION->GetDirProperty("SHOW_TWO_COL") == "Y"):?> col2<?else:?> col1<?endif?>">
            <h1><?=$APPLICATION->ShowTitle(false)?></h1>
            <div class="content">
    <?endif?>

    <?/* $APPLICATION->IncludeComponent(
        "bitrix:main.include",
        "",
        Array(
            "AREA_FILE_SHOW" => "file",
            "AREA_FILE_SUFFIX" => "inc",
            "EDIT_TEMPLATE" => "",
            "PATH" => SITE_DIR . "include/header.php"
        )
    ); */?>


    <?/* if ($APPLICATION->GetDirProperty("MAIN_PAGE") != "Y"): */?><!--
        <?/*$APPLICATION->IncludeComponent(
            "bitrix:breadcrumb",
            "breadcrumb",
            Array(
                "PATH" => "",
                "SITE_ID" => "s1",
                "START_FROM" => "0"
            )
        );*/?>

        <div class="container">
            <div class="row">
                <?/* if ($APPLICATION->GetDirProperty("SHOW_LEFT_SIDEBAR") == "Y"): */?>
                    <div class="col-md-4 sidebar">
                        <?/* if ($APPLICATION->GetDirProperty("SHOW_LEFT_MENU") == "Y"): */?>
                            <?/* $APPLICATION->IncludeComponent(
                                "bitrix:menu",
                                "left.menu",
                                array(
                                    "ALLOW_MULTI_SELECT" => "N",
                                    "CHILD_MENU_TYPE" => "left",
                                    "DELAY" => "N",
                                    "MAX_LEVEL" => "1",
                                    "MENU_CACHE_GET_VARS" => array(),
                                    "MENU_CACHE_TIME" => "3600000",
                                    "MENU_CACHE_TYPE" => "A",
                                    "MENU_CACHE_USE_GROUPS" => "N",
                                    "ROOT_MENU_TYPE" => "left",
                                    "USE_EXT" => "Y",
                                    "COMPONENT_TEMPLATE" => "left.menu"
                                ),
                                false
                            ); */?>
                        <?/*endif;*/?>
                    </div>
                    <div class="col-md-8">
                <?/*else:*/?>
                    <div class="col-md-12">
                <?/*endif;*/?>
                <h1><?/*= $APPLICATION->ShowTitle(false) */?></h1>
    --><?/*endif*/?>