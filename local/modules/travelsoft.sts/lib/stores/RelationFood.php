<?php

namespace travelsoft\sts\stores;

use travelsoft\sts\adapters\MiddlewareHighloadblock;

/**
 * Класс для работы с таблицей питания
 *
 * @author juliya.sharlova
 * @copyright (c) 2017, travelsoft
 */
class RelationFood extends MiddlewareHighloadblock
{
    protected static $storeName = 'relationfood';
}